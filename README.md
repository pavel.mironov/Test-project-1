# KfE2e

After `npm i` do `npm run postinstall`.

## Running end-to-end tests

Run `npm run test` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
