import { by, element, browser, ExpectedConditions as EC, protractor } from 'protractor';
import { AppPage as page } from './components/app.po';
import { SuccessProfileListPageObjects as listPO } from './components/sp-list.po';
import { OrgDataLocators as dataPL } from './components/org-data.locators';
import { OrgDataPageObject as dataPO } from './components/org-data.po';
import { SpinnerPO as spinner } from './components/spinner';

let parentOrg = {
  name: {
    origin: ``
    , modified: `Test ASDFG || main parent`
  }
  , currency: {
    origin: ``
    , short: ``
    , modified: ``
    , modifiedShort: ``
  }
  , revenue: {
    origin: 1
    , modified: 123
  }
  , assets: {
    origin: 1
    , modified: 234
  }
  , capitalization: {
    origin: 1
    , modified: 345
  }
  , totalEmployees: {
    origin: 1
    , modified: 456
  }
  , revenueInternal: {
    origin: ``
    , modified: ``
  }
  , industry: {
    origin: ``
    , modified: ``
  }
  , segment: {
    origin: ``
    , modified: ``
  }
  , ownershipStructure: {
    origin: ``
    , modified: ``
  }
  , ownershipType: {
    origin: ``
    , modified: ``
  }
  , gradingStructure: {
    origin: ``
    , modified: ``
  }
  , gradingAcrossAll: {
    origin: ``
    , modified: ``
  }

  , country1: {
    i: 0
    , revenue: 12
    , headcount: 13
    , name: {
      title: ``
    }
    , map: {
      title: ``
    }
    , currency: {
      title: ``
    }
  }
};

let subOrg1 = {
  name: `Test ASDFG || org 1`
  , totalRevenue: 123123
  , totalEmployees: 234234
  , currency: {
    title: ``
    , short: ``
  }
  , businessScope: {
    title: ``
  }
  , governance: {
    title: ``
  }
  , industry: {
    title: ``
  }
  , segment: {
    title: ``
  }
  , revenueInternal: {
    title: ``
  }

  , country1: {
    i: 1
    , revenue: 456
    , headcount: 567
    , name: {
      title: ``
    }
    , map: {
      title: ``
    }
    , currency: {
      title: ``
    }
  }
};

let subOrg2 = {
  name: `Test ASDFG || org 2`
  , totalRevenue: 456456
  , totalEmployees: 567567
  , currency: {
    title: ``
    , short: ``
  }
  , businessScope: {
    title: ``
  }
  , governance: {
    title: ``
  }
  , industry: {
    title: ``
  }
  , segment: {
    title: ``
  }
  , revenueInternal: {
    title: ``
  }

  , country1: {
    i: 1
    , revenue: 123
    , headcount: 234
    , name: {
      title: ``
    }
    , map: {
      title: ``
    }
    , currency: {
      title: ``
    }
  }
};

let subOrg3 = {
  name: `Test ASDFG || org 3`
  , totalRevenue: 123123
  , totalEmployees: 234234
  , currency: {
    title: ``
    , short: ``
  }
  , businessScope: {
    title: ``
  }
  , governance: {
    title: ``
  }
  , industry: {
    title: ``
  }
  , segment: {
    title: ``
  }
  , revenueInternal: {
    title: ``
  }

  , country1: {
    i: 1
    , revenue: 456
    , headcount: 567
    , name: {
      title: ``
    }
    , map: {
      title: ``
    }
    , currency: {
      title: ``
    }
  }
};

describe('ORG DATA', () => {
  beforeEach(async () => {
    await page.login();
    await browser.waitForAngularEnabled(false);
  });

  afterEach(async () => {
    await browser.manage().logs().get('browser').then(async (browserLog) => {
      if (browserLog.length > 0) {
        await browserLog.forEach(async log => {
          await expect(browserLog.length > 0).toBeFalsy(`error in console`);//.toBeFalsy(log.message);
        });
      }
    });
  });

  it(`Add Subsidiary Organization`, async () => {
    await page.moveToOrgData();

    await dataPL.subListNames.count().then(async (am) => {
      for (let i = am; i > 0; i--) {
        let t = await dataPL.subListNames.get(i - 1).getText();
        if (t == subOrg1.name) {
          await dataPO.expandSubNameByNo(i);
          await dataPO.deleteSub();
        }
      }
    });

    await browser.wait(EC.presenceOf(dataPL.addSubOrgBtn));
    await dataPL.addSubOrgBtn.click();

    await dataPO.setSubName(subOrg1.name);
    await dataPO.setSubCurrencyR();
    await dataPO.setSubTotalRevenue(subOrg1.totalRevenue);
    await dataPO.setSubTotalEmployees(subOrg1.totalEmployees);
    await dataPO.setSubBusinessScopeR();
    await dataPO.setSubGovernanceR();
    await dataPO.setSubIndustryR();
    await dataPO.setSubSegmentR();
    await dataPO.setSubRevenueInternalR();

    await dataPO.clickAddCountryBtn();
    await dataPO.clickAddCountryBtn();

    await dataPO.clickDeleteCountryBtn(1);
    await dataPO.clickDeleteCountryBtn(2);

    await dataPO.setCountryNameR(subOrg1.country1.i);
    await dataPO.setCountryMapR(subOrg1.country1.i);
    await dataPO.setCountryRevenue(subOrg1.country1.i, subOrg1.country1.revenue);
    await dataPO.setCountryCurrencyR(subOrg1.country1.i);
    await dataPO.setCountryHeadcount(subOrg1.country1.i, subOrg1.country1.headcount);

    subOrg1.currency.title = await dataPL.currencyDropdown.getText();
    subOrg1.currency.short = await dataPL.revenueAddInfo.getText();
    subOrg1.businessScope.title = await dataPL.scopeDropdown.getText();
    subOrg1.governance.title = await dataPL.governanceDropdown.getText();
    subOrg1.industry.title = await dataPL.industryDropdown.getText();
    subOrg1.segment.title = await dataPL.segmentDropdown.getText();
    subOrg1.revenueInternal.title = await dataPL.revenueDropdown.getText();

    subOrg1.country1.name.title = await dataPL.countryNameDDs.get(subOrg1.country1.i - 1).getText();
    subOrg1.country1.map.title = await dataPL.countryMapDDs.get(subOrg1.country1.i - 1).getText();
    subOrg1.country1.currency.title = await dataPL.countryCurrencyDDs.get(subOrg1.country1.i - 1).getText();

    // console.log(subOrg1);

    await dataPO.saveSubOrg();
    await dataPO.expandSubDetailsByName(subOrg1.name);

    // check expanded content
    let actualSub = await dataPL.expandedSubData.getText();
    await expect(parseInt(actualSub[0].replace(subOrg1.currency.short, ``).replace(/\s/g, ``)) == subOrg1.totalRevenue)
      .toBeTruthy(`Currency: expected '${subOrg1.totalRevenue}' instead of '${parseInt(actualSub[0].replace(subOrg1.currency.short, ``))}'`);
    await expect(actualSub[0].indexOf(subOrg1.currency.short) !== -1)
      .toBeTruthy(`Currency: expected '${actualSub[0]}' to contain '${subOrg1.currency.short}'`);
    await expect(parseInt(actualSub[1].replace(/\s/g, ``)) == subOrg1.totalEmployees)
      .toBeTruthy(`Currency: expected '${subOrg1.totalEmployees}' instead of '${parseInt(actualSub[1].replace(/\s/g, ``))}'`);
    await expect(actualSub[2] == subOrg1.businessScope.title)
      .toBeTruthy(`Currency: expected '${subOrg1.businessScope.title}' instead of '${actualSub[2]}'`);
    await expect(actualSub[3] == subOrg1.governance.title)
      .toBeTruthy(`Currency: expected '${subOrg1.governance.title}' instead of '${actualSub[3]}'`);
    await expect(actualSub[4] == subOrg1.industry.title)
      .toBeTruthy(`Currency: expected '${subOrg1.industry.title}' instead of '${actualSub[4]}'`);
    await expect(actualSub[5] == subOrg1.segment.title)
      .toBeTruthy(`Currency: expected '${subOrg1.segment.title}' instead of '${actualSub[5]}'`);
    await expect(actualSub[6] == subOrg1.revenueInternal.title)
      .toBeTruthy(`Currency: expected '${subOrg1.revenueInternal.title}' instead of '${actualSub[6]}'`);

  });

  it(`Edit Subsidiary Organization`, async () => {
    await page.moveToOrgData();

    await dataPL.subListNames.count().then(async (am) => {
      for (let i = am; i > 0; i--) {
        let t = await dataPL.subListNames.get(i - 1).getText();
        if (t == subOrg2.name) {
          await dataPO.expandSubNameByNo(i);
          await dataPO.deleteSub();
        }
        if (t == subOrg3.name) {
          await dataPO.expandSubNameByNo(i);
          await dataPO.deleteSub();
        }
      }
    });

    await browser.wait(EC.presenceOf(dataPL.addSubOrgBtn));
    await dataPL.addSubOrgBtn.click();

    await dataPO.setSubName(subOrg2.name);
    await dataPO.setSubCurrencyR();
    await dataPO.setSubTotalRevenue(subOrg2.totalRevenue);
    await dataPO.setSubTotalEmployees(subOrg2.totalEmployees);
    await dataPO.setSubBusinessScopeR();
    await dataPO.setSubGovernanceR();
    await dataPO.setSubIndustryR();
    await dataPO.setSubSegmentR();
    await dataPO.setSubRevenueInternalR();

    await dataPO.clickAddCountryBtn();
    await dataPO.clickAddCountryBtn();

    await dataPO.clickDeleteCountryBtn(1);
    await dataPO.clickDeleteCountryBtn(2);

    await dataPO.setCountryNameR(subOrg2.country1.i);
    await dataPO.setCountryMapR(subOrg2.country1.i);
    await dataPO.setCountryRevenue(subOrg2.country1.i, subOrg2.country1.revenue);
    await dataPO.setCountryCurrencyR(subOrg2.country1.i);
    await dataPO.setCountryHeadcount(subOrg2.country1.i, subOrg2.country1.headcount);

    await dataPO.saveSubOrg();
    await dataPO.expandSubDetailsByName(subOrg2.name);

    await dataPO.editSub();

    await dataPO.setSubName(subOrg3.name);
    await dataPO.setSubCurrencyR();
    await dataPO.setSubTotalRevenue(subOrg3.totalRevenue);
    await dataPO.setSubTotalEmployees(subOrg3.totalEmployees);
    await dataPO.setSubBusinessScopeR();
    await dataPO.setSubGovernanceR();
    await dataPO.setSubIndustryR();
    await dataPO.setSubSegmentR();
    await dataPO.setSubRevenueInternalR();

    await dataPO.clickDeleteCountryBtn(1);
    await dataPO.clickAddCountryBtn();

    await dataPO.setCountryNameR(subOrg3.country1.i);
    await dataPO.setCountryMapR(subOrg3.country1.i);
    await dataPO.setCountryRevenue(subOrg3.country1.i, subOrg3.country1.revenue);
    await dataPO.setCountryCurrencyR(subOrg3.country1.i);
    await dataPO.setCountryHeadcount(subOrg3.country1.i, subOrg3.country1.headcount);

    subOrg3.currency.title = await dataPL.currencyDropdown.getText();
    subOrg3.currency.short = await dataPL.revenueAddInfo.getText();
    subOrg3.businessScope.title = await dataPL.scopeDropdown.getText();
    subOrg3.governance.title = await dataPL.governanceDropdown.getText();
    subOrg3.industry.title = await dataPL.industryDropdown.getText();
    subOrg3.segment.title = await dataPL.segmentDropdown.getText();
    subOrg3.revenueInternal.title = await dataPL.revenueDropdown.getText();

    subOrg3.country1.name.title = await dataPL.countryNameDDs.get(subOrg3.country1.i - 1).getText();
    subOrg3.country1.map.title = await dataPL.countryMapDDs.get(subOrg3.country1.i - 1).getText();
    subOrg3.country1.currency.title = await dataPL.countryCurrencyDDs.get(subOrg3.country1.i - 1).getText();

    await dataPO.saveSubOrg();
    await dataPO.expandSubDetailsByName(subOrg3.name);

    // console.log(subOrg3);

    let actualSub = await dataPL.expandedSubData.getText();
    await expect(parseInt(actualSub[0].replace(subOrg3.currency.short, ``).replace(/\s/g, ``)) == subOrg3.totalRevenue)
      .toBeTruthy(`Currency: expected '${subOrg3.totalRevenue}' instead of '${parseInt(actualSub[0].replace(subOrg3.currency.short, ``))}'`);
    await expect(actualSub[0].indexOf(subOrg3.currency.short) !== -1)
      .toBeTruthy(`Currency: expected '${actualSub[0]}' to contain '${subOrg3.currency.short}'`);
    await expect(parseInt(actualSub[1].replace(/\s/g, ``)) == subOrg3.totalEmployees)
      .toBeTruthy(`Currency: expected '${subOrg3.totalEmployees}' instead of '${parseInt(actualSub[1].replace(/\s/g, ``))}'`);
    await expect(actualSub[2] == subOrg3.businessScope.title)
      .toBeTruthy(`Currency: expected '${subOrg3.businessScope.title}' instead of '${actualSub[2]}'`);
    await expect(actualSub[3] == subOrg3.governance.title)
      .toBeTruthy(`Currency: expected '${subOrg3.governance.title}' instead of '${actualSub[3]}'`);
    await expect(actualSub[4] == subOrg3.industry.title)
      .toBeTruthy(`Currency: expected '${subOrg3.industry.title}' instead of '${actualSub[4]}'`);
    await expect(actualSub[5] == subOrg3.segment.title)
      .toBeTruthy(`Currency: expected '${subOrg3.segment.title}' instead of '${actualSub[5]}'`);
    await expect(actualSub[6] == subOrg3.revenueInternal.title)
      .toBeTruthy(`Currency: expected '${subOrg3.revenueInternal.title}' instead of '${actualSub[6]}'`);

  });

  it(`Edit Parent Organization`, async () => {
    await page.moveToOrgData();

    parentOrg.name.origin = await dataPL.subListNames.get(0).getText();
    let revenue = await dataPL.expandedData.get(0).getText();
    let assets = await dataPL.expandedData.get(1).getText();
    let capitalization = await dataPL.expandedData.get(2).getText();
    let employees = await dataPL.expandedData.get(3).getText();
    parentOrg.ownershipStructure.origin = await dataPL.expandedData.get(7).getText();
    parentOrg.ownershipType.origin = await dataPL.expandedData.get(8).getText();
    parentOrg.gradingStructure.origin = await dataPL.expandedData.get(9).getText();
    parentOrg.gradingAcrossAll.origin = await dataPL.expandedData.get(10).getText();

    await dataPO.clickEditParentOrg();
    parentOrg.currency.origin = await dataPL.currencyDropdown.getText();
    parentOrg.currency.short = await dataPL.revenueAddInfo.getText();
    parentOrg.industry.origin = await dataPL.industryDropdown.getText();
    parentOrg.segment.origin = await dataPL.segmentDropdown.getText();
    parentOrg.revenueInternal.origin = await dataPL.revenueDropdown.getText();
    parentOrg.revenue.origin = parseInt(revenue.trim().replace(/\s/g, ``).replace(parentOrg.currency.short, ``));
    parentOrg.assets.origin = parseInt(assets.trim().replace(/\s/g, ``).replace(parentOrg.currency.short, ``));
    parentOrg.capitalization.origin = parseInt(capitalization.trim().replace(/\s/g, ``).replace(parentOrg.currency.short, ``));
    parentOrg.totalEmployees.origin = parseInt(employees.trim().replace(/\s/g, ``));
    parentOrg.revenue.origin = parseInt(revenue.trim().replace(/\s/g, ``).replace(parentOrg.currency.short, ``));
    parentOrg.gradingStructure

    await dataPO.setSubName(parentOrg.name.modified);
    await dataPO.setSubCurrencyR();
    await dataPO.setSubTotalRevenue(parentOrg.revenue.modified);
    await dataPO.setSubTotalEmployees(parentOrg.totalEmployees.modified);
    await dataPO.setSubIndustryR();
    await dataPO.setSubSegmentR();
    await dataPO.setSubRevenueInternalR();
    await dataPO.setGlobalAssets(parentOrg.assets.modified);
    await dataPO.setMarketCapitalization(parentOrg.capitalization.modified);
    await dataPO.setOwnershipStructureR();
    await dataPO.setOwnershipTypeR();
    // await dataPO.setGradeStructureRadio(`Yes`);
    // await dataPO.setGradeAcrossAllRadio(`No`);

    // parentOrg.gradingStructure.modified = `Yes`;
    // parentOrg.gradingAcrossAll.modified = `No`;
    parentOrg.currency.modified = await dataPL.currencyDropdown.getText();
    parentOrg.currency.modifiedShort = await dataPL.revenueAddInfo.getText();
    parentOrg.industry.modified = await dataPL.industryDropdown.getText();
    parentOrg.segment.modified = await dataPL.segmentDropdown.getText();
    parentOrg.revenueInternal.modified = await dataPL.revenueDropdown.getText();
    parentOrg.ownershipStructure.modified = await dataPL.ownershipStructureDD.getText();
    parentOrg.ownershipType.modified = await dataPL.ownershipTypeDD.getText();

    await dataPO.clickAddCountryBtn();

    parentOrg.country1.i = await dataPL.countryNameDDs.count();
    await dataPO.setCountryNameR(parentOrg.country1.i);
    await dataPO.setCountryMapR(parentOrg.country1.i);
    await dataPO.setCountryRevenue(parentOrg.country1.i, parentOrg.country1.revenue);
    await dataPO.setCountryCurrencyR(parentOrg.country1.i);
    await dataPO.setCountryHeadcount(parentOrg.country1.i, parentOrg.country1.headcount);

    parentOrg.country1.name.title = await dataPL.countryNameDDs.get(parentOrg.country1.i - 1).getText();
    parentOrg.country1.map.title = await dataPL.countryMapDDs.get(parentOrg.country1.i - 1).getText();
    parentOrg.country1.currency.title = await dataPL.countryCurrencyDDs.get(parentOrg.country1.i - 1).getText();

    await dataPO.saveSubOrg();

    let actualParent = await dataPL.expandedData.getText();
    await expect(parseInt(actualParent[0].replace(parentOrg.currency.modifiedShort, ``).replace(/\s/g, ``)) == parentOrg.revenue.modified)
      .toBeTruthy(`Parent Organization(revenue): expected '${parentOrg.revenue.modified}' instead of '${actualParent[0].replace(parentOrg.currency.modifiedShort, ``).replace(/\s/g, ``)}'`);
    await expect(actualParent[0].indexOf(parentOrg.currency.modifiedShort) !== -1)
      .toBeTruthy(`Parent Organization(revenue currency): expected '${actualParent[0]}' to contain '${parentOrg.currency.modifiedShort}'`);
    await expect(parseInt(actualParent[1].replace(parentOrg.currency.modifiedShort, ``).replace(/\s/g, ``)) == parentOrg.assets.modified)
      .toBeTruthy(`Parent Organization(assets): expected '${parentOrg.assets.modified}' instead of '${actualParent[1].replace(parentOrg.currency.modifiedShort, ``).replace(/\s/g, ``)}'`);
    await expect(parseInt(actualParent[2].replace(parentOrg.currency.modifiedShort, ``).replace(/\s/g, ``)) == parentOrg.capitalization.modified)
      .toBeTruthy(`Parent Organization(capitalization): expected '${parentOrg.capitalization.modified}' instead of '${actualParent[2].replace(parentOrg.currency.modifiedShort, ``).replace(/\s/g, ``)}'`);
    await expect(parseInt(actualParent[3].replace(/\s/g, ``)) == parentOrg.totalEmployees.modified)
      .toBeTruthy(`Parent Organization(employees): expected '${parentOrg.totalEmployees.modified}' instead of '${actualParent[3].replace(/\s/g, ``)}'`);
    await expect(actualParent[4] == parentOrg.revenueInternal.modified)
      .toBeTruthy(`Parent Organization(revenue from IO): expected '${parentOrg.revenueInternal.modified}' instead of '${actualParent[4]}'`);
    await expect(actualParent[5] == parentOrg.industry.modified)
      .toBeTruthy(`Parent Organization(industry): expected '${parentOrg.industry.modified}' instead of '${actualParent[5]}'`);
    await expect(actualParent[6] == parentOrg.segment.modified)
      .toBeTruthy(`Parent Organization(segment): expected '${parentOrg.segment.modified}' instead of '${actualParent[6]}'`);
    await expect(actualParent[7] == parentOrg.ownershipStructure.modified)
      .toBeTruthy(`Parent Organization(ownership structure): expected '${parentOrg.ownershipStructure.modified}' instead of '${actualParent[7]}'`);
    await expect(actualParent[8] == parentOrg.ownershipType.modified)
      .toBeTruthy(`Parent Organization(ownership type): expected '${parentOrg.ownershipType.modified}' instead of '${actualParent[8]}'`);
    // await expect(actualParent[9] == parentOrg.gradingStructure.modified)
    //   .toBeTruthy(`Parent Organization(grading structure): expected '${parentOrg.gradingStructure.modified}' instead of '${actualParent[9]}'`);
    // await expect(actualParent[10] == parentOrg.gradingAcrossAll.modified)
    //   .toBeTruthy(`Parent Organization(grading across all): expected '${parentOrg.gradingAcrossAll.modified}' instead of '${actualParent[10]}'`);


    await dataPO.clickEditParentOrg();

    await dataPO.setSubName(parentOrg.name.origin);
    await dataPO.setSubCurrencyByTitle(parentOrg.currency.origin);
    await dataPO.setSubTotalRevenue(parentOrg.revenue.origin);
    await dataPO.setSubTotalEmployees(parentOrg.totalEmployees.origin);
    await dataPO.setSubIndustryByTitle(parentOrg.industry.origin);
    await dataPO.setSubSegmentByTitle(parentOrg.segment.origin);
    await dataPO.setSubRevenueInternalByTitle(parentOrg.revenueInternal.origin);
    await dataPO.setGlobalAssets(parentOrg.assets.origin);
    await dataPO.setMarketCapitalization(parentOrg.capitalization.origin);
    await dataPO.setOwnershipStructureByTitle(parentOrg.ownershipStructure.origin);
    await dataPO.setOwnershipTypeByTitle(parentOrg.ownershipType.origin);
    // await dataPO.setGradeStructureRadio(parentOrg.gradingStructure.origin);
    // await dataPO.setGradeAcrossAllRadio(parentOrg.gradingAcrossAll.origin);

    // console.log(parentOrg);

    await dataPO.clickDeleteCountryBtn(parentOrg.country1.i);
    await dataPO.saveSubOrg();
  });



});