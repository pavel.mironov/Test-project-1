import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { AppPage as page } from './components/app.po';

import { SuccessProfileListPageObjects as listPO } from './components/sp-list.po';
import { SuccessProfileDetailsPageLocators as detailsPL } from './components/sp-details.locators';
import { SuccessProfileDetailsPageObjects as detailsPO } from './components/sp-details.po';
import { SuccessProfileSummaryPageObjects as summaryPO } from './components/sp-summary.po';
import { SuccessProfileSummaryPageLocators as summaryPL } from './components/sp-summary.locators';
import { SuccessProfileLandingPageObjects as landingPO } from './components/sp-landing.po';
import { SuccessProfileLandingPageLocators as landingPL } from './components/sp-landing.locators';
import { SpinnerPO as spinner } from './components/spinner';

const xlsx = require(`xlsx`);

describe('SP', async () => {
  beforeAll(async () => {
    await page.login();
    await browser.waitForAngularEnabled(false);
  });

  afterEach(async () => {
    await browser.manage().logs().get('browser').then(async (browserLog) => {
      if (browserLog.length > 0) {
        await browserLog.forEach(async log => {
          await expect(browserLog.length > 0).toBeFalsy(`error in console`);//.toBeFalsy(log.message);
        });
      }
    });
  });

  const uploadFIle = xlsx.readFile(`./e2e/src/files/SuccessProfiles_upload_File.xlsx`);
  let spMeta = xlsx.utils.sheet_to_json(uploadFIle.Sheets['SuccessProfiles - Metadata']);
  let respMap = xlsx.utils.sheet_to_json(uploadFIle.Sheets['SuccessProfiles - Resp Level']);
  let lookMap = xlsx.utils.sheet_to_json(uploadFIle.Sheets['Lookup Library']);
  let ucpMap = xlsx.utils.sheet_to_json(uploadFIle.Sheets['UCP']);
  let pmtMap = xlsx.utils.sheet_to_json(uploadFIle.Sheets['ProfileMatchSliders']);

  const lookupFIle = xlsx.readFile(`./e2e/src/files/Success Profiles Lookup.xlsx`);
  let funcInfo = xlsx.utils.sheet_to_json(lookupFIle.Sheets['JobFamilySubFamily']);
  let levelInfo = xlsx.utils.sheet_to_json(lookupFIle.Sheets['KFLevels']);
  let respInfo = xlsx.utils.sheet_to_json(lookupFIle.Sheets['Levels Library']);
  let cogAbilInfo = xlsx.utils.sheet_to_json(lookupFIle.Sheets['Cog Abilities']);
  let ucpInfo = xlsx.utils.sheet_to_json(lookupFIle.Sheets['UCP Sliders']);

  let sp = spMeta[0];
  // await spMeta.forEach(async (sp, spIndex) => {
  it(`UPLOAD ${sp['SP Name']}`, async () => {
    await page.moveToSPList();
    await listPO.setFilter(5, 3);
    await listPO.searchProfile(sp['SP Name']);
    await listPO.openSPFromListByName(sp['SP Name']);

    let actualSPName = await detailsPL.profileName.getText();
    await expect(actualSPName == sp['SP Name']).toBeTruthy(`Details(sp name): should be '${sp['SP Name']}' instead of '${actualSPName}'`);
    let actualSPDesc = await detailsPL.profileDescription.getText();
    await expect(actualSPDesc == sp['SPdescription']).toBeTruthy(`Details(sp desc): should be '${sp['SPdescription']}' instead of '${actualSPDesc}'`);

    await levelInfo.forEach(async (level, levelIndex) => {
      if (level['KFLevelCode'].trim() == sp['KF4DLevelCode'].trim()) {
        let actualLevel = await detailsPL.levelInfo.get(0).getText();
        await expect(actualLevel == level['LevelName']).toBeTruthy(`Details(sp level): should be '${level['LevelName']}' instead of '${actualLevel}'`);
        let actualSubLevel = await detailsPL.levelInfo.get(1).getText();
        await expect(actualSubLevel.replace(`|`, ``).trim() == level['SubLevelName'])
          .toBeTruthy(`Details(sp sublevel): should be '${level['SubLevelName']}' instead of '${actualSubLevel.replace(`|`, ``).trim()}'`);
      }
    });

    await funcInfo.forEach(async (func, funcIndex) => {
      if (sp['FuncID'] == func['JobFamilyID'] && sp['SubFuncID'] == func['JobSubFamilyID']) {
        let actualFunc = await detailsPL.functionInfo.get(0).getText();
        await expect(actualFunc == func['JobFamilyName']).toBeTruthy(`Details(sp function): should be '${func['JobFamilyName']}' instead of '${actualFunc}'`);
        let actualSubFunc = await detailsPL.functionInfo.get(1).getText();
        await expect(actualSubFunc.replace(`|`, ``).trim() == func['JobSubFamilyName'])
          .toBeTruthy(`Details(sp subfunction): should be '${func['JobSubFamilyName']}' instead of '${actualSubFunc.replace(`|`, ``).trim()}'`);
      }
    });

    let actualGrade = await detailsPL.gradeInfo.getText();
    await expect(actualGrade == sp['Grade']).toBeTruthy(`Details(sp grade): should be '${sp['Grade']}' instead of '${actualGrade}'`);

    await spinner.delay(1000);
    let actualRoleLocation = await detailsPL.regionName.getText();
    await expect(actualRoleLocation == `Global (2018 Norm)`).toBeTruthy(`Details(sp region): should be 'Global (2018 Norm)' instead of '${actualRoleLocation}'`);

    await respMap.forEach(async (couple, coupleIndex) => {
      if (couple['JRTDETAILD'] == sp['JRTDETAILID']) {
        await respInfo.forEach(async (resp, respIndex) => {
          if (resp['ProductInternalCode'] == couple['HubRespID'] && resp['Level'].indexOf(`LEVEL ${couple['Level']} - `) !== -1) {
            if (couple['sequence'] < 6) {
              let actualRespName = await detailsPL.detailResps.get(couple['sequence'] - 1).getText();
              await expect(actualRespName == resp['Name'])
                .toBeTruthy(`Details(resp#${couple['sequence']}): should be '${resp['Name']}' instead of '${actualRespName}'`);
              let actualRespDesc = await detailsPL.detailRespDescs.get(couple['sequence'] - 1).getText();
              await expect(actualRespDesc.trim() == resp['Level Description'].trim())
                .toBeTruthy(`Details(resp#${couple['sequence']}): should be '${resp['Level Description'].trim()}' instead of '${actualRespDesc.trim()}'`);
            }
          }
        });
      }
    });

    await lookMap.forEach(async (coup, coupIndex) => {
      if (coup['Element'] == 'Education' && coup['Level'] == sp['Education']) {
        let actualEducation = await detailsPL.detailEduDesc.getText();
        await expect(actualEducation == coup['Description'])
          .toBeTruthy(`Details(education): should be '${coup['Description']}' instead of '${actualEducation}'`);
      }
      if (coup['Element'] == 'General Experience' && coup['Level'] == sp['GenExp']) {
        let actualGenExp = await detailsPL.detailExpDescs.get(0).getText();
        await expect(actualGenExp.indexOf(`(${coup['Description'].trim()})`) !== -1)
          .toBeTruthy(`Details(general experience): should contain (${coup['Description'].trim()})`);
      }
      if (coup['Element'] == 'Managerial Experience' && coup['Level'] == sp['MgrExp']) {
        let actualManExp = await detailsPL.detailExpDescs.get(1).getText();
        await expect(actualManExp.indexOf(`(${coup['Description'].trim()})`) !== -1)
          .toBeTruthy(`Details(managerial experience): should contain (${coup['Description'].trim()})`);
      }
    });

    // summary
    await detailsPO.openSummaryViaActions();
    await summaryPO.clickDetailsTab();
    let actualRoleSum = await summaryPL.roleSummary.getText();
    await expect(actualRoleSum == sp['Role Description'])
      .toBeTruthy(`Summary(details): role description should be '${sp['SP Description']}' instead of '${actualRoleSum}'`);
    let actualLevelInfo = await summaryPL.detailsSummary.get(0).getText();
    await levelInfo.forEach(async (level, levelIndex) => {
      if (level['KFLevelCode'].trim() == sp['KF4DLevelCode'].trim()) {
        await expect(actualLevelInfo == `${level['LevelName']} | ${level['SubLevelName']}`)
          .toBeTruthy(`Summary(details): level info should be '${level['LevelName']} | ${level['SubLevelName']}' instead of '${actualLevelInfo}'`);
      }
    });
    let actualFuncInfo = await summaryPL.detailsSummary.get(1).getText();
    await funcInfo.forEach(async (func, funcIndex) => {
      if (sp['FuncID'] == func['JobFamilyID'] && sp['SubFuncID'] == func['JobSubFamilyID']) {
        await expect(actualFuncInfo == `${func['JobFamilyName']} | ${func['JobSubFamilyName']}`)
          .toBeTruthy(`Summary(details): function info should be '${func['JobFamilyName']} | ${func['JobSubFamilyName']}' instead of '${actualFuncInfo}'`);
      }
    });
    let actualRegionInfo = await summaryPL.detailsSummary.get(2).getText();
    await expect(actualRegionInfo == `Global (2018 Norm)`).toBeTruthy(`Summary(details): region should be 'Global (2018 Norm)' instead of '${actualRegionInfo}'`);
    let actualGradeInfo = await summaryPL.gradeSummary.get(0).getText();
    await expect(actualGradeInfo == sp['Grade']).toBeTruthy(`Summary(details): grade should be '${sp['Grade']}' instead of '${actualGradeInfo}'`);

    await summaryPO.clickAccountabilityTab();
    await summaryPO.clickResponsibilitiesTab();
    await expect(await summaryPL.wcSummaryTitles.get(0).getText() == `Dealing with Emotional Stress`).toBeTruthy(`Summary(work characteristics): wrong title for DES characteristic`);
    await expect(await summaryPL.wcSummaryTitles.get(1).getText() == `Managing Conflict`).toBeTruthy(`Summary(work characteristics): wrong title for MC characteristic`);
    await expect(await summaryPL.wcSummaryTitles.get(2).getText() == `Collaboration and Partnering`).toBeTruthy(`Summary(work characteristics): wrong title for CP characteristic`);
    await lookMap.forEach(async (couple, coupleIndex) => {
      if (couple['Element'] == `Work characteristics`) {
        if (couple['Level'] == sp['WC_DES']) {
          let actualDESlvl = await summaryPL.wcSummaryLevels.get(0).getText();
          await expect(couple['Description'].toLowerCase().indexOf(actualDESlvl.toLowerCase()) !== -1).toBeTruthy(`Summary(work characteristics): wrong level for DES characteristic`);
        }
        if (couple['Level'] == sp['WC_MC']) {
          let actualMClvl = await summaryPL.wcSummaryLevels.get(1).getText();
          await expect(couple['Description'].toLowerCase().indexOf(actualMClvl.toLowerCase()) !== -1).toBeTruthy(`Summary(work characteristics): wrong level for MC characteristic`);
        }
        if (couple['Level'] == sp['WC_CP']) {
          let actualCPlvl = await summaryPL.wcSummaryLevels.get(2).getText();
          await expect(couple['Description'].toLowerCase().indexOf(actualCPlvl.toLowerCase()) !== -1).toBeTruthy(`Summary(work characteristics): wrong level for CP characteristic`);
        }
      }
    });

    await respMap.forEach(async (couple, coupleIndex) => {
      if (couple['JRTDETAILD'] == sp['JRTDETAILID']) {
        await respInfo.forEach(async (resp, respIndex) => {
          if (resp['ProductInternalCode'] == couple['HubRespID'] && resp['Level'].indexOf(`LEVEL ${couple['Level']} - `) !== -1) {
            // console.log(couple['sequence'], resp['Name'], couple['Level'])
            let actualRespName = await summaryPL.respSummaryNames.get(couple['sequence'] - 1).getText();
            let actualRespLevel = await summaryPL.respSummaryLevels.get(couple['sequence'] - 1).getText();
            let actualRespDesc = await summaryPL.respSummaryDescs.get(couple['sequence'] - 1).getText();
            await expect(actualRespName == resp['Name'])
              .toBeTruthy(`Summary(resp#${couple['sequence']}): should be '${resp['Name']}' instead of '${actualRespName}'`);
            await expect(actualRespLevel == couple['Level'])
              .toBeTruthy(`Summary(resp#${couple['sequence']}): should be '${couple['Level']}' instead of '${actualRespLevel}'`);
            await expect(actualRespDesc.trim() == resp['Level Description'].trim())
              .toBeTruthy(`Summary(resp#${couple['sequence']}): should be '${resp['Level Description'].trim()}' instead of '${actualRespDesc.trim()}'`);
          }
        });
      }
    });

    await spinner.delay(1000);
    await summaryPO.clickCapabilityTab();
    await summaryPO.clickEducationTab();
    await lookMap.forEach(async (coup, coupIndex) => {
      if (coup['Element'] == 'Education' && coup['Level'] == sp['Education']) {
        let actualEducationName = await summaryPL.eduSummaryNames.get(0).getText();
        await expect(actualEducationName == `General Education`)
          .toBeTruthy(`Summary(education): title should be 'General Education' instead of '${actualEducationName}'`);
        let actualEducationLvl = await summaryPL.eduSummaryLevels.get(0).getText();
        await expect(actualEducationLvl == sp['Education'])
          .toBeTruthy(`Summary(education): level should be '${sp['Education']}' instead of '${actualEducationLvl}'`);
        let actualEducationDesc = await summaryPL.eduSummaryDescs.get(0).getText();
        await expect(actualEducationDesc == coup['Description'])
          .toBeTruthy(`Summary(education): description should be '${coup['Description']}' instead of '${actualEducationDesc}'`);
      }
    });

    await spinner.delay(1000);
    await summaryPO.clickExperienceTab();
    await lookMap.forEach(async (coup, coupIndex) => {
      if (coup['Element'] == 'General Experience' && coup['Level'] == sp['GenExp']) {
        let actualGenExpName = await summaryPL.expSummaryNames.get(0).getText();
        await expect(actualGenExpName == `General Experience`)
          .toBeTruthy(`Summary(general experience): title should be 'General Experience' instead of '${actualGenExpName}'`);
        let actualGenExpLvl = await summaryPL.expSummaryLevels.get(0).getText();
        await expect(actualGenExpLvl == sp['GenExp'])
          .toBeTruthy(`Summary(general experience): level should be '${sp['GenExp']}' instead of '${actualGenExpLvl}'`);
        let actualGenExpDesc = await summaryPL.expSummaryDescs.get(0).getText();
        await expect(actualGenExpDesc.indexOf(`(${coup['Description'].trim()})`) !== -1)
          .toBeTruthy(`Summary(general experience): description should contain '${coup['Description'].trim()}'`);
      }
      if (coup['Element'] == 'Managerial Experience' && coup['Level'] == sp['MgrExp']) {
        let actualManExpName = await summaryPL.expSummaryNames.get(1).getText();
        await expect(actualManExpName == `Managerial Experience`)
          .toBeTruthy(`Summary(managerial experience): title should be 'Managerial Experience' instead of '${actualManExpName}'`);
        let actualManExpLvl = await summaryPL.expSummaryLevels.getText();
        await expect(actualManExpLvl[1] == sp['MgrExp'])
          .toBeTruthy(`Summary(managerial experience): level should be '${sp['MgrExp']}' instead of '${actualManExpLvl[1]}'`);
        let actualManExpDesc = await summaryPL.expSummaryDescs.get(1).getText();
        await expect(actualManExpDesc.indexOf(`(${coup['Description'].trim()})`) !== -1)
          .toBeTruthy(`Summary(managerial experience): description should contain '${coup['Description'].trim()}'`);
      }
    });

    await spinner.delay(1000);
    await summaryPO.clickIdentityTab();
    await summaryPO.clickCognitiveAbilitiesTab();

    await lookMap.forEach(async (type, typeIndex) => {
      if (type['Element'] == `CogTestType` && type['Level'] == sp['CogTestType']) {
        await cogAbilInfo.forEach(async (ability, abilityIndex) => {
          // Cog_Numerical	Cog_Verbal	Cog_LogicCheck
          if (ability['Cognitive Ability Code'] == `${type['Description'].trim().toUpperCase()}_NUMERICAL`) {
            let actualNumericalCAName = await summaryPL.cogAbilSummaryNames.get(0).getText();
            let actualNumericalCADesc = await summaryPL.cogAbilSummaryDescs.get(0).getText();
            let actualNumericalCALevel = await summaryPL.cogAbilSummaryLevels.get(0).getText();
            await expect(actualNumericalCAName == `Numerical`)
              .toBeTruthy(`Summary(cognitive abilities): name should be 'Numerical'`);
            await expect(actualNumericalCADesc == ability['Description'])
              .toBeTruthy(`Summary(cognitive abilities): description should be '${ability['Description']}' instead of '${actualNumericalCADesc}'`);
            if (sp['Cog_Numerical'] == `Yes`) {
              await expect(actualNumericalCALevel == `Recommended`)
                .toBeTruthy(`Summary(cognitive abilities): level should be 'Recommended' instead of '${actualNumericalCADesc}'`);
            } else {
              await expect(actualNumericalCALevel == `Optional`)
                .toBeTruthy(`Summary(cognitive abilities): level should be 'Optional' instead of '${actualNumericalCADesc}'`);
            }
          }
          if (ability['Cognitive Ability Code'] == `${type['Description'].trim().toUpperCase()}_VERBAL`) {
            let actualVerbalCAName = await summaryPL.cogAbilSummaryNames.get(1).getText();
            let actualVerbalCADesc = await summaryPL.cogAbilSummaryDescs.get(1).getText();
            let actualVerbalCALevel = await summaryPL.cogAbilSummaryLevels.get(1).getText();
            await expect(actualVerbalCAName == `Verbal`)
              .toBeTruthy(`Summary(cognitive abilities): name should be 'Verbal'`);
            await expect(actualVerbalCADesc == ability['Description'])
              .toBeTruthy(`Summary(cognitive abilities): description should be '${ability['Description']}' instead of '${actualVerbalCADesc}'`);
            if (sp['Cog_Numerical'] == `Yes`) {
              await expect(actualVerbalCALevel == `Recommended`)
                .toBeTruthy(`Summary(cognitive abilities): level should be 'Recommended' instead of '${actualVerbalCALevel}'`);
            } else {
              await expect(actualVerbalCALevel == `Optional`)
                .toBeTruthy(`Summary(cognitive abilities): level should be 'Optional' instead of '${actualVerbalCALevel}'`);
            }
          }
          if (ability['Cognitive Ability Code'] == `${type['Description'].trim().toUpperCase()}_Logical`) {
            let actualLogicalCAName = await summaryPL.cogAbilSummaryNames.get(2).getText();
            let actualLogicalCADesc = await summaryPL.cogAbilSummaryDescs.get(2).getText();
            let actualLogicalCALevel = await summaryPL.cogAbilSummaryLevels.get(2).getText();
            await expect(actualLogicalCAName == `Logical`)
              .toBeTruthy(`Summary(cognitive abilities): name should be 'Logical'`);
            await expect(actualLogicalCADesc == ability['Description'])
              .toBeTruthy(`Summary(cognitive abilities): description should be '${ability['Description']}' instead of '${actualLogicalCADesc}'`);
            if (sp['Cog_Numerical'] == `Yes`) {
              await expect(actualLogicalCALevel == `Recommended`)
                .toBeTruthy(`Summary(cognitive abilities): level should be 'Recommended' instead of '${actualLogicalCALevel}'`);
            } else {
              await expect(actualLogicalCALevel == `Optional`)
                .toBeTruthy(`Summary(cognitive abilities): level should be 'Optional' instead of '${actualLogicalCALevel}'`);
            }
          }
        });
      }
    });

    // // CHECK ON EDIT PAGE 
    await spinner.delay(1000);
    await detailsPO.openEditPageViaActions();
    await landingPO.selectEditPageItem(4);

    await expect(await landingPL.cultureNames.get(0).getText() == `Collaborative`)
      .toBeTruthy(`Cultures: expected 'Collborative' instead of '${await landingPL.cultureNames.get(0).getText()}'`);
    await expect(await landingPL.cultureNames.get(1).getText() == `Innovative`)
      .toBeTruthy(`Cultures: expected 'Innovative' instead of '${await landingPL.cultureNames.get(1).getText()}'`);
    await expect(await landingPL.cultureNames.get(2).getText() == `Structured`)
      .toBeTruthy(`Cultures: expected 'Structured' instead of '${await landingPL.cultureNames.get(2).getText()}'`);
    await expect(await landingPL.cultureNames.get(3).getText() == `Competitive`)
      .toBeTruthy(`Cultures: expected 'Competitive' instead of '${await landingPL.cultureNames.get(3).getText()}'`);


    await browser.wait(EC.presenceOf(landingPL.ucpSliderNames.get(0)));
    await ucpMap.forEach(async (map, mapIndex) => {
      if (map[Object.keys(map)[2]] == sp['SP Name'] && map[Object.keys(map)[1]] == `NEWGLOBAL2018`) {
        await ucpInfo.forEach(async (info, infoIndex) => {
          for (let i = 3; i < 27; i++) {
            if (Object.keys(map)[i] == info['DomainCode']) {
              // console.log(info['DomainOrder'], info['DomainText'], map[Object.keys(map)[i]]); // order, text n value
              let actualUCPName = await landingPL.ucpSliderNames.get(info['DomainOrder'] - 1).getText();
              let actualUCPValue = await landingPL.sliderValues.get(info['DomainOrder'] - 1).getText();
              await expect(actualUCPName == info['DomainText'])
                .toBeTruthy(`UCP Slider(${info['DomainOrder']}): title expected '${info['DomainText']}' instead of '${actualUCPName}'`);
              await expect(actualUCPValue == map[Object.keys(map)[i]].toFixed(2))
                .toBeTruthy(`UCP Slider(${info['DomainOrder']}): value expected '${map[Object.keys(map)[i]].toFixed(2)}' instead of '${actualUCPValue}'`);
            }
          }
        });
      }
    });

    await spinner.delay(1000);
    await landingPO.clickCancelButton();

    await detailsPO.openPMTViaActions();
    console.log(sp['TalentHubClusterID']);
    console.log(pmtMap[0]);


    // await page.moveToSPList();
    // await listPO.clearAllFilters();


  });
  // });




});