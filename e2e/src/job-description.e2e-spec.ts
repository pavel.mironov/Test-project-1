import { browser, ExpectedConditions as EC } from 'protractor';
import { AppPage as page } from './components/app.po';
import { checkFileExistsWithDelay, checkFileStatWithDelay, deleteFileIfExists } from './components/utils';
import { SuccessProfileListPageObjects as spListPO } from './components/sp-list.po';
import { JobDescriptionDetailsLocators as jdDetailsPL } from './components/jd-details.locators';
import { JobDescriptionPageObject as jdDetailsPO } from './components/jd-details.po';
import { JobDescriptionListPageObjects as jdListPO } from './components/jd-list.po';
import { SuccessProfileDetailsPageObjects as spDetailsPO } from './components/sp-details.po';
import { SuccessProfileDetailsPageLocators as spDetailsPL } from './components/sp-details.locators';
import { SuccessProfileLandingPageObjects as landingPO } from './components/sp-landing.po';
import { SpinnerPO as spinner } from './components/spinner';

const now = new Date();
let profileName = `Test ASDFG || `;
let profileNameU = `Test ASDFG || ${now.toUTCString()}`;

describe('Job description', () => {
  beforeEach(async () => {
    await page.login();
    await browser.waitForAngularEnabled(false);
  });

  afterEach(async () => {
    await browser.manage().logs().get('browser').then(async (browserLog) => {
      if (browserLog.length > 0) {
        await browserLog.forEach(async log => {
          await expect(browserLog.length > 0).toBeFalsy(`error in console`);//.toBeFalsy(log.message);
        });
      }
    });
  });

  // xit(`Delete old JD`, async () => {
  //   await page.moveToJDList();
  //   await jdListPO.searchJobDesctiption(profileName).then(async () => {
  //     await jdListPO.getCount().then(async (am) => {
  //       for (let i = am; i > 0; i--) {
  //         await jdListPO.deleteJDViaThreeDotsMenu();
  //       };
  //     });
  //   });

  // });

  xit(`JD report load`, async () => {
    await page.moveToJDList();
    await jdListPO.searchJobDesctiption(profileName);

    await jdListPO.openJDFromListByNo(1);

    const minFileSize = 250000;
    const downloadTimeout = 30; //sec
    let fileName = 'Job Description.pdf';
    console.log(fileName);
    await jdDetailsPO.openActionsMenu();
    await jdDetailsPO.clickActionsItem(1);
    deleteFileIfExists(fileName);
    await expect(await checkFileExistsWithDelay(fileName, downloadTimeout)).toBeTruthy(`Can't load SP report for SP(${profileName})`);
    await expect(await checkFileStatWithDelay(fileName, minFileSize, downloadTimeout)).toBeTruthy(`File must be greater than ${minFileSize}(SP name: ${profileName})`);
  });

  xit(`JD IG report load`, async () => {
    await page.moveToJDList();
    await jdListPO.searchJobDesctiption(profileName);

    await jdListPO.openJDFromListByNo(1);

    const minFileSize = 250000;
    const downloadTimeout = 30; //sec
    let jdId = await jdDetailsPO.getJobDescriptionId();
    let fileName = `InterviewGuide_${jdId}.pdf`;
    console.log(fileName);
    await jdDetailsPO.openActionsMenu();
    await jdDetailsPO.clickActionsItem(2);
    deleteFileIfExists(fileName);
    await expect(await checkFileExistsWithDelay(fileName, downloadTimeout)).toBeTruthy(`Can't load SP report for SP(${profileName})`);
    await expect(await checkFileStatWithDelay(fileName, minFileSize, downloadTimeout)).toBeTruthy(`File must be greater than ${minFileSize}(SP name: ${profileName})`);
  });

  let respName = 'Test Resp name 1';
  let bcName = 'Test BC name 1';
  let tcName = 'Test TC name 1';
  let generalExp = 'Test g experience 1';
  let managerialExp = 'Test m experience 1';
  let generalEdu = 'Test g education 1';
  it(`Edit / Save / version`, async () => {
    await page.moveToSPList();
    await spListPO.clearSearch();
    await spListPO.setFilter(5, 3);
    await spListPO.setFilter(1, 2);

    await spListPO.openSPFromListByNo(1);
    let pName = await spDetailsPL.profileName.getText();
    console.log(pName);
    await spDetailsPO.createJobDescriptionViaActions();
    let actualTextLink = await jdDetailsPL.basedOnLink.getText();
    await expect(actualTextLink[0].indexOf(pName) !== -1).toBeTruthy(`JD details: expected '${actualTextLink}' to contain '${pName}'`);
    await expect(actualTextLink[0].indexOf(`Version 1.0`) !== -1).toBeTruthy(`JD details: expected '${actualTextLink}' to contain 'Version 1.0'`);

    await jdDetailsPO.editTitle(profileNameU);
    await jdDetailsPO.changeRespName(1, respName);
    await jdDetailsPO.changeBCName(1, bcName);
    await jdDetailsPO.changeTCName(1, tcName);
    await jdDetailsPO.changeGenExpName(generalExp);
    await jdDetailsPO.changeManExpName(managerialExp);
    await jdDetailsPO.changeGenEducation(generalEdu);

    await jdDetailsPO.saveJobDescription();

    let title = await jdDetailsPL.title.getText();
    await expect(title.indexOf(profileNameU.toUpperCase()) !== -1).toBeTruthy(`Title different from: ${title}`);

    let resptitle = await jdDetailsPL.respTitles.get(0).getText();
    await expect(resptitle.indexOf(respName) !== -1).toBeTruthy(`Resps`);

    let bctitle = await jdDetailsPL.bcTitles.get(0).getText();
    await expect(bctitle.indexOf(bcName) !== -1).toBeTruthy(`BC`);

    let tctitle = await jdDetailsPL.tcTitles.get(0).getText();
    await expect(tctitle.indexOf(tcName) !== -1).toBeTruthy(`TC`);

    let exptitle1 = await jdDetailsPL.expTiles.get(0).getText();
    await expect(exptitle1.indexOf(generalExp) !== -1).toBeTruthy(`general EXP`);
    let exptitle2 = await jdDetailsPL.expTiles.get(1).getText();
    await expect(exptitle2.indexOf(managerialExp) !== -1).toBeTruthy(`managerial EXP`);

    let edutitle = await jdDetailsPL.edutitles.get(0).getText();
    await expect(edutitle.indexOf(generalEdu) !== -1).toBeTruthy(`general EDU`);


  });
});