import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { JobListLocators as listPL } from './job-list.locators';
import { SpinnerPO as spinner } from './spinner';

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

export class JobListPageObjects {
  static async moveToJobsList() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.jobListTab.get(0)), browser.params.cooldown.medium, `Jobs tab title isn't clickable`);
    await spinner.waitUntilSpinnerOff();
    await listPL.jobListTab.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickAddNewJobButton() {
    await browser.wait(EC.elementToBeClickable(listPL.addNewJobButton), browser.params.cooldown.medium, `No Add New Job button`);
    await listPL.addNewJobButton.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async searchProfile(text: string) {
    await browser.wait(EC.elementToBeClickable(listPL.searchField), browser.params.cooldown.medium, `There is no search field`);
    await listPL.searchField.clear();
    await listPL.searchField.sendKeys(text, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  };

  static async searchJobByTitle(text: string) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.searchDropdown), browser.params.cooldown.medium, `Search type dropdown isn't clickable`);
    await listPL.searchDropdown.click();
    await browser.wait(EC.elementToBeClickable(listPL.searchDropdownItems.get(0)), browser.params.cooldown.medium, `Can't set search by Job Title`); // by Title
    await listPL.searchDropdownItems.get(0).click();
    await browser.wait(EC.elementToBeClickable(listPL.searchField), browser.params.cooldown.medium, `There is no search field`);
    await listPL.searchField.clear();
    await listPL.searchField.sendKeys(text, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  };

  static async searchJobByCode(text: string) {
    await browser.wait(EC.elementToBeClickable(listPL.searchDropdown), browser.params.cooldown.medium, `Search type dropdown isn't clickable`);
    await listPL.searchDropdown.click();
    await browser.wait(EC.elementToBeClickable(listPL.searchDropdownItems.get(1)), browser.params.cooldown.medium, `Can't set search by Job Title`); // by Code
    await listPL.searchDropdownItems.get(1).click();
    await browser.wait(EC.elementToBeClickable(listPL.searchField), browser.params.cooldown.medium, `There is no search field`);
    await listPL.searchField.clear();
    await listPL.searchField.sendKeys(text, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  };

  static async searchJobByFunction(text: string) {
    await browser.wait(EC.elementToBeClickable(listPL.searchDropdown), browser.params.cooldown.medium, `Search type dropdown isn't clickable`);
    await listPL.searchDropdown.click();
    await browser.wait(EC.elementToBeClickable(listPL.searchDropdownItems.get(2)), browser.params.cooldown.medium, `Can't set search by Job Title`); // by Function
    await listPL.searchDropdownItems.get(2).click();
    await browser.wait(EC.elementToBeClickable(listPL.searchField), browser.params.cooldown.medium, `There is no search field`);
    await listPL.searchField.clear();
    await listPL.searchField.sendKeys(text, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  };

  static async clearSearch() {
    await browser.wait(EC.presenceOf(listPL.searchField), browser.params.cooldown.medium, 'There is no search field');
    await listPL.searchField.clear();
    await listPL.searchField.sendKeys(`a`, protractor.Key.BACK_SPACE, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  }

  static async clickUseThisBtn(k = 1) {
    await browser.wait(EC.elementToBeClickable(listPL.useThisBtns.get(k - 1)), browser.params.cooldown.medium, `No Use This radio(${k})`);
    await listPL.useThisBtns.get(k - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setJobName(text: string) {
    await browser.wait(EC.elementToBeClickable(listPL.customizeJobInputs.get(0)), browser.params.cooldown.medium, `There is no Job Name input`);
    await listPL.customizeJobInputs.get(0).clear();
    await listPL.customizeJobInputs.get(0).sendKeys(text);
    await spinner.waitUntilSpinnerOff();
  };

  static async setJobCode(text: string) {
    await browser.wait(EC.elementToBeClickable(listPL.customizeJobInputs.get(1)), browser.params.cooldown.medium, `There is no Job Name code`);
    await listPL.customizeJobInputs.get(1).clear();
    await listPL.customizeJobInputs.get(1).sendKeys(text);
    await spinner.waitUntilSpinnerOff();
  };

  static async setDropdownValue(dropdownN = 1, itemN = 1) {
    await browser.wait(EC.elementToBeClickable(listPL.customizeJobDropdown.get(dropdownN - 1)), browser.params.cooldown.medium, `There is no Dropdown(${dropdownN})`);
    await listPL.customizeJobDropdown.get(dropdownN - 1).click();
    await listPL.customizeJobDropdownItems.get(itemN - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setDropdownValueEdit(dropdownN = 1, itemN = 1) {
    await browser.wait(EC.elementToBeClickable(listPL.customizeJobDropdownE.get(dropdownN - 1)), browser.params.cooldown.medium, `There is no Dropdown(${dropdownN})`);
    await listPL.customizeJobDropdownE.get(dropdownN - 1).click();
    await listPL.customizeJobDropdownItems.get(itemN - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async customizeJob(name = `Test ASDFG`, code = `ASDFG`) {
    await this.setJobName(name);
    await this.setJobCode(code);
    let am = await listPL.customizeJobDropdown.count();
    for (let i = 0; i < am + 1; i++) {
      await this.setDropdownValue(i, 2);
    }
    await spinner.waitUntilSpinnerOff();
  };

  static async clickAddCustomizeJob() {
    await browser.wait(EC.elementToBeClickable(listPL.customizeJobBtns.get(1)), browser.params.cooldown.medium, `No Add Job(add new job dialog) Button`);
    await listPL.customizeJobBtns.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickCancelCustomizeJob() {
    await browser.wait(EC.elementToBeClickable(listPL.customizeJobBtns.get(0)), browser.params.cooldown.medium, `No Cancel(add new job dialog) Button`);
    await listPL.customizeJobBtns.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickAddAnotherJob() {
    await browser.wait(EC.elementToBeClickable(listPL.customizeJobBtns.get(1)), browser.params.cooldown.medium, `No Add(job created) Button`);
    await listPL.customizeJobBtns.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async openJobFromList(posNum = 1) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.jobListNames.get(posNum - 1)), browser.params.cooldown.medium, `No Job(pos ${posNum}) in list`);
    await listPL.jobListNames.get(posNum - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async openJobFromAddList(posNum = 1) {
    await browser.wait(EC.elementToBeClickable(listPL.addListNames.get(posNum - 1)), browser.params.cooldown.medium, `No Job(pos ${posNum}) in add list`);
    await listPL.addListNames.get(posNum - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async getCount() {
    await spinner.waitUntilSpinnerOff();
    try {
      const v = await listPL.filterTitle.getText().then(async (text: string) => { return text; });
      const vv = v.split(' ');
      return parseInt(vv[3], 10);
    }
    catch{
      return 0;
    }
  }

  static async clickFilterDropdown() {
    await browser.wait(EC.elementToBeClickable(listPL.filterDropdown), browser.params.cooldown.medium, `No filter dropdown`);
    await browser.executeScript(`window.scrollTo(0,0)`);
    await listPL.filterDropdown.click();
    await spinner.waitUntilSpinnerOff();
  };


  static async setFunctionFilteByName(functionName: string) {
    await this.clickFilterDropdown();
    await browser.wait(EC.elementToBeClickable(listPL.filterSections.get(1)), browser.params.cooldown.medium, `No sections in filter dropdown`);
    await listPL.filterSections.get(1).click();
    await browser.wait(EC.elementToBeClickable(listPL.filterSectionSearch), browser.params.cooldown.medium, `No search in filter dropdown`);
    await listPL.filterSectionSearch.clear();
    await listPL.filterSectionSearch.sendKeys(functionName, protractor.Key.ENTER);
    await browser.wait(EC.elementToBeClickable(listPL.filterItemCheckboxes.get(0)), browser.params.cooldown.medium, `Nothing found in filter(${functionName}) dropdown`);
    await listPL.filterItemCheckboxes.get(0).click();
    await spinner.waitUntilSpinnerOff();

    await browser.wait(EC.elementToBeClickable(listPL.filterSections.get(1)), browser.params.cooldown.medium, `No sections in filter dropdown`);
    await listPL.filterSections.get(1).click();
    await this.clickFilterDropdown();
  };

  static async setGradeFilter(grade: string) {
    await this.clickFilterDropdown();
    await browser.wait(EC.elementToBeClickable(listPL.filterSections.get(5)), browser.params.cooldown.medium, `No sections in filter dropdown`);
    await listPL.filterSections.get(5).click();

    let am1 = await listPL.filterGradeSets.count();
    let k = 0
    for (k; k < am1; k++) {
      if (await listPL.filterGradeSets.get(k).getText() == `Korn Ferry Standard Grades`){
        await listPL.filterGradeSets.get(k).click();
      };
    };

    let am = await listPL.filterItemNames.count();
    for (let i = 0; i < am; i++) {
      let t = await listPL.filterItemNames.get(i).getText();
      if (t.indexOf(grade) !== -1) {
        await browser.wait(EC.elementToBeClickable(listPL.filterItemCheckboxes.get(i)), browser.params.cooldown.medium, ``);
        await browser.executeScript("arguments[0].scrollIntoView();", listPL.filterGradeSets.get(0).getWebElement());
        await listPL.filterItemCheckboxes.get(i).click();
        await spinner.waitUntilSpinnerOff();
        break;
        // 
      };
    }
    await spinner.waitUntilSpinnerOff();

    await browser.wait(EC.elementToBeClickable(listPL.filterSections.get(5)), browser.params.cooldown.medium, `No sections in filter dropdown`);
    await browser.executeScript(`window.scrollTo(0,0)`);
    await listPL.filterSections.get(5).click();
    await this.clickFilterDropdown();
  };

  static async setUpdatedByFilter(name: string) {
    await this.clickFilterDropdown();
    await browser.wait(EC.elementToBeClickable(listPL.filterSections.get(3)), browser.params.cooldown.medium, `No sections in filter dropdown`);
    await listPL.filterSections.get(3).click();
    let am = await listPL.filterItemNames.count();
    for (let i = 0; i < am; i++) {
      let t = await listPL.filterItemNames.get(i).getText();
      if (t.indexOf(name) !== -1) {
        await browser.wait(EC.elementToBeClickable(listPL.filterItemCheckboxes.get(i)), browser.params.cooldown.medium, ``);
        await listPL.filterItemCheckboxes.get(i).click();
        break;
      };
    }
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.filterSections.get(3)), browser.params.cooldown.medium, `No sections in filter dropdown`);
    await listPL.filterSections.get(3).click();
    await this.clickFilterDropdown();
  };

  static async setKFHPFilter(minValue: string, maxValue: string) {
    await this.clickFilterDropdown();
    await browser.wait(EC.elementToBeClickable(listPL.filterSections.get(2)), browser.params.cooldown.medium, `No sections in filter dropdown`);
    await listPL.filterSections.get(2).click();
    await browser.wait(EC.elementToBeClickable(listPL.filterInputs.get(0)), browser.params.cooldown.medium, `No inputs fo KF HP filter`);
    await listPL.filterInputs.get(0).clear();
    await listPL.filterInputs.get(0).sendKeys(minValue);
    await listPL.filterInputs.get(1).clear();
    await listPL.filterInputs.get(1).sendKeys(maxValue);
    await browser.wait(EC.elementToBeClickable(listPL.filterSections.get(2)), browser.params.cooldown.medium, `No sections in filter dropdown`);
    await spinner.waitUntilSpinnerOff();
    await listPL.filterSections.get(2).click();
    await spinner.waitUntilSpinnerOff();
    await this.clickFilterDropdown();
  };

  static async setModifiedDateFilter(dateFrom: Date, dateTo: Date) {
    await this.clickFilterDropdown();
    await browser.wait(EC.elementToBeClickable(listPL.filterSections.get(4)), browser.params.cooldown.medium, `No sections in filter dropdown`);
    await listPL.filterSections.get(4).click();
    await browser.wait(EC.elementToBeClickable(listPL.filterInputs.get(0)), browser.params.cooldown.medium, `No inputs fo KF Modified Date filter`);

    await listPL.filterInputs.get(0).click();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight/2)`);
    await listPL.filterFooterButtons.get(0).click();
    let dayFrom = dateFrom.getDate();
    let monthFrom = dateFrom.getMonth();
    let yearFrom = dateFrom.getFullYear();
    let i = 0;
    while (1) {
      let t = await listPL.filterCalendarHeader.getText();
      if (t.indexOf(monthNames[monthFrom].toUpperCase()) !== -1 && t.indexOf(yearFrom.toString()) !== -1) {
        break;
      };
      await listPL.filterCalendarArrows.get(1).click()
      i++;
      if (i > 25) break;
    };
    let am = await listPL.filterCalendarActiveDates.count();
    for (let i = 0; i < am; i++) {
      let t2 = await listPL.filterCalendarActiveDates.get(i).getText();
      if (t2 == dayFrom.toString()) {
        await listPL.filterCalendarActiveDates.get(i).click();
        break;
      };
    };
    await listPL.filterFooterButtons.get(1).click();
    await spinner.waitUntilSpinnerOff();

    await listPL.filterInputs.get(1).click();
    let dayTo = dateTo.getDate();
    let monthTo = dateTo.getMonth();
    let yearTo = dateTo.getFullYear();
    i = 0;
    while (1) {
      let t = await listPL.filterCalendarHeader.getText();
      if (t.indexOf(monthNames[monthTo].toUpperCase()) !== -1 && t.indexOf(yearTo.toString()) !== -1) {
        break;
      };
      await listPL.filterCalendarArrows.get(1).click()
      i++;
      if (i > 25) break;
    };
    am = await listPL.filterCalendarActiveDates.count();
    for (let i = 0; i < am; i++) {
      let t2 = await listPL.filterCalendarActiveDates.get(i).getText();
      if (t2 == dayTo.toString()) {
        await listPL.filterCalendarActiveDates.get(i).click();
        break;
      };
    };
    await listPL.filterFooterButtons.get(1).click();
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.filterSections.get(4)), browser.params.cooldown.medium, `No sections in filter dropdown`);
    await browser.executeScript(`window.scrollTo(0,0)`);
    await listPL.filterSections.get(4).click();
    await spinner.waitUntilSpinnerOff();
    await this.clickFilterDropdown();
  };

  static async setMatrixView() {
    await browser.wait(EC.elementToBeClickable(listPL.viewBtns.get(1)), browser.params.cooldown.medium, `No Matrix View button`);
    await listPL.viewBtns.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setFlatListView() {
    await browser.wait(EC.elementToBeClickable(listPL.viewBtns.get(0)), browser.params.cooldown.medium, `No Flat List View button`);
    await listPL.viewBtns.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickMatrixItem(k = 1) {
    await browser.wait(EC.elementToBeClickable(listPL.matrixItems.get(k - 1)), browser.params.cooldown.medium, `item(${k}) not clickable`);
    await browser.executeScript("arguments[0].scrollIntoView();", listPL.matrix.getWebElement());
    try { await listPL.prevBtn.click(); } catch{ }
    await listPL.matrixItems.get(k - 1).click();
    // try { await listPL.matrixItems.get(k - 1).click(); }
    // catch{
    //   await browser.executeScript("arguments[0].scrollIntoView();", listPL.matrixItems.get(k - 1).getWebElement());
    //   await listPL.matrixItems.get(k - 1).click();
    // }
    await spinner.waitUntilSpinnerOff();
  };

  static async setRecentlyModified() {
    await browser.wait(EC.elementToBeClickable(listPL.recentlyModifiedDropdown), browser.params.cooldown.medium, `No Recenly Modified drop down`);
    await listPL.recentlyModifiedDropdown.click();
    await browser.wait(EC.elementToBeClickable(listPL.recentlyModifiedDropdownItems.get(0)), browser.params.cooldown.medium, `No Recenly Modified option in dropdown list`);
    await listPL.recentlyModifiedDropdownItems.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setRecentlyViewed() {
    await browser.wait(EC.elementToBeClickable(listPL.recentlyModifiedDropdown), browser.params.cooldown.medium, `No Recenly Viewed drop down`);
    await listPL.recentlyModifiedDropdown.click();
    await browser.wait(EC.elementToBeClickable(listPL.recentlyModifiedDropdownItems.get(1)), browser.params.cooldown.medium, `No Recenly Viewed option in dropdown list`);
    await listPL.recentlyModifiedDropdownItems.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clearAllFilters() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.clearAllFiltersLink), browser.params.cooldown.medium, `Clear Filter link isn't clickable`);
    await browser.executeScript(`window.scrollTo(0,200)`);
    await listPL.clearAllFiltersLink.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickSummaryReportLink() {
    await browser.wait(EC.elementToBeClickable(listPL.dashboardDownloadListIcons.get(0)), browser.params.cooldown.medium, `No Summary Report(1) icon`);
    await listPL.dashboardDownloadListIcons.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async addToCompare(rowNum = 1) {
    await browser.wait(EC.elementToBeClickable(listPL.compareCheckboxes.get(rowNum - 1)), browser.params.cooldown.medium, `No Summary Report(1) icon`);
    await listPL.compareCheckboxes.get(rowNum - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickCompareAll() {
    await browser.wait(EC.elementToBeClickable(listPL.compareAllBtn), browser.params.cooldown.medium, `No Compare All button`);
    await listPL.compareAllBtn.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setComparison(sectionN: number, itemN: number) {
    if (sectionN > 2) {
      await spinner.waitUntilSpinnerOff();
      await browser.wait(EC.elementToBeClickable(listPL.comparisonDropdown), browser.params.cooldown.medium, `Comparison dropdown isn't clickable`);
      await listPL.comparisonDropdown.click();
      await browser.wait(EC.elementToBeClickable(listPL.comparisonSections.get(sectionN - 3)), browser.params.cooldown.medium, `Comparison dropdown section(${sectionN}) isn't clickable`);
      await listPL.comparisonSections.get(sectionN - 3).click();

      await browser.wait(EC.elementToBeClickable(listPL.comparisonListItems.get(itemN - 1)), browser.params.cooldown.medium, `Comparison dropdown item(${itemN}) isn't clickable`);
      await listPL.comparisonListItems.get(itemN - 1).click();
      await spinner.waitUntilSpinnerOff();

      await browser.wait(EC.elementToBeClickable(listPL.comparisonDropdown), browser.params.cooldown.medium, `Comparison dropdown isn't clickable`);
      await listPL.comparisonDropdown.click();
      await browser.wait(EC.elementToBeClickable(listPL.comparisonSections.get(sectionN - 3)), browser.params.cooldown.medium, `Comparison dropdown section(${sectionN}) isn't clickable`);
      await listPL.comparisonSections.get(sectionN - 3).click();
      await browser.wait(EC.elementToBeClickable(listPL.comparisonDropdown), browser.params.cooldown.medium, `Comparison dropdown isn't clickable`);
      await listPL.comparisonDropdown.click();
    }
    else {
      await spinner.waitUntilSpinnerOff();
      await browser.wait(EC.elementToBeClickable(listPL.comparisonDropdown), browser.params.cooldown.medium, `Comparison dropdown isn't clickable`);
      await listPL.comparisonDropdown.click();
      await browser.wait(EC.elementToBeClickable(listPL.comparisonEmptySections.get(sectionN - 1)), browser.params.cooldown.medium, `Comparison dropdown section(${sectionN}) isn't clickable`);
      await listPL.comparisonEmptySections.get(sectionN - 1).click();
    };
    await spinner.waitUntilSpinnerOff();
  };

  static async removeProfileFromCompareView(num = 1) {
    await browser.wait(EC.elementToBeClickable(listPL.removeFromCompareView.get(num - 1)), browser.params.cooldown.medium, `Remove button(${num}) isn't clickable`);
    await listPL.removeFromCompareView.get(num - 1).click();
    await browser.wait(EC.elementToBeClickable(listPL.compareRemoveDialogueActions.get(1)), browser.params.cooldown.small, `No Yes button in dialogue`);
    await listPL.compareRemoveDialogueActions.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickRemoveFromCompareList(num = 1) {
    await browser.wait(EC.elementToBeClickable(listPL.listCompareRemoveButtons.get(num - 1)), browser.params.cooldown.small, `No Remove(${num}) button`);
    await listPL.listCompareRemoveButtons.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickBackButton() {
    await browser.wait(EC.elementToBeClickable(listPL.footerButtons.get(0)), browser.params.cooldown.medium, `Back button isn't clickable`);
    await listPL.footerButtons.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setToggleShowDescriptions() {
    await browser.wait(EC.elementToBeClickable(listPL.toggleShowDescription), browser.params.cooldown.medium, `There is no Show Desription toggle`);
    await listPL.toggleShowDescription.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setToggleShowDifferences() {
    await browser.wait(EC.elementToBeClickable(listPL.toggleShowDifference), browser.params.cooldown.medium, `There is no Show Differences toggle`);
    await listPL.toggleShowDifference.click();
    await spinner.waitUntilSpinnerOff();
  };

}

