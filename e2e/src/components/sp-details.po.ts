import { by, element, browser, ExpectedConditions as EC } from 'protractor';
import { By } from 'selenium-webdriver';
import { SuccessProfileDetailsPageLocators as detailsPL } from './sp-details.locators';
import { SpinnerPO as spinner } from './spinner';


export class SuccessProfileDetailsPageObjects {
  static async delay(ms: number) {
    return new Promise((resolve) => { setTimeout(resolve, ms) });
  }

  static async clickSPReportLink() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(detailsPL.reportLinks.get(0)), browser.params.cooldown.medium, `No report link(1)`);
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await detailsPL.reportLinks.get(0).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickDownloadButton() {
    await browser.wait(EC.elementToBeClickable(detailsPL.downloadButtons), browser.params.cooldown.medium, `No download buttons`);
    await detailsPL.downloadButtons.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickSPIGReportLink() {
    await browser.wait(EC.elementToBeClickable(detailsPL.reportLinks.get(1)), browser.params.cooldown.medium, `No report link(2)`);
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await detailsPL.reportLinks.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async getSPReportTitle() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.presenceOf(detailsPL.profileName), browser.params.cooldown.large, `No header`);
    return detailsPL.profileName.getText();
  }

  static async getProfileId(): Promise<string> {
    const url = await browser.getCurrentUrl();
    const parts = url.split('/');
    return parts[parts.length - 1];
  }

  static async clickMagnifier() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(detailsPL.magnifiers.get(0)), browser.params.cooldown.small, `No magnifier`);
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,0)`);
    await detailsPL.magnifiers.get(0).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async openActionsMenu() {
    await spinner.waitUntilSpinnerOff()
    await browser.wait(EC.presenceOf(detailsPL.actionsMenu), browser.params.cooldown.large, `No dropdown with actions`);
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,0)`);
    await detailsPL.actionsMenu.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async openViewMenu() {
    await browser.wait(EC.elementToBeClickable(detailsPL.selectViewMenu), browser.params.cooldown.large, `No Select View menu dropdown`);
    await browser.executeScript(`window.scrollTo(0,0)`);
    await detailsPL.selectViewMenu.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickItemInActions(num: number) {
    await browser.wait(EC.presenceOf(detailsPL.actionMenuItems.get(num - 1)), browser.params.cooldown.small, `No actions in menu`);
    await detailsPL.actionMenuItems.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async openEditPageViaActions() {
    await this.openActionsMenu();
    await this.clickItemInActions(2);
  };

  static async openPMTViaActions() {
    await this.openActionsMenu();
    await this.clickItemInActions(5);
  };

  static async clickClosePMT() {
    await browser.wait(EC.presenceOf(detailsPL.PMTbtns.get(0)), browser.params.cooldown.small, `No Close button`);
    await detailsPL.PMTbtns.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async openSummaryViaActions() {
    await this.openActionsMenu();
    await this.clickItemInActions(1);
  };

  static async createJobDescriptionViaActions() {
    await this.openActionsMenu();
    await this.clickItemInActions(4);
  }

  static async deleteViaActions() {
    await this.openActionsMenu();
    await this.clickItemInActions(3);
    await this.clickSubmitDeleteButton();
  }

  static async clickSubmitDeleteButton() {
    await detailsPL.submitDeleteButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static generateRandomInt(minV: number, maxV: number) {
    return Math.floor(Math.random() * (maxV - minV) + minV);
  }

  static async clickSubmitButton(num: number) {
    const submitButton = await element(by.css(`.action button:nth-child(${num})`));
    await browser.wait(EC.elementToBeClickable(submitButton), browser.params.cooldown.small, `No submit button`);
    await submitButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async moveToTab(tabNo: number) {
    await spinner.waitUntilSpinnerOff();
    const h = element.all(by.css('.second-level div.ng-star-inserted>a')).get(tabNo);
    await browser.wait(EC.presenceOf(h), browser.params.cooldown.medium, `No second level tabs`)
    await h.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async saveProfileAs(optN: number) {
    const saveButton = await element(by.css(`div.right button:nth-child(${optN})`));
    await browser.wait(EC.presenceOf(saveButton), browser.params.cooldown.medium, `No Save button`);

    const confButton = await element(by.css(`footer > button:nth-child(2) > span > span`));
    await confButton.click();

    await spinner.waitUntilSpinnerOff();
    await saveButton.click().then(async () => {
      await spinner.waitUntilSpinnerOff();
      await this.delay(3000);
    });
  }

  static async getTraits() {
    await browser.wait(EC.presenceOf(detailsPL.topTraits.get(0)), browser.params.cooldown.small, `No top traits section`);
    const ot = await detailsPL.topTraits.map((tr) => {
      return {
        trait: tr.element(by.css(`.top-competency-name`)).getText(),
        level: tr.element(by.css(`.top-competency-data`)).getText()
      }
    });
    var t = [];
    await ot.forEach((e, i) => { t.push(`${e['name']}: ${e['level']}`); });
    return t;
  }

  static async getDrivers() {
    await browser.wait(EC.presenceOf(detailsPL.topDrivers.get(0)), browser.params.cooldown.small, `No top drivers section`);
    const ot = await detailsPL.topDrivers.map((tr) => {
      return {
        driver: tr.element(by.css(`.top-competency-name`)).getText(),
        level: tr.element(by.css(`.top-competency-data`)).getText()
      }
    });
    var t = [];
    await ot.forEach((e, i) => { t.push(`${e['name'].trim()}: ${e['level'].trim()}`); });
    return t;
  }

  static async confirmUpdatePMT() {
    await detailsPL.confirmPMTUpdate.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async checkDetailsTopTC(tcs) {
    await spinner.waitUntilSpinnerOff();
    const t = element.all(by.css(`.skills-card kf-chart li`));
    await browser.wait(EC.presenceOf(t.get(3)), browser.params.cooldown.medium, `Details page: there is no top TC section`);
    await t.count().then(async (a) => {
      for (let i = 3; i < a; i++) {
        await expect(
          async () => {
            for (let j = 0; j < tcs.length; j++) {
              if (await t.get(i).getText() == tcs[j]) return true;
            }
            return false;
          }).toBeTruthy(`Expected next set of drivers: ${tcs}. There is no ${await t.get(i).getText()}`);
      }
    });
  }

}