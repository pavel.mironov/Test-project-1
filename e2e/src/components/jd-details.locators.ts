import { by, element } from 'protractor';

export class JobDescriptionDetailsLocators {
  static basedOnLink = element.all(by.css(`span.jd-link a`));
  static actionButtons = element.all(by.css(`.actions .button`));
  static saveDialoguButtons = element.all(by.css(`footer .button`));

  static actionsDropdown = element(by.css(`.dropdown button`));
  static actionsDropdownItems = element.all(by.css(`.dropdown .items.open kf-dropdown-item`));

  static respNames = element.all(by.css(`kf-jd-section-competency:nth-child(3) .section .card .heading input`));
  static respDescriptions = element.all(by.css(`kf-jd-section-competency:nth-child(3) .section .card .description textarea`));
  static bcNames = element.all(by.css(`kf-jd-section-competency:nth-child(4) .section .card .heading input`));
  static bcDescriptions = element.all(by.css(`kf-jd-section-competency:nth-child(4) .section .card .description textarea`));
  static tcNames = element.all(by.css(`kf-jd-section-competency:nth-child(5) .section .card .heading input`));
  static tcDescriptions = element.all(by.css(`kf-jd-section-competency:nth-child(5) .section .card .description textarea`));
  static expNames = element.all(by.css(`kf-jd-section-competency:nth-child(6) .section .card .heading input`));
  static expDescriptions = element.all(by.css(`kf-jd-section-competency:nth-child(6) .section .card .description textarea`));
  static eduNames = element.all(by.css(`kf-jd-section-competency:nth-child(7) .section .card .heading input`));
  static eduDescriptions = element.all(by.css(`kf-jd-section-competency:nth-child(7) .section .card .description textarea`));

  static additionalInfo = element(by.css(`kf-jd-section-competency:nth-child(8) .section .card .description textarea`));

  static title = element(by.css(`h1`));
  static respTitles = element.all(by.css(`kf-jd-section-competency:nth-child(4) .card`));
  static bcTitles = element.all(by.css(`kf-jd-section-competency:nth-child(5) .card`));
  static tcTitles = element.all(by.css(`kf-jd-section-competency:nth-child(6) .card`));
  static expTiles = element.all(by.css(`kf-jd-section-competency:nth-child(7) .card`));
  static edutitles = element.all(by.css(`kf-jd-section-competency:nth-child(8) .card`));

  static editTitle = element(by.css(`.description input`));
}