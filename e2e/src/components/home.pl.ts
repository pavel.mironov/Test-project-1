import { by, element } from 'protractor';

export class HomePageLocators {
  static searchHome = element(by.css(`.search input`));
}