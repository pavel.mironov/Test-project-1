import { by, element } from 'protractor';

export class ContentLibraryLocators {
  static navigateIcons = element.all(by.css(`.global-header .header .icon`));
  static clmIcon = element(by.css(`.global-header .kf-icon-clm`));

  static menuItems = element.all(by.css(`.menu`));

  static addRespButton = element(by.css(`button.add .button`));

  static nameInput = element(by.css(`.name-input input`));
  static categoryDD = element(by.css(`kf-dropdown:not(.category-dropdown) .dropdown button > span`));
  static openedDDItems = element.all(by.css(`.items.align-right.open .item span`));

  static levelDefinitions = element.all(by.css(`textarea`));
  static saveRespButton = element.all(by.css(`.slider-header button .button`));

  static searchResp = element(by.css(`#search input`));

  static respNamesA = element.all(by.css(`kf-collapsible-item:not(.filtered) .name-container > a`));
  static threeDotsMenus = element.all(by.css(`.kf-icon-dots-three-horizontal`));

  static detailFormButton = element.all(by.css(`.kf-slider-header-right .button`));
  static closeIcon = element(by.css(`.slider-header .kf-icon-close`));

  static activeToggles = element.all(by.css(`kf-collapsible-item:not(.filtered) .kf-toggle.selected`));

}