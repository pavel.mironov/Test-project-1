import { by, element } from 'protractor';

export class SuccessProfileLandingPageLocators {
  static editButtons = element.all(by.css(`.edit button`));
  static profileName = element(by.css('.field-container input'));
  static nextButton = element(by.css(`.ui-sidebar-active.ui-sidebar-bottom div.slider-footer button:nth-child(2)`));
  static cancelButton = element(by.css(`.ui-sidebar-active.ui-sidebar-bottom div.slider-footer button:nth-child(1)`));
  static saveButton = element(by.css(`div.right button:nth-child(2)`));
  static saveAsButton = element(by.css(`div.right button:nth-child(1)`));
  static backButton = element(by.css(`div.left button`));
  static deleteRespButtons = element.all(by.css(`.responsibility-editor-container tbody tr > td.delete .icon`));
  static addRespButton = element(by.css(`.responsibility-editor-container .add-competency-button`));
  static listResps = element.all(by.css('#listbox div:not(.disabled) span'));
  static listSearchInput = element(by.css(`.search-input input`));
  static submitAddRespButton = element(by.css(`.ui-sidebar-active div.slider-body button:nth-child(2)`));
  static inputRespNames = element.all(by.css(`.responsibility-editor-container tr > td input`));
  static confirmUpdate = element(by.css(`.slider-footer div.custom-footer-content.slider-content > button:nth-child(2)`));
  static cancelUpdate = element(by.css(`.slider-footer div.custom-footer-content.slider-content > button:nth-child(1)`));

  static deleteTaskButtons = element.all(by.css(`tr > td.delete-row > kf-icon`));
  static submitTaskDelete = element.all(by.css(`footer button:nth-child(2)`));
  static addTaskButton = element(by.css(`.add-task-button`));
  static taskTypeDropdowns = element.all(by.css(`tr .dropdown`));
  static typeDropdownItems = element.all(by.css(`.open kf-dropdown-item`));
  static taskTextFields = element.all(by.css(`tr textarea`));

  static deleteBCButtons = element.all(by.css(`tr td.delete`));
  static addBCButton = element(by.css(`kfthcl-add-custom-competencies .add-competency-button`));
  static bcDropdownItems = element.all(by.css('.ui-sidebar-active #listbox div:not(.disabled)'));
  static submitAddBCButton = element(by.css(`.ui-sidebar-active div.slider-body button:nth-child(2)`));
  static inputBCNames = element.all(by.css(`tr > td input`));

  static deleteTCButtons = element.all(by.css(`tr td.delete kf-icon`));
  static addTCButton = element(by.css(`.add-competency-button`));
  static tcDropdownItems = element.all(by.css('.ui-sidebar-active #listbox div:not(.disabled)'));
  static submitAddTCButton = element(by.css(`.ui-sidebar-active div.slider-body button:nth-child(2)`));
  static inputTCNames = element.all(by.css(`tr > td.competency-col input`));
  static deleteTCSkills = element.all(by.css(`kf-sp-draggable-skills-container  kf-chip > kf-icon`));
  static addTCSkills = element.all(by.css(`.add-skills`));
  static addSkillSearch = element.all(by.css(`.search-input input`));
  static addTCSkillsItems = element.all(by.css(`.items > .item`));
  static confirmAddSkillButtons = element.all(by.css(`.add-custom-skill-actions button`));

  static deleteTechButtons = element.all(by.css(`tr > td.delete-row > kf-icon`));
  static submitDelTechButton = element.all(by.css(`footer button:nth-child(2)`));
  static addTechButton = element(by.css('.add-commo-button'));
  static inputTechNames = element.all(by.css(`.tech-tool input`));
  static addTechExampleButtons = element.all(by.css(`tr span.add-example`));
  static techExampleInputs = element.all(by.css(`.add-example input`));

  static deleteToolButtons = element.all(by.css(`tr > td.delete-row > kf-icon`));
  static submitDelToolButton = element.all(by.css(`footer button:nth-child(2)`));
  static addToolButton = element(by.css(`.add-commo-button`));
  static toolNameInputs = element.all(by.css(`.tech-tool input`));
  static addToolExampleButtons = element.all(by.css(`tr span.add-example`));
  static toolExampleInputs = element.all(by.css(`.add-example input`));

  static delEduButtons = element.all(by.css(`.education-editor-container tr .delete-row`));
  static delExpButtons = element.all(by.css(`.experience-editor-container tr .delete-row`));
  static addEducationButton = element(by.css(`.education-editor-container .add-competency-button`));
  static addExperienceButton = element(by.css(`.experience-editor-container .add-competency-button`));
  static eeListItems = element.all(by.css(`li div.ui-chkbox`));
  static inputEduNames = element.all(by.css(`.education-editor-container tr input`));
  static inputExpNames = element.all(by.css(`.experience-editor-container tr input`));

  static regionDropdown = element(by.css(`.regional-norm-section .dropdown button`));
  static regionDropdownItems = element.all(by.css(`.items.open kf-dropdown-item`));
  static globalRoleCheckbox = element(by.css(`.checkbox`));
  static radioButtons = element.all(by.css(`kf-radio-button .radio`));

  static dropdownItems = element.all(by.css(`.items.open kf-dropdown-item`));
  static dropdownItemNames = element.all(by.css(`.items.open kf-dropdown-item span`));
  static levelDropdowns = element.all(by.css(`.row-wrapper:nth-child(1) form div.sub-container kf-dropdown`));
  static functionDropdowns = element.all(by.css(`.row-wrapper:nth-child(2) form div.sub-container kf-dropdown`));
  static profileDescription = element(by.css(`textarea`));

  static closeNoticeButton = element(by.css(`footer button:nth-child(2)`));
  static confirmUpdateButton = element(by.css(`.slider-footer div.custom-footer-content.slider-content > button:nth-child(2)`));
  static leaveNoticeButton = element.all(by.css(`.ui-dialog-content footer button:nth-child(1)`)).get(1);

  static cultureNames = element.all(by.css(`.draggable-container .competency-text`));

  static sliders = element.all(by.css(`.job-factor-slider input`));
  static inputUCPSliders = element.all(by.css(`.range-stepper-box input`));
  static addUCPButtons = element.all(by.css(`kf-icon.kf-icon-add`));
  static sliderValues = element.all(by.css(`.range-stepper-box div.value`));
  static ucpSliderNames = element.all(by.css(`.range-stepper-box > p.kf-text`));

  static numericalAssesment = element.all(by.css(`.card:nth-child(1) .radio`));
  static verbalAssesment = element.all(by.css(`.card:nth-child(2) .radio`));
  static checkingAssesment = element.all(by.css(`.card:nth-child(3) .radio`));

  static messagePMT = element(by.css(`div.recommendation-bar > span.recommendation-msg`));
  static messagePMTwButton = element(by.css(`div.recommendation-bar > div > span.recommendation-msg`));
  static landingDetails = element.all(by.css(`kfthcl-sp-edit-landing-section:nth-child(1) .summary > div > div.ng-star-inserted`));
  static landingAccountability = element.all(by.css(`kfthcl-sp-edit-landing-section:nth-child(2) .summary > div > div.ng-star-inserted`));
  static landingCapability = element.all(by.css(`kfthcl-sp-edit-landing-section:nth-child(3) .summary > div > div.ng-star-inserted`));
  static landingIdentity = element.all(by.css(`kfthcl-sp-edit-landing-section:nth-child(4) .summary > div > div.ng-star-inserted`));

  static respLVLUPButtons = element.all(by.css(`tbody tr .kf-icon-plus`));
  static respLVLDOWNButtons = element.all(by.css(`tbody tr .kf-icon-minus`));

  static doneSetUCPSlidersButton = element.all(by.css(`.slider-footer button:nth-child(2)`));
  static recomendationsDoneButton = element.all(by.css(`.recommendations .button`));

  static saveConfirmButtons = element.all(by.css(`footer button:nth-child(2)`));


  static wcTitles = element.all(by.css(`.workchar-card-container .title`));
  static wcDescs = element.all(by.css(`.workchar-card-container .description`));
  static wcLevels = element.all(by.css(`.workchar-card-container .level .value`));

  static respNames = element.all(by.css(`.modal-descriptions-title h3`));
  static respLevels = element.all(by.css(`.descriptions-container.active .descriptions-level`));
  static respDescs = element.all(by.css(`.descriptions-container.active .descriptions-description.kf-text`));
  static respCoreActiveFlags = element.all(by.css(`kf-toggle > span.selected`));
  static respCoreInactiveFlags = element.all(by.css(`kf-toggle > span:not(.selected)`));
}