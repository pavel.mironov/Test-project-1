import { by, element } from 'protractor';

export class SuccessProfileListLocators {
  static spinnerByID = element(by.id(`spinner`));
  static spinnerByCSS = element(by.css(`div#spinner`));
  static tabSPList = element(by.css('.second-level>div:nth-child(2) a'));

  static filterButtons = element.all(by.css(`.filters button`));
  static filterValues = element.all(by.css(`.items.open .item`));

  static searchList = element(by.css('kf-meta-search-filter input'));
  static filterTitle = element(by.css('div.filter-title div'));
  static rowNames = element.all(by.css(`tr .name-column a`));
  static rowTR = element.all(by.css(`tr.ng-star-inserted`));
  static threeDotsMenu = element.all(by.css(`tr td.last-column kf-icon`));
  static itemThreeDotsMenu = element.all(by.css(`.items.open kf-dropdown-item`));
  static submitDeleteThreeDots = element.all(by.css(`footer button:nth-child(2)`));

  static listViewicons = element.all(by.css(`.sel-btn`));
  static functionFilterSearch = element(by.css(`.search input`));

  static functionFilterListItems = element.all(by.css(`.item > kf-checkbox > .checkbox`));
  static matrixItems = element.all(by.css(`.subsection-data .item`));
  static matrixField = element.all(by.css(`.subsection-data`));

  static popupData = element.all(by.css(`.popup-data div`));
  static popupLinks = element.all(by.css(`.popup-data a`));

  static compareAllButton = element(by.css(`.compare-btn span.button`));
  static removeFromCompareView = element.all(by.css(`.cell-header .close`));
  static addToCompareView = element.all(by.css(`.empty .add-profile a`));
  static addToCompareLink = element.all(by.css(`.popup-data a`))
  static addRelatedToCompareView = element.all(by.css(`.empty .dropdown button`));
  static listRelatedProfiles = element.all(by.css(`.empty .items.open kf-dropdown-item`));

  static comparisonDropdown = element(by.css(`.comparison-dropdown .clickable`));
  static comparisonSections = element.all(by.css(`.section .section-title`));
  static comparisonListItems = element.all(by.css(`.subitem`));

  static toggles = element.all(by.css(`kf-toggle`));
  static highlighted = element.all(by.css(`.highlighted`));
  static levelDescriptions = element.all(by.css(`.description`));

  static footerButtons = element.all(by.css(`.footer-content button`));

  static listCompareRemoveButtons = element.all(by.css(`.sp-card .kf-icon-close`));
  static listCompareContainer = element.all(by.css(`.bucket-container`));
  static rNavButtons = element.all(by.css(`.r-sticky .nav-button`));
  static lNavButtons = element.all(by.css(`.l-sticky .nav-button`));

  static clearAllFiltersLink = element(by.css(`.applied-filters-well a`));
  static scaleGrades = element.all(by.css(`.l-sticky .y-scale-values .y-label .kf-text`));

  static pmtButtons = element.all(by.css(`footer .button`));

  static compareLeftColumn = element.all(by.css(`.cell-left`));
  static compareProfile1 = element.all(by.css(`tbody tr td:nth-child(2) .cell div.ng-star-inserted`));
  static compareProfile2 = element.all(by.css(`tbody tr td:nth-child(3) .cell div.ng-star-inserted`));
  static compareProfile3 = element.all(by.css(`tbody tr td:nth-child(4) .cell div.ng-star-inserted`));
  static compareProfile4 = element.all(by.css(`tbody tr td:nth-child(5) .cell div.ng-star-inserted`));
  static compareProfile5 = element.all(by.css(`tbody tr td:nth-child(6) .cell div.ng-star-inserted`));

  static selectToCompareProfiles = element.all(by.css(`.compare-table .checkbox`));
  static selectToCompareButtons = element.all(by.css(`.actions .button`));

  static compareListNames = element.all(by.css(`.bucket-container .title a`));

  static matrixDownloadButton = element(by.css(`.download a`));

  static compareEditButton = element(by.css(`.footer-content .right .button`));
  static compareCancelButton = element(by.css(`.footer-content .left .button`));

  static compareEditAddResp = element(by.css(`.add-competency-button span`));
  static compareEditRemoveAllRespButtons = element.all(by.css(`.cell-left a`));
  static compareEditDelRespP1 = element.all(by.css(`tr td:nth-child(2) .cell .close`));
  static compareEditDelRespP2 = element.all(by.css(`tr td:nth-child(3) .cell .close`));
  static compareEditDelRespP3 = element.all(by.css(`tr td:nth-child(4) .cell .close`));
  static compareEditDelRespP4 = element.all(by.css(`tr td:nth-child(5) .cell .close`));
  static compareEditDelRespP5 = element.all(by.css(`tr td:nth-child(6) .cell .close`));

  static notDisabledResps = element.all(by.css(`.add-custom-competencies-listitem:not(.disabled)`));
  static actionButtons = element.all(by.css(`.add-custom-competency-actions .button`));

  static compareEditAddRespButtons = element.all(by.css(`tr td .cell .add a`));

  static compareEditAddRespP1 = element.all(by.css(`tr td:nth-child(2) .cell .add a`));
  static compareEditAddRespP2 = element.all(by.css(`tr td:nth-child(3) .cell .add a`));
  static compareEditAddRespP3 = element.all(by.css(`tr td:nth-child(4) .cell .add a`));
  static compareEditAddRespP4 = element.all(by.css(`tr td:nth-child(5) .cell .add a`));
  static compareEditAddRespP5 = element.all(by.css(`tr td:nth-child(6) .cell .add a`));

  static compareEditPrimaryCheckboxP1 = element.all(by.css(`tr td:nth-child(2) .checkbox`));
  static compareEditPrimaryCheckboxP2 = element.all(by.css(`tr td:nth-child(3) .checkbox`));
  static compareEditPrimaryCheckboxP3 = element.all(by.css(`tr td:nth-child(4) .checkbox`));
  static compareEditPrimaryCheckboxP4 = element.all(by.css(`tr td:nth-child(5) .checkbox`));
  static compareEditPrimaryCheckboxP5 = element.all(by.css(`tr td:nth-child(6) .checkbox`));

  static compareEditLevelDropdownP1 = element.all(by.css(`tr td:nth-child(2) .dropdown`));
  static compareEditLevelDropdownP2 = element.all(by.css(`tr td:nth-child(3) .dropdown`));
  static compareEditLevelDropdownP3 = element.all(by.css(`tr td:nth-child(4) .dropdown`));
  static compareEditLevelDropdownP4 = element.all(by.css(`tr td:nth-child(5) .dropdown`));
  static compareEditLevelDropdownP5 = element.all(by.css(`tr td:nth-child(6) .dropdown`));

  static compareEditLevelTextP1 = element.all(by.css(`tr td:nth-child(2) .dropdown button .kf-text`));
  static compareEditLevelTextP2 = element.all(by.css(`tr td:nth-child(3) .dropdown button .kf-text`));
  static compareEditLevelTextP3 = element.all(by.css(`tr td:nth-child(4) .dropdown button .kf-text`));
  static compareEditLevelTextP4 = element.all(by.css(`tr td:nth-child(5) .dropdown button .kf-text`));
  static compareEditLevelTextP5 = element.all(by.css(`tr td:nth-child(6) .dropdown button .kf-text`));

  static openedDropdownItems = element.all(by.css(`.items.open .item:not(.disabled)`));

  static compareEditRemoveRespP1 = element.all(by.css(`tr td:nth-child(2) .cell .close`));
  static compareEditRemoveRespP2 = element.all(by.css(`tr td:nth-child(3) .cell .close`));
  static compareEditRemoveRespP3 = element.all(by.css(`tr td:nth-child(4) .cell .close`));
  static compareEditRemoveRespP4 = element.all(by.css(`tr td:nth-child(5) .cell .close`));
  static compareEditRemoveRespP5 = element.all(by.css(`tr td:nth-child(6) .cell .close`));

  static comparisonDropdownEdit = element(by.css(`.comparison-dropdown-edit .clickable`));

  static compareViewCopyToEditLinks = element.all(by.css(`.copy-to-edit a`));
  static compareViewNewCopyName = element(by.css(`.profile-copy-dialog input`));
  static compareViewCopyDialogButtons = element.all(by.css(`.ui-dialog-content footer .button`));
  static compareViewActionButtons = element.all(by.css(`.ui-sidebar-active .slider-footer .button`));

  static compareEditRespOCompNames = element.all(by.css(`.cell-left span`));

  static recommendationActions = element.all(by.css(`.recommendation-actions-apply-dismiss a`));
  static recomendDialogActions = element.all(by.css(`.recommendation-modal-footer .button span`));
}