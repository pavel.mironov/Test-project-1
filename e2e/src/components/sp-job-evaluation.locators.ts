import { browser, by, element, ExpectedConditions as EC } from 'protractor';

export class JobEvaluationPageLocators {
  static detailsJELink = element(by.css(`.profile-info .info-row a.je-link`));
  static editJEButton = element(by.css(`.custom-footer-content button:nth-child(2)`));
  static cancelJEButton = element(by.css(`.ui-sidebar-active .footer-left button > span`));
  static saveButton = element.all(by.css(`.ui-sidebar-active .footer-right button > span`));
  static nextButton = element.all(by.css(`.ui-sidebar-active .footer-right button > span`)).get(1);
  static inputTitle = element(by.css(`.save-title input`));

  static submitSaveButton = element(by.css(`.ui-dialog-content button:nth-child(2)`));
  static textareaRationaleKH = element(by.css(`.rationale-section.know-how textarea`));
  static textareaRationalePS = element(by.css(`.rationale-section.problem-solving textarea`));
  static textareaRationaleAC = element(by.css(`.rationale-section.accountability textarea`));

  static tabKH = element.all(by.css(`.detail-tabs .header .info-box-title`)).get(0);
  static tabPS = element.all(by.css(`.detail-tabs .header .info-box-title`)).get(1);
  static tabAC = element.all(by.css(`.detail-tabs .header .info-box-title`)).get(2);
  static rationaleDesc = element(by.css(`.rationaleDescription`));

  static dropdownKHTech = element(by.css(`.know-how-box .info-box:nth-child(1) .info-dropdown`));
  static dropdownKHPlan = element(by.css(`.know-how-box .info-box:nth-child(2) .info-dropdown`));
  static dropdownKHComm = element(by.css(`.know-how-box .info-box:nth-child(3) .info-dropdown`));

  static dropdownPSEnv = element(by.css(`.problem-solving-box .info-box:nth-child(1) .info-dropdown`));
  static dropdownPSChal = element(by.css(`.problem-solving-box .info-box:nth-child(2) .info-dropdown`));

  static dropdownACFree = element(by.css(`.accountability-box .info-box:nth-child(1) .info-dropdown`));
  static dropdownACMagn = element(by.css(`.accountability-box .info-box:nth-child(2) .info-dropdown`));
  static dropdownACNatu = element(by.css(`.accountability-box .info-box:nth-child(3) .info-dropdown`));

  static openedDropdownLvls = element.all(by.css(`div.option .level-btn span`));
  static openedDropdownDescT = element.all(by.css(`div.option .level-description .name :not(.badge)`));
  static openedDropdownDescD = element.all(by.css(`div.option .level-description p`));
  static openedDropdownInfo = element.all(by.css(`.dimension-description span`));

  static tabHeaderContentKH = element.all(by.css(`.headers .header:nth-child(1) .info-box`));
  static tabHeaderContentPS = element.all(by.css(`.headers .header:nth-child(2) .info-box`));
  static tabHeaderContentAC = element.all(by.css(`.headers .header:nth-child(3) .info-box`));

  static tabCompLevelStats = element.all(by.css(`.competency-level .competency-level-stat`));
  static tabCompLevelTitle = element.all(by.css(`.competency-level .competency-description span`));
  static tabCompLevelDescription = element.all(by.css(`.competency-level .competency-description p`));
  static tabCompLevelName = element.all(by.css(`.competency-level .competency-level-name`));

  static textCharts = element.all(by.css(`.data-viz kf-progress-half-donut h4`));
  static editTextCharts = element.all(by.css(`.edit-job-component .data-viz kf-progress-half-donut h4`));
  static jeScoresKH = element.all(by.css(`.know-how-box .info-box-container .info-box`));
  static jeScoresKH4 = element(by.css(`.know-how-box .info-box-view`));
  static jeScoresPS = element.all(by.css(`.problem-solving-box .info-box-container .info-box`));
  static jeScoresPS4 = element(by.css(`.problem-solving-box .info-box-view`));
  static jeScoresAC = element.all(by.css(`.accountability-box .info-box-container .info-box`));
  static jeScoresAC4 = element(by.css(`.accountability-box .info-box-view`));

}