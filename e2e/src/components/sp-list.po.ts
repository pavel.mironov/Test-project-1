import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { SuccessProfileListLocators as listPL } from './sp-list.locators';
import { SpinnerPO as spinner } from './spinner';


export class SuccessProfileListPageObjects {

  static async searchProfile(text: string) {
    await browser.wait(EC.presenceOf(listPL.searchList), browser.params.cooldown.medium, 'No search field');
    await listPL.searchList.clear();
    await listPL.searchList.sendKeys(text, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  }

  static async clearSearch() {
    await browser.wait(EC.presenceOf(listPL.searchList), browser.params.cooldown.medium, 'No search field');
    await listPL.searchList.clear();
    await listPL.searchList.sendKeys(`a`, protractor.Key.BACK_SPACE, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  }

  static async clickFilter(filterNo: number) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(await listPL.filterButtons.get(filterNo - 1)), browser.params.cooldown.medium, `Can't click filter(${filterNo}) button`);
    await browser.executeScript(`window.scrollTo(0,0)`);
    await listPL.filterButtons.get(filterNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickItemInFilter(valueNo: number) {
    await browser.wait(EC.elementToBeClickable(listPL.filterValues.get(valueNo - 1)), browser.params.cooldown.medium, `Can't click filter value(${valueNo})`);
    await listPL.filterValues.get(valueNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async resetFilter() {
    await element(by.css('a.utility')).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setFilter(filterNo: number, valueNo: number) {
    await this.clickFilter(filterNo);
    await spinner.waitUntilSpinnerOff();
    await this.clickItemInFilter(valueNo);
    await spinner.waitUntilSpinnerOff();
    await this.clickFilter(filterNo);
    await spinner.waitUntilSpinnerOff();
  }

  static async setFunctionFilter(filterNo: number, text: string) {
    await this.clickFilter(filterNo);
    await spinner.waitUntilSpinnerOff();
    await this.setFunctionFilterSearch(text);
    await this.setFunctionFilterCheckbox();
    await spinner.waitUntilSpinnerOff();
    await this.clickFilter(filterNo);
    await spinner.waitUntilSpinnerOff();
  }

  static async getCount() {
    await spinner.waitUntilSpinnerOff();
    try {
      const v = await listPL.filterTitle.getText().then(async (text: string) => { return text; });
      const vv = v.split(' ');
      return parseInt(vv[3], 10);
    }
    catch{
      return 0;
    }
  }

  static async clicksubmitDeleteViaThreeDotsMenu() {
    await browser.wait(EC.elementToBeClickable(listPL.submitDeleteThreeDots.get(0)), browser.params.cooldown.large, `No dialogue with submit button`);
    await listPL.submitDeleteThreeDots.get(0).click();
  }

  static async deleteSPViaThreeDotsMenu() {
    await spinner.waitUntilSpinnerOff();
    await this.openThreeDotsMenu(1);
    await this.clickItemThreeDotsMenu(4);
    await this.clicksubmitDeleteViaThreeDotsMenu();
    await spinner.waitUntilSpinnerOff();
  }

  static async openSPFromListByNo(rowNo = 1) {
    await browser.wait(EC.elementToBeClickable(listPL.rowTR.get(0)), browser.params.cooldown.large, `There is empty list but shouldn't`);
    let rowH = await listPL.rowTR.get(0).getSize();
    let m = 513;    // document.body.scrollHeight
    let cap = rowNo / 20 | 0;
    for (let i = 0; i < cap + 1; i++) {
      await browser.wait(EC.presenceOf(listPL.rowNames.get(i * 20 - 1)), browser.params.cooldown.large, `Infinite scroll fail 1`);
      await browser.executeScript(`window.scrollTo(0,${m + i * 20 * rowH.height + 240})`);
      await spinner.delay(1000);
      await spinner.waitUntilSpinnerOff();
    }

    await browser.wait(EC.elementToBeClickable(listPL.rowNames.get(rowNo - 1)), browser.params.cooldown.large, `Fail to wait row with num ${rowNo}`);
    await browser.executeScript(`window.scrollTo(0,${m + rowNo * rowH.height})`);
    await spinner.delay(2000);
    await spinner.waitUntilSpinnerOff();
    await listPL.rowNames.get(rowNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async copySPFromListByName(name: string) {
    var i = 1;
    while (1) {
      if (await listPL.rowNames.get(i - 1).getText().then((text: string) => { return text; }) == name) {
        break;
      } else {
        i++;
      }
    }
    await this.copyProfileViaThreeDotsMenu(i);
  }

  static async openSPFromListByName(name: string) {
    var i = 1;
    while (1) {
      if (await listPL.rowNames.get(i - 1).getText().then((text: string) => { return text; }) == name) {
        break;
      } else {
        i++;
      }
    }
    await this.openSPFromListByNo(i);
  }

  static async openProfileByID(id: number) {
    await spinner.waitUntilSpinnerOff();
    await browser.get(`/app/tarc/#/tarc/sp/detail/${id}`);
    await spinner.waitUntilSpinnerOff();
  }

  static async openThreeDotsMenu(rowNo: number) {
    await browser.wait(EC.elementToBeClickable(listPL.threeDotsMenu.get(rowNo - 1)), browser.params.cooldown.medium, `Can't find tree dots menu`);
    await listPL.threeDotsMenu.get(rowNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickItemThreeDotsMenu(rowNo: number) {
    await browser.wait(EC.elementToBeClickable(listPL.itemThreeDotsMenu.get(rowNo - 1)), browser.params.cooldown.medium, `No dropdown for three dots menu`);
    await listPL.itemThreeDotsMenu.get(rowNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async copyProfileViaThreeDotsMenu(rowNo = 1) {
    await this.openThreeDotsMenu(rowNo);
    await this.clickItemThreeDotsMenu(1);
  }

  static async setMatrixView() {
    await browser.wait(EC.elementToBeClickable(listPL.listViewicons.get(1)), browser.params.cooldown.medium, `Can't change view: no icon`);
    await browser.executeScript(`window.scrollTo(0,0)`);
    await listPL.listViewicons.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setListView() {
    await browser.wait(EC.elementToBeClickable(listPL.listViewicons.get(0)), browser.params.cooldown.medium, `Can't change view: no icon`);
    await listPL.listViewicons.get(0).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setFunctionFilterSearch(text: string) {
    await browser.wait(EC.elementToBeClickable(listPL.functionFilterSearch), browser.params.cooldown.medium, `No search field in Function filter`);
    await listPL.functionFilterSearch.clear();
    await listPL.functionFilterSearch.sendKeys(text, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  }

  static async setFunctionFilterCheckbox() {
    await browser.wait(EC.elementToBeClickable(listPL.functionFilterListItems.get(0)), browser.params.cooldown.medium, `No checkboxes in Function filter list`);
    await listPL.functionFilterListItems.get(0).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async openItemPopup(num: number) {
    await browser.wait(EC.presenceOf(listPL.matrixItems.get(num - 1)), browser.params.cooldown.medium, `There is no item in matrix with num ${num}`);
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await listPL.matrixItems.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setCompareCheckbox() {
    await browser.wait(EC.elementToBeClickable(listPL.popupData.get(6)), browser.params.cooldown.small, `No compare checkbox in popup`);
    try {
      await listPL.popupData.get(6).click();
    }
    catch{
      await browser.executeScript(`window.scrollTo(0,400)`);
      await listPL.popupData.get(6).click();
    }
  }

  static async clickCompareLink() {
    await browser.wait(EC.elementToBeClickable(listPL.addToCompareLink.get(1)), browser.params.cooldown.small, `No compare link in popup`);
    try {
      await listPL.addToCompareLink.get(1).click();
    }
    catch{
      await browser.executeScript(`window.scrollTo(0,400)`);
      await listPL.addToCompareLink.get(1).click();
    }
  }

  static async closeItemPopup() {
    await spinner.delay(2500);
    await listPL.matrixField.get(0).click();
  }

  static async clickCompareAllButton() {
    await browser.wait(EC.elementToBeClickable(listPL.compareAllButton), browser.params.cooldown.small, `No Compare All button`);
    await listPL.compareAllButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickCompareEditButton() {
    await browser.wait(EC.elementToBeClickable(listPL.compareEditButton), browser.params.cooldown.small, `Compare edit button isn't clickable`);
    await listPL.compareEditButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickCopyToEditLink(num = 0) {
    await browser.wait(EC.elementToBeClickable(listPL.compareViewCopyToEditLinks.get(0)), browser.params.cooldown.small, `Copy to Edit(${num}) link isn't clickable`);
    await listPL.compareViewCopyToEditLinks.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setCopyName(spName: string) {
    await browser.wait(EC.elementToBeClickable(listPL.compareViewNewCopyName), browser.params.cooldown.small, `There is no input for new copy name`);
    await listPL.compareViewNewCopyName.clear();
    await listPL.compareViewNewCopyName.sendKeys(spName);
    await spinner.waitUntilSpinnerOff();
  }

  static async clickCreateCopy() {
    await browser.wait(EC.elementToBeClickable(listPL.compareViewCopyDialogButtons.get(1)), browser.params.cooldown.small, `There is no action buttons`);
    await listPL.compareViewCopyDialogButtons.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickSaveCompareView() {
    await browser.wait(EC.elementToBeClickable(listPL.compareViewActionButtons.get(1)), browser.params.cooldown.small, `There is no save button`);
    await listPL.compareViewActionButtons.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickContinueNSave() {
    await browser.wait(EC.elementToBeClickable(listPL.recomendDialogActions.get(1)), browser.params.cooldown.medium, `There is no Continue and Save button`);
    await listPL.recomendDialogActions.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async removeProfileFromCompareView(num: number) {
    await browser.wait(EC.elementToBeClickable(listPL.removeFromCompareView.get(num - 1)), browser.params.cooldown.medium, `Remove button(${num}) isn't clickable`);
    await spinner.waitUntilSpinnerOff();
    await listPL.removeFromCompareView.get(num - 1).click();
  }

  static async removeAllProfileFromCompareView() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.removeFromCompareView.get(0)), browser.params.cooldown.medium, `There is no remove buttons`);    
    await listPL.removeFromCompareView.count().then(async am => {
      for (let i = 0; i < am; i++) {
        await listPL.removeFromCompareView.get(0).click();
      }
    });


  }

  static async clickAddNewProfile() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.addToCompareView.get(0)), browser.params.cooldown.small, `No Add New Profile link`);
    await listPL.addToCompareView.get(0).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async addToCompareCustom(num: number) {
    await this.openItemPopup(num);
    await this.setCompareCheckbox();
    await this.setMatrixView(); // await this.closeItemPopup();
  }

  static async addToCompare(num: number) {
    await this.openItemPopup(num);
    await this.clickCompareLink();
    await spinner.waitUntilSpinnerOff();
    await this.clickToCompareCheckbox(1);
    await this.confirmToCompare();
    await this.setMatrixView(); // await this.closeItemPopup();
  }

  static async createCopyviaPMT(num: number) {
    await this.openItemPopup(num);
    await browser.wait(EC.elementToBeClickable(listPL.popupLinks.get(0)), browser.params.cooldown.medium, `PMT link isn't clickable`);
    await listPL.popupLinks.get(0).click();
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.pmtButtons.get(1)), browser.params.cooldown.medium, `PMT Button isn't clickable`);
    await listPL.pmtButtons.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickToCompareCheckbox(num: number) {
    await browser.wait(EC.elementToBeClickable(listPL.selectToCompareProfiles.get(num - 1)), browser.params.cooldown.small, `No checkboxes in list`);
    await listPL.selectToCompareProfiles.get(num - 1).click();
  }

  static async confirmToCompare() {
    await browser.wait(EC.elementToBeClickable(listPL.selectToCompareButtons.get(1)), browser.params.cooldown.small, `No Confirm button`);
    await listPL.selectToCompareButtons.get(1).click();
  }

  static async clickRemoveFromCompareList(num: number) {
    await browser.wait(EC.elementToBeClickable(listPL.listCompareRemoveButtons.get(num - 1)), browser.params.cooldown.small, `No Remove(${num}) button`);
    await listPL.listCompareRemoveButtons.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async removeAllFromCompareList() {
    await spinner.waitUntilSpinnerOff();
    await listPL.listCompareRemoveButtons.count().then(async am => {
      for (let i = 0; i < am; i++) {
        await listPL.listCompareRemoveButtons.get(0).click();
      }
    });
  }

  static async addRelatedToCompare(num: number) {
    await browser.wait(EC.elementToBeClickable(listPL.addRelatedToCompareView.get(0)), browser.params.cooldown.medium, `Add related button isn't clickable`);
    await spinner.waitUntilSpinnerOff();
    await listPL.addRelatedToCompareView.get(0).click();
    await listPL.listRelatedProfiles.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setComparison(sectionN: number, itemN: number) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.comparisonDropdown), browser.params.cooldown.medium, `Comparison dropdown isn't clickable`);
    await listPL.comparisonDropdown.click();
    await listPL.comparisonSections.get(sectionN - 1).click();
    await listPL.comparisonListItems.get(itemN - 1).click();
    await spinner.waitUntilSpinnerOff();
    await listPL.comparisonDropdown.click();
    await listPL.comparisonSections.get(sectionN - 1).click();
    await listPL.comparisonDropdown.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setComparisonEditPage(sectionN: number, itemN: number) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.comparisonDropdownEdit), browser.params.cooldown.medium, `Comparison dropdown isn't clickable`);
    await listPL.comparisonDropdownEdit.click();
    await listPL.comparisonSections.get(sectionN - 1).click();
    await listPL.comparisonListItems.get(itemN - 1).click();
    await spinner.waitUntilSpinnerOff();
    await listPL.comparisonDropdownEdit.click();
    await listPL.comparisonSections.get(sectionN - 1).click();
    await listPL.comparisonDropdownEdit.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickBackButton() {
    await browser.wait(EC.elementToBeClickable(listPL.footerButtons.get(0)), browser.params.cooldown.medium, `Back button isn't clickable`);
    await listPL.footerButtons.get(0).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clearAllFilters() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(listPL.clearAllFiltersLink), browser.params.cooldown.medium, `Clear Filter link isn't clickable`);
    await browser.executeScript(`window.scrollTo(0,200)`);
    await listPL.clearAllFiltersLink.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clearAllFiltersF() {
    try {
      await spinner.waitUntilSpinnerOff();
      await browser.wait(EC.elementToBeClickable(listPL.clearAllFiltersLink), 5 * 1000, `Clear Filter link isn't clickable`);
      await browser.executeScript(`window.scrollTo(0,0)`);
      await listPL.clearAllFiltersLink.click();
    }
    catch{ }
  }

  static async compareEditRemoveAllRespOComp() {
    await browser.wait(EC.elementToBeClickable(listPL.compareEditRemoveAllRespButtons.get(0)), browser.params.cooldown.medium, `Remove All link for resp() isn't clickable`);
    await listPL.compareEditRemoveAllRespButtons.count().then(async a => {
      while (await listPL.compareEditRemoveAllRespButtons.count() > 0) {
        await browser.executeScript(`window.scrollTo(0,0)`);
        await listPL.compareEditRemoveAllRespButtons.get(0).click();
      }
    });
    await spinner.waitUntilSpinnerOff();
  };

  static async compareEditAddRespOComp(am = 1) {
    for (let i = 0; i < am; i++) {
      await browser.wait(EC.elementToBeClickable(listPL.compareEditAddResp), browser.params.cooldown.medium, `Add Responsibilities button isn't clickable`);
      await listPL.compareEditAddResp.click();
      await spinner.waitUntilSpinnerOff();
      await browser.wait(EC.elementToBeClickable(listPL.notDisabledResps.get(3)), browser.params.cooldown.medium, `No responsibilities(not disabled) in add list`);
      await listPL.notDisabledResps.get(3).click();
      await listPL.actionButtons.get(1).click();
      await spinner.waitUntilSpinnerOff();

      await listPL.compareEditAddRespButtons.count().then(async am => {
        for (let k = 0; k < am; k++) {
          await browser.wait(EC.elementToBeClickable(listPL.compareEditAddRespButtons.get(0)), browser.params.cooldown.small, `Add(+) button isn't clickable`);
          await listPL.compareEditAddRespButtons.get(0).click();
        }
        await spinner.waitUntilSpinnerOff();
      });
    }
  };

  static async compareEditRespOComp(spNum: Number, respCompNum: number, primaryCheckbox: boolean, lvlInc: number) {
    if (spNum == 1) {
      if (primaryCheckbox == true) {
        await browser.wait(EC.elementToBeClickable(listPL.compareEditPrimaryCheckboxP1.get(respCompNum - 1)), browser.params.cooldown.medium, `Primary checkbox(${respCompNum}) isn't clickable`);
        await listPL.compareEditPrimaryCheckboxP1.get(respCompNum - 1).click();
      }
      if (lvlInc > 0) {
        await browser.wait(EC.elementToBeClickable(listPL.compareEditLevelDropdownP1.get(respCompNum - 1)), browser.params.cooldown.medium, `Level dropdown(${respCompNum}) isn't clickable`);
        await listPL.compareEditLevelDropdownP1.get(respCompNum - 1).click();
      }
    }
    if (spNum == 2) {
      if (primaryCheckbox == true) {
        await browser.wait(EC.elementToBeClickable(listPL.compareEditPrimaryCheckboxP2.get(respCompNum - 1)), browser.params.cooldown.medium, `Primary checkbox(${respCompNum }) isn't clickable`);
        await listPL.compareEditPrimaryCheckboxP2.get(respCompNum - 1).click();
      }
      if (lvlInc > 0) {
        await browser.wait(EC.elementToBeClickable(listPL.compareEditLevelDropdownP2.get(respCompNum - 1)), browser.params.cooldown.medium, `Level dropdown(${respCompNum}) isn't clickable`);
        await listPL.compareEditLevelDropdownP2.get(respCompNum - 1).click();
      }
    }
    if (spNum == 3) {
      if (primaryCheckbox == true) {
        await browser.wait(EC.elementToBeClickable(listPL.compareEditPrimaryCheckboxP3.get(respCompNum - 1)), browser.params.cooldown.medium, `Primary checkbox(${respCompNum }) isn't clickable`);
        await listPL.compareEditPrimaryCheckboxP3.get(respCompNum - 1).click();
      }
      if (lvlInc > 0) {
        await browser.wait(EC.elementToBeClickable(listPL.compareEditLevelDropdownP3.get(respCompNum - 1)), browser.params.cooldown.medium, `Level dropdown(${respCompNum }) isn't clickable`);
        await listPL.compareEditLevelDropdownP3.get(respCompNum - 1).click();
      }
    }
    if (spNum == 4) {
      if (primaryCheckbox == true) {
        await browser.wait(EC.elementToBeClickable(listPL.compareEditPrimaryCheckboxP4.get(respCompNum - 1)), browser.params.cooldown.medium, `Primary checkbox(${respCompNum }) isn't clickable`);
        await listPL.compareEditPrimaryCheckboxP4.get(respCompNum - 1).click();
      }
      if (lvlInc > 0) {
        await browser.wait(EC.elementToBeClickable(listPL.compareEditLevelDropdownP4.get(respCompNum - 1)), browser.params.cooldown.medium, `Level dropdown(${respCompNum }) isn't clickable`);
        await listPL.compareEditLevelDropdownP4.get(respCompNum - 1).click();
      }
    }
    if (spNum == 5) {
      if (primaryCheckbox == true) {
        await browser.wait(EC.elementToBeClickable(listPL.compareEditPrimaryCheckboxP5.get(respCompNum - 1)), browser.params.cooldown.medium, `Primary checkbox(${respCompNum }) isn't clickable`);
        await listPL.compareEditPrimaryCheckboxP5.get(respCompNum - 1).click();
      }
      if (lvlInc > 0) {
        await browser.wait(EC.elementToBeClickable(listPL.compareEditLevelDropdownP5.get(respCompNum - 1)), browser.params.cooldown.medium, `Level dropdown(${respCompNum }) isn't clickable`);
        await listPL.compareEditLevelDropdownP5.get(respCompNum - 1).click();
      }
    }

    if (lvlInc > 0) {
      await browser.wait(EC.elementToBeClickable(listPL.openedDropdownItems.get(lvlInc - 1)), browser.params.cooldown.medium, `No opened dropdown`);
      await listPL.openedDropdownItems.get(lvlInc - 1).click();
    }
  };

  static async compareEditClickRemoveRespOComp(spNum: number, respNum: number) {
    if (spNum == 1) {
      await browser.wait(EC.elementToBeClickable(listPL.compareEditDelRespP1.get(respNum - 1)), browser.params.cooldown.medium, `delete button for resp(${respNum}) isn't clickable`);
      await listPL.compareEditDelRespP1.get(respNum - 1).click();
      await spinner.waitUntilSpinnerOff();
    }
    if (spNum == 2) {
      await browser.wait(EC.elementToBeClickable(listPL.compareEditDelRespP2.get(respNum - 1)), browser.params.cooldown.medium, `delete button for resp(${respNum}) isn't clickable`);
      await listPL.compareEditDelRespP2.get(respNum - 1).click();
      await spinner.waitUntilSpinnerOff();
    }
    if (spNum == 3) {
      await browser.wait(EC.elementToBeClickable(listPL.compareEditDelRespP3.get(respNum - 1)), browser.params.cooldown.medium, `delete button for resp(${respNum}) isn't clickable`);
      await listPL.compareEditDelRespP3.get(respNum - 1).click();
      await spinner.waitUntilSpinnerOff();
    }
    if (spNum == 4) {
      await browser.wait(EC.elementToBeClickable(listPL.compareEditDelRespP4.get(respNum - 1)), browser.params.cooldown.medium, `delete button for resp(${respNum}) isn't clickable`);
      await listPL.compareEditDelRespP4.get(respNum - 1).click();
      await spinner.waitUntilSpinnerOff();
    }
    if (spNum == 5) {
      await browser.wait(EC.elementToBeClickable(listPL.compareEditDelRespP5.get(respNum - 1)), browser.params.cooldown.medium, `delete button for resp(${respNum}) isn't clickable`);
      await listPL.compareEditDelRespP5.get(respNum - 1).click();
      await spinner.waitUntilSpinnerOff();
    }

  };

  static async dismissAllRecommendations() {
    try {
      await browser.wait(EC.elementToBeClickable(listPL.recommendationActions.get(1)), browser.params.cooldown.medium, `There is no Dismiss All link`);
      await listPL.recommendationActions.get(1).click();
      await spinner.waitUntilSpinnerOff();
    }
    catch{ }
  };

}

