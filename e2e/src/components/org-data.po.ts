import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { OrgDataLocators as dataPL } from './org-data.locators';
import { SpinnerPO as spinner } from './spinner';

export class OrgDataPageObject {
  static async clickAddSubOrg() {
    await browser.wait(EC.presenceOf(dataPL.addSubOrgBtn), browser.params.cooldown.small, `There is no Add Subsidiary Organization link`);
    await dataPL.addSubOrgBtn.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setSubName(name = ``) {
    await browser.wait(EC.presenceOf(dataPL.subOrgName), browser.params.cooldown.small, `There is no Subsidiary Name input`);
    await dataPL.subOrgName.clear();
    await dataPL.subOrgName.sendKeys(name);
  };

  static async clickSubCurrencyDD() {
    await browser.wait(EC.presenceOf(dataPL.currencyDropdown), browser.params.cooldown.small, `There is no Currency dropdown`);
    await dataPL.currencyDropdown.click();
  };

  static async randomFromList(am) {
    return Math.floor(Math.random() * (am - 1) + 1);
  };

  static async setSubCurrencyR() {
    await this.clickSubCurrencyDD();
    await this.clickOpenDDItem(await this.randomFromList(await dataPL.openedDDItems.count()));
  }

  static async setSubCurrencyByTitle(title = ``) {
    await this.clickSubCurrencyDD();
    let i = 1;
    while (1) {
      if (await dataPL.openedDDItems.get(i - 1).getText().then((text: string) => { return text; }) == title) {
        await this.clickOpenDDItem(i);
        break;
      } else {
        i++;
        if (i > await dataPL.openedDDItems.count()) {
          await expect(false).toBeTruthy(`There is no Currency(${title}) in list`);
          break;
        }
      }
    }
  }

  static async setSubTotalRevenue(revenueN = 0) {
    await browser.wait(EC.presenceOf(dataPL.revenueInput), browser.params.cooldown.small, `There is no Total Revenue input`);
    await dataPL.revenueInput.clear();
    await dataPL.revenueInput.sendKeys(revenueN);
  };

  static async setSubTotalEmployees(employeeN = 0) {
    await browser.wait(EC.presenceOf(dataPL.employeesInput), browser.params.cooldown.small, `There is no Total Employee input`);
    await dataPL.employeesInput.clear();
    await dataPL.employeesInput.sendKeys(employeeN);
  };

  static async clickBusinessScopeDD() {
    await browser.wait(EC.presenceOf(dataPL.scopeDropdown), browser.params.cooldown.small, `There is no Business Scope dropdown`);
    await dataPL.scopeDropdown.click();
  };

  static async setSubBusinessScopeR() {
    await this.clickBusinessScopeDD();
    await this.clickOpenDDItem(await this.randomFromList(await dataPL.openedDDItems.count()));
  };

  static async clickSubGovernanceDD() {
    await browser.wait(EC.presenceOf(dataPL.governanceDropdown), browser.params.cooldown.small, `There is no Governance dropdown`);
    await dataPL.governanceDropdown.click();
  };

  static async clickOpenDDItem(numInList = 1) {
    await browser.wait(EC.presenceOf(dataPL.openedDDItems.get(numInList - 1)), browser.params.cooldown.small, `There is no Item(${numInList}) in opened dropdown`);
    await dataPL.openedDDItems.get(numInList - 1).click();
    // await spinner.waitUntilSpinnerOff();
  };

  static async setSubGovernanceR() {
    await this.clickSubGovernanceDD();
    await this.clickOpenDDItem(await this.randomFromList(await dataPL.openedDDItems.count()));
  };

  static async clickIndustryDD() {
    await browser.wait(EC.presenceOf(dataPL.industryDropdown), browser.params.cooldown.small, `There is no Industry dropdown`);
    await dataPL.industryDropdown.click();
  };

  static async setSubIndustryR() {
    await this.clickIndustryDD();
    await this.clickOpenDDItem(await this.randomFromList(await dataPL.openedDDItems.count()));
  };

  static async setSubIndustryByTitle(title = ``) {
    await this.clickIndustryDD();
    let i = 1;
    while (1) {
      if (await dataPL.openedDDItems.get(i - 1).getText().then((text: string) => { return text; }) == title) {
        await this.clickOpenDDItem(i);
        break;
      } else {
        i++;
        if (i > await dataPL.openedDDItems.count()) {
          await expect(false).toBeTruthy(`There is no Industry(${title}) in list`);
          break;
        }
      }
    }
  };

  static async clickSegmentDD() {
    await browser.wait(EC.presenceOf(dataPL.segmentDropdown), browser.params.cooldown.small, `There is no Segment dropdown`);
    await dataPL.segmentDropdown.click();
  };

  static async setSubSegmentR() {
    await this.clickSegmentDD();
    await this.clickOpenDDItem(await this.randomFromList(await dataPL.openedDDItems.count()));
  };

  static async setSubSegmentByTitle(title = ``) {
    await this.clickSegmentDD();
    let i = 1;
    while (1) {
      if (await dataPL.openedDDItems.get(i - 1).getText().then((text: string) => { return text; }) == title) {
        await this.clickOpenDDItem(i);
        break;
      } else {
        i++;
        if (i > await dataPL.openedDDItems.count()) {
          await expect(false).toBeTruthy(`There is no Segment(${title}) in list`);
          break;
        }
      }
    }
  };

  static async clickRevenueInternalDD() {
    await browser.wait(EC.presenceOf(dataPL.revenueDropdown), browser.params.cooldown.small, `There is no Revenue from Internal Operations dropdown`);
    await dataPL.revenueDropdown.click();
  };

  static async setSubRevenueInternalR() {
    await this.clickRevenueInternalDD();
    await this.clickOpenDDItem(await this.randomFromList(await dataPL.openedDDItems.count()));
  };

  static async setSubRevenueInternalByTitle(title = ``) {
    await this.clickRevenueInternalDD();
    let i = 1;
    while (1) {
      if (await dataPL.openedDDItems.get(i - 1).getText().then((text: string) => { return text; }) == title) {
        await this.clickOpenDDItem(i);
        break;
      } else {
        i++;
        if (i > await dataPL.openedDDItems.count()) {
          await expect(false).toBeTruthy(`There is no Revenue(${title}) in list`);
          break;
        }
      }
    }
  };

  static async clickAddCountryBtn() {
    await browser.wait(EC.presenceOf(dataPL.addCountryBtn), browser.params.cooldown.small, `There is no Add Country button`);
    await dataPL.addCountryBtn.click();
  };

  static async clickDeleteCountryBtn(numInList = 1) {
    await browser.wait(EC.presenceOf(dataPL.deleteCountryBtns.get(numInList - 1)), browser.params.cooldown.small, `There is no Delete(x) button for Country(${numInList})`);
    await dataPL.deleteCountryBtns.get(numInList - 1).click();
  };

  static async clickCountryNameDD(countryN = 1) {
    await browser.wait(EC.presenceOf(dataPL.countryNameDDs.get(countryN - 1)), browser.params.cooldown.small, `There is no Country(${countryN}) dropdown`);
    await dataPL.countryNameDDs.get(countryN - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setCountryNameR(countryN = 1) {
    await this.clickCountryNameDD(countryN);
    await this.clickOpenDDItem(await this.randomFromList(await dataPL.openedDDItems.count()));
    await spinner.waitUntilSpinnerOff();
  };

  static async clickCountryMapDD(countryN = 1) {
    await browser.wait(EC.presenceOf(dataPL.countryMapDDs.get(countryN - 1)), browser.params.cooldown.small, `There is no Country(${countryN}) Map dropdown`);
    await dataPL.countryMapDDs.get(countryN - 1).click();
  };

  static async setCountryMapR(countryN = 1) {
    await this.clickCountryMapDD(countryN);
    await this.clickOpenDDItem(await this.randomFromList(await dataPL.openedDDItems.count()));
  };

  static async clickCountryCurrencyDD(countryN = 1) {
    await browser.wait(EC.presenceOf(dataPL.countryCurrencyDDs.get(countryN - 1)), browser.params.cooldown.small, `There is no Country(${countryN}) Currency dropdown`);
    await dataPL.countryCurrencyDDs.get(countryN - 1).click();
  };

  static async setCountryCurrencyR(countryN = 1) {
    await this.clickCountryCurrencyDD(countryN);
    await this.clickOpenDDItem(await this.randomFromList(await dataPL.openedDDItems.count()));
  };

  static async setCountryRevenue(countryN = 1, amount = 0) {
    await browser.wait(EC.presenceOf(dataPL.countryRevenueInputs.get(countryN - 1)), browser.params.cooldown.small, `There is no Country(${countryN}) Revenue input`);
    await dataPL.countryRevenueInputs.get(countryN - 1).clear();
    await dataPL.countryRevenueInputs.get(countryN - 1).sendKeys(amount);
  };

  static async setCountryHeadcount(countryN = 1, amount = 0) {
    await browser.wait(EC.presenceOf(dataPL.countryHeadcountInputs.get(countryN - 1)), browser.params.cooldown.small, `There is no Country(${countryN}) Headcount input`);
    await dataPL.countryHeadcountInputs.get(countryN - 1).clear();
    await dataPL.countryHeadcountInputs.get(countryN - 1).sendKeys(amount);
  };

  static async clickSaveBtn() {
    await browser.wait(EC.presenceOf(dataPL.footerBtns.get(1)), browser.params.cooldown.small, `There is no Save button`);
    await dataPL.footerBtns.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickCancelBtn() {
    await browser.wait(EC.presenceOf(dataPL.footerBtns.get(0)), browser.params.cooldown.small, `There is no Cancel button`);
    await dataPL.footerBtns.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickDialogApprove() {
    await browser.wait(EC.presenceOf(dataPL.dialogFooterBtns.get(1)), browser.params.cooldown.small, `There is no Ok button in dialog`);
    await dataPL.dialogFooterBtns.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickDialogDeny() {
    await browser.wait(EC.presenceOf(dataPL.dialogFooterBtns.get(0)), browser.params.cooldown.small, `There is no Cancels button in dialog`);
    await dataPL.dialogFooterBtns.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async saveSubOrg() {
    await this.clickSaveBtn();
    await this.clickDialogApprove();
    await spinner.waitUntilSpinnerOff();
  };

  static async expandSubNameByNo(subNoInList = 1) {
    await browser.wait(EC.presenceOf(dataPL.subListNames.get(subNoInList - 1)), browser.params.cooldown.small, `There is Sub(${subNoInList}) in List`);
    await dataPL.subListNames.get(subNoInList - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async expandSubDetailsByName(subName: string) {
    let i = 1;
    while (1) {
      if (await dataPL.subListNames.get(i - 1).getText().then((text: string) => { return text; }) == subName) {
        await this.expandSubNameByNo(i);
        break;
      } else {
        i++;
        if (i > await dataPL.subListNames.count()) {
          await expect(false).toBeTruthy(`There is no Organization(${subName}) in list`);
          break;
        }
      }
    }
  };

  static async deleteSub() {
    await browser.wait(EC.presenceOf(dataPL.threeDotsMenu), browser.params.cooldown.small, `There is no three dots menu`);
    await dataPL.threeDotsMenu.click();
    await browser.wait(EC.presenceOf(dataPL.threeDotsMenuOptions.get(1)), browser.params.cooldown.small, `There is no second options in three dots menu`); //delete
    await dataPL.threeDotsMenuOptions.get(1).click();
    await this.clickDialogApprove();
    await spinner.waitUntilSpinnerOff();
  };

  static async editSub() {
    await browser.wait(EC.presenceOf(dataPL.threeDotsMenu), browser.params.cooldown.small, `There is no three dots menu`);
    await dataPL.threeDotsMenu.click();
    await browser.wait(EC.presenceOf(dataPL.threeDotsMenuOptions.get(0)), browser.params.cooldown.small, `There is no first options in three dots menu`); //edit
    await dataPL.threeDotsMenuOptions.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickEditParentOrg() {
    await browser.wait(EC.presenceOf(dataPL.editBtns.get(0)), browser.params.cooldown.small, `There is no Edit Button for parent organization`);
    await dataPL.editBtns.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setGlobalAssets(amount = 0) {
    await browser.wait(EC.presenceOf(dataPL.globalAssetsInput), browser.params.cooldown.small, `There is no Global Assets input`);
    await dataPL.globalAssetsInput.clear();
    await dataPL.globalAssetsInput.sendKeys(amount);
  };

  static async setMarketCapitalization(amount = 0) {
    await browser.wait(EC.presenceOf(dataPL.marketCapitalizationInput), browser.params.cooldown.small, `There is no Market Capitalization input`);
    await dataPL.marketCapitalizationInput.clear();
    await dataPL.marketCapitalizationInput.sendKeys(amount);
  };

  static async clickOwnershipStructureDD() {
    await browser.wait(EC.presenceOf(dataPL.ownershipStructureDD), browser.params.cooldown.small, `There is no Ownership Structure dropdown`);
    await dataPL.ownershipStructureDD.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setOwnershipStructureR() {
    await this.clickOwnershipStructureDD();
    await this.clickOpenDDItem(await this.randomFromList(await dataPL.openedDDItems.count()));
  };

  static async setOwnershipStructureByTitle(title = ``) {
    await this.clickOwnershipStructureDD();
    let i = 1;
    while (1) {
      if (await dataPL.openedDDItems.get(i - 1).getText().then((text: string) => { return text; }) == title) {
        await this.clickOpenDDItem(i);
        break;
      } else {
        i++;
        if (i > await dataPL.openedDDItems.count()) {
          await expect(false).toBeTruthy(`There is no Ownership Structure(${title}) in list`);
          break;
        }
      }
    }
  };

  static async clickOwnershipTypeDD() {
    await browser.wait(EC.presenceOf(dataPL.ownershipTypeDD), browser.params.cooldown.small, `There is no Ownership Structure dropdown`);
    await dataPL.ownershipTypeDD.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setOwnershipTypeR() {
    await this.clickOwnershipTypeDD();
    await this.clickOpenDDItem(await this.randomFromList(await dataPL.openedDDItems.count()));
  };

  static async setOwnershipTypeByTitle(title = ``) {
    await this.clickOwnershipTypeDD();
    let i = 1;
    while (1) {
      if (await dataPL.openedDDItems.get(i - 1).getText().then((text: string) => { return text; }) == title) {
        await this.clickOpenDDItem(i);
        break;
      } else {
        i++;
        if (i > await dataPL.openedDDItems.count()) {
          await expect(false).toBeTruthy(`There is no Ownership Type(${title}) in list`);
          break;
        }
      }
    }
  };

  static async setGradeStructureRadio(value = ``) {
    if (value == `Yes`) {
      await browser.wait(EC.presenceOf(dataPL.gradingStructureRadioBtns.get(0)), browser.params.cooldown.small, `There is no Grading Structure radio buttons`);
      await dataPL.gradingStructureRadioBtns.get(0).click();
    }
    else if (value == `No`) {
      await browser.wait(EC.presenceOf(dataPL.gradingStructureRadioBtns.get(1)), browser.params.cooldown.small, `There is no Grading Structure radio buttons`);
      await dataPL.gradingStructureRadioBtns.get(1).click();
    }
  };

  static async setGradeAcrossAllRadio(value = ``) {
    if (value == `Yes`) {
      await browser.wait(EC.presenceOf(dataPL.gradingAcrossAllRadioBtns.get(0)), browser.params.cooldown.small, `There is no Grading Across all radio buttons`);
      await dataPL.gradingAcrossAllRadioBtns.get(0).click();
    }
    else if (value == `No`) {
      await browser.wait(EC.presenceOf(dataPL.gradingAcrossAllRadioBtns.get(1)), browser.params.cooldown.small, `There is no Grading Across all radio buttons`);
      await dataPL.gradingAcrossAllRadioBtns.get(1).click();
    }
  };


}