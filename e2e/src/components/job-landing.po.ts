import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { JobLandingLocators as landingPL } from './job-landing.locators';
import { SpinnerPO as spinner } from './spinner';
import { JobDetailsLocators } from './job-details.locators';


export class JobLandingPageObjects {
  static async clickEditButton(num: number) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.editButtons.get(num - 1)), browser.params.cooldown.medium, `There is no Edit(${num}) button on landing page`);
    await spinner.waitUntilSpinnerOff();
    await landingPL.editButtons.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async changeJobName(newName: string) {
    await browser.wait(EC.elementToBeClickable(landingPL.detailsName), browser.params.cooldown.medium, `No Name input`);
    await landingPL.detailsName.clear();
    await landingPL.detailsName.sendKeys(newName);
  };

  static async changeJobCode(newCode: string) {
    await browser.wait(EC.elementToBeClickable(landingPL.detailsCode), browser.params.cooldown.medium, `No Code input`);
    await landingPL.detailsCode.clear();
    await landingPL.detailsCode.sendKeys(newCode);
  };

  static async changeJobLevelByName(text: string) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.detailsLevelFuncDDs.get(0)), browser.params.cooldown.medium, `No Level dropdown`);
    await landingPL.detailsLevelFuncDDs.get(0).click();
    let am = await landingPL.dropdownItemNames.count();
    for (let i = 0; i < am; i++) {
      let t = await landingPL.dropdownItemNames.get(i).getText();
      if (t.indexOf(text) !== -1) {
        await landingPL.dropdownItems.get(i).click();
        break;
      }
    }
  };

  static async changeJobSubLevel(text: string) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.detailsLevelFuncDDs.get(1)), browser.params.cooldown.medium, `No Sub-level dropdown`);
    await landingPL.detailsLevelFuncDDs.get(1).click();
    let am = await landingPL.dropdownItemNames.count();
    for (let i = 0; i < am; i++) {
      let t = await landingPL.dropdownItemNames.get(i).getText();
      if (t.indexOf(text) !== -1) {
        await landingPL.dropdownItems.get(i).click();
        break;
      }
    }
  };

  static async changeJobFunction(text: string) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.detailsLevelFuncDDs.get(2)), browser.params.cooldown.medium, `No Function dropdown`);
    await landingPL.detailsLevelFuncDDs.get(2).click();
    let am = await landingPL.dropdownItemNames.count();
    for (let i = 0; i < am; i++) {
      let t = await landingPL.dropdownItemNames.get(i).getText();
      if (t.indexOf(text) !== -1) {
        await landingPL.dropdownItems.get(i).click();
        break;
      }
    }
  };

  static async changeJobSubFunction(text: string) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.detailsLevelFuncDDs.get(3)), browser.params.cooldown.medium, `No Sub-function dropdown`);
    await landingPL.detailsLevelFuncDDs.get(3).click();
    let am = await landingPL.dropdownItemNames.count();
    for (let i = 0; i < am; i++) {
      let t = await landingPL.dropdownItemNames.get(i).getText();
      if (t.indexOf(text) !== -1) {
        await landingPL.dropdownItems.get(i).click();
        break;
      }
    }
  };

  static async setDropdownValue(dropdownN = 1, itemN = 1) {
    await browser.wait(EC.elementToBeClickable(landingPL.detailsPropDDs.get(dropdownN - 1)), browser.params.cooldown.medium, `There is no Property Dropdown(${dropdownN})`);
    await landingPL.detailsPropDDs.get(dropdownN - 1).click();
    await landingPL.dropdownItems.get(itemN - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickNextButton() {
    await browser.wait(EC.elementToBeClickable(landingPL.editActionButtons.get(1)), browser.params.cooldown.medium, `Next button isn't clickable`);
    await landingPL.editActionButtons.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async deleteRespByNum(num: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.removeRowButtons.get(num - 1)), browser.params.cooldown.medium, `X button for resp(${num}) isn't clickable`);
    await landingPL.removeRowButtons.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async deleteAllResps() {
    await spinner.waitUntilSpinnerOff();
    await landingPL.removeRowButtons.count().then(async am => {
      for (let i = 0; i < am; i++) {
        await landingPL.removeRowButtons.get(0).click();
      }
    });
  }

  static async clickAddRespButton() {
    await browser.wait(EC.elementToBeClickable(landingPL.addRespButton), browser.params.cooldown.medium, `Add Resp button isn't clickable`);
    await landingPL.addRespButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickRespInAddList(num = 1) {
    await browser.wait(EC.elementToBeClickable(landingPL.notDisabledRespsInList.get(num - 1)), browser.params.cooldown.medium, `Empty Resps list`);
    await landingPL.notDisabledRespsInList.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickAddRespInAddList() {
    await browser.wait(EC.elementToBeClickable(landingPL.actionButtonsInList.get(1)), browser.params.cooldown.medium, `Add button isn't clickable`);
    await landingPL.actionButtonsInList.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async addResp() {
    await this.clickAddRespButton();
    await this.clickRespInAddList(1);
    await this.clickAddRespInAddList();
  };

  static async renameResp(pos = 1, newName = `a`) {
    await browser.wait(EC.elementToBeClickable(landingPL.respNameInputs.get(pos - 1)), browser.params.cooldown.medium, `Input(${pos}) isn't active`);
    await landingPL.respNameInputs.get(pos - 1).clear();
    await landingPL.respNameInputs.get(pos - 1).sendKeys(newName);
    await spinner.waitUntilSpinnerOff();
  };

  static async confirmPMTUpdate() {
    try {
      await spinner.waitUntilSpinnerOff();
      await landingPL.confirmPMTButtons.get(1).click();
      await spinner.waitUntilSpinnerOff();
    }
    catch{ }
  };

  static async deleteAllTasks() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.deleteTaskButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await browser.wait(EC.elementToBeClickable(landingPL.deleteTaskButtons.get(i - 1)), browser.params.cooldown.small, `Delete Task button isn't clickable`);
        await landingPL.deleteTaskButtons.get(i - 1).click();
        await browser.wait(EC.elementToBeClickable(landingPL.submitTaskDelete.get(1)), browser.params.cooldown.small, `Can's submit deleting`);
        await landingPL.submitTaskDelete.get(1).click();
      }
    });
  };

  static async addTask() {
    await browser.wait(EC.visibilityOf(landingPL.addTaskButton), browser.params.cooldown.medium, `No add task button`);
    await landingPL.addTaskButton.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async editTask(taskNum: number, typeNum: number, taskText: string) {
    await landingPL.taskTypeDropdowns.get(taskNum - 1).click();
    await landingPL.typeDropdownItems.get(typeNum - 1).click();
    await landingPL.taskTextFields.get(taskNum - 1).clear();
    await landingPL.taskTextFields.get(taskNum - 1).sendKeys(taskText);
  };

  static async saveJob() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.saveButton), browser.params.cooldown.medium, `No Save button`);
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.saveButton.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async recommendationsDone() {
    try {
      await landingPL.recomendationsDoneButton.get(0).click();
      await spinner.waitUntilSpinnerOff();
    }
    catch{ }
  };

  static async deleteBCompetency(rNum: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.deleteBCButtons.get(rNum - 1)), browser.params.cooldown.medium, `Delete button(${rNum}) not visible`);

  };

  static async deleteAllBCompetencies() {
    await spinner.waitUntilSpinnerOff();
    await landingPL.deleteBCButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.deleteBCButtons.get(0).click();
      }
    });
  };

  static async clickAddBCButton() {
    await landingPL.addBCButton.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickItemInBCList(rowNo: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.bcDropdownItems.get(rowNo - 1)), browser.params.cooldown.medium, `No list with BC`);
    await landingPL.bcDropdownItems.get(rowNo - 1).click();
  };

  static async clickSubmitBCAddButton() {
    await landingPL.submitAddBCButton.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async addBCompetency() {
    await this.clickAddBCButton();
    await this.clickItemInBCList(0);
    await this.clickSubmitBCAddButton();
    await spinner.waitUntilSpinnerOff();
  };

  static async editBCompetencyName(fieldNum: number, text: string) {
    await browser.wait(EC.visibilityOf(landingPL.inputBCNames.get(fieldNum - 1)), browser.params.cooldown.medium, `Input field(${fieldNum}) not visible`);
    await landingPL.inputBCNames.get(fieldNum - 1).clear();
    await landingPL.inputBCNames.get(fieldNum - 1).sendKeys(text);
  };

  static async deleteTCompetency(rNum: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.deleteTCButtons.get(rNum - 1)), browser.params.cooldown.medium, `Delete button(${rNum}) not visible`);
    await landingPL.deleteTCButtons.get(rNum - 1).click();
  };

  static async deleteAllTCompetencies() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.deleteTCButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await this.deleteTCompetency(i);
      }
    });
  };

  static async clickAddTCButton() {
    await landingPL.addTCButton.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickItemInTCList(itemNo: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.tcDropdownItems.get(itemNo - 1)), browser.params.cooldown.large, `No list with TC`);
    await landingPL.tcDropdownItems.get(itemNo - 1).click();
  };

  static async clickSubmitAddTCButton() {
    await landingPL.submitAddTCButton.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async addTCompetency() {
    await this.clickAddTCButton();
    await this.clickItemInTCList(1);
    await this.clickSubmitAddTCButton();
    await spinner.waitUntilSpinnerOff();
  };

  static async editTCompetencyName(fieldNum: number, text: string) {
    await browser.wait(EC.visibilityOf(landingPL.inputTCNames.get(fieldNum - 1)), browser.params.cooldown.medium, `Input field(${fieldNum}) not visible`);
    await landingPL.inputTCNames.get(fieldNum - 1).clear();
    await landingPL.inputTCNames.get(fieldNum - 1).sendKeys(text);
  };

  static async deleteAllSkills() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.deleteTCSkills.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.deleteTCSkills.get(i - 1).click();
      }
    });
  };

  static async clickDeleteSkillButton(rowNo: number) {
    await browser.wait(EC.presenceOf(landingPL.deleteTCSkills.get(rowNo - 1)), browser.params.cooldown.small, `No delete skill button`);
    await landingPL.deleteTCSkills.get(rowNo - 1).click();
  };

  static async clickAddSkillsLink(rowNo: number) {
    await browser.wait(EC.visibilityOf(landingPL.addTCSkills.get(rowNo - 1)), browser.params.cooldown.medium, `No add skill link`);
    await landingPL.addTCSkills.get(rowNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async addNewSkill(rowNo: number, text: string) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.addSkillSearch.get(rowNo)), browser.params.cooldown.medium, `No skill search input`);
    await landingPL.addSkillSearch.get(rowNo).sendKeys(text, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.addTCSkillsItems.get(0)), browser.params.cooldown.medium, `No skills list`);
    await landingPL.addTCSkillsItems.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async confirmAddSkill(row: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.confirmAddSkillButtons.get(row * 2 - 1)), browser.params.cooldown.medium, `No skill search input`);
    await landingPL.confirmAddSkillButtons.get(row * 2 - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async addNewSkillActions(row: number, text: string) {
    await this.clickAddSkillsLink(row);
    await this.addNewSkill(row, text);
    await this.confirmAddSkill(row);
    await spinner.waitUntilSpinnerOff();
  };


  static async clickDelToolButton(rowNo: number) {
    await browser.wait(EC.presenceOf(landingPL.deleteToolButtons.get(rowNo - 1)), browser.params.cooldown.small, `No delete button`);
    await landingPL.deleteToolButtons.get(rowNo - 1).click();
  };

  static async clickSubmitDelTool() {
    await browser.wait(EC.presenceOf(landingPL.submitDelToolButton.get(1)), browser.params.cooldown.small, `No dialog popup`);
    await landingPL.submitDelToolButton.get(1).click();
  };

  static async deleteTool(tNum: number) {
    await this.clickDelToolButton(tNum);
    await this.clickSubmitDelTool();
  };

  static async deleteAllTools() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.deleteToolButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.deleteToolButtons.get(i - 1).click();
        await landingPL.submitDelToolButton.get(1).click();
      }
    });
  };

  static async clickAddTool() {
    await browser.wait(EC.visibilityOf(landingPL.addToolButton), browser.params.cooldown.small, `No add technology button`);
    await landingPL.addToolButton.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async editToolName(tNum: number, tText: string) {
    await browser.wait(EC.presenceOf(landingPL.toolNameInputs.get(tNum - 1)), browser.params.cooldown.small, `No input field`);
    await landingPL.toolNameInputs.get(tNum - 1).clear();
    await landingPL.toolNameInputs.get(tNum - 1).sendKeys(tText);
  };

  static async clickAddToolExample(rowNo: number) {
    await browser.wait(EC.presenceOf(landingPL.addToolExampleButtons.get(rowNo - 1)), browser.params.cooldown.small, `No add example button`);
    await landingPL.addToolExampleButtons.get(rowNo - 1).click();
  };

  static async editToolExampleText(rowNo: number, text: string) {
    await landingPL.toolExampleInputs.get(rowNo - 1).sendKeys(text);
  };

  static async addToolExample(rNum: number, text: string) {
    await this.clickAddToolExample(rNum);
    await this.editToolExampleText(rNum, text);
  };

  static async deleteEducation(rNum: number) {
    await browser.wait(EC.presenceOf(landingPL.delEduButtons.get(rNum - 1)), browser.params.cooldown.small, `No delete education button`);
    await landingPL.delEduButtons.get(rNum - 1).click();
  };

  static async deleteExperience(rNum: number) {
    await browser.wait(EC.presenceOf(landingPL.delExpButtons.get(rNum - 1)), browser.params.cooldown.small, `No delete experience button`);
    await landingPL.delExpButtons.get(rNum - 1).click();
  };

  static async deleteAllEducation() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.delEduButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.delEduButtons.get(i - 1).click();
      }
    });
  };

  static async deleteAllExperience() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.delExpButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.delExpButtons.get(i - 1).click();
      }
    });
  };

  static async deleteAllEduNExp() {
    await this.deleteAllEducation();
    await this.deleteAllExperience();
  };

  static async clickAddEducationButton() {
    await browser.wait(EC.presenceOf(landingPL.addEducationButton), browser.params.cooldown.small, `No add education button`);
    await landingPL.addEducationButton.click();
  };

  static async addAllEducation() {
    await this.clickAddEducationButton();
    await landingPL.eeListItems.each(async (item) => {
      await item.click();
    });
  };

  static async clickAddExperienceButton() {
    await browser.wait(EC.presenceOf(landingPL.addExperienceButton), browser.params.cooldown.small, `No add experience button`);
    await landingPL.addExperienceButton.click();
  };

  static async addAllExperience() {
    await this.clickAddExperienceButton();
    await landingPL.eeListItems.each(async (item) => {
      await item.click();
    });
  };

  static async editEducationName(rNum: number, text: string) {
    await browser.wait(EC.presenceOf(landingPL.inputEduNames.get(rNum - 1)), browser.params.cooldown.small, `No name field`);
    await landingPL.inputEduNames.get(rNum - 1).clear();
    await landingPL.inputEduNames.get(rNum - 1).sendKeys(text);
  };

  static async editExperienceName(rNum: number, text: string) {
    await browser.wait(EC.presenceOf(landingPL.inputExpNames.get(rNum - 1)), browser.params.cooldown.small, `No name field`);
    await landingPL.inputExpNames.get(rNum - 1).clear();
    await landingPL.inputExpNames.get(rNum - 1).sendKeys(text);
  };

}

