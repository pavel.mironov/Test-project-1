import { by, element } from 'protractor';

export class ProjectListLocators {
  static tabsGlobal = element.all(by.css(`.first-level .menu-item`));
  static assessmentProjectTab = element(by.css(`.second-level a`));
  static homeSearch = element(by.css(`input`));
  static filterTitle = element(by.css(`div.filter-title div`));

  static projectInputs = element.all(by.css(`.card input`));
  static projectEndDate = element(by.css(`.date`));

  static calendarTD = element.all(by.css(`.date-group td:not(.disabled)`));

  static createControls = element.all(by.css(`.control-group .button`));
  static dialogActions = element.all(by.css(`.ui-dialog .ui-dialog-content footer .button`));
  static createFinishButtons = element.all(by.css(`.adjust-section .button`));
  static dialogButtons = element.all(by.css(`.ui-dialog-content .button-group .button`));

  static listRow = element.all(by.css(`tbody tr`));
  static projectListNames = element.all(by.css(`tr>td:nth-child(2)`));
  static projectListSPName = element.all(by.css(`tr>td:nth-child(4)`));
  static projectListCreatedDate = element.all(by.css(`tr>td:nth-child(5)`));
  static projectListEndDate = element.all(by.css(`tr>td:nth-child(6)`));


}