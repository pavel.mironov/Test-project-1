import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { JobDescriptionListLocators as listPL } from './jd-list.locators';
import { SpinnerPO as spinner } from './spinner';


export class JobDescriptionListPageObjects {

  static async searchJobDesctiption(text: string) {
    await browser.wait(EC.presenceOf(listPL.searchList), browser.params.cooldown.medium, 'No search field');
    await listPL.searchList.clear();
    await listPL.searchList.sendKeys(text, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  }

  static async clearSearch() {
    await browser.wait(EC.presenceOf(listPL.searchList), browser.params.cooldown.medium, 'No search field');
    await listPL.searchList.clear();
    await listPL.searchList.sendKeys(`a`, protractor.Key.BACK_SPACE);
    await spinner.waitUntilSpinnerOff();
  }

  static async getCount() {
    try {
      const v = await listPL.filterTitle.getText().then(async (text: string) => { return text; });
      const vv = v.split(' ');
      return parseInt(vv[3], 10);
    }
    catch{
      return 0;
    }
  }

  static async openThreeDotsMenu(rowNo: number) {
    await browser.wait(EC.elementToBeClickable(listPL.threeDotsMenu.get(rowNo - 1)), browser.params.cooldown.medium, `Can't find tree dots menu`);
    await listPL.threeDotsMenu.get(rowNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickItemThreeDotsMenu(rowNo: number) {
    await browser.wait(EC.elementToBeClickable(listPL.itemThreeDotsMenu.get(rowNo - 1)), browser.params.cooldown.medium, `No dropdown for three dots menu`);
    await listPL.itemThreeDotsMenu.get(rowNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clicksubmitDeleteViaThreeDotsMenu() {
    await browser.wait(EC.elementToBeClickable(listPL.submitDeleteThreeDots.get(0)), browser.params.cooldown.large, `No dialogue with submit button`);
    await listPL.submitDeleteThreeDots.get(0).click();
  }

  static async deleteJDViaThreeDotsMenu() {
    await this.openThreeDotsMenu(1);
    await this.clickItemThreeDotsMenu(4);
    await this.clicksubmitDeleteViaThreeDotsMenu();
    await spinner.waitUntilSpinnerOff();
  }

  static async openJDFromListByNo(rowNo: number) {
    let rowH = await listPL.rowTR.get(0).getSize();
    let m = 513;    // document.body.scrollHeight
    let cap = rowNo / 20 | 0;
    for (let i = 0; i < cap + 1; i++) {
      await browser.wait(EC.presenceOf(listPL.rowNames.get(i * 20 - 1)), browser.params.cooldown.large, `There is no rows in jd list`);
      await browser.executeScript(`window.scrollTo(0,${m + i * 20 * rowH.height + 240})`);
      await spinner.delay(1000);
      await spinner.waitUntilSpinnerOff();
    }

    await browser.wait(EC.elementToBeClickable(listPL.rowNames.get(rowNo - 1)), browser.params.cooldown.large, `There is no row with num ${rowNo}`);
    await browser.executeScript(`window.scrollTo(0,${m + rowNo * rowH.height})`);
    await spinner.delay(2000);
    await spinner.waitUntilSpinnerOff();
    await listPL.rowNames.get(rowNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  

}