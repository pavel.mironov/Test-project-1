import { by, element, browser, ExpectedConditions as EC } from 'protractor';
import { JobDescriptionDetailsLocators as detailsPL } from './jd-details.locators';
import { SpinnerPO as spinner } from './spinner';

export class JobDescriptionPageObject {
  static async clickSaveButton() {
    await browser.wait(EC.presenceOf(detailsPL.actionButtons.get(1)), 60 * 1000, `Job Description details: There is no Save button`);
    await detailsPL.actionButtons.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickConfirmSaveButton() {
    await browser.wait(EC.presenceOf(detailsPL.saveDialoguButtons.get(3)), 60 * 1000, `Job Description details: There is no dialogue for confirmation`);
    await detailsPL.saveDialoguButtons.get(3).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async saveJobDescription() {
    await this.clickSaveButton();
    await this.clickConfirmSaveButton();
  }

  static async openActionsMenu() {
    await browser.wait(EC.elementToBeClickable(detailsPL.actionsDropdown), browser.params.cooldown.large, `There is no Actions dropdown`);
    await detailsPL.actionsDropdown.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickActionsItem(iNo: number) {
    await browser.wait(EC.elementToBeClickable(detailsPL.actionsDropdownItems.get(iNo - 1)), browser.params.cooldown.large, `There is no item with num ${iNo} in Actions dropdown`);
    await detailsPL.actionsDropdownItems.get(iNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async getJobDescriptionId(): Promise<string> {
    const url = await browser.getCurrentUrl();
    const parts = url.split('/');
    const res = parts[parts.length - 1].split(';');
    console.log(res);
    return res[0];
  }

  static async editTitle(text: string) {
    await browser.wait(EC.presenceOf(detailsPL.editTitle), browser.params.cooldown.large, `No input for jd title`);
    await detailsPL.editTitle.clear();
    await detailsPL.editTitle.sendKeys(text);
  }

  static async changeRespName(row: number, text: string) {
    await browser.wait(EC.presenceOf(detailsPL.respNames.get(row - 1)), browser.params.cooldown.large, `No resp name(${row} row)`);
    await detailsPL.respNames.get(row - 1).clear();
    await detailsPL.respNames.get(row - 1).sendKeys(text);
  }

  static async changeBCName(row: number, text: string) {
    await browser.wait(EC.presenceOf(detailsPL.bcNames.get(row - 1)), browser.params.cooldown.large, `No BC name(${row} row)`);
    await detailsPL.bcNames.get(row - 1).clear();
    await detailsPL.bcNames.get(row - 1).sendKeys(text);
  }

  static async changeTCName(row: number, text: string) {
    await browser.wait(EC.presenceOf(detailsPL.tcNames.get(row - 1)), browser.params.cooldown.large, `No TC name(${row} row)`);
    await detailsPL.tcNames.get(row - 1).clear();
    await detailsPL.tcNames.get(row - 1).sendKeys(text);
  }

  static async changeGenExpName(text: string) {
    await browser.wait(EC.presenceOf(detailsPL.expNames.get(0)), browser.params.cooldown.large, `No g experience name`);
    await detailsPL.expNames.get(0).clear();
    await detailsPL.expNames.get(0).sendKeys(text);
  }

  static async changeManExpName(text: string) {
    await browser.wait(EC.presenceOf(detailsPL.expNames.get(1)), browser.params.cooldown.large, `No m experience name`);
    await detailsPL.expNames.get(1).clear();
    await detailsPL.expNames.get(1).sendKeys(text);
  }

  static async changeGenEducation(text: string) {
    await browser.wait(EC.presenceOf(detailsPL.eduNames.get(0)), browser.params.cooldown.large, `No education name`);
    await detailsPL.eduNames.get(0).clear();
    await detailsPL.eduNames.get(0).sendKeys(text);
  }
}