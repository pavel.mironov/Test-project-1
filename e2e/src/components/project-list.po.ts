import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { ProjectListLocators as listPL } from './project-list.locators';
import { SuccessProfileDetailsPageLocators as spDetailsPL } from './sp-details.locators';
import { SuccessProfileDetailsPageObjects as spDetailsPO } from './sp-details.po';
import { SpinnerPO as spinner } from './spinner';


export class ProjectListPageObject {
  static async searchProject(text: string) {
    await browser.wait(EC.presenceOf(listPL.homeSearch), browser.params.cooldown.medium, 'No search field');
    await listPL.homeSearch.clear();
    await listPL.homeSearch.sendKeys(text, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  }

  static async getCount() {
    await spinner.waitUntilSpinnerOff();
    try {
      const v = await listPL.filterTitle.getText().then(async (text: string) => { return text; });
      const vv = v.split(' ');
      return parseInt(vv[3], 10);
    }
    catch{
      return 0;
    }
  }

  static async createAssessTypeProject() {
    await spDetailsPO.openActionsMenu();
    await spDetailsPO.clickItemInActions(6);
    await spDetailsPL.projectTypeHeader.click();
    await browser.wait(EC.elementToBeClickable(spDetailsPL.projectType.get(0)), browser.params.cooldown.medium, `Project type radio button isn't clickable`);
    await spDetailsPL.projectType.get(0).click();
    await browser.wait(EC.elementToBeClickable(spDetailsPL.createProjectDialogueButtons.get(1)), browser.params.cooldown.medium, `Confirm create button isn't clickable`);
    await spDetailsPL.createProjectDialogueButtons.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setProjectName(name: string) {
    await listPL.projectInputs.get(0).sendKeys(name);
  }

  static async setProjectEndDate(endDate: string) {
    await browser.wait(EC.elementToBeClickable(listPL.projectEndDate), browser.params.cooldown.medium, `Calendar isn't clickable`);
    await listPL.projectEndDate.click();
    await listPL.calendarTD.count().then(async a => {
      for (let i = a - 1; i > -1; i--) {
        let t = await listPL.calendarTD.get(i).getText();
        if (t == endDate) {
          await browser.wait(EC.elementToBeClickable(listPL.calendarTD.get(i)), browser.params.cooldown.medium, `Calendar date isn't clickable`);
          await listPL.calendarTD.get(i).click();
          break;
        }
      }
    });
  }

  static async clickContinueCreation() {
    await spinner.delay(2000);
    await browser.wait(EC.elementToBeClickable(listPL.createControls.get(1)), browser.params.cooldown.medium, `Continue button isn't clickable`);
    await listPL.createControls.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickContinueDialog() {
    await spinner.delay(2000);
    await browser.wait(EC.elementToBeClickable(listPL.dialogActions.get(14)), browser.params.cooldown.medium, `Continue button isn't clickable`);
    await listPL.dialogActions.get(14).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickFinishCreation() {
    await spinner.delay(2000);
    await browser.wait(EC.elementToBeClickable(listPL.createFinishButtons.get(2)), browser.params.cooldown.medium, `Finish button isn't clickable`);
    await listPL.createFinishButtons.get(2).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickConfirmDialogue() {
    await browser.wait(EC.elementToBeClickable(listPL.dialogButtons.get(3)), browser.params.cooldown.medium, `Confirm button in dialog isn't clickable`);
    await listPL.dialogButtons.get(3).click();
    await spinner.waitUntilSpinnerOff();
  }

}

