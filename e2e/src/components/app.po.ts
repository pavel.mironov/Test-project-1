import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { HomePageLocators as homePL } from './home.pl';
import { LoginPageOblject as loginPO } from './login.po';
import { SuccessProfileListLocators as spListPL } from './sp-list.locators';
import { SuccessProfileListPageObjects as spListPO } from './sp-list.po';
import { JobDescriptionListLocators as jdListPL } from './jd-list.locators';
import { ProjectListLocators as pListPL } from './project-list.locators';
import { SpinnerPO as spinner } from './spinner';


export class AppPage {
  static async navigateTo(path: string) {
    await spinner.delay(5000);
    return await browser.get(`/${path}`);
  }

  static async login(k = ``) {
    await this.navigateTo('app/tarc/#/login');
    await spinner.waitUntilSpinnerOff();
    if (k == `userOne`) await loginPO.login(browser.params.userOne.login, browser.params.userOne.password);
    else if (k == `userTwo`) await loginPO.login(browser.params.userTwo.login, browser.params.userTwo.password);
    else if (k == `userThree`) await loginPO.login(browser.params.userThree.login, browser.params.userThree.password);
    else await loginPO.login(browser.params.userOne.login, browser.params.userOne.password);

    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.presenceOf(homePL.searchHome),
      60 * 1000,
      `Can't find search input at home page`
    );
    await spinner.waitUntilSpinnerOff();
  }

  static async logout() {
    await loginPO.logout();
    await spinner.waitUntilSpinnerOff();
  }

  static async moveToAssessTab() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.presenceOf(pListPL.tabsGlobal.get(1)),
      browser.params.cooldown.medium,
      `No Assess tab in navigation panel`);
    await pListPL.tabsGlobal.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async moveToHomeTab() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.presenceOf(pListPL.tabsGlobal.get(0)),
      browser.params.cooldown.medium,
      `No Home tab in navigation panel`);
    await pListPL.tabsGlobal.get(0).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async moveToAssessmentProjectTab() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.presenceOf(pListPL.assessmentProjectTab),
      browser.params.cooldown.medium,
      `No Assessment Project tab in navigation panel`);
    await pListPL.assessmentProjectTab.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async moveToArchitectTab() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.presenceOf(pListPL.tabsGlobal.get(4)),
      browser.params.cooldown.medium,
      `No Architect tab in navigation panel`);
    await pListPL.tabsGlobal.get(4).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async moveToSPList() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.presenceOf(spListPL.tabSPList),
      browser.params.cooldown.medium,
      `No second level tabs in navigation panel`);
    await browser.executeScript(`window.scrollTo(0,0)`);
    await spListPL.tabSPList.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async moveToJDList() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.presenceOf(jdListPL.tabJDList),
      browser.params.cooldown.medium,
      `No second level tabs in navigation`);
    await jdListPL.tabJDList.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async getCount() {
    try {
      await spinner.waitUntilSpinnerOff();
      const filterTitle = await element(by.css('div.filter-title div'));
      const v = await filterTitle.getText().then(async (text: string) => { return text; });
      const vv = v.split(' ');
      return parseInt(vv[3], 10);
    }
    catch{
      return 0;
    }
  }

  static generateRandomInt(minV: number, maxV: number) {
    return Math.floor(Math.random() * (maxV - minV) + minV);
  }

  static async searchProfileHome(text: string) {
    await this.moveToSPList();
    await spListPO.clearAllFiltersF();
    await spinner.waitUntilSpinnerOff();
    await spListPO.searchProfile(text);
    await spinner.waitUntilSpinnerOff();
  };

  static async moveToOrgData() {
    let icon = await element(by.css(`.global-header .kf-icon-orgdata`));
    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.presenceOf(icon),
      browser.params.cooldown.medium,
      `No Org Data icon in global navigation panel`);
    await icon.click();
    await spinner.waitUntilSpinnerOff();
  };
}