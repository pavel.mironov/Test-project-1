import { by, element } from 'protractor';

export class SuccessProfileSummaryPageLocators {
  static headers0 = element.all(by.css('div.header.ng-star-inserted'))
  static headers1 = element.all(by.css('.detail-tabs.nested div.header'));
  static summaryRecords = element.all(by.css(`tr.ng-star-inserted`));
  static summaryCloseButton = element.all(by.css(`div.slider-footer div:nth-child(1) > button`));

  static summaryRecordNames = element.all(by.css(`tr .competency-col .competency-text`));

  static recordNames = element.all(by.css(`tr .competency-col .competency-text`));
  static recordValueByNo(rowNo: number) {
    return element.all(by.css(`tr:nth-child(${rowNo}) rect`))
  }

  static summarySkills = element.all(by.css(`tr .description-td span`));

  static roleSummary = element(by.css(`.role-summary p`));
  static detailsSummary = element.all(by.css(`.profile-info-row .value.kf-text`));
  static gradeSummary = element.all(by.css(`.grade-chart .value-container span`));

  static wcSummaryTitles = element.all(by.css(`.workchar-card-container .title`));
  static wcSummaryDescs = element.all(by.css(`.workchar-card-container .description`));
  static wcSummaryLevels = element.all(by.css(`.workchar-card-container .level .value`));

  static respSummaryNames = element.all(by.css(`.responsibility-table td.competency-col .competency-text`));
  static respSummaryLevels = element.all(by.css(`.responsibility-table .stepper-container .level-stat`));
  static respSummaryDescs = element.all(by.css(`.responsibility-table td.description-td .kf-text`));

  static eduSummaryNames = element.all(by.css(` td.competency-col .competency-text`));
  static eduSummaryLevels = element.all(by.css(`.stepper-container .level-stat`));
  static eduSummaryDescs = element.all(by.css(` td.description-td .kf-text`));

  static expSummaryNames = element.all(by.css(` td.competency-col .competency-text`));
  static expSummaryLevels = element.all(by.css(`.stepper-container .level-stat`));
  static expSummaryDescs = element.all(by.css(` td.description-td .kf-text`));

  static cogAbilSummaryNames = element.all(by.css(`.task-card-container .task-text:nth-child(1)`));
  static cogAbilSummaryDescs = element.all(by.css(`.task-card-container .task-description-container .kf-text`));
  static cogAbilSummaryLevels = element.all(by.css(`.task-card-container .task-text:nth-child(2)`));

  static editSPButton = element(by.css(`.ui-sidebar-active .footer-right .button`));
}