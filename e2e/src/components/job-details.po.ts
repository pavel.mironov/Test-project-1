import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { JobDetailsLocators as detailsPL } from './job-details.locators';
import { SpinnerPO as spinner } from './spinner';


export class JobDetailsPageObjects {
  static async openActionMenu() {
    await browser.wait(EC.elementToBeClickable(detailsPL.actionsMenuButton), browser.params.cooldown.medium, `No Actions dropdown`);
    await browser.executeScript(`window.scrollTo(0,0)`);
    await detailsPL.actionsMenuButton.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickActionMenuItem(itemNum = 1) {
    await browser.wait(EC.elementToBeClickable(detailsPL.actionsMenuItems.get(itemNum - 1)), browser.params.cooldown.medium, `No Item(${itemNum}) in Actions dropdown`);
    await detailsPL.actionsMenuItems.get(itemNum - 1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async confirmDeleteJob() {
    await browser.wait(EC.elementToBeClickable(detailsPL.deleteJobButtons.get(1)), browser.params.cooldown.medium, `Can't confirm job delete`);
    await detailsPL.deleteJobButtons.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async deleteJobViaActions() {
    await this.openActionMenu();
    await this.clickActionMenuItem(7);
    await this.confirmDeleteJob();
  };

  static async editJobViaActions() {
    await this.openActionMenu();
    await this.clickActionMenuItem(6);
    await spinner.waitUntilSpinnerOff();
  };

  static async clickSummaryReportLink() {
    await browser.wait(EC.elementToBeClickable(detailsPL.jobSummaryReportLink), browser.params.cooldown.medium, `No Summary Report link`);
    await detailsPL.jobSummaryReportLink.click();
    await spinner.waitUntilSpinnerOff();
  };


  static async openJE() {
    await this.openActionMenu()
    await this.clickActionMenuItem(2);
  };

  static async clickCancelJE() {
    await browser.wait(EC.elementToBeClickable(detailsPL.jeActions.get(0)), browser.params.cooldown.medium, `No Cancel Button for JE`);
    await detailsPL.jeActions.get(0).click();
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(detailsPL.jeConfirmActions.get(0)), browser.params.cooldown.medium, `No Confirm Cancel Button for JE`);
    await detailsPL.jeConfirmActions.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickCancelJEwoCofirm() {
    await browser.wait(EC.elementToBeClickable(detailsPL.jeActions.get(0)), browser.params.cooldown.medium, `No Cancel Button for JE`);
    await detailsPL.jeActions.get(0).click();
    await spinner.waitUntilSpinnerOff();    
  };

}

