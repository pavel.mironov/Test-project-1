
import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { By } from 'selenium-webdriver';

const spinnerByID = element(by.id(`spinner`));
const spinnerByCSS = element(by.css(`spinner`));

export class SpinnerPO {
  static async delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  static async elementNotExist(loc: By) {
    return await element.all(loc).then((items) => {
      if (items.length == 0) { return true; }
      else { return false; }
    });
  }

  static async spinnerNotVisible() {
    try {
      return await spinnerByID.isDisplayed().then((r) => { return !r; });
    } catch{ }
  }

  static async waitUntilSpinnerOff() {
    try {
      await this.delay(1000);
      await browser.wait(
        this.spinnerNotVisible(),
        browser.params.cooldown.large,
        'Spinner still visible'
      );
      await browser.wait(
        EC.not(EC.visibilityOf(spinnerByID)),
        browser.params.cooldown.large,
        'Spinner still visible'
      );
    }
    catch{
      await this.delay(1000);
      await browser.wait(
        this.elementNotExist(by.id(`spinner`)),
        browser.params.cooldown.large,
        `Spinner still here`
      );
    }
  }



}