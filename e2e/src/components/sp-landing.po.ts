import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { SuccessProfileLandingPageLocators as landingPL } from './sp-landing.locators';
import { SpinnerPO as spinner } from './spinner';
import { By } from 'selenium-webdriver';

export class SuccessProfileLandingPageObjects {

  static async selectEditPageItem(num: number) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.editButtons.get(num - 1)), browser.params.cooldown.large, `Edit Button(${num}) isn't clickable`)
    await spinner.waitUntilSpinnerOff();
    await landingPL.editButtons.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async editProfileTitle(text: string) {
    await browser.wait(EC.presenceOf(landingPL.profileName), browser.params.cooldown.medium, `Name field isn't show`)
    await landingPL.profileName.clear();
    await landingPL.profileName.sendKeys(text);
    await spinner.waitUntilSpinnerOff();
  }

  static async clickNextButton() {
    await browser.wait(EC.presenceOf(landingPL.nextButton), browser.params.cooldown.small, `No next button`);
    await landingPL.nextButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickCancelButton() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.presenceOf(landingPL.cancelButton), browser.params.cooldown.small, `No next button`);
    await landingPL.cancelButton.click();
    await this.clickLeaveNotice();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickLeaveNotice() {
    try {
      await landingPL.leaveNoticeButton.click();
    }
    catch { }
  }

  static async saveProfile() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.saveButton), browser.params.cooldown.medium, `No Save button`);
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.saveButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async saveProfileAs() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.saveAsButton), browser.params.cooldown.medium, `No Save button`);
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.saveAsButton.click();
    await browser.wait(EC.elementToBeClickable(landingPL.saveConfirmButtons.get(1)), browser.params.cooldown.medium, `No Confirm button`);
    await landingPL.saveConfirmButtons.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickBackButton() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.backButton), browser.params.cooldown.medium, `No Back button`);
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.backButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async deleteAllResponsibilities() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.deleteRespButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.deleteRespButtons.get(i - 1).click();
      }
    });
  }

  static async deleteResponsibility(num: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.deleteRespButtons.get(num - 1)));
    await landingPL.deleteRespButtons.get(num - 1).click();
  }

  static async clickAddRespButton() {
    await landingPL.addRespButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickItemInRespsList(rowNo: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.listResps.get(rowNo - 1)), browser.params.cooldown.small, `No list with responsibilities`);
    await landingPL.listResps.get(rowNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickSubmitAddRespButton() {
    await landingPL.submitAddRespButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async addResponsibility() {
    await this.clickAddRespButton();
    await this.clickItemInRespsList(0);
    await this.clickSubmitAddRespButton();
    await spinner.waitUntilSpinnerOff();
  };

  static async addResponsibilityByName(title = ``) {
    await this.clickAddRespButton();

    await browser.wait(EC.elementToBeClickable(landingPL.listSearchInput), browser.params.cooldown.small, `No Search input`);
    await landingPL.listSearchInput.clear();
    await landingPL.listSearchInput.sendKeys(title);

    let i = 1;
    while (1) {
      if (await landingPL.listResps.get(i - 1).getText().then((text: string) => { return text; }) == title) {
        await this.clickItemInRespsList(i);
        break;
      } else {
        i++;
        if (i > await landingPL.listResps.count()) {
          await expect(false).toBeTruthy(`There is no Responsibility(${title}) in list`);
          break;
        }
      }
    };
    await this.clickSubmitAddRespButton();
    await spinner.waitUntilSpinnerOff();
  }

  static async editResponsibilityName(fieldNum: number, text: string) {
    await browser.wait(EC.visibilityOf(landingPL.inputRespNames.get(fieldNum - 1)), browser.params.cooldown.medium, `Input field(${fieldNum}) not visible`);
    await landingPL.inputRespNames.get(fieldNum - 1).clear();
    await landingPL.inputRespNames.get(fieldNum - 1).sendKeys(text);
  }

  static async tryConfirmUpdate() {
    try {
      await landingPL.confirmUpdate.click();
      await spinner.waitUntilSpinnerOff();
    } catch{ }
  };

  static async ConfirmUpdate() {
    await landingPL.confirmUpdate.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async deleteAllTasks() {
    await spinner.waitUntilSpinnerOff();
    await landingPL.deleteTaskButtons.count().then(async (am) => {
      for (var i = 0; i < am; i++) {
        await landingPL.deleteTaskButtons.get(0).click();
        await landingPL.submitTaskDelete.get(1).click();
      }
    });
  }

  static async deleteTask(num: number) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.deleteTaskButtons.get(num - 1)), browser.params.cooldown.medium, `No delete button`);
    await landingPL.deleteTaskButtons.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
    await landingPL.submitTaskDelete.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async addTask() {
    await browser.wait(EC.visibilityOf(landingPL.addTaskButton), browser.params.cooldown.medium, `No add task button`);
    await landingPL.addTaskButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async editTask(taskNum: number, typeNum: number, taskText: string) {
    await landingPL.taskTypeDropdowns.get(taskNum - 1).click();
    await landingPL.typeDropdownItems.get(typeNum - 1).click();
    await landingPL.taskTextFields.get(taskNum - 1).clear();
    await landingPL.taskTextFields.get(taskNum - 1).sendKeys(taskText);
  }

  static async deleteBCompetency(rNum: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.deleteBCButtons.get(rNum - 1)), browser.params.cooldown.medium, `Delete button(${rNum}) not visible`);

  }

  static async deleteAllBCompetencies() {
    await spinner.waitUntilSpinnerOff();
    await landingPL.deleteBCButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.deleteBCButtons.get(0).click();
      }
    });
  }

  static async clickAddBCButton() {
    await landingPL.addBCButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickItemInBCList(rowNo: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.bcDropdownItems.get(rowNo - 1)), browser.params.cooldown.medium, `No list with BC`);
    await landingPL.bcDropdownItems.get(rowNo - 1).click();
  }

  static async clickSubmitBCAddButton() {
    await landingPL.submitAddBCButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async addBCompetency() {
    await this.clickAddBCButton();
    await this.clickItemInBCList(0);
    await this.clickSubmitBCAddButton();
    await spinner.waitUntilSpinnerOff();
  }

  static async editBCompetencyName(fieldNum: number, text: string) {
    await browser.wait(EC.visibilityOf(landingPL.inputBCNames.get(fieldNum - 1)), browser.params.cooldown.medium, `Input field(${fieldNum}) not visible`);
    await landingPL.inputBCNames.get(fieldNum - 1).clear();
    await landingPL.inputBCNames.get(fieldNum - 1).sendKeys(text);
  }

  static async deleteTCompetency(rNum: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.deleteTCButtons.get(rNum - 1)), browser.params.cooldown.medium, `Delete button(${rNum}) not visible`);
    await landingPL.deleteTCButtons.get(rNum - 1).click();
  }

  static async deleteAllTCompetencies() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.deleteTCButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await this.deleteTCompetency(i);
      }
    });
  }

  static async clickAddTCButton() {
    await landingPL.addTCButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickItemInTCList(itemNo: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.tcDropdownItems.get(itemNo - 1)), browser.params.cooldown.large, `No list with TC`);
    await landingPL.tcDropdownItems.get(itemNo - 1).click();
  }

  static async clickSubmitAddTCButton() {
    await landingPL.submitAddTCButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async addTCompetency() {
    await this.clickAddTCButton();
    await this.clickItemInTCList(1);
    await this.clickSubmitAddTCButton();
    await spinner.waitUntilSpinnerOff();
  }

  static async editTCompetencyName(fieldNum: number, text: string) {
    await browser.wait(EC.visibilityOf(landingPL.inputTCNames.get(fieldNum - 1)), browser.params.cooldown.medium, `Input field(${fieldNum}) not visible`);
    await landingPL.inputTCNames.get(fieldNum - 1).clear();
    await landingPL.inputTCNames.get(fieldNum - 1).sendKeys(text);
  }

  static async deleteAllSkills() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.deleteTCSkills.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.deleteTCSkills.get(i - 1).click();
      }
    });
  }

  static async clickDeleteSkillButton(rowNo: number) {
    await browser.wait(EC.presenceOf(landingPL.deleteTCSkills.get(rowNo - 1)), browser.params.cooldown.small, `No delete skill button`);
    await landingPL.deleteTCSkills.get(rowNo - 1).click();
  }

  static async clickAddSkillsLink(rowNo: number) {
    await browser.wait(EC.visibilityOf(landingPL.addTCSkills.get(rowNo - 1)), browser.params.cooldown.medium, `No add skill link`);
    await landingPL.addTCSkills.get(rowNo - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async addNewSkill(rowNo: number, text: string) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.addSkillSearch.get(rowNo)), browser.params.cooldown.medium, `No skill search input`);
    await landingPL.addSkillSearch.get(rowNo).sendKeys(text);
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.elementToBeClickable(landingPL.addTCSkillsItems.get(0)), browser.params.cooldown.large, `No skills list`);
    await landingPL.addTCSkillsItems.get(0).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async confirmAddSkill(row: number) {
    await browser.wait(EC.elementToBeClickable(landingPL.confirmAddSkillButtons.get(row * 2 - 1)), browser.params.cooldown.large, `No skill search input`);
    await landingPL.confirmAddSkillButtons.get(row * 2 - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async addNewSkillActions(row: number, text: string) {
    await this.clickAddSkillsLink(row);
    await this.addNewSkill(row, text);
    await this.confirmAddSkill(row);
    await spinner.waitUntilSpinnerOff();
  }

  static async clickDeleteTechButton(rowNo: number) {
    await browser.wait(EC.presenceOf(landingPL.deleteTechButtons.get(rowNo - 1)), browser.params.cooldown.small, `No delete button`);
    await landingPL.deleteTechButtons.get(rowNo - 1).click();
  }

  static async clickSubmitDelTech() {
    await browser.wait(EC.presenceOf(landingPL.submitDelTechButton.get(1)), browser.params.cooldown.small, `No dialog popup`);
    await landingPL.submitDelTechButton.get(1).click();
  }

  static async deleteTechnology(tNum: number) {
    await this.clickDeleteTechButton(tNum);
    await this.clickSubmitDelTech();
  }

  static async deleteAllTechnologies() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.deleteTechButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.deleteTechButtons.get(i - 1).click();
        await landingPL.submitDelTechButton.get(1).click();
      }
    });
  }

  static async clickAddTechnology() {
    await browser.wait(EC.visibilityOf(landingPL.addTechButton), browser.params.cooldown.small, `No add technology button`);
    await landingPL.addTechButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async editTechnologyName(tNum: number, tText: string) {
    await browser.wait(EC.presenceOf(landingPL.inputTechNames.get(tNum - 1)), browser.params.cooldown.small, `No input field`);
    await landingPL.inputTechNames.get(tNum - 1).sendKeys(tText);
  }

  static async clickAddTEchExample(rowNo: number) {
    await browser.wait(EC.presenceOf(landingPL.addTechExampleButtons.get(rowNo - 1)), browser.params.cooldown.small, `No add example button`);
    await landingPL.addTechExampleButtons.get(rowNo - 1).click();
  }

  static async editTechExampleText(rowNo: number, text: string) {
    await landingPL.techExampleInputs.get(rowNo - 1).sendKeys(text);
  }

  static async addTechnologyExample(rNum: number, text: string) {
    await this.clickAddTEchExample(rNum);
    await this.editTechExampleText(rNum, text);
  }

  static async clickDelToolButton(rowNo: number) {
    await browser.wait(EC.presenceOf(landingPL.deleteToolButtons.get(rowNo - 1)), browser.params.cooldown.small, `No delete button`);
    await landingPL.deleteToolButtons.get(rowNo - 1).click();
  }

  static async clickSubmitDelTool() {
    await browser.wait(EC.presenceOf(landingPL.submitDelToolButton.get(1)), browser.params.cooldown.small, `No dialog popup`);
    await landingPL.submitDelToolButton.get(1).click();
  }

  static async deleteTool(tNum: number) {
    await this.clickDelToolButton(tNum);
    await this.clickSubmitDelTool();
  }

  static async deleteAllTools() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.deleteToolButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.deleteToolButtons.get(i - 1).click();
        await landingPL.submitDelToolButton.get(1).click();
      }
    });
  }

  static async clickAddTool() {
    await browser.wait(EC.visibilityOf(landingPL.addToolButton), browser.params.cooldown.small, `No add technology button`);
    await landingPL.addToolButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async editToolName(tNum: number, tText: string) {
    await browser.wait(EC.presenceOf(landingPL.toolNameInputs.get(tNum - 1)), browser.params.cooldown.small, `No input field`);
    await landingPL.toolNameInputs.get(tNum - 1).clear();
    await landingPL.toolNameInputs.get(tNum - 1).sendKeys(tText);
  }

  static async clickAddToolExample(rowNo: number) {
    await browser.wait(EC.presenceOf(landingPL.addToolExampleButtons.get(rowNo - 1)), browser.params.cooldown.small, `No add example button`);
    await landingPL.addToolExampleButtons.get(rowNo - 1).click();
  }

  static async editToolExampleText(rowNo: number, text: string) {
    await landingPL.toolExampleInputs.get(rowNo - 1).sendKeys(text);
  }

  static async addToolExample(rNum: number, text: string) {
    await this.clickAddToolExample(rNum);
    await this.editToolExampleText(rNum, text);
  }

  static async deleteEducation(rNum: number) {
    await browser.wait(EC.presenceOf(landingPL.delEduButtons.get(rNum - 1)), browser.params.cooldown.small, `No delete education button`);
    await landingPL.delEduButtons.get(rNum - 1).click();
  }

  static async deleteExperience(rNum: number) {
    await browser.wait(EC.presenceOf(landingPL.delExpButtons.get(rNum - 1)), browser.params.cooldown.small, `No delete experience button`);
    await landingPL.delExpButtons.get(rNum - 1).click();
  }

  static async deleteAllEducation() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.delEduButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.delEduButtons.get(i - 1).click();
      }
    });
  }

  static async deleteAllExperience() {
    await spinner.waitUntilSpinnerOff();
    await browser.executeScript(`window.scrollTo(0,document.body.scrollHeight)`);
    await landingPL.delExpButtons.count().then(async (am) => {
      for (var i = am; i > 0; i--) {
        await landingPL.delExpButtons.get(i - 1).click();
      }
    });
  }

  static async deleteAllEduNExp() {
    await this.deleteAllEducation();
    await this.deleteAllExperience();
  }

  static async clickAddEducationButton() {
    await browser.wait(EC.presenceOf(landingPL.addEducationButton), browser.params.cooldown.small, `No add education button`);
    await landingPL.addEducationButton.click();
  }

  static async addAllEducation() {
    await this.clickAddEducationButton();
    await landingPL.eeListItems.each(async (item) => {
      await item.click();
    });
  }

  static async clickAddExperienceButton() {
    await browser.wait(EC.presenceOf(landingPL.addExperienceButton), browser.params.cooldown.small, `No add experience button`);
    await landingPL.addExperienceButton.click();
  }

  static async addAllExperience() {
    await this.clickAddExperienceButton();
    await landingPL.eeListItems.each(async (item) => {
      await item.click();
    });
  }

  static async editEducationName(rNum: number, text: string) {
    await browser.wait(EC.presenceOf(landingPL.inputEduNames.get(rNum - 1)), browser.params.cooldown.small, `No name field`);
    await landingPL.inputEduNames.get(rNum - 1).clear();
    await landingPL.inputEduNames.get(rNum - 1).sendKeys(text);
  }

  static async editExperienceName(rNum: number, text: string) {
    await browser.wait(EC.presenceOf(landingPL.inputExpNames.get(rNum - 1)), browser.params.cooldown.small, `No name field`);
    await landingPL.inputExpNames.get(rNum - 1).clear();
    await landingPL.inputExpNames.get(rNum - 1).sendKeys(text);
  }

  static async clickRegionDropdown() {
    await browser.wait(EC.presenceOf(landingPL.regionDropdown), browser.params.cooldown.small, `No region dropdown sliders`);
    await spinner.waitUntilSpinnerOff();
    await landingPL.regionDropdown.click();
  }

  static async clickRegionDropdownItem(rNum: number) {
    await browser.wait(EC.presenceOf(landingPL.regionDropdownItems.get(rNum - 1)), browser.params.cooldown.small, `No region dropdown sliders`);
    await spinner.waitUntilSpinnerOff();
    await landingPL.regionDropdownItems.get(rNum - 1).click();
  }

  static async getRegionDropdownItemText(rNum: number) {
    await browser.wait(EC.presenceOf(landingPL.regionDropdownItems.get(rNum - 1)), browser.params.cooldown.small, `No region dropdown sliders`);
    await spinner.waitUntilSpinnerOff();
    return await landingPL.regionDropdownItems.get(rNum - 1).getText();
  }

  static async editRegion(iNum: number) {
    await this.setGlobalRadio(2);
    await this.clickRegionDropdown();
    const regionName = await this.getRegionDropdownItemText(iNum);
    await this.clickRegionDropdownItem(iNum);
    return regionName;
  }

  static async setRadioButton(rNum: number) {
    await browser.wait(EC.presenceOf(landingPL.radioButtons.get(rNum - 1)), browser.params.cooldown.small / 20, `Landing page / edit details: no radio button`);
    await landingPL.radioButtons.get(rNum - 1).click();
  }

  static async setGlobalRoleCheckbox() {
    await browser.wait(EC.presenceOf(landingPL.globalRoleCheckbox), browser.params.cooldown.medium, `Landing page / edit details: no global radio button`);
    await landingPL.globalRoleCheckbox.click();
  }

  static async setGlobalRadio(p: number) {
    try {
      await this.setRadioButton(p);
    }
    catch {
      await this.setGlobalRoleCheckbox();
      await this.setRadioButton(p);
    }
  }

  static async setAllUCPSlidersMin() {
    await browser.wait(EC.presenceOf(landingPL.inputUCPSliders.get(0)), browser.params.cooldown.small, `No sliders`);
    await landingPL.inputUCPSliders.count().then(async (am) => {
      for (let i = 0; i < am; i++) {
        await landingPL.inputUCPSliders.get(i).getSize().then(async (a) => {
          await browser.actions().dragAndDrop(landingPL.inputUCPSliders.get(i), { x: -a.width / 2, y: 0 }).perform();
        });
        await spinner.delay(250);
      }
    });
  }

  static async setAllUCPSlidersMax() {
    await browser.wait(EC.presenceOf(landingPL.inputUCPSliders.get(0)), browser.params.cooldown.small, `No sliders`);
    await landingPL.inputUCPSliders.count().then(async (am) => {
      for (let i = 0; i < am; i++) {
        await landingPL.inputUCPSliders.get(i).getSize().then(async (a) => {
          await browser.actions().dragAndDrop(landingPL.inputUCPSliders.get(i), { x: a.width / 2, y: 0 }).perform();
        });
        await spinner.delay(250);
      }
    });
  }

  static async clickProfileLevelDropdown() {
    await browser.wait(EC.presenceOf(landingPL.levelDropdowns.get(0)), browser.params.cooldown.medium, `Dropdowns didn't load`)
    await landingPL.levelDropdowns.get(0).click();
  }

  static async clickProfileSubLevelDropdown() {
    await browser.wait(EC.presenceOf(landingPL.levelDropdowns.get(1)), browser.params.cooldown.medium, `Dropdowns didn't load`)
    await landingPL.levelDropdowns.get(1).click();
  }

  static async clickDropdownItem(iNum: number) {
    await landingPL.dropdownItems.get(iNum - 1).click();
  }

  static async clickDropdownItemByName(text: string) {
    await landingPL.dropdownItems.count().then(async (am) => {
      for (let i = 0; i < am; i++) {
        if (await landingPL.dropdownItems.get(i).getText() == text) {
          await landingPL.dropdownItems.get(i).click();
          break;
        };
      };
    });
  }

  static async setProfileLevel(dNum: number) {
    await this.clickProfileLevelDropdown();
    const profileLevel = await landingPL.dropdownItems.get(dNum - 1).getText().then((text: string) => { return text; });
    await this.clickDropdownItem(dNum);
    return profileLevel;
  }


  static async setProfileLevelByName(name: string) {
    await this.clickProfileLevelDropdown();
    await this.clickDropdownItemByName(name);
  }

  static async setProfileSubLevelByName(name: string) {
    await this.clickProfileSubLevelDropdown();
    await this.clickDropdownItemByName(name);
    await this.clickNoticeOkButton();
  }

  static async setProfileSubLevel(dNum: number) {
    await this.clickProfileSubLevelDropdown();
    const profileSubLevel = await landingPL.dropdownItems.get(dNum - 1).getText().then((text: string) => { return text; });
    await this.clickDropdownItem(dNum);
    await this.clickNoticeOkButton();
    return profileSubLevel;
  }

  static async clickNoticeOkButton() {
    try {
      await browser.wait(EC.visibilityOf(landingPL.closeNoticeButton), browser.params.cooldown.small / 10, `Notice: no button`);
      await landingPL.closeNoticeButton.click();
    } catch{ }
  }

  static async clickProfileFunctionDropdown() {
    await browser.wait(EC.presenceOf(landingPL.functionDropdowns.get(0)), browser.params.cooldown.medium, `Dropdowns didn't load`)
    await landingPL.functionDropdowns.get(0).click();
  }

  static async clickProfileSubFunctionDropdown() {
    await browser.wait(EC.presenceOf(landingPL.functionDropdowns.get(1)), browser.params.cooldown.medium, `Dropdowns didn't load`)
    await landingPL.functionDropdowns.get(1).click();
    await this.clickNoticeOkButton();
  }

  static async setProfileFunction(dNum: number) {
    await this.clickProfileFunctionDropdown();
    const profileFunction = await landingPL.dropdownItems.get(dNum - 1).getText().then((text: string) => { return text; });
    await this.clickDropdownItem(dNum);
    return profileFunction;
  }

  static async setProfileFunctionByName(text: string) {
    await this.clickProfileFunctionDropdown();
    for (let i = 0; i < await landingPL.dropdownItems.count(); i++) {
      let t = await landingPL.dropdownItems.get(i).getText();
      if (t.indexOf(text) !== -1) {
        await landingPL.dropdownItems.get(i).click();
        break;
      }
    }
  }

  static async setProfileSubFunction(dNum: number) {
    await this.clickProfileSubFunctionDropdown();
    const profileSubFunction = await landingPL.dropdownItems.get(dNum - 1).getText().then((text: string) => { return text; });
    await this.clickDropdownItem(dNum);
    return profileSubFunction;
  }

  static async setProfileSubFunctionByName(text: string) {
    await this.clickProfileSubFunctionDropdown();
    for (let i = 0; i < await landingPL.dropdownItems.count(); i++) {
      let t = await landingPL.dropdownItems.get(i).getText();
      if (t.indexOf(text) !== -1) {
        await landingPL.dropdownItems.get(i).click();
        break;
      }
    }
  }

  static async editProfileDescription(text: string) {
    await browser.wait(EC.presenceOf(landingPL.profileDescription), browser.params.cooldown.medium, `Textarea didn't show`)
    await landingPL.profileDescription.clear();
    await landingPL.profileDescription.sendKeys(text);
  }

  static async confirmUpdate(n: number) {
    try {
      await landingPL.confirmUpdate.click();
      await spinner.waitUntilSpinnerOff();
    }
    catch{ };
  };


  static async clickRegionDropdownItemByName(name: string) {
    await spinner.waitUntilSpinnerOff();
    await landingPL.dropdownItemNames.count().then(async (am) => {
      for (let i = 0; i < am; i = i + 1) {
        if (await landingPL.dropdownItemNames.get(i).getText() == name) {
          await landingPL.dropdownItemNames.get(i).click();
          break;
        }
      };
    });
  }

  static async editRegionByName(name: string) {
    console.log(name);
    if (name.indexOf(`Global`) == -1) {
      await landingPL.radioButtons.get(1).click();
      await this.clickRegionDropdown();
      await this.clickRegionDropdownItemByName(name);
      await spinner.waitUntilSpinnerOff();
    } else {
      await landingPL.radioButtons.get(0).click();
    }
  }

  static async setCulture(set) {
    let t = set.sort((a, b) => a[1] - b[1]);
    // console.log(`Culture Settings:`);
    // t.forEach((a) => { console.log(`   '${a[0]}' = ${a[1]}`); });
    for (let i = 0; i < t.length; i = i + 1) {
      const allCultures = element.all(by.css(`.cursor-move div.card`));
      await allCultures.count().then(async (am) => {
        for (let j = 0; j < am; j = j + 1) {
          let k = j + 1;
          const cultureText = await element(by.css(`.cursor-move div.card:nth-child(${k}) .competency-text`)).getText().then((t: string) => { return t; });
          const dragHandle = await element(by.css(`.cursor-move div.card:nth-child(${k}) .drag-handle`));
          if (cultureText == t[i][0]) {
            await dragHandle.getSize().then(async (a) => {
              await browser.actions().dragAndDrop(dragHandle, { x: 0, y: a.height * (am - k) + 10 }).mouseUp().perform();
            });
            break;
          };
        };
      });
    };
  }

  static async setUCPSlider(sI: number, sV: number) {
    await browser.wait(EC.presenceOf(landingPL.inputUCPSliders.get(sI)), browser.params.cooldown.medium, `No sliders`);

    let v1 = (sV - 1) % 0.2;
    let v2 = sV - 1 - v1;
    await landingPL.inputUCPSliders.get(sI).getSize().then(async (a) => {
      let xV = (v2 / 4 * a.width) - (a.width / 2);
      await browser.actions().dragAndDrop(landingPL.inputUCPSliders.get(sI), { x: xV, y: 0 }).perform();
      await spinner.delay(50);
    });

    await browser.executeScript('window.scrollTo(0,20)');
    let k = 0;
    while (parseFloat(await landingPL.sliderValues.get(sI).getText()) != sV && k < 20) {
      await landingPL.addUCPButtons.get(sI).click();
      k++;
    }

    await expect(parseFloat(await landingPL.sliderValues.get(sI).getText()) == sV).toBeTruthy(`Wrong slider setting`);
  }

  static async setCongnitiveAbilities(v1: number, v2: number, v3: number) {
    await this.setCongnitiveAbility(1, v1);
    await this.setCongnitiveAbility(2, v2);
    await this.setCongnitiveAbility(3, v3);
    await spinner.waitUntilSpinnerOff();
  }

  static async setCongnitiveAbility(row: number, v: number) {
    if (row == 1) {
      await browser.wait(EC.elementToBeClickable(landingPL.numericalAssesment.get(v)), browser.params.cooldown.small, `Assesment radio buttons not clickable`)
      await landingPL.numericalAssesment.get(v).click();
    }
    if (row == 2) {
      await browser.wait(EC.elementToBeClickable(landingPL.verbalAssesment.get(v)), browser.params.cooldown.small, `Assesment radio buttons not clickable`)
      await landingPL.verbalAssesment.get(v).click();
    }
    if (row == 3) {
      await browser.wait(EC.elementToBeClickable(landingPL.checkingAssesment.get(v)), browser.params.cooldown.small, `Assesment radio buttons not clickable`)
      await landingPL.checkingAssesment.get(v).click();
    }
  }

  static async clickDoneSetUCPSliders() {
    await browser.wait(EC.elementToBeClickable(landingPL.doneSetUCPSlidersButton.get(1)), browser.params.cooldown.small, `Edit identity page: Done button isn't clickable`);
    await landingPL.doneSetUCPSlidersButton.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async allRespLVLUP(a: number) {
    await spinner.waitUntilSpinnerOff();
    await landingPL.respLVLUPButtons.count().then(async (am) => {
      for (let i = 0; i < am; i++) {
        await this.responsibilityLVLUP(i + 1, a);
      }
    });
  }

  static async allRespLVLDOWN(a: number) {
    await spinner.waitUntilSpinnerOff();
    await landingPL.respLVLDOWNButtons.count().then(async (am) => {
      for (let i = 0; i < am; i++) {
        await this.responsibilityLVLDOWN(i + 1, a);
      }
    });
  }

  static async responsibilityLVLUP(rowN: number, am: number) {
    await browser.wait(EC.visibilityOf(landingPL.respLVLUPButtons.get(rowN - 1)), browser.params.cooldown.small, `No plus level button`);
    for (let i = 0; i < am; i++) { await landingPL.respLVLUPButtons.get(rowN - 1).click(); }
  }

  static async responsibilityLVLDOWN(rowN: number, am: number) {
    await browser.wait(EC.visibilityOf(landingPL.respLVLDOWNButtons.get(rowN - 1)), browser.params.cooldown.small, `No minus level button`);
    for (let i = 0; i < am; i++) { await landingPL.respLVLDOWNButtons.get(rowN - 1).click(); }
  }

  static async setPMTSlidersMIN() {
    await browser.wait(EC.presenceOf(landingPL.sliders.get(0)), browser.params.cooldown.small, `Can't find PMT sliders`);
    await landingPL.sliders.each(async (e) => {
      await e.getSize().then(async (a) => {
        await browser.actions().dragAndDrop(e, { x: -a.width, y: 0 }).perform();
        await spinner.delay(250);
      });
    });
  }

  static async setPMTSlidersMAX() {
    await browser.wait(EC.presenceOf(landingPL.sliders.get(0)), browser.params.cooldown.small, `Can't find PMT sliders`);
    await landingPL.sliders.each(async (e) => {
      await e.getSize().then(async (a) => {
        await browser.actions().dragAndDrop(e, { x: a.width, y: 0 }).perform();
        await spinner.delay(250);
      });
    });
  }

  static async changeProfileName(profileName: string) {
    await this.selectEditPageItem(1);
    await this.editProfileTitle(profileName);
    await this.clickNextButton();
  }

  static async recommendationsDone() {
    try {
      await landingPL.recomendationsDoneButton.get(0).click();
      await spinner.waitUntilSpinnerOff();
    }
    catch{ }
  }

}