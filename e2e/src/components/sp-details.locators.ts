import { by, element } from 'protractor';

export class SuccessProfileDetailsPageLocators {
  static reportLinks = element.all(by.css(`div.reports-row a`));
  static magnifiers = element.all(by.css(`kf-icon.zoom-icon`));

  static actionsMenu = element(by.css(`button.menu-action-btn`));
  static actionMenuItems = element.all(by.css(`.action-dropdown .dropdown-items div`));

  static selectViewMenu = element(by.css(`button.menu-select-btn`));
  static selectViewMenuItems = element.all(by.css(`.select-dropdown .dropdown-items div`));

  static submitDeleteButton = element(by.css(`button:nth-child(2) span.weight-heavy`));

  static confirmPMTUpdate = element(by.css(`.slider-footer div.custom-footer-content.slider-content > button:nth-child(2)`));
  static PMTbtns = element.all(by.css(`footer .button`));

  static topTraits = element.all(by.css(`.top-section.traits .single-top-competency`));
  static topDrivers = element.all(by.css(`.section.top-section:not(.traits):not(.ng-star-inserted) .single-top-competency`));

  static numericalAbility = element(by.css(`.ability:nth-child(1) kf-icon.kf-icon-check-line`))
  static verbalAbility = element(by.css(`.ability:nth-child(2) kf-icon.kf-icon-check-line`))
  static checkingAbility = element(by.css(`.ability:nth-child(3) kf-icon.kf-icon-check-line`))
  static numericalAbilityChecked = element(by.css(`.ability:nth-child(1) kf-icon.kf-icon-check-solid`))
  static verbalAbilityChecked = element(by.css(`.ability:nth-child(2) kf-icon.kf-icon-check-solid`))
  static checkingAbilityChecked = element(by.css(`.ability:nth-child(3) kf-icon.kf-icon-check-solid`))

  static profileName = element(by.css(`.profile-header h1.title`));
  static regionName = element(by.css(`.profile-info .info-row.role-location span.info-value`));
  static profileType = element.all(by.css(`.profile-info .info-row.profile-type .info-value span`));
  static levelInfo = element.all(by.css(`.profile-info .info-row.level .info-value span`));
  static functionInfo = element.all(by.css(`.profile-info .info-row.function .info-value span`));
  static gradeInfo = element(by.css(`.profile-info .info-row.grade .info-value`));
  static profileDescription = element(by.css(`h4.profile-desc`));
  static shortProfileInfo = element(by.css(`.profile-info .info-row.short-profile .info-value span`));

  static downloadButtons = element(by.css(`.ui-sidebar-active .footer-right button`));

  static traits = element.all(by.css(`.traits .trait`));
  static drivers = element.all(by.css(`kf-chart-hex text`));

  static profileDetailsText = element.all(by.css(`span.details`));

  static allCompetencies = element.all(by.css(`.skills-card kf-chart li`));

  static projectType = element.all(by.css(`.option .radio`));
  static projectTypeHeader = element(by.css(`.project-type div.header`));
  static createProjectDialogueButtons = element.all(by.css(`.project-type-footer button`));

  static detailResps = element.all(by.css(`.responsibility-card .item h5`));
  static detailRespDescs = element.all(by.css(`.responsibility-card .item p`));
  static detailRespCategoties = element.all(by.css(`.treemap-captions span.kf-text`));
  static detailTasks = element.all(by.css(`.tasks .task span`));
  static detailTools = element.all(by.css(`.tools .tile span`));
  static detailTSkills = element.all(by.css(`.techs .tile span`));
  static detailsTCs = element.all(by.css(`.technical-comps li`));
  static detailsBCs = element.all(by.css(`.behavioral-comps li`));
  static detailEducation = element(by.css(`.md .education span.weight-heavy`));
  static detailEduDesc = element.all(by.css(`.md .education span`)).get(1);
  static detailExperinces = element.all(by.css(`.md .experience> div > span:not(.description)`));
  static detailExpDescs = element.all(by.css(`.md .experience> div span.description`));

}