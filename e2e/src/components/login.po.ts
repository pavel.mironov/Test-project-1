import { browser, ExpectedConditions as EC } from 'protractor';
import { LoginPageLocators as loginPL } from './login.locators';
import { SpinnerPO as spinner } from './spinner';

export class LoginPageOblject {
  static async login(user: string, password: string) {
    await browser.wait(
      EC.presenceOf(loginPL.emailInput),
      browser.params.cooldown.medium,
      `Login page loading fails`);

    await loginPL.emailInput.clear();
    await loginPL.emailInput.sendKeys(user);
    await loginPL.passwordInput.clear();
    await loginPL.passwordInput.sendKeys(password);

    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.elementToBeClickable(loginPL.submitButton),
      browser.params.cooldown.medium,
      `Submit button on login page isn't clickable`);
    await loginPL.submitButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async logout() {
    await browser.wait(
      EC.elementToBeClickable(loginPL.profileIcon),
      browser.params.cooldown.medium,
      `Profile Icon isn't clickable`
    )
    await spinner.waitUntilSpinnerOff();
    await loginPL.profileIcon.click();
    await spinner.waitUntilSpinnerOff();

    await browser.wait(
      EC.elementToBeClickable(loginPL.profileMenuItems.get(1)),
      browser.params.cooldown.medium,
      `Logout menu item isn't clickable`);
    await loginPL.profileMenuItems.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }
}