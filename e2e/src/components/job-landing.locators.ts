import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';

export class JobLandingLocators {
  static editButtons = element.all(by.css(`.section .edit-btn .button`));

  static detailsName = element.all(by.css(`input`)).get(0);
  static detailsDescription = element(by.css(`textarea`));
  static detailsCode = element.all(by.css(`input`)).get(1);
  static detailsLevelFuncDDs = element.all(by.css(`.level-func-container kf-dropdown .dropdown`));
  static detailsPropDDs = element.all(by.css(`.job-properties-container kf-dropdown .dropdown`));
  static dropdownItems = element.all(by.css(`.items.open kf-dropdown-item`));
  static dropdownItemNames = element.all(by.css(`.items.open kf-dropdown-item span`));
  static actionButtons = element.all(by.css(`.wizard-custom-actions .button`));

  static editActionButtons = element.all(by.css(`.ui-sidebar-active.ui-sidebar-bottom div.slider-footer button`));

  static removeRowButtons = element.all(by.css(`.delete-row`));
  static addRespButton = element(by.css(`.add-competency-button span`));
  static notDisabledRespsInList = element.all(by.css(`.add-custom-competencies-listitem:not(.disabled)`));
  static actionButtonsInList = element.all(by.css(`.add-custom-competency-actions .button`));

  static respNameInputs = element.all(by.css(`.edit-mode input`));
  static confirmPMTButtons = element.all(by.css(`.slider-footer div.custom-footer-content.slider-content > button`));

  static deleteTaskButtons = element.all(by.css(`tr > td.delete-row > kf-icon`));
  static submitTaskDelete = element.all(by.css(`footer button:nth-child(2)`));
  static addTaskButton = element(by.css(`.add-task-button`));
  static taskTypeDropdowns = element.all(by.css(`tr .dropdown`));
  static typeDropdownItems = element.all(by.css(`.open kf-dropdown-item`));
  static taskTextFields = element.all(by.css(`tr textarea`));

  static saveButton = element(by.css(`div.right button:nth-child(2)`));
  static recomendationsDoneButton = element.all(by.css(`.recommendations .button`));

  static deleteBCButtons = element.all(by.css(`tr td.delete`));
  static addBCButton = element(by.css(`.add-competency-button`));
  static bcDropdownItems = element.all(by.css('.ui-sidebar-active #listbox div:not(.disabled)'));
  static submitAddBCButton = element(by.css(`.ui-sidebar-active div.slider-body button:nth-child(2)`));
  static inputBCNames = element.all(by.css(`tr > td input`));

  static deleteTCButtons = element.all(by.css(`tr td.delete kf-icon`));
  static addTCButton = element(by.css(`.add-competency-button`));
  static tcDropdownItems = element.all(by.css('.ui-sidebar-active #listbox div:not(.disabled)'));
  static submitAddTCButton = element(by.css(`.ui-sidebar-active div.slider-body button:nth-child(2)`));
  static inputTCNames = element.all(by.css(`tr > td.competency-col input`));
  static deleteTCSkills = element.all(by.css(`kf-sp-draggable-skills-container  kf-chip > kf-icon`));
  static addTCSkills = element.all(by.css(`.add-skills`));
  static addSkillSearch = element.all(by.css(`.search-input input`));
  static addTCSkillsItems = element.all(by.css(`.items > .item`));
  static confirmAddSkillButtons = element.all(by.css(`.add-custom-skill-actions button`));
  
  static deleteToolButtons = element.all(by.css(`tr > td.delete-row > kf-icon`));
  static submitDelToolButton = element.all(by.css(`footer button:nth-child(2)`));
  static addToolButton = element(by.css(`.add-commo-button`));
  static toolNameInputs = element.all(by.css(`.tech-tool input`));
  static addToolExampleButtons = element.all(by.css(`tr span.add-example`));
  static toolExampleInputs = element.all(by.css(`.add-example input`));

  static delEduButtons = element.all(by.css(`.education-editor-container tr .delete-row`));
  static delExpButtons = element.all(by.css(`.experience-editor-container tr .delete-row`));
  static addEducationButton = element(by.css(`.education-editor-container .add-competency-button`));
  static addExperienceButton = element(by.css(`.experience-editor-container .add-competency-button`));
  static eeListItems = element.all(by.css(`li div.ui-chkbox`));
  static inputEduNames = element.all(by.css(`.education-editor-container tr input`));
  static inputExpNames = element.all(by.css(`.experience-editor-container tr input`));

  static landingDetails = element.all(by.css(`kfthcl-sp-edit-landing-section:nth-child(1) .summary > div > div.ng-star-inserted`));
  static landingAccountability = element.all(by.css(`kfthcl-sp-edit-landing-section:nth-child(2) .summary > div > div.ng-star-inserted`));
  static landingCapability = element.all(by.css(`kfthcl-sp-edit-landing-section:nth-child(3) .summary > div > div.ng-star-inserted`));
  static landingIdentity = element.all(by.css(`kfthcl-sp-edit-landing-section:nth-child(4) .summary > div > div.ng-star-inserted`));
}