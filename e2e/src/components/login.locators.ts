import { by, element } from 'protractor';

export class LoginPageLocators {
  static emailInput = element(by.css(`input[type=email]`));
  static passwordInput = element(by.css(`input[type=password]`));
  static submitButton = element(by.css(`button.submitButton`));

  static profileIcon = element(by.css(`.global-header .header .kf-icon-profile`));
  static profileMenuItems = element.all(by.css(`.profile-menu li`));
}