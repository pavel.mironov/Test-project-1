import { by, element } from 'protractor';

export class OrgDataLocators {
  static addSubOrgBtn = element(by.css(`.org-data-header a.org-add-subsidiary`));

  static subOrgName = element(by.css(`.name input`));
  static currencyDropdown = element(by.css(`.currency .dropdown button span`));
  static revenueInput = element(by.css(`.revenue input`));
  static revenueAddInfo = element(by.css(`.revenue .additional-info`));
  static employeesInput = element(by.css(`.employees input`));

  static openedDDItems = element.all(by.css(`.open .item`));

  static scopeDropdown = element(by.css(`.scope .dropdown button span`));
  static governanceDropdown = element(by.css(`.governance .dropdown button span`));

  static industryDropdown = element(by.css(`.industry .dropdown button span`));
  static segmentDropdown = element(by.css(`.segment .dropdown button span`));
  static revenueDropdown = element(by.css(`.percentage .dropdown button span`));
  static subCountryDropdowns = element.all(by.css(`.subCountry .dropdown button span`));

  static addCountryBtn = element(by.css(`.organization-add-country`));
  static deleteCountryBtns = element.all(by.css(`.organization-custom-set .kf-icon-close`));

  static countryNameDDs = element.all(by.css(`.organization-custom-set .subCountry button span`));
  static countryMapDDs = element.all(by.css(`.organization-custom-set .mappedOrg button span`));
  static countryRevenueInputs = element.all(by.css(`.organization-custom-set .subRevenue input`));
  static countryCurrencyDDs = element.all(by.css(`.organization-custom-set .subCurrency button span`));
  static countryHeadcountInputs = element.all(by.css(`.organization-custom-set .subEmployees input`));

  static footerBtns = element.all(by.css(`.ui-sidebar-active .slider-footer button`));
  static dialogFooterBtns = element.all(by.css(`.ui-dialog-draggable footer button`));

  static subListNames = element.all(by.css(`.wrapper .header .title`));

  static threeDotsMenu = element(by.css(`.open .menu-subsidiary`));
  static threeDotsMenuOptions = element.all(by.css(`.open .popup-content li`));

  static expandedSubData = element.all(by.css(`.org-data-container .open .org-data-item > span:nth-child(2)`));
  static expandedData = element.all(by.css(`.open .org-data-item > span:nth-child(2)`));

  static editBtns = element.all(by.css(`.wrapper.selected .header a`));

  static globalAssetsInput = element(by.css(`.assets input`));
  static marketCapitalizationInput = element(by.css(`.capitalization input`));
  static ownershipStructureDD = element(by.css(`.ownership .dropdown button span`));
  static ownershipTypeDD = element(by.css(`.ownershipType .dropdown button span`));
  static gradingStructureRadioBtns = element.all(by.css(`.gradeStatus div.radio-text label`));
  static gradingAcrossAllRadioBtns = element.all(by.css(`.gradeCountry div.radio-text label`));

}