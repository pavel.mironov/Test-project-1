import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';

export class JobDetailsLocators {
  static title = element(by.css(`.profile-header .title`));
  static code = element(by.css(`.info-row:not(.function):not(.level):not(.role-location) .info-value`));
  static level = element(by.css(`.info-row.level .info-value span:nth-child(1)`));
  static subLevel = element(by.css(`.info-row.level .info-value span:nth-child(2)`));
  static function = element(by.css(`.info-row.function .info-value span:nth-child(1)`));
  static subFunction = element(by.css(`.info-row.function .info-value span:nth-child(2)`));
  static detailGrade = element.all(by.css(`.info-row.grade .info-value span`));

  static grade = element(by.css(`.job-evaluation-blocks:nth-child(1) h4.desc-position-bottom`));
  static shortProfile = element(by.css(`.job-evaluation-blocks:nth-child(2) h4.desc-position-bottom`));
  static hayPoints = element(by.css(`.job-evaluation-blocks:nth-child(3) h4.desc-position-bottom`));

  static resps = element.all(by.css(`.responsibility-list-card .sub-category h5`));
  static BC = element.all(by.css(`.behavioral-comps li`));
  static TC = element.all(by.css(`.technical-comps li`));

  static actionsMenuButton = element(by.css(`button.menu-action-btn`));
  static actionsMenuItems = element.all(by.css(`.dropdown-items div`));
  static deleteJobButtons = element.all(by.css(`.ui-dialog-content .button`));

  static jobSummaryReportLink = element(by.css(`.reports-row a`));

  static jeActions = element.all(by.css(`.competency-detail-slider .custom-footer-content .button`));
  static jeConfirmActions = element.all(by.css(`footer .button`));

  
}