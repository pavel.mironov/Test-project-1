const fs = require('fs');

export function deleteFileIfExists(path) {
  if (fs.existsSync(path)) {
    fs.unlinkSync(path);
  }
}

export function getFileStat(path) {
  return fs.statSync(path);
}

export async function checkFileStatWithDelay(path, size, timeDelay) {
  try {
    fs.statSync(path);
    let i = timeDelay;
    while (i > 0) {
      await delay(1000);
      if (await fs.statSync(path).size > size) { return true; }
      i = i - 1;
    }
  }
  catch{
    return false;
  }
}

export async function checkFileExists(path) {
  if (await fs.existsSync(path)) {
    return fs.existsSync(path);
  } else {
    return false;
  }
}

function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export async function checkFileExistsWithDelay(path, timeDelay) {
  let i = timeDelay;
  while (i > 0) {
    await delay(1000);
    if (await fs.existsSync(path)) { return fs.existsSync(path); }
    i = i - 1;
  }
  return false;
}

export function getCSVFileDataT(path) {
  const csv = fs.readFileSync(path, 'utf8');
  var allTextLines = csv.split(/\r\n|\n/);
  var lines = [];
  for (var i = 0; i < allTextLines.length; i++) {
    var data = allTextLines[i].split(',');
    var tarr = [];
    for (var j = 0; j < data.length; j++) {
      tarr.push(data[j]);
    }
    lines.push(tarr);
  }
  return lines;
}

export function getCSVFileData(path) {
  const csv = fs.readFileSync(path, 'utf8');
  var allTextLines = csv.split(/\r\n|\n/);
  var lines = [];
  for (var j = 0; j < allTextLines[0].split(',').length; j++) {
    var tarr = [];
    for (var i = 0; i < allTextLines.length; i++) {
      var data = allTextLines[i].split(',');
      tarr.push(data[j]);
    }
    lines.push(tarr);
  }
  return lines;
}

export function getRegionSettings(i: number, line) {
  if (line[i] == `Global`) return [line[i], `1`];
  else return [line[i], `2`];
}

export function getDetailSettings(ar) {
  if (ar[2] != ``) {
    const r = ar[2].split(`_`);
    return [
      parseInt(r[1], 10),
      parseInt(r[2], 10)
    ];
  } else {
    if (ar[1] != ``) {
      const r2 = ar[1].split(`_`);
      return [
        parseInt(r2[1], 10),
        parseInt(r2[2], 10)
      ];
    } else {
      const r3 = ar[0].split(`_`);
      return [
        parseInt(r3[1], 10),
        1
      ];
    }
  }
}

export function getTraitsWithValues(s: number, e: number, lines) {
  let topTraits = [];
  for (let j = s; j < e; j = j + 2)  topTraits.push([lines[1][j], lines[1][j + 1]]);

  let topTraitsValues = [];
  topTraits.forEach((a) => {
    for (let i = 0; i < lines[0].length; i = i + 1) {
      if (lines[0][i] == a[0]) {
        topTraitsValues.push([a[1], lines[1][i]]);
        break;
      };
    };
  });
  return topTraitsValues;
}

export function getTraits(s: number, e: number, lines) {
  let topTraits = [];
  for (let j = s; j < e; j = j + 2)  topTraits.push(lines[1][j + 1]);
  return topTraits;
}

export function getDriversWithValues(s: number, e: number, lines) {
  let topDrivers = [];
  for (let j = s; j < e; j = j + 2)  topDrivers.push([lines[1][j], lines[1][j + 1]]);

  let topDriversValues = [];
  topDrivers.forEach((a) => {
    for (let i = 0; i < lines[0].length; i = i + 1) {
      if (lines[0][i] == a[0]) {
        topDriversValues.push([a[1], lines[1][i]]);
        break;
      };
    };
  });
  return topDriversValues;
}

export function getDrivers(s: number, e: number, lines) {
  let topDrivers = [];
  for (let j = s; j < e; j = j + 2)  topDrivers.push(lines[1][j + 1]);
  return topDrivers;
}