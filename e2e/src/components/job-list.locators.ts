import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';

export class JobListLocators {
  static jobListTab = element.all(by.css(`.second-level .menu-item a`));
  static addNewJobButton = element(by.css(`.add-new-job p`));
  static searchField = element(by.css(`.search-control input`));
  static useThisBtns = element.all(by.css(`.use-profile-btn`));
  static useSelectedProfileBtn = element(by.css(`.next-btn .button`));

  static customizeJobInputs = element.all(by.css(`.ui-dialog-content input`));
  static customizeJobBtns = element.all(by.css(`.ui-dialog-content .button`));
  static customizeJobDropdown = element.all(by.css(`.add-job-right-col .dropdown`));
  static customizeJobDropdownE = element.all(by.css(`.dropdown`));
  static customizeJobDropdownItems = element.all(by.css(`.open .item`));  

  static addListNames = element.all(by.css(`.job-name h4`));
  static jobListNames = element.all(by.css(`a.job-name`));
  static jobListCode = element.all(by.css(`tbody tr td:nth-child(4) span`));
  static jobListFunction = element.all(by.css(`tbody tr td:nth-child(5) span`));
  static jobListHP = element.all(by.css(`tbody tr td:nth-child(6) span`));
  static jobListGrade = element.all(by.css(`tbody tr td:nth-child(7) span`));

  static filterTitle = element(by.css(`div.filter-title div`));
  static filterDropdown = element(by.css(`.mutli-type-filter button`));

  static filterSections = element.all(by.css(`.section .section-header`));
  static filterItemNames = element.all(by.css(`.section .item span`));
  static filterGradeSets = element.all(by.css(`.items .section-header .wrapped.kf-text`));
  static filterGradeSetCheckbox = element.all(by.css(`.items .section-header .checkbox`));
  static filterSectionSearch = element(by.css(`.section .searchDropDown input`));
  static filterItemCheckboxes = element.all(by.css(`.section .item .checkbox`));
  static filterInputs = element.all(by.css(`.section input`));
  static filterFooterButtons = element.all(by.css(`footer .button`));
  static filterCalendarArrows = element.all(by.css(`header .arrow`));
  static filterCalendarHeader = element(by.css(`header span`));
  static filterCalendarActiveDates = element.all(by.css(`.card td:not(.disabled)`));

  static viewBtns = element.all(by.css(`.view-selector .sel-btn`));
  static matrix = element(by.css(`.subsection-data`));
  static matrixItems = element.all(by.css(`.subsection-data div.item`));
  static prevBtn = element(by.css(`.nav-subheader .prev`));

  static popupName = element(by.css(`.popup-data div a`));
  static popupCode = element(by.css(`.popup-data span.job-code`));
  static popupGrade = element.all(by.css(`.popup-data div`)).get(1);
  static popupShortProfile = element.all(by.css(`.popup-data div`)).get(2);
  static popupHayPoints = element.all(by.css(`.popup-data div`)).get(3);

  static dashboardJobNames = element.all(by.css(`.profiles .wrapper a.title`));
  static dashboardJobCodes = element.all(by.css(`.profiles .wrapper .code`));
  static dashboardJobLevel = element.all(by.css(`.profiles .wrapper div.body > div:nth-child(1)`));
  static dashboardJobFunctions = element.all(by.css(`.profiles .wrapper .job-function-container:nth-child(2)`));
  static dashboardJobSubFunctions = element.all(by.css(`.profiles .wrapper .job-function-container:nth-child(3)`));
  static dashboardJobGrades = element.all(by.css(`.profiles .wrapper .stats-item:nth-child(1) .title`));
  static dashboardJobShortProfiles = element.all(by.css(`.profiles .wrapper .stats-item:nth-child(2) .title`));
  static dashboardJobHayPoints = element.all(by.css(`.profiles .wrapper .stats-item:nth-child(3) .title`));

  static recentlyModifiedDropdown = element(by.css(`.dropdown button`));
  static recentlyModifiedDropdownItems = element.all(by.css(`.dropdown .item span`));

  static gradesListNames = element.all(by.css(`.kf-chart-grades li`));
  static gradesListAmounts = element.all(by.css(`.kf-chart-grades text`));

  static searchDropdown = element(by.css(`.search-control-wrapper .dropdown button`));
  static searchDropdownItems = element.all(by.css(`.search-control-wrapper .dropdown .item span`));

  static clearAllFiltersLink = element(by.css(`.applied-filters-well a`));

  static dashboardDownloadListNames = element.all(by.css(`.download-item a:nth-child(1)`));
  static dashboardDownloadListIcons = element.all(by.css(`.download-item a kf-icon`));

  static matrixDownloadButton = element(by.css(`.download a`));

  static compareCheckboxes = element.all(by.css(`tr div.checkbox`));

  static compareAllBtn = element(by.css(`.compare-btn span.button`));
  static removeFromCompareView = element.all(by.css(`.cell-header .close`));
  static addToCompareView = element.all(by.css(`.empty .add-profile a`)); // &
  static addRelatedToCompareView = element.all(by.css(`.empty .dropdown button`)); // &
  static listRelatedProfiles = element.all(by.css(`.empty .items.open kf-dropdown-item`));

  static listCompareRemoveButtons = element.all(by.css(`.sp-card .kf-icon-close`));
  static compareRemoveDialogueActions = element.all(by.css(`footer .button`));

  static comparisonDropdown = element(by.css(`.comparison-dropdown .clickable`));
  static comparisonSections = element.all(by.css(`.section .section-title`));
  static comparisonEmptySections = element.all(by.css(`div.item`));
  static comparisonListItems = element.all(by.css(`.subitem`));

  static footerButtons = element.all(by.css(`.footer-content button`));

  static cellsLeftColumn = element.all(by.css(`td:nth-child(1) span`));
  static cellsSP1 = element.all(by.css(`td:nth-child(2) span`));
  static cellsSP2 = element.all(by.css(`td:nth-child(3) span`));
  static cellsSP3 = element.all(by.css(`td:nth-child(4) span`));
  static cellsSP4 = element.all(by.css(`td:nth-child(5) span`));
  static cellsSP5 = element.all(by.css(`td:nth-child(6) span`));

  static cellDataSP1 = element.all(by.css(`td:nth-child(2) div>div>div`));
  static cellDataSP2 = element.all(by.css(`td:nth-child(3) div>div>div`));
  static cellDataSP3 = element.all(by.css(`td:nth-child(4) div>div>div`));
  static cellDataSP4 = element.all(by.css(`td:nth-child(5) div>div>div`));
  static cellDataSP5 = element.all(by.css(`td:nth-child(6) div>div>div`));

  static jeCellsSP1 = element.all(by.css(`td:nth-child(2) div.cell`));
  static jeCellsSP2 = element.all(by.css(`td:nth-child(3) div.cell`));
  static jeCellsSP3 = element.all(by.css(`td:nth-child(4) div.cell`));
  static jeCellsSP4 = element.all(by.css(`td:nth-child(5) div.cell`));
  static jeCellsSP5 = element.all(by.css(`td:nth-child(6) div.cell`));

  static jeCellsScoresSP1 = element.all(by.css(`td:nth-child(2) div:not(.cell-left)>div>div.kf-text`));
  static jeCellsScoresSP2 = element.all(by.css(`td:nth-child(3) div:not(.cell-left)>div>div.kf-text`));
  static jeCellsScoresSP3 = element.all(by.css(`td:nth-child(4) div:not(.cell-left)>div>div.kf-text`));
  static jeCellsScoresSP4 = element.all(by.css(`td:nth-child(5) div:not(.cell-left)>div>div.kf-text`));
  static jeCellsScoresSP5 = element.all(by.css(`td:nth-child(6) div:not(.cell-left)>div>div.kf-text`));

  static toggleShowDifference = element(by.css(`kf-toggle:nth-child(1) span.kf-toggle`));
  static toggleShowDescription = element(by.css(`kf-toggle:nth-child(2) span.kf-toggle`));
}

