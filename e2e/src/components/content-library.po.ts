import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { ContentLibraryLocators as clPL } from './content-library.locators';
import { SpinnerPO as spinner } from './spinner';


export class ContentLibraryPageObjects {
  static async moveToCL() {
    await browser.wait(EC.elementToBeClickable(clPL.clmIcon), browser.params.cooldown.medium, `No CLM icon`);
    await clPL.clmIcon.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async moveToBC() {
    await browser.wait(EC.elementToBeClickable(clPL.menuItems.get(0)), browser.params.cooldown.medium, `No menu item(BC)`);
    await clPL.menuItems.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickTCMenuItem() {
    await browser.wait(EC.elementToBeClickable(clPL.menuItems.get(1)), browser.params.cooldown.medium, `No menu item(TC)`);
    await clPL.menuItems.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async moveToResps() {
    await browser.wait(EC.elementToBeClickable(clPL.menuItems.get(2)), browser.params.cooldown.medium, `No menu item(Responsibilities)`);
    await clPL.menuItems.get(2).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickFunctionsMenuItem() {
    await browser.wait(EC.elementToBeClickable(clPL.menuItems.get(3)), browser.params.cooldown.medium, `No menu item(Functioms)`);
    await clPL.menuItems.get(3).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async addNewResp() {
    await browser.wait(EC.elementToBeClickable(clPL.addRespButton), browser.params.cooldown.medium, `No add responsibility button`);
    await clPL.addRespButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setRespName(text: string) {
    await browser.wait(EC.presenceOf(clPL.nameInput), browser.params.cooldown.medium, `No input for responsibility name`);
    await clPL.nameInput.clear();
    await clPL.nameInput.sendKeys(text);
  }

  static async openCategoryDropdown() {
    await browser.wait(EC.elementToBeClickable(clPL.categoryDD), browser.params.cooldown.medium, `No category dropdown`);
    await clPL.categoryDD.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setCategory(numInList = 1) {
    await this.openCategoryDropdown();
    await this.clickOpenedDDItem(numInList);
  };

  static async setCategoryByName(title = ``) {
    await this.openCategoryDropdown();
    let i = 1;
    while (1) {
      if (await clPL.openedDDItems.get(i - 1).getText().then((text: string) => { return text; }) == title) {
        await this.clickOpenedDDItem(i);
        break;
      } else {
        i++;
        if (i > await clPL.openedDDItems.count()) {
          await expect(false).toBeTruthy(`There is no Currency(${title}) in list`);
          break;
        }
      }
    }
  };

  static async clickOpenedDDItem(num: number) {
    await browser.wait(EC.elementToBeClickable(clPL.openedDDItems.get(num - 1)), browser.params.cooldown.medium, `No item(${num}) in opened list`);
    await clPL.openedDDItems.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setLevelDefinition(num: number, text: string) {
    await browser.wait(EC.elementToBeClickable(clPL.levelDefinitions.get(num - 1)), browser.params.cooldown.medium, `There is no Level ${num} definition`);
    await clPL.levelDefinitions.get(num - 1).clear();
    await clPL.levelDefinitions.get(num - 1).sendKeys(text);
  };

  static async saveNewResp() {
    await browser.wait(EC.elementToBeClickable(clPL.saveRespButton.get(0)), browser.params.cooldown.medium, `There is no Save button`);
    await clPL.saveRespButton.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async saveEditResp() {
    await browser.wait(EC.elementToBeClickable(clPL.saveRespButton.get(1)), browser.params.cooldown.medium, `There is no Save button`);
    await clPL.saveRespButton.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async searchResp(text: string) {
    await browser.wait(EC.elementToBeClickable(clPL.searchResp), browser.params.cooldown.medium, `There is no Search input`);
    await clPL.searchResp.clear();
    await clPL.searchResp.sendKeys(text, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  };

  static async clearSearchResp() {
    await browser.wait(EC.presenceOf(clPL.searchResp), browser.params.cooldown.medium, `There is no Search input`);
    await clPL.searchResp.clear();
    await clPL.searchResp.sendKeys(`a`, protractor.Key.BACK_SPACE, protractor.Key.ENTER);
    await spinner.waitUntilSpinnerOff();
  };

  static async openRespByName(title: string) {
    await browser.wait(EC.presenceOf(clPL.respNamesA.get(0)), browser.params.cooldown.medium, `There is no names on page`);
    await clPL.respNamesA.count().then(async am => {
      for (let i = 0; i < am; i++) {
        let t = await clPL.respNamesA.get(i).getText();
        if (t == title) {
          await clPL.respNamesA.get(i).click();
          break;
        };
      };
    });
    await spinner.waitUntilSpinnerOff();
  };

  static async startEdit() {
    await browser.wait(EC.elementToBeClickable(clPL.detailFormButton.get(0)), browser.params.cooldown.medium, `There is no Edit button`);
    await clPL.detailFormButton.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async  clickClose() {
    await browser.wait(EC.elementToBeClickable(clPL.closeIcon), browser.params.cooldown.medium, `There is no Close(x) button`);
    await clPL.closeIcon.click();
    await spinner.waitUntilSpinnerOff();
  };

  static async setToggleOff(num = 1) {
    await browser.wait(EC.elementToBeClickable(clPL.activeToggles.get(num - 1)), browser.params.cooldown.medium, `There is no Active Toggles`);
    await clPL.activeToggles.get(num - 1).click();
    await spinner.waitUntilSpinnerOff();
  };



}