import { by, element } from 'protractor';

export class JobDescriptionListLocators {
  static tabJDList = element(by.css(`.second-level>div:nth-child(3) a`));
  static searchList = element(by.css(`input`));
  static rowTR = element.all(by.css(`tr.ng-star-inserted`));
  static rowNames = element.all(by.css(`tr .name-column a`));

  static filterTitle = element(by.css(`div.filter-title div`));
  static threeDotsMenu = element.all(by.css(`tr td.last-column kf-icon`));
  static itemThreeDotsMenu = element.all(by.css(`.items.open kf-dropdown-item`));
  static submitDeleteThreeDots = element.all(by.css(`footer button:nth-child(2)`));

 
}