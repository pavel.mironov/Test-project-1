import { browser, by, element, ExpectedConditions as EC } from 'protractor';
import { JobEvaluationPageLocators as jePL } from './sp-job-evaluation.locators';
import { SpinnerPO as spinner } from './spinner';

export class JobEvaluationPageObjects {
  static async openJE() {
    await browser.wait(
      EC.elementToBeClickable(jePL.detailsJELink),
      browser.params.cooldown.medium,
      `JE link isn't clickable`);
    await browser.executeScript(`window.scrollTo(0,0)`);
    await jePL.detailsJELink.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickEditJEButton() {
    await browser.wait(
      EC.elementToBeClickable(jePL.editJEButton),
      browser.params.cooldown.medium,
      `Edit Job Evaluation button isn't clickable`);
    await jePL.editJEButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setJENameForSave(name: string) {
    await jePL.inputTitle.clear();
    await jePL.inputTitle.sendKeys(name);
  }

  static async confirmSave() {
    await browser.wait(
      EC.elementToBeClickable(jePL.submitSaveButton),
      browser.params.cooldown.medium,
      `Confirm Save JE button isn't clickable`);
    await jePL.submitSaveButton.click();
    await spinner.waitUntilSpinnerOff();
  }

  static async setRationaleKnowHow(t: string) {
    await browser.wait(
      EC.presenceOf(jePL.textareaRationaleKH),
      browser.params.cooldown.medium,
      `There is no textarea for Know-How section`);
    await jePL.textareaRationaleKH.clear();
    await jePL.textareaRationaleKH.sendKeys(t);
  }

  static async setRationaleProblemSolving(t: string) {
    await browser.wait(
      EC.presenceOf(jePL.textareaRationalePS),
      browser.params.cooldown.medium,
      `There is no textarea for Problem Solving section`);
    await jePL.textareaRationalePS.clear();
    await jePL.textareaRationalePS.sendKeys(t);
  }

  static async setRationaleAccountability(t: string) {
    await browser.wait(
      EC.presenceOf(jePL.textareaRationaleAC),
      browser.params.cooldown.medium,
      `There is no textarea for Accountability section`);
    await jePL.textareaRationaleAC.clear();
    await jePL.textareaRationaleAC.sendKeys(t);
  }

  static async saveJEAs(name: string) {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.elementToBeClickable(jePL.nextButton),
      browser.params.cooldown.medium,
      `Next button isn't clickable`);
    await jePL.nextButton.click();
    await spinner.waitUntilSpinnerOff();
    await this.setJENameForSave(name);
    await this.confirmSave();
    await spinner.waitUntilSpinnerOff();
  };

  static async saveJE() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.elementToBeClickable(jePL.saveButton.get(1)),
      browser.params.cooldown.medium,
      `Save JE button isn't clickable`);
    await jePL.saveButton.get(1).click();
    await spinner.waitUntilSpinnerOff();
    // await this.confirmSave();
    await spinner.waitUntilSpinnerOff();
  }

  static async saveJEWOConfirm() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(
      EC.elementToBeClickable(jePL.saveButton.get(1)),
      browser.params.cooldown.medium,
      `Save JE button isn't clickable`);
    await jePL.saveButton.get(1).click();
    await spinner.waitUntilSpinnerOff();
    // await this.confirmSave();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickCancelButton() {
    await browser.wait(EC.elementToBeClickable(jePL.cancelJEButton), browser.params.cooldown.medium, `There is no Cancel button`);
    await jePL.cancelJEButton.click();
    await spinner.waitUntilSpinnerOff();

  };
}