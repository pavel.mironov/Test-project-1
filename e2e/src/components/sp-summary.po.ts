import { by, element, browser, ExpectedConditions as EC } from 'protractor';
import { SuccessProfileDetailsPageLocators as detailsPL } from './sp-details.locators';
import { SuccessProfileSummaryPageLocators as summaryPL } from './sp-summary.locators';
import { SpinnerPO as spinner } from './spinner';


export class SuccessProfileSummaryPageObjects {

  static async openSummary() {
    await browser.wait(EC.elementToBeClickable(detailsPL.magnifiers.get(0)), browser.params.cooldown.large, `There is no magnifier`);
    await browser.executeScript(`window.scrollTo(0,0)`);
    await spinner.waitUntilSpinnerOff();
    await detailsPL.magnifiers.get(0).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async getCurrentTabTRCount() {
    return summaryPL.summaryRecords.count();
  };
  
  static async clickDetailsTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers0.get(0)), browser.params.cooldown.small, `There is no high level headers`);
    await summaryPL.headers0.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickAccountabilityTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers0.get(1)), browser.params.cooldown.small, `There is no high level headers`);
    await summaryPL.headers0.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickCapabilityTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers0.get(2)), browser.params.cooldown.small, `There is no high level headers`);
    await summaryPL.headers0.get(2).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickIdentityTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers0.get(3)), browser.params.cooldown.small, `There is no high level headers`);
    await summaryPL.headers0.get(3).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickResponsibilitiesTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers1.get(0)), browser.params.cooldown.small, `There is no second level headers`);
    await summaryPL.headers1.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickBCTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers1.get(0)), browser.params.cooldown.small, `There is no second level headers`);
    await summaryPL.headers1.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickTCTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers1.get(1)), browser.params.cooldown.small, `There is no second level headers`);
    await summaryPL.headers1.get(1).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async clickEducationTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers1.get(3)), browser.params.cooldown.small, `There is no second level headers`);
    await summaryPL.headers1.get(3).click(); 
    await spinner.waitUntilSpinnerOff();   
  }

  static async clickExperienceTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers1.get(4)), browser.params.cooldown.small, `There is no second level headers`);
    await summaryPL.headers1.get(4).click(); 
    await spinner.waitUntilSpinnerOff();   
  }

  static async clickTraitsTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers1.get(0)), browser.params.cooldown.small, `There is no second level headers`);
    await summaryPL.headers1.get(0).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickDriversTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers1.get(1)), browser.params.cooldown.small, `There is no second level headers`);
    await summaryPL.headers1.get(1).click();
    await spinner.waitUntilSpinnerOff();
  };

  static async clickCognitiveAbilitiesTab() {
    await browser.wait(EC.presenceOf(summaryPL.headers1.get(2)), browser.params.cooldown.small, `There is no second level headers`);
    await summaryPL.headers1.get(2).click();    
    await spinner.waitUntilSpinnerOff();
  };

  static async closeSummary() {
    await browser.wait(EC.elementToBeClickable(summaryPL.summaryCloseButton.get(2)), browser.params.cooldown.small, `Close button isnt clickable`);
    await summaryPL.summaryCloseButton.get(2).click();
    await spinner.waitUntilSpinnerOff();
  }

  static async getSummaryAllTraits() {
    await this.clickIdentityTab();
    await this.clickTraitsTab();
    await browser.wait(EC.presenceOf(summaryPL.recordNames.get(0)), browser.params.cooldown.medium, `Summary page: no traits`);

    var traits = [];
    await summaryPL.recordNames.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        const traitName = await summaryPL.recordNames.get(i).getText().then((text: string) => { return text; });
        const traitValue = summaryPL.recordValueByNo(i + 1);
        await browser.wait(EC.visibilityOf(traitValue.get(0)), browser.params.cooldown.medium, `Summary page: no trait level`);
        let k = 0;
        await traitValue.count().then(async (am) => {
          for (let j = 0; j < am; j = j + 1) {
            let ff = await traitValue.get(j).getAttribute(`fill`).then((text: string) => { return text; });
            if (ff == `#62bc62`) k = k + 1;
          }
        });
        traits.push(` ${traitName}(${k})`);
      };
    });
    return traits;
  }

  static async getSummaryAllDrivers() {
    await this.clickIdentityTab();
    await this.clickDriversTab();
    await browser.wait(EC.presenceOf(summaryPL.recordNames.get(0)), browser.params.cooldown.medium, `Summary page: no drivers`);

    var drivers = [];
    await summaryPL.recordNames.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        const driverName = await summaryPL.recordNames.get(i).getText().then((text: string) => { return text; });
        const driverValue = summaryPL.recordValueByNo(i + 1);
        await browser.wait(EC.visibilityOf(driverValue.get(0)), browser.params.cooldown.medium, `Summary page: no driver level`);
        let k = 0;
        await driverValue.count().then(async (am) => {
          for (let j = 0; j < am; j = j + 1) {
            let ff = await driverValue.get(j).getAttribute(`fill`).then((text: string) => { return text; });
            if (ff == `#62bc62`) k = k + 1;
          }
        });
        drivers.push(` ${driverName}(${k})`);
      };
    });
    return drivers;
  }

  static async clickEditButton() {
    await spinner.waitUntilSpinnerOff();
    await browser.wait(EC.presenceOf(summaryPL.editSPButton), browser.params.cooldown.small, `There is no Edit Success Profile button`);
    await summaryPL.editSPButton.click();
    await spinner.waitUntilSpinnerOff();
  };

}