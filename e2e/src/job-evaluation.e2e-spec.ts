import { browser, ExpectedConditions as EC } from 'protractor';
import { AppPage as page } from './components/app.po';
import { getCSVFileDataT } from './components/utils';
import { SuccessProfileListPageObjects as listPO } from './components/sp-list.po';
import { JobEvaluationPageObjects as jePO } from './components/sp-job-evaluation.po';
import { JobEvaluationPageLocators as jePL } from './components/sp-job-evaluation.locators';
import { SpinnerPO as spinner } from './components/spinner';

const profileName1 = `Test ASDFG || JE copy`;
const profileName2 = `Test ASDFG || JE rationale`;
const profileName3 = `Test ASDFG || JE content`;
const profileName4 = `Test ASDFG || JE editing`;
const profileName5 = `Test ASDFG || JE editing NQ`;

let subFunctionN = `Stores Visual Merchandising`;

beforeEach(async () => {
  await page.login();
  await browser.waitForAngularEnabled(false);
});

afterEach(async () => {
  await browser.manage().logs().get('browser').then(async (browserLog) => {
    if (browserLog.length > 0) {
      await browserLog.forEach(async log => {
        await expect(browserLog.length > 0).toBeFalsy(`error in console`);//.toBeFalsy(log.message);
      });
    }
  });
});

describe('Job Evaluation page.', () => {
  it(`Copy SP with JE`, async () => {
    await page.searchProfileHome(profileName1).then(async () => {
      await page.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });

    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFunctionFilter(4, subFunctionN);
    await listPO.openSPFromListByNo(1);
    await jePO.openJE();

    // Get values 'before'
    //chart Values

    //KH
    await jePL.tabKH.click();
    //Tech
    const statKHTechBefore = await jePL.tabCompLevelStats.get(0).getText();
    const descKHTechTBefore = await jePL.tabCompLevelTitle.get(0).getText();
    const descKHTechDBefore = await jePL.tabCompLevelDescription.get(0).getText();
    const nameKHTechBefore = await jePL.tabCompLevelName.get(0).getText();
    //Plan
    const statKHPlanBefore = await jePL.tabCompLevelStats.get(1).getText();
    const descKHPlanTBefore = await jePL.tabCompLevelTitle.get(1).getText();
    const descKHPlanDBefore = await jePL.tabCompLevelDescription.get(1).getText();
    const nameKHPlanBefore = await jePL.tabCompLevelName.get(1).getText();
    //Comm
    const statKHCommBefore = await jePL.tabCompLevelStats.get(2).getText();
    const descKHCommTBefore = await jePL.tabCompLevelTitle.get(2).getText();
    const descKHCommDBefore = await jePL.tabCompLevelDescription.get(2).getText();
    const nameKHCommBefore = await jePL.tabCompLevelName.get(2).getText();

    //PS
    await jePL.tabPS.click();
    //Free
    const statPSThinBefore = await jePL.tabCompLevelStats.get(0).getText();
    const descPSThinTBefore = await jePL.tabCompLevelTitle.get(0).getText();
    const descPSThinDBefore = await jePL.tabCompLevelDescription.get(0).getText();
    const namePSThinBefore = await jePL.tabCompLevelName.get(0).getText();
    //Chal
    const statPSChalBefore = await jePL.tabCompLevelStats.get(1).getText();
    const descPSChalTBefore = await jePL.tabCompLevelTitle.get(1).getText();
    const descPSChalDBefore = await jePL.tabCompLevelDescription.get(1).getText();
    const namePSChalBefore = await jePL.tabCompLevelName.get(1).getText();

    //AC
    await jePL.tabAC.click();
    //Free
    const statACFreeBefore = await jePL.tabCompLevelStats.get(0).getText();
    const descACFreeTBefore = await jePL.tabCompLevelTitle.get(0).getText();
    const descACFreeDBefore = await jePL.tabCompLevelDescription.get(0).getText();
    const nameACFreeBefore = await jePL.tabCompLevelName.get(0).getText();
    //Magn
    const statACMagnBefore = await jePL.tabCompLevelStats.get(1).getText();
    const descACMagnTBefore = await jePL.tabCompLevelTitle.get(1).getText();
    const descACMagnDBefore = await jePL.tabCompLevelDescription.get(1).getText();
    const nameACMagnBefore = await jePL.tabCompLevelName.get(1).getText();
    //Natu
    const statACNatuBefore = await jePL.tabCompLevelStats.get(2).getText();
    const descACNatuTBefore = await jePL.tabCompLevelTitle.get(2).getText();
    const descACNatuDBefore = await jePL.tabCompLevelDescription.get(2).getText();
    const nameACNatuBefore = await jePL.tabCompLevelName.get(2).getText();

    await jePO.clickEditJEButton();
    const gradeChartBefore = await jePL.editTextCharts.get(0).getText();
    const shortprofileChartBefore = await jePL.editTextCharts.get(1).getText();
    const haypointsChartBefore = await jePL.editTextCharts.get(2).getText();
    await jePO.saveJEAs(profileName1);

    // Get values 'after'
    //chart Values
    const gradeChartAfter = await jePL.textCharts.get(0).getText();
    const shortprofileChartAfter = await jePL.textCharts.get(1).getText();
    const haypointsChartAfter = await jePL.textCharts.get(2).getText();

    //KH
    await jePL.tabKH.click();
    //Tech
    const statKHTechAfter = await jePL.tabCompLevelStats.get(0).getText();
    const descKHTechTAfter = await jePL.tabCompLevelTitle.get(0).getText();
    const descKHTechDAfter = await jePL.tabCompLevelDescription.get(0).getText();
    const nameKHTechAfter = await jePL.tabCompLevelName.get(0).getText();
    //Plan
    const statKHPlanAfter = await jePL.tabCompLevelStats.get(1).getText();
    const descKHPlanTAfter = await jePL.tabCompLevelTitle.get(1).getText();
    const descKHPlanDAfter = await jePL.tabCompLevelDescription.get(1).getText();
    const nameKHPlanAfter = await jePL.tabCompLevelName.get(1).getText();
    //Comm
    const statKHCommAfter = await jePL.tabCompLevelStats.get(2).getText();
    const descKHCommTAfter = await jePL.tabCompLevelTitle.get(2).getText();
    const descKHCommDAfter = await jePL.tabCompLevelDescription.get(2).getText();
    const nameKHCommAfter = await jePL.tabCompLevelName.get(2).getText();

    //PS
    await jePL.tabPS.click();
    //Free
    const statPSThinAfter = await jePL.tabCompLevelStats.get(0).getText();
    const descPSThinTAfter = await jePL.tabCompLevelTitle.get(0).getText();
    const descPSThinDAfter = await jePL.tabCompLevelDescription.get(0).getText();
    const namePSThinAfter = await jePL.tabCompLevelName.get(0).getText();
    //Chal
    const statPSChalAfter = await jePL.tabCompLevelStats.get(1).getText();
    const descPSChalTAfter = await jePL.tabCompLevelTitle.get(1).getText();
    const descPSChalDAfter = await jePL.tabCompLevelDescription.get(1).getText();
    const namePSChalAfter = await jePL.tabCompLevelName.get(1).getText();

    //AC
    await jePL.tabAC.click();
    //Free
    const statACFreeAfter = await jePL.tabCompLevelStats.get(0).getText();
    const descACFreeTAfter = await jePL.tabCompLevelTitle.get(0).getText();
    const descACFreeDAfter = await jePL.tabCompLevelDescription.get(0).getText();
    const nameACFreeAfter = await jePL.tabCompLevelName.get(0).getText();
    //Magn
    const statACMagnAfter = await jePL.tabCompLevelStats.get(1).getText();
    const descACMagnTAfter = await jePL.tabCompLevelTitle.get(1).getText();
    const descACMagnDAfter = await jePL.tabCompLevelDescription.get(1).getText();
    const nameACMagnAfter = await jePL.tabCompLevelName.get(1).getText();
    //Natu
    const statACNatuAfter = await jePL.tabCompLevelStats.get(2).getText();
    const descACNatuTAfter = await jePL.tabCompLevelTitle.get(2).getText();
    const descACNatuDAfter = await jePL.tabCompLevelDescription.get(2).getText();
    const nameACNatuAfter = await jePL.tabCompLevelName.get(2).getText();

    // Check it
    // charts values
    await expect(gradeChartBefore == gradeChartAfter).toBeTruthy(`Expected ${gradeChartBefore} instead of ${gradeChartAfter}`);
    await expect(shortprofileChartBefore == shortprofileChartAfter).toBeTruthy(`Expected ${shortprofileChartBefore} instead of ${shortprofileChartAfter}`);
    await expect(haypointsChartBefore == haypointsChartAfter).toBeTruthy(`Expected ${haypointsChartBefore} instead of ${haypointsChartAfter}`);

    //KH
    await expect(statKHTechBefore == statKHTechAfter).toBeTruthy(`Expected ${statKHTechBefore} instead of ${statKHTechAfter}`);
    await expect(descKHTechTBefore == descKHTechTAfter).toBeTruthy(`Expected ${descKHTechTBefore} instead of ${descKHTechTAfter}`);
    await expect(descKHTechDBefore == descKHTechDAfter).toBeTruthy(`Expected ${descKHTechDBefore} instead of ${descKHTechDAfter}`);
    await expect(nameKHTechBefore == nameKHTechAfter).toBeTruthy(`Expected ${nameKHTechBefore} instead of ${nameKHTechAfter}`);

    await expect(statKHPlanBefore == statKHPlanAfter).toBeTruthy(`Expected ${statKHPlanBefore} instead of ${statKHPlanAfter}`);
    await expect(descKHPlanTBefore == descKHPlanTAfter).toBeTruthy(`Expected ${descKHPlanTBefore} instead of ${descKHPlanTAfter}`);
    await expect(descKHPlanDBefore == descKHPlanDAfter).toBeTruthy(`Expected ${descKHPlanDBefore} instead of ${descKHPlanDAfter}`);
    await expect(nameKHPlanBefore == nameKHPlanAfter).toBeTruthy(`Expected ${nameKHPlanBefore} instead of ${nameKHPlanAfter}`);

    await expect(statKHCommBefore == statKHCommAfter).toBeTruthy(`Expected ${statKHCommBefore} instead of ${statKHCommAfter}`);
    await expect(descKHCommTBefore == descKHCommTAfter).toBeTruthy(`Expected ${descKHCommTBefore} instead of ${descKHCommTAfter}`);
    await expect(descKHCommDBefore == descKHCommDAfter).toBeTruthy(`Expected ${descKHCommDBefore} instead of ${descKHCommDAfter}`);
    await expect(nameKHCommBefore == nameKHCommAfter).toBeTruthy(`Expected ${nameKHCommBefore} instead of ${nameKHCommAfter}`);

    //PS
    await expect(statPSThinBefore == statPSThinAfter).toBeTruthy(`Expected ${statPSThinBefore} instead of ${statPSThinAfter}`);
    await expect(descPSThinTBefore == descPSThinTAfter).toBeTruthy(`Expected ${descPSThinTBefore} instead of ${descPSThinTAfter}`);
    await expect(descPSThinDBefore == descPSThinDAfter).toBeTruthy(`Expected ${descPSThinDBefore} instead of ${descPSThinDAfter}`);
    await expect(namePSThinBefore == namePSThinAfter).toBeTruthy(`Expected ${namePSThinBefore} instead of ${namePSThinAfter}`);

    await expect(statPSChalBefore == statPSChalAfter).toBeTruthy(`Expected ${statPSChalBefore} instead of ${statPSChalAfter}`);
    await expect(descPSChalTBefore == descPSChalTAfter).toBeTruthy(`Expected ${descPSChalTBefore} instead of ${descPSChalTAfter}`);
    await expect(descPSChalDBefore == descPSChalDAfter).toBeTruthy(`Expected ${descPSChalDBefore} instead of ${descPSChalDAfter}`);
    await expect(namePSChalBefore == namePSChalAfter).toBeTruthy(`Expected ${namePSChalBefore} instead of ${namePSChalAfter}`);

    //AC
    await expect(statACFreeBefore == statACFreeAfter).toBeTruthy(`Expected ${statACFreeBefore} instead of ${statACFreeAfter}`);
    await expect(descACFreeTBefore == descACFreeTAfter).toBeTruthy(`Expected ${descACFreeTBefore} instead of ${descACFreeTAfter}`);
    await expect(descACFreeDBefore == descACFreeDAfter).toBeTruthy(`Expected ${descACFreeDBefore} instead of ${descACFreeDAfter}`);
    await expect(nameACFreeBefore == nameACFreeAfter).toBeTruthy(`Expected ${nameACFreeBefore} instead of ${nameACFreeAfter}`);

    await expect(statACMagnBefore == statACMagnAfter).toBeTruthy(`Expected ${statACMagnBefore} instead of ${statACMagnAfter}`);
    await expect(descACMagnTBefore == descACMagnTAfter).toBeTruthy(`Expected ${descACMagnTBefore} instead of ${descACMagnTAfter}`);
    await expect(descACMagnDBefore == descACMagnDAfter).toBeTruthy(`Expected ${descACMagnDBefore} instead of ${descACMagnDAfter}`);
    await expect(nameACMagnBefore == nameACMagnAfter).toBeTruthy(`Expected ${nameACMagnBefore} instead of ${nameACMagnAfter}`);

    await expect(statACNatuBefore == statACNatuAfter).toBeTruthy(`Expected ${statACNatuBefore} instead of ${statACNatuAfter}`);
    await expect(descACNatuTBefore == descACNatuTAfter).toBeTruthy(`Expected ${descACNatuTBefore} instead of ${descACNatuTAfter}`);
    await expect(descACNatuDBefore == descACNatuDAfter).toBeTruthy(`Expected ${descACNatuDBefore} instead of ${descACNatuDAfter}`);
    await expect(nameACNatuBefore == nameACNatuAfter).toBeTruthy(`Expected ${nameACNatuBefore} instead of ${nameACNatuAfter}`);
  });

  const rationaleKH = `how know o7`;
  const rationalePS = `solving problem o7`;
  const rationaleAC = `accountability o7`;
  it(`Editing JE Rationale sections`, async () => {
    await page.searchProfileHome(profileName2).then(async () => {
      await page.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });

    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFunctionFilter(4, subFunctionN);
    await listPO.openSPFromListByNo(1);
    await jePO.openJE();
    await jePO.clickEditJEButton();

    await jePO.setRationaleKnowHow(rationaleKH);
    await jePO.setRationaleProblemSolving(rationalePS);
    await jePO.setRationaleAccountability(rationaleAC);

    await jePO.saveJEAs(profileName2);

    await jePL.tabKH.click();
    await expect(await jePL.rationaleDesc.getText() == rationaleKH)
      .toBeTruthy(`Know How Rationale contains '${await jePL.rationaleDesc.getText()}' instead of '${rationaleKH}'`);

    await jePL.tabPS.click();
    await expect(await jePL.rationaleDesc.getText() == rationalePS)
      .toBeTruthy(`Know How Rationale contains '${await jePL.rationaleDesc.getText()}' instead of '${rationalePS}'`);

    await jePL.tabAC.click();
    await expect(await jePL.rationaleDesc.getText() == rationaleAC)
      .toBeTruthy(`Know How Rationale contains '${await jePL.rationaleDesc.getText()}' instead of '${rationaleAC}'`);
  });

  it(`JE Dropdowns content`, async () => {
    // Get 'expected' values from files
    // KH Tech
    let linesKHTech = getCSVFileDataT(browser.params.files.jeKHTech);
    let ddKHTechTitle = linesKHTech[0][0].replace(`"`, ``);
    for (let j = 1; j < ddKHTechTitle[0].length - 2; j++) ddKHTechTitle = ddKHTechTitle + ',' + linesKHTech[0][j].replace(`"`, ``);
    var ddKHTechDescr = linesKHTech[1][0].replace(`"`, ``);
    for (let j = 1; j < linesKHTech[1].length - 2; j++) ddKHTechDescr = ddKHTechDescr + ',' + linesKHTech[1][j].replace(`"`, ``);
    let levelsKHTech = [];
    for (let i = 3; i < linesKHTech.length - 1; i++) levelsKHTech.push(linesKHTech[i][0]);
    let levelsTitleKHTech = [];
    for (let i = 3; i < linesKHTech.length - 1; i = i + 3) levelsTitleKHTech.push(linesKHTech[i][1]);
    let levelsDescrKHTech = [];
    for (let i = 3; i < linesKHTech.length - 1; i = i + 3) {
      let r = linesKHTech[i][2].replace(`"`, ``);
      for (let j = 3; j < linesKHTech[i].length; j++) r = r + ',' + linesKHTech[i][j].replace(`"`, ``);
      levelsDescrKHTech.push(r);
    }
    // KH Plan
    let linesKHPlan = getCSVFileDataT(browser.params.files.jeKHPlan);
    let ddKHPlanTitle = linesKHPlan[0][0].replace(`"`, ``);
    for (let j = 1; j < linesKHPlan[0].length - 2; j++) ddKHPlanTitle = ddKHPlanTitle + ',' + linesKHPlan[0][j].replace(`"`, ``);
    let ddKHPlanDescr = linesKHPlan[1][0].replace(`"`, ``);
    for (let j = 1; j < linesKHPlan[1].length - 2; j++) ddKHPlanDescr = ddKHPlanDescr + ',' + linesKHPlan[1][j].replace(`"`, ``);
    let levelsKHPlan = [];
    for (let i = 3; i < linesKHPlan.length - 1; i++) levelsKHPlan.push(linesKHPlan[i][0]);
    let levelsTitleKHPlan = [];
    for (let i = 3; i < linesKHPlan.length - 1; i = i + 3) levelsTitleKHPlan.push(linesKHPlan[i][1]);
    let levelsDescrKHPlan = [];
    for (let i = 3; i < linesKHPlan.length - 1; i = i + 3) {
      let r = linesKHPlan[i][2].replace(`"`, ``);
      for (let j = 3; j < linesKHPlan[i].length; j++) r = r + ',' + linesKHPlan[i][j].replace(`"`, ``);
      levelsDescrKHPlan.push(r);
    }
    // KH Comm
    let linesKHComm = getCSVFileDataT(browser.params.files.jeKHComm);
    let ddKHCommTitle = linesKHComm[0][0].replace(`"`, ``);
    for (let j = 1; j < linesKHComm[0].length - 2; j++) ddKHCommTitle = ddKHCommTitle + ',' + linesKHComm[0][j].replace(`"`, ``);
    let ddKHCommDescr = linesKHComm[1][0].replace(`"`, ``);
    for (let j = 1; j < linesKHComm[1].length - 2; j++) ddKHCommDescr = ddKHCommDescr + ',' + linesKHComm[1][j].replace(`"`, ``);
    let levelsKHComm = [];
    for (let i = 3; i < linesKHComm.length - 1; i++) levelsKHComm.push(linesKHComm[i][0]);
    let levelsTitleKHComm = [];
    for (let i = 3; i < linesKHComm.length - 1; i++) levelsTitleKHComm.push(linesKHComm[i][1]);
    let levelsDescrKHComm = [];
    for (let i = 3; i < linesKHComm.length - 1; i++) {
      let r = linesKHComm[i][2].replace(`"`, ``);
      for (let j = 3; j < linesKHComm[i].length; j++) r = r + ',' + linesKHComm[i][j].replace(`"`, ``);
      levelsDescrKHComm.push(r);
    }
    // PS Env
    let linesPSEnv = getCSVFileDataT(browser.params.files.jePSEnv);
    let ddPSEnvTitle = linesPSEnv[0][0].replace(`"`, ``);
    for (let j = 1; j < linesPSEnv[0].length - 2; j++) ddPSEnvTitle = ddPSEnvTitle + ',' + linesPSEnv[0][j].replace(`"`, ``);
    let ddPSEnvDescr = linesPSEnv[1][0].replace(`"`, ``);
    for (let j = 1; j < linesPSEnv[1].length - 2; j++) ddPSEnvDescr = ddPSEnvDescr + ',' + linesPSEnv[1][j].replace(`"`, ``);
    let levelsPSEnv = [];
    for (let i = 3; i < linesPSEnv.length - 1; i++) levelsPSEnv.push(linesPSEnv[i][0]);
    let levelsTitlePSEnv = [];
    for (let i = 3; i < linesPSEnv.length - 1; i = i + 2) levelsTitlePSEnv.push(linesPSEnv[i][1]);
    let levelsDescrPSEnv = [];
    for (let i = 3; i < linesPSEnv.length - 1; i = i + 2) {
      let r = linesPSEnv[i][2].replace(`"`, ``);
      for (let j = 3; j < linesPSEnv[i].length; j++) r = r + ',' + linesPSEnv[i][j].replace(`"`, ``);
      levelsDescrPSEnv.push(r);
    }
    // PS Chal
    let linesPSChal = getCSVFileDataT(browser.params.files.jePSChal);
    let ddPSChalTitle = linesPSChal[0][0].replace(`"`, ``);
    for (let j = 1; j < linesPSChal[0].length - 2; j++) ddPSChalTitle = ddPSChalTitle + ',' + linesPSChal[0][j].replace(`"`, ``);
    let ddPSChalDescr = linesPSChal[1][0].replace(`"`, ``);
    for (let j = 1; j < linesPSChal[1].length - 2; j++) ddPSChalDescr = ddPSChalDescr + ',' + linesPSChal[1][j].replace(`"`, ``);
    let levelsPSChal = [];
    for (let i = 3; i < linesPSChal.length - 1; i++) levelsPSChal.push(linesPSChal[i][0]);
    let levelsTitlePSChal = [];
    for (let i = 3; i < linesPSChal.length - 1; i = i + 2) levelsTitlePSChal.push(linesPSChal[i][1]);
    let levelsDescrPSChal = [];
    for (let i = 3; i < linesPSChal.length - 1; i = i + 2) {
      let r = linesPSChal[i][2].replace(`"`, ``);
      for (let j = 3; j < linesPSChal[i].length; j++) r = r + ',' + linesPSChal[i][j].replace(`"`, ``);
      levelsDescrPSChal.push(r);
    }
    // AC Free
    let linesACFree = getCSVFileDataT(browser.params.files.jeACFree);
    let ddACFreeTitle = linesACFree[0][0].replace(`"`, ``);
    for (let j = 1; j < linesACFree[0].length - 2; j++) ddACFreeTitle = ddACFreeTitle + ',' + linesACFree[0][j].replace(`"`, ``);
    let ddACFreeDescr = linesACFree[1][0].replace(`"`, ``);
    for (let j = 1; j < linesACFree[1].length - 2; j++) ddACFreeDescr = ddACFreeDescr + ',' + linesACFree[1][j].replace(`"`, ``);
    let levelsACFree = [];
    for (let i = 3; i < linesACFree.length - 1; i++) levelsACFree.push(linesACFree[i][0]);
    let levelsTitleACFree = [];
    for (let i = 3; i < linesACFree.length - 1; i = i + 3) levelsTitleACFree.push(linesACFree[i][1]);
    let levelsDescrACFree = [];
    for (let i = 3; i < linesACFree.length - 1; i = i + 3) {
      let r = linesACFree[i][2].replace(`"`, ``);
      for (let j = 3; j < linesACFree[i].length; j++) r = r + ',' + linesACFree[i][j].replace(`"`, ``);
      levelsDescrACFree.push(r);
    }
    // AC Magn
    let linesACMagn = getCSVFileDataT(browser.params.files.jeACMagn);
    let ddACMagnTitle = linesACMagn[0][0].replace(`"`, ``);
    for (let j = 1; j < linesACMagn[0].length - 2; j++) ddACMagnTitle = ddACMagnTitle + ',' + linesACMagn[0][j].replace(`"`, ``);
    let ddACMagnDescr = linesACMagn[1][0].replace(`"`, ``);
    for (let j = 1; j < linesACMagn[1].length - 2; j++) ddACMagnDescr = ddACMagnDescr + ',' + linesACMagn[1][j].replace(`"`, ``);
    let levelsACMagn = [];
    for (let i = 3; i < linesACMagn.length - 1; i++) levelsACMagn.push(linesACMagn[i][0]);
    let levelsTitleACMagn = [];
    levelsTitleACMagn.push(linesACMagn[3][1]);
    for (let i = 4; i < linesACMagn.length - 1; i = i + 3) levelsTitleACMagn.push(linesACMagn[i][1]);
    let levelsDescrACMagn = [];
    let r = linesACMagn[3][2].replace(`"`, ``);
    for (let j = 3; j < linesACMagn[3].length; j++) r = r + ',' + linesACMagn[3][j].replace(`"`, ``);
    levelsDescrACMagn.push(r);
    for (let i = 4; i < linesACMagn.length - 1; i = i + 3) {
      let r = linesACMagn[i][2].replace(`"`, ``);
      for (let j = 3; j < linesACMagn[i].length; j++) r = r + ',' + linesACMagn[i][j].replace(`"`, ``);
      levelsDescrACMagn.push(r);
    }
    // AC Natu
    let linesACNatu = getCSVFileDataT(browser.params.files.jeACNatu);
    let ddACNatuTitle = linesACNatu[0][0].replace(`"`, ``);
    for (let j = 1; j < linesACNatu[0].length - 2; j++) ddACNatuTitle = ddACNatuTitle + ',' + linesACNatu[0][j].replace(`"`, ``);
    let ddACNatuDescr = linesACNatu[1][0].replace(`"`, ``);
    for (let j = 1; j < linesACNatu[1].length - 2; j++) ddACNatuDescr = ddACNatuDescr + ',' + linesACNatu[1][j].replace(`"`, ``);
    let levelsACNatu = [];
    for (let i = 3; i < linesACNatu.length - 1; i++) levelsACNatu.push(linesACNatu[i][0]);
    let levelsTitleACNatu = [];
    levelsTitleACNatu.push(linesACNatu[3][1]);
    for (let i = 5; i < linesACNatu.length - 1; i = i + 3) levelsTitleACNatu.push(linesACNatu[i][1]);
    let levelsDescrACNatu = [];
    let rr = linesACNatu[3][2].replace(`"`, ``);
    for (let j = 3; j < linesACNatu[3].length; j++) rr = rr + ',' + linesACNatu[3][j].replace(`"`, ``);
    levelsDescrACNatu.push(rr);
    for (let i = 5; i < linesACNatu.length - 1; i = i + 3) {
      let r = linesACNatu[i][2].replace(`"`, ``);
      for (let j = 3; j < linesACNatu[i].length; j++) r = r + ',' + linesACNatu[i][j].replace(`"`, ``);
      levelsDescrACNatu.push(r);
    }
    // AC Natu Non Quantified
    let linesACNatuNQ = getCSVFileDataT(browser.params.files.jeACNatuNQ);
    let ddACNatuNQTitle = linesACNatuNQ[0][0].replace(`"`, ``);
    for (let j = 1; j < linesACNatuNQ[0].length - 2; j++) ddACNatuNQTitle = ddACNatuNQTitle + ',' + linesACNatuNQ[0][j].replace(`"`, ``);
    let ddACNatuNQDescr = linesACNatuNQ[1][0].replace(`"`, ``);
    for (let j = 1; j < linesACNatuNQ[1].length - 2; j++) ddACNatuNQDescr = ddACNatuNQDescr + ',' + linesACNatuNQ[1][j].replace(`"`, ``);
    let levelsACNatuNQ = [];
    for (let i = 3; i < linesACNatuNQ.length - 1; i++) levelsACNatuNQ.push(linesACNatuNQ[i][0]);
    let levelsTitleACNatuNQ = [];
    for (let i = 3; i < linesACNatuNQ.length - 1; i = i + 3) levelsTitleACNatuNQ.push(linesACNatuNQ[i][1]);
    let levelsDescrACNatuNQ = [];
    for (let i = 3; i < linesACNatuNQ.length - 1; i = i + 3) {
      let r = linesACNatuNQ[i][2].replace(`"`, ``);
      for (let j = 3; j < linesACNatuNQ[i].length; j++) r = r + ',' + linesACNatuNQ[i][j].replace(`"`, ``);
      levelsDescrACNatuNQ.push(r);
    }

    await page.searchProfileHome(profileName3).then(async () => {
      await page.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });

    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFunctionFilter(4, subFunctionN);
    await listPO.openSPFromListByNo(1);
    await jePO.openJE();
    await jePO.clickEditJEButton();

    // KH Tech
    await jePL.dropdownKHTech.click();
    let ttTech = await jePL.openedDropdownInfo.get(0).getText();
    await expect(ttTech.toLowerCase().trim() == ddKHTechTitle.toLowerCase().trim())
      .toBeTruthy(`KH Tech dropdown title: expected 1 instead of 2: \n 1| ${ddKHTechTitle.trim()} |\n 2| ${ttTech.trim()} |\n`);
    let ddTech = await jePL.openedDropdownInfo.get(1).getText();
    await expect(ddTech.toLowerCase().trim() == ddKHTechDescr.toLowerCase().trim())
      .toBeTruthy(`KH Tech dropdown description: expected 1 instead of 2: \n 1| ${ddKHTechDescr.trim()} |\n 2| ${ddTech.trim()} |\n`);
    await jePL.openedDropdownLvls.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let e = await jePL.openedDropdownLvls.get(i).getText();
        await expect(e.toLowerCase().trim() == levelsKHTech[i].toLowerCase())
          .toBeTruthy(`KH Tech(${i + 1} in list): expected ${levelsKHTech[i]} instead of ${e.trim()}`);
      }
    });
    await jePL.openedDropdownDescT.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let t = await jePL.openedDropdownDescT.get(i).getText();
        await expect(t.toLowerCase().trim() == levelsTitleKHTech[i].toLowerCase().trim())
          .toBeTruthy(`KH Tech(${i + 1} in list): expected ${levelsDescrKHTech[i]} instead of ${t.trim()}`);
        let d = await jePL.openedDropdownDescD.get(i).getText();
        await expect(d.toLowerCase().trim() == levelsDescrKHTech[i].toLowerCase().trim())
          .toBeTruthy(`KH Tech(${i + 1} in list): expected 1 instead of 2: \n1: ${levelsDescrKHTech[i]} \n2: ${d.trim()} \n`);
      }
    });

    // KH Plan
    await jePL.dropdownKHPlan.click();
    let ttPlan = await jePL.openedDropdownInfo.get(0).getText();
    await expect(ttPlan.toLowerCase().trim() == ddKHPlanTitle.toLowerCase().trim())
      .toBeTruthy(`KH Plan dropdown title: expected 1 instead of 2: \n 1| ${ddKHPlanTitle.trim()} |\n 2| ${ttPlan.trim()} |\n`);
    let ddPlan = await jePL.openedDropdownInfo.get(1).getText();
    await expect(ddPlan.toLowerCase().trim() == ddKHPlanDescr.toLowerCase().trim())
      .toBeTruthy(`KH Plan dropdown description: expected 1 instead of 2: \n 1| ${ddKHPlanDescr.trim()} |\n 2| ${ddPlan.trim()} |\n`);
    await jePL.openedDropdownLvls.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let e = await jePL.openedDropdownLvls.get(i).getText();
        await expect(e.toLowerCase().trim() == levelsKHPlan[i].toLowerCase().trim())
          .toBeTruthy(`KH Plan(${i + 1} in list): expected ${levelsKHPlan[i].trim()} instead of ${e.trim()}`);
      }
    });
    await jePL.openedDropdownDescT.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let t = await jePL.openedDropdownDescT.get(i).getText();
        await expect(t.toLowerCase().trim() == levelsTitleKHPlan[i].toLowerCase().trim())
          .toBeTruthy(`KH Plan(${i + 1} in list): expected ${levelsTitleKHPlan[i]} instead of ${t.trim()}`);
        let d = await jePL.openedDropdownDescD.get(i).getText();
        await expect(d.toLowerCase().trim() == levelsDescrKHPlan[i].toLowerCase().trim())
          .toBeTruthy(`KH Plan(${i + 1} in list): expected 1 instead of 2: \n1: ${levelsDescrKHPlan[i]} \n2: ${d.trim()} \n`);
      }
    });

    // KH Comm
    await jePL.dropdownKHComm.click();
    let ttComm = await jePL.openedDropdownInfo.get(0).getText();
    await expect(ttComm.toLowerCase().trim() == ddKHCommTitle.toLowerCase().trim())
      .toBeTruthy(`KH Comm dropdown title: expected 1 instead of 2: \n 1| ${ddKHCommTitle.trim()} |\n 2| ${ttComm.trim()} |\n`);
    let ddComm = await jePL.openedDropdownInfo.get(1).getText();
    await expect(ddComm.toLowerCase().trim() == ddKHCommDescr.toLowerCase().trim())
      .toBeTruthy(`KH Comm dropdown description: expected 1 instead of 2: \n 1| ${ddKHCommDescr.trim()} |\n 2| ${ddComm.trim()} |\n`);
    await jePL.openedDropdownLvls.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let e = await jePL.openedDropdownLvls.get(i).getText();
        await expect(e.toLowerCase().trim() == levelsKHComm[i].toLowerCase().trim())
          .toBeTruthy(`KH Comm(${i + 1} in list): expected ${levelsKHComm[i].trim()} instead of ${e.trim()}`);
      }
    });
    await jePL.openedDropdownDescT.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let t = await jePL.openedDropdownDescT.get(i).getText();
        await expect(t.toLowerCase().trim() == levelsTitleKHComm[i].toLowerCase().trim())
          .toBeTruthy(`KH Comm(${i + 1} in list): expected ${levelsTitleKHComm[i]} instead of ${t.trim()}`);
        let d = await jePL.openedDropdownDescD.get(i).getText();
        await expect(d.toLowerCase().trim() == levelsDescrKHComm[i].toLowerCase().trim())
          .toBeTruthy(`KH Comm(${i + 1} in list): expected 1 instead of 2: \n1: ${levelsDescrKHComm[i]} \n2: ${d.trim()} \n`);
      }
    });

    // PS Env
    await jePL.dropdownPSEnv.click();
    let ttEnv = await jePL.openedDropdownInfo.get(0).getText();
    await expect(ttEnv.toLowerCase().trim() == ddPSEnvTitle.toLowerCase().trim())
      .toBeTruthy(`PS Env dropdown title: expected 1 instead of 2: \n 1| ${ddPSEnvTitle.trim()} |\n 2| ${ttEnv.trim()} |\n`);
    let ddEnv = await jePL.openedDropdownInfo.get(1).getText();
    await expect(ddEnv.toLowerCase().trim() == ddPSEnvDescr.toLowerCase().trim())
      .toBeTruthy(`PS Env dropdown description: expected 1 instead of 2: \n 1| ${ddPSEnvDescr.trim()} |\n 2| ${ddEnv.trim()} |\n`);
    await jePL.openedDropdownLvls.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let e = await jePL.openedDropdownLvls.get(i).getText();
        await expect(e.toLowerCase().trim() == levelsPSEnv[i].toLowerCase().trim())
          .toBeTruthy(`PS Env(${i + 1} in list): expected ${levelsPSEnv[i].trim()} instead of ${e.trim()}`);
      }
    });
    await jePL.openedDropdownDescT.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let t = await jePL.openedDropdownDescT.get(i).getText();
        await expect(t.toLowerCase().trim() == levelsTitlePSEnv[i].toLowerCase().trim())
          .toBeTruthy(`PS Env(${i + 1} in list): expected ${levelsTitlePSEnv[i]} instead of ${t.trim()}`);
        let d = await jePL.openedDropdownDescD.get(i).getText();
        await expect(d.toLowerCase().trim() == levelsDescrPSEnv[i].toLowerCase().trim())
          .toBeTruthy(`PS Env(${i + 1} in list): expected 1 instead of 2: \n1: ${levelsDescrPSEnv[i]} \n2: ${d.trim()} \n`);
      }
    });

    // PS Chal
    await jePL.dropdownPSChal.click();
    let ttChal = await jePL.openedDropdownInfo.get(0).getText();
    await expect(ttChal.toLowerCase().trim() == ddPSChalTitle.toLowerCase().trim())
      .toBeTruthy(`PS Chal dropdown title: expected 1 instead of 2: \n 1| ${ddPSChalTitle.trim()} |\n 2| ${ttChal.trim()} |\n`);
    let ddChal = await jePL.openedDropdownInfo.get(1).getText();
    await expect(ddChal.toLowerCase().trim() == ddPSChalDescr.toLowerCase().trim())
      .toBeTruthy(`PS Chal dropdown description: expected 1 instead of 2: \n 1| ${ddPSChalDescr.trim()} |\n 2| ${ddChal.trim()} |\n`);
    await jePL.openedDropdownLvls.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let e = await jePL.openedDropdownLvls.get(i).getText();
        await expect(e.toLowerCase().trim() == levelsPSChal[i].toLowerCase().trim())
          .toBeTruthy(`PS Chal(${i + 1} in list): expected ${levelsPSChal[i].trim()} instead of ${e.trim()}`);
      }
    });
    await jePL.openedDropdownDescT.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let t = await jePL.openedDropdownDescT.get(i).getText();
        await expect(t.toLowerCase().trim() == levelsTitlePSChal[i].toLowerCase().trim())
          .toBeTruthy(`PS Chal(${i + 1} in list): expected ${levelsTitlePSChal[i]} instead of ${t.trim()}`);
        let d = await jePL.openedDropdownDescD.get(i).getText();
        await expect(d.toLowerCase().trim() == levelsDescrPSChal[i].toLowerCase().trim())
          .toBeTruthy(`PS Chal(${i + 1} in list): expected 1 instead of 2: \n1: ${levelsDescrPSChal[i]} \n2: ${d.trim()} \n`);
      }
    });

    // AC Free
    await jePL.dropdownACFree.click();
    let ttFree = await jePL.openedDropdownInfo.get(0).getText();
    await expect(ttFree.toLowerCase().trim() == ddACFreeTitle.toLowerCase().trim())
      .toBeTruthy(`AC Free dropdown title: expected 1 instead of 2: \n 1| ${ddACFreeTitle.trim()} |\n 2| ${ttFree.trim()} |\n`);
    let ddFree = await jePL.openedDropdownInfo.get(1).getText();
    await expect(ddFree.toLowerCase().trim() == ddACFreeDescr.toLowerCase().trim())
      .toBeTruthy(`AC Free dropdown description: expected 1 instead of 2: \n 1| ${ddACFreeDescr.trim()} |\n 2| ${ddFree.trim()} |\n`);
    await jePL.openedDropdownLvls.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let e = await jePL.openedDropdownLvls.get(i).getText();
        await expect(e.toLowerCase().trim() == levelsACFree[i].toLowerCase().trim())
          .toBeTruthy(`AC Free(${i + 1} in list): expected ${levelsACFree[i].trim()} instead of ${e.trim()}`);
      }
    });
    await jePL.openedDropdownDescT.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let t = await jePL.openedDropdownDescT.get(i).getText();
        await expect(t.toLowerCase().trim() == levelsTitleACFree[i].toLowerCase().trim())
          .toBeTruthy(`AC Free(${i + 1} in list): expected ${levelsTitleACFree[i]} instead of ${t.trim()}`);
        let d = await jePL.openedDropdownDescD.get(i).getText();
        await expect(d.toLowerCase().trim() == levelsDescrACFree[i].toLowerCase().trim())
          .toBeTruthy(`AC Free(${i + 1} in list): expected 1 instead of 2: \n1: ${levelsDescrACFree[i]} \n2: ${d.trim()} \n`);
      }
    });

    // AC Magn
    await jePL.dropdownACMagn.click();
    let ttMagn = await jePL.openedDropdownInfo.get(0).getText();
    await expect(ttMagn.toLowerCase().trim() == ddACMagnTitle.toLowerCase().trim())
      .toBeTruthy(`AC Magn dropdown title: expected 1 instead of 2: \n 1| ${ddACMagnTitle.trim()} |\n 2| ${ttMagn.trim()} |\n`);
    let ddMagn = await jePL.openedDropdownInfo.get(1).getText();
    await expect(ddMagn.toLowerCase().trim() == ddACMagnDescr.toLowerCase().trim())
      .toBeTruthy(`AC Magn dropdown description: expected 1 instead of 2: \n 1| ${ddACMagnDescr.trim()} |\n 2| ${ddMagn.trim()} |\n`);
    await jePL.openedDropdownLvls.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let e = await jePL.openedDropdownLvls.get(i).getText();
        await expect(e.toLowerCase().trim() == levelsACMagn[i].toLowerCase().trim())
          .toBeTruthy(`AC Magn(${i + 1} in list): expected ${levelsACMagn[i].trim()} instead of ${e.trim()}`);
      }
    });
    await jePL.openedDropdownDescT.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let t = await jePL.openedDropdownDescT.get(i).getText();
        await expect(t.toLowerCase().trim() == levelsTitleACMagn[i].toLowerCase().trim())
          .toBeTruthy(`AC Magn(${i + 1} in list): expected ${levelsTitleACMagn[i]} instead of ${t.trim()}`);
        // Check currency
        let d = await jePL.openedDropdownDescD.get(i).getText();
        await expect(d.indexOf(`USD`) !== -1 || d.indexOf(`$`))
          .toBeTruthy(`AC Magn(${i + 1} in list): there is no info about currency(${d.trim()})`);
        // Check correct numbers
        await expect(d.trim().indexOf(levelsDescrACMagn[i].trim()) !== -1)
          .toBeTruthy(`AC Magn(${i + 1} in list): expected 1 instead of 2: \n1: ${levelsDescrACMagn[i]} \n2: ${d.trim()} \n`);
      }
    });

    // AC Natu
    await jePL.openedDropdownLvls.get(1).click();
    await spinner.waitUntilSpinnerOff();
    await jePL.dropdownACNatu.click();
    let ttNatu = await jePL.openedDropdownInfo.get(0).getText();
    await expect(ttNatu.toLowerCase().trim() == ddACNatuTitle.toLowerCase().trim())
      .toBeTruthy(`AC Natu dropdown title: expected 1 instead of 2: \n 1| ${ddACNatuTitle.trim()} |\n 2| ${ttNatu.trim()} |\n`);
    let ddNatu = await jePL.openedDropdownInfo.get(1).getText();
    await expect(ddNatu.toLowerCase().trim() == ddACNatuDescr.toLowerCase().trim())
      .toBeTruthy(`AC Natu dropdown description: expected 1 instead of 2: \n 1| ${ddACNatuDescr.trim()} |\n 2| ${ddNatu.trim()} |\n`);
    await jePL.openedDropdownLvls.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let e = await jePL.openedDropdownLvls.get(i).getText();
        await expect(e.toLowerCase().trim() == levelsACNatu[i].toLowerCase().trim())
          .toBeTruthy(`AC Natu(${i + 1} in list): expected ${levelsACNatu[i].trim()} instead of ${e.trim()}`);
      }
    });
    await jePL.openedDropdownDescT.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let t = await jePL.openedDropdownDescT.get(i).getText();
        await expect(t.toLowerCase().trim() == levelsTitleACNatu[i].toLowerCase().trim())
          .toBeTruthy(`AC Natu(${i + 1} in list): expected ${levelsTitleACNatu[i]} instead of ${t.trim()}`);
        let d = await jePL.openedDropdownDescD.get(i).getText();
        await expect(d.toLowerCase().trim() == levelsDescrACNatu[i].toLowerCase().trim())
          .toBeTruthy(`AC Natu(${i + 1} in list): expected 1 instead of 2: \n1: ${levelsDescrACNatu[i]} \n2: ${d.trim()} \n`);
      }
    });

    // AC Natu Non Quantified
    await jePL.dropdownACMagn.click();
    await jePL.openedDropdownLvls.get(0).click();
    await spinner.waitUntilSpinnerOff();
    await jePL.dropdownACNatu.click();
    let ttNatuNQ = await jePL.openedDropdownInfo.get(0).getText();
    await expect(ttNatuNQ.toLowerCase().trim() == ddACNatuNQTitle.toLowerCase().trim())
      .toBeTruthy(`AC Natu NQ dropdown title: expected 1 instead of 2: \n 1| ${ddACNatuNQTitle.trim()} |\n 2| ${ttNatuNQ.trim()} |\n`);
    let ddNatuNQ = await jePL.openedDropdownInfo.get(1).getText();
    await expect(ddNatuNQ.toLowerCase().trim() == ddACNatuNQDescr.toLowerCase().trim())
      .toBeTruthy(`AC Natu NQ dropdown description: expected 1 instead of 2: \n 1| ${ddACNatuNQDescr.trim()} |\n 2| ${ddNatuNQ.trim()} |\n`);
    await jePL.openedDropdownLvls.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let e = await jePL.openedDropdownLvls.get(i).getText();
        await expect(e.toLowerCase().trim() == levelsACNatuNQ[i].toLowerCase().trim())
          .toBeTruthy(`AC Natu NQ(${i + 1} in list): expected ${levelsACNatuNQ[i].trim()} instead of ${e.trim()}`);
      }
    });
    await jePL.openedDropdownDescT.count().then(async (a) => {
      for (let i = 0; i < a; i++) {
        let t = await jePL.openedDropdownDescT.get(i).getText();
        await expect(t.toLowerCase().trim() == levelsTitleACNatuNQ[i].toLowerCase().trim())
          .toBeTruthy(`AC Natu NQ(${i + 1} in list): expected ${levelsTitleACNatuNQ[i]} instead of ${t.trim()}`);
        let d = await jePL.openedDropdownDescD.get(i).getText();
        await expect(d.toLowerCase().trim() == levelsDescrACNatuNQ[i].toLowerCase().trim())
          .toBeTruthy(`AC Natu NQ(${i + 1} in list): expected 1 instead of 2: \n1: ${levelsDescrACNatuNQ[i]} \n2: ${d.trim()} \n`);
      }
    });
  });

  it(`JE change dropdowns values (except Non Quantified)`, async () => {
    await page.searchProfileHome(profileName4).then(async () => {
      await page.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });

    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFunctionFilter(4, subFunctionN);
    await listPO.openSPFromListByNo(1);
    await jePO.openJE();
    await jePO.clickEditJEButton();

    // Get amount of items in dropdowns
    await jePL.dropdownKHTech.click();
    const amKHTech = await jePL.openedDropdownLvls.count();
    await jePL.dropdownKHPlan.click();
    const amKHPlan = await jePL.openedDropdownLvls.count();
    await jePL.dropdownKHComm.click();
    const amKHComm = await jePL.openedDropdownLvls.count();

    await jePL.dropdownPSEnv.click();
    let amPSEnv = await jePL.openedDropdownLvls.count();
    await jePL.dropdownPSChal.click();
    const amPSChal = await jePL.openedDropdownLvls.count();

    await jePL.dropdownACFree.click();
    const amACFree = await jePL.openedDropdownLvls.count();
    await jePL.dropdownACMagn.click();
    const amACMagn = await jePL.openedDropdownLvls.count();
    await jePL.openedDropdownLvls.get(1).click();
    await spinner.waitUntilSpinnerOff();
    await jePL.dropdownACNatu.click();
    const amACNatu = await jePL.openedDropdownLvls.count();
    await jePL.openedDropdownLvls.get(0).click();
    await spinner.waitUntilSpinnerOff();

    let cap = Math.max(amKHTech, amKHPlan, amKHComm, amPSEnv, amPSChal, amACFree, amACMagn, amACNatu);

    await jePO.saveJEAs(profileName4);
    await jePO.clickEditJEButton();

    for (let i = 0; i < cap; i++) { //0 //cap
      // console.log(i);
      // Get n Set values in dropdowns
      // KH TECH 
      if (i < amKHTech) {
        await jePL.dropdownKHTech.click();
        await spinner.waitUntilSpinnerOff();
        var titleKHTech = await jePL.openedDropdownInfo.get(0).getText();
        var lvlKHTech = await jePL.openedDropdownLvls.get(i % amKHTech).getText();
        var descKHTech1 = await jePL.openedDropdownDescT.get(i % amKHTech / 3 | 0).getText();
        var descKHTech2 = await jePL.openedDropdownDescD.get(i % amKHTech / 3 | 0).getText();
        await jePL.openedDropdownLvls.get(i % amKHTech).click();
        await spinner.waitUntilSpinnerOff();
      }
      // KH PLAN 
      if (i < amKHPlan) {
        await jePL.dropdownKHPlan.click();
        await spinner.waitUntilSpinnerOff();
        var titleKHPlan = await jePL.openedDropdownInfo.get(0).getText();
        var lvlKHPlan = await jePL.openedDropdownLvls.get(i % amKHPlan).getText();
        var descKHPlan1 = await jePL.openedDropdownDescT.get(i % amKHPlan / 3 | 0).getText();
        var descKHPlan2 = await jePL.openedDropdownDescD.get(i % amKHPlan / 3 | 0).getText();
        await jePL.openedDropdownLvls.get(i % amKHPlan).click();
        await spinner.waitUntilSpinnerOff();
      }
      // KH COMM 
      if (i < amKHComm) {
        await jePL.dropdownKHComm.click();
        await spinner.waitUntilSpinnerOff();
        var titleKHComm = await jePL.openedDropdownInfo.get(0).getText();
        var lvlKHComm = await jePL.openedDropdownLvls.get(i % amKHComm).getText();
        var descKHComm1 = await jePL.openedDropdownDescT.get(i % amKHComm).getText();
        var descKHComm2 = await jePL.openedDropdownDescD.get(i % amKHComm).getText();
        await jePL.openedDropdownLvls.get(i % amKHComm).click();
        await spinner.waitUntilSpinnerOff();
      }
      // PS ENV 
      await jePL.dropdownPSEnv.click();
      amPSEnv = await jePL.openedDropdownLvls.count();
      if (i < amPSEnv) {
        await spinner.waitUntilSpinnerOff();
        var titlePSEnv = await jePL.openedDropdownInfo.get(0).getText();
        var lvlPSEnv = await jePL.openedDropdownLvls.get(i % amPSEnv).getText();
        var descPSEnv1 = await jePL.openedDropdownDescT.get(i % amPSEnv / 2 | 0).getText();
        var descPSEnv2 = await jePL.openedDropdownDescD.get(i % amPSEnv / 2 | 0).getText();
        await jePL.openedDropdownLvls.get(i % amPSEnv).click();
        await spinner.waitUntilSpinnerOff();
      }
      // PS CHAL 
      if (i < amPSChal) {
        await jePL.dropdownPSChal.click();
        await spinner.waitUntilSpinnerOff();
        var titlePSChal = await jePL.openedDropdownInfo.get(0).getText();
        var lvlPSChal = await jePL.openedDropdownLvls.get(i % amPSChal).getText();
        var descPSChal1 = await jePL.openedDropdownDescT.get(i % amPSChal / 2 | 0).getText();
        var descPSChal2 = await jePL.openedDropdownDescD.get(i % amPSChal / 2 | 0).getText();
        await jePL.openedDropdownLvls.get(i % amPSChal).click();
        await spinner.waitUntilSpinnerOff();
      }
      // AC FREE 
      if (i < amACFree) {
        await jePL.dropdownACFree.click();
        await spinner.waitUntilSpinnerOff();
        var titleACFree = await jePL.openedDropdownInfo.get(0).getText();
        var lvlACFree = await jePL.openedDropdownLvls.get(i % amACFree).getText();
        if ((i % amACFree / 3 | 0) < 8) {
          var descACFree1 = await jePL.openedDropdownDescT.get(i % amACFree / 3 | 0).getText();
          var descACFree2 = await jePL.openedDropdownDescD.get(i % amACFree / 3 | 0).getText();
        } else {
          var descACFree1 = await jePL.openedDropdownDescT.get(8).getText();
          var descACFree2 = await jePL.openedDropdownDescD.get(8).getText();
        }
        await jePL.openedDropdownLvls.get(i % amACFree).click();
        await spinner.waitUntilSpinnerOff();
      }
      // AC MAGN 
      if (i < amACMagn && i > 0) {
        await jePL.dropdownACMagn.click();
        await spinner.waitUntilSpinnerOff();
        var titleACMagn = await jePL.openedDropdownInfo.get(0).getText();
        var lvlACMagn = await jePL.openedDropdownLvls.get(i % amACMagn).getText();
        if (i % amACMagn == 0) {
          var descACMagn1 = await jePL.openedDropdownDescT.get(i % amACMagn / 3 | 0).getText();
          var descACMagn2 = await jePL.openedDropdownDescD.get(i % amACMagn / 3 | 0).getText();
        } else {
          var descACMagn1 = await jePL.openedDropdownDescT.get(((i - 1) % amACMagn / 3 | 0) + 1).getText();
          var descACMagn2 = await jePL.openedDropdownDescD.get(((i - 1) % amACMagn / 3 | 0) + 1).getText();
        }
        await jePL.openedDropdownLvls.get(i % amACMagn).click();
        await spinner.waitUntilSpinnerOff();
      }
      // AC NATU 
      if (i < amACNatu) {
        await jePL.dropdownACNatu.click();
        await spinner.waitUntilSpinnerOff();
        var titleACNatu = await jePL.openedDropdownInfo.get(0).getText();
        var lvlACNatu = await jePL.openedDropdownLvls.get(i % amACNatu).getText();
        if (i < 2) {
          var descACNatu1 = await jePL.openedDropdownDescT.get(i % amACNatu / 2 | 0).getText();
          var descACNatu2 = await jePL.openedDropdownDescD.get(i % amACNatu / 2 | 0).getText();
        } else {
          if (i > 7) {
            var descACNatu1 = await jePL.openedDropdownDescT.get((i + 1) / 3 | 0).getText();
            var descACNatu2 = await jePL.openedDropdownDescD.get((i + 1) / 3 | 0).getText();
          } else {
            var descACNatu1 = await jePL.openedDropdownDescT.get((i + 1) % amACNatu / 3 | 0).getText();
            var descACNatu2 = await jePL.openedDropdownDescD.get((i + 1) % amACNatu / 3 | 0).getText();
          }
        }
        await jePL.openedDropdownLvls.get(i % amACNatu).click();
        await spinner.waitUntilSpinnerOff();
      }

      await jePO.saveJE();
      
      // Check Values
      // KH TECH CHECKS
      if (i < amKHTech) {
        await jePL.tabKH.click();
        await spinner.waitUntilSpinnerOff();
        expect(await jePL.tabHeaderContentKH.get(0).getText() == lvlKHTech)
          .toBeTruthy(`KH Tech(${i + 1} in list): Expected "${await lvlKHTech}" instead of "${await jePL.tabHeaderContentKH.get(0).getText()}"`);
        expect(await jePL.tabCompLevelStats.get(0).getText() == lvlKHTech)
          .toBeTruthy(`KH Tech(${i + 1} in list): Expected "${await lvlKHTech}" instead of "${await jePL.tabCompLevelStats.get(0).getText()}"`);
        let khTechName = await jePL.tabCompLevelName.get(0).getText();
        expect(khTechName.toLowerCase().trim() == descKHTech1.toLowerCase().trim())
          .toBeTruthy(`KH Tech(${i + 1} in list): Expected "${descKHTech1}" instead of "${await jePL.tabCompLevelName.get(0).getText()}"`);
        expect(await jePL.tabCompLevelTitle.get(0).getText() == titleKHTech)
          .toBeTruthy(`KH Tech(${i + 1} in list): Expected "${titleKHTech}" instead of "${await jePL.tabCompLevelTitle.get(0).getText()}"`);
        expect(await jePL.tabCompLevelDescription.get(0).getText() == descKHTech2)
          .toBeTruthy(`KH Tech(${i + 1} in list): Expected "${descKHTech2}" instead of "${await jePL.tabCompLevelDescription.get(0).getText()}"`);
      }
      // KH PLAN CHECKS
      if (i < amKHPlan) {
        expect(await jePL.tabHeaderContentKH.get(1).getText() == lvlKHPlan)
          .toBeTruthy(`KH Plan(${i + 1} in list): Expected "${lvlKHPlan}" instead of "${await jePL.tabHeaderContentKH.get(1).getText()}"`);
        expect(await jePL.tabCompLevelStats.get(1).getText() == lvlKHPlan)
          .toBeTruthy(`KH Plan(${i + 1} in list): Expected "${lvlKHPlan}" instead of "${await jePL.tabCompLevelStats.get(1).getText()}"`);
        let khPlanName = await jePL.tabCompLevelName.get(1).getText();
        expect(khPlanName.toLowerCase().trim() == descKHPlan1.toLowerCase().trim())
          .toBeTruthy(`KH Plan(${i + 1} in list): Expected "${descKHPlan1}" instead of "${await jePL.tabCompLevelName.get(1).getText()}"`);
        expect(await jePL.tabCompLevelTitle.get(1).getText() == titleKHPlan)
          .toBeTruthy(`KH Plan(${i + 1} in list): Expected "${titleKHPlan}" instead of "${await jePL.tabCompLevelTitle.get(1).getText()}"`);
        expect(await jePL.tabCompLevelDescription.get(1).getText() == descKHPlan2)
          .toBeTruthy(`KH Plan(${i + 1} in list): Expected "${descKHPlan2}" instead of "${await jePL.tabCompLevelDescription.get(1).getText()}"`);
      }
      // KH COMM CHECKS
      if (i < amKHComm) {
        expect(await jePL.tabHeaderContentKH.get(2).getText() == lvlKHComm)
          .toBeTruthy(`KH Comm(${i + 1} in list): Expected "${lvlKHComm}" instead of "${await jePL.tabHeaderContentKH.get(2).getText()}"`);
        expect(await jePL.tabCompLevelStats.get(2).getText() == lvlKHComm)
          .toBeTruthy(`KH Comm(${i + 1} in list): Expected "${lvlKHComm}" instead of "${await jePL.tabCompLevelStats.get(2).getText()}"`);
        let khCommName = await jePL.tabCompLevelName.get(2).getText();
        expect(khCommName.toLowerCase().trim() == descKHComm1.toLowerCase().trim())
          .toBeTruthy(`KH Comm(${i + 1} in list): Expected "${descKHComm1}" instead of "${await jePL.tabCompLevelName.get(2).getText()}"`);
        expect(await jePL.tabCompLevelTitle.get(2).getText() == titleKHComm)
          .toBeTruthy(`KH Comm(${i + 1} in list): Expected "${titleKHComm}" instead of "${await jePL.tabCompLevelTitle.get(2).getText()}"`);
        expect(await jePL.tabCompLevelDescription.get(2).getText() == descKHComm2)
          .toBeTruthy(`KH Comm(${i + 1} in list): Expected "${descKHComm2}" instead of "${await jePL.tabCompLevelDescription.get(2).getText()}"`);
      }
      // PS ENV CHECKS
      if (i < amPSEnv) {
        await jePL.tabPS.click();
        await spinner.waitUntilSpinnerOff();
        expect(await jePL.tabHeaderContentPS.get(0).getText() == lvlPSEnv)
          .toBeTruthy(`PS Env(${i + 1} in list): Expected "${lvlPSEnv}" instead of "${await jePL.tabHeaderContentPS.get(0).getText()}"`);
        expect(await jePL.tabCompLevelStats.get(0).getText() == lvlPSEnv)
          .toBeTruthy(`PS Env(${i + 1} in list): Expected "${lvlPSEnv}" instead of "${await jePL.tabCompLevelStats.get(0).getText()}"`);
        let psEnvName = await jePL.tabCompLevelName.get(0).getText();
        expect(psEnvName.toLowerCase().trim() == descPSEnv1.toLowerCase().trim())
          .toBeTruthy(`PS Env(${i + 1} in list): Expected "${descPSEnv1}" instead of "${await jePL.tabCompLevelName.get(0).getText()}"`);
        expect(await jePL.tabCompLevelTitle.get(0).getText() == titlePSEnv)
          .toBeTruthy(`PS Env(${i + 1} in list): Expected "${titlePSEnv}" instead of "${await jePL.tabCompLevelTitle.get(0).getText()}"`);
        expect(await jePL.tabCompLevelDescription.get(0).getText() == descPSEnv2)
          .toBeTruthy(`PS Env(${i + 1} in list): Expected "${descPSEnv2}" instead of "${await jePL.tabCompLevelDescription.get(0).getText()}"`);
      }
      // PS CHAL CHECKS
      if (i < amPSChal) {
        expect(await jePL.tabHeaderContentPS.get(1).getText() == lvlPSChal)
          .toBeTruthy(`PS Chal(${i + 1} in list): Expected "${lvlPSChal}" instead of "${await jePL.tabHeaderContentPS.get(1).getText()}"`);
        expect(await jePL.tabCompLevelStats.get(1).getText() == lvlPSChal)
          .toBeTruthy(`PS Chal(${i + 1} in list): Expected "${lvlPSChal}" instead of "${await jePL.tabCompLevelStats.get(1).getText()}"`);
        let psChalName = await jePL.tabCompLevelName.get(1).getText();
        expect(psChalName.toLowerCase().trim() == descPSChal1.toLowerCase().trim())
          .toBeTruthy(`PS Chal(${i + 1} in list): Expected "${descPSChal1}" instead of "${await jePL.tabCompLevelName.get(1).getText()}"`);
        expect(await jePL.tabCompLevelTitle.get(1).getText() == titlePSChal)
          .toBeTruthy(`PS Chal(${i + 1} in list): Expected "${titlePSChal}" instead of "${await jePL.tabCompLevelTitle.get(1).getText()}"`);
        expect(await jePL.tabCompLevelDescription.get(1).getText() == descPSChal2)
          .toBeTruthy(`PS Chal(${i + 1} in list): Expected "${descPSChal2}" instead of "${await jePL.tabCompLevelDescription.get(1).getText()}"`);
      }
      // AC FREE CHECKS
      if (i < amACFree) {
        await jePL.tabAC.click();
        await spinner.waitUntilSpinnerOff();
        expect(await jePL.tabHeaderContentAC.get(0).getText() == lvlACFree)
          .toBeTruthy(`AC Free(${i + 1} in list): Expected "${lvlACFree}" instead of "${await jePL.tabHeaderContentAC.get(0).getText()}"`);
        expect(await jePL.tabCompLevelStats.get(0).getText() == lvlACFree)
          .toBeTruthy(`AC Free(${i + 1} in list): Expected "${lvlACFree}" instead of "${await jePL.tabCompLevelStats.get(0).getText()}"`);
        let acFreeName = await jePL.tabCompLevelName.get(0).getText();
        expect(acFreeName.toLowerCase().trim() == descACFree1.toLowerCase().trim())
          .toBeTruthy(`AC Free(${i + 1} in list): Expected "${descACFree1}" instead of "${await jePL.tabCompLevelName.get(0).getText()}"`);
        expect(await jePL.tabCompLevelTitle.get(0).getText() == titleACFree)
          .toBeTruthy(`AC Free(${i + 1} in list): Expected "${titleACFree}" instead of "${await jePL.tabCompLevelTitle.get(0).getText()}"`);
        expect(await jePL.tabCompLevelDescription.get(0).getText() == descACFree2)
          .toBeTruthy(`AC Free(${i + 1} in list): Expected "${descACFree2}" instead of "${await jePL.tabCompLevelDescription.get(0).getText()}"`);
      }
      // AC MAGN CHECKS
      if (i < amACMagn && i > 0) {
        expect(await jePL.tabHeaderContentAC.get(1).getText() == lvlACMagn)
          .toBeTruthy(`AC Magn(${i + 1} in list): Expected "${lvlACMagn}" instead of "${await jePL.tabHeaderContentAC.get(1).getText()}"`);
        expect(await jePL.tabCompLevelStats.get(1).getText() == lvlACMagn)
          .toBeTruthy(`AC Magn(${i + 1} in list): Expected "${lvlACMagn}" instead of "${await jePL.tabCompLevelStats.get(1).getText()}"`);
        let acMagnName = await jePL.tabCompLevelName.get(1).getText();
        expect(acMagnName.toLowerCase().trim() == descACMagn1.toLowerCase().trim())
          .toBeTruthy(`AC Magn(${i + 1} in list): Expected "${descACMagn1}" instead of "${await jePL.tabCompLevelName.get(1).getText()}"`);
        expect(await jePL.tabCompLevelTitle.get(1).getText() == titleACMagn)
          .toBeTruthy(`AC Magn(${i + 1} in list): Expected "${titleACMagn}" instead of "${await jePL.tabCompLevelTitle.get(1).getText()}"`);
        // currency 
        let m = await jePL.tabCompLevelDescription.get(1).getText();
        expect(m.indexOf(`USD`) !== -1 || m.indexOf(`$`) !== -1)
          .toBeTruthy(`AC Magn(${i + 1} in list): there is no info about currency (${m.trim()})`);
        // amount
        let expectedAmount = descACMagn2.trim().replace(`USD `, ``).replace(`$`, ``).replace(/,/g, '').replace(/ /g, '').split(`-`);
        let actualAmount = m.trim().replace(`USD `, ``).replace(`$`, ``).replace(/,/g, '').replace(/ /g, '').split(`-`);
        expect(() => {
          for (let i = 0; i < expectedAmount.length; i++) {
            if (expectedAmount[i] == actualAmount[i]) return false;
          }
          return true;
        }).toBeTruthy(`AC Magn(${i + 1} in list): Expected amount "${descACMagn2}" instead of "${m}"`);
      }
      // AC NATU CHECKS
      if (i < amACNatu) {
        expect(await jePL.tabHeaderContentAC.get(2).getText() == lvlACNatu)
          .toBeTruthy(`AC Natu(${i + 1} in list): Expected "${lvlACNatu}" instead of "${await jePL.tabHeaderContentAC.get(2).getText()}"`);
        expect(await jePL.tabCompLevelStats.get(2).getText() == lvlACNatu)
          .toBeTruthy(`AC Natu(${i + 1} in list): Expected "${lvlACNatu}" instead of "${await jePL.tabCompLevelStats.get(2).getText()}"`);
        let acNatuName = await jePL.tabCompLevelName.get(2).getText();
        expect(acNatuName.toLowerCase().trim() == descACNatu1.toLowerCase().trim())
          .toBeTruthy(`AC Natu(${i + 1} in list): Expected "${descACNatu1}" instead of "${await jePL.tabCompLevelName.get(2).getText()}"`);
        expect(await jePL.tabCompLevelTitle.get(2).getText() == titleACNatu)
          .toBeTruthy(`AC Natu(${i + 1} in list): Expected "${titleACNatu}" instead of "${await jePL.tabCompLevelTitle.get(2).getText()}"`);
        expect(await jePL.tabCompLevelDescription.get(2).getText() == descACNatu2)
          .toBeTruthy(`AC Natu(${i + 1} in list): Expected "${descACNatu2}" instead of "${await jePL.tabCompLevelDescription.get(2).getText()}"`);
      }

      await jePO.clickEditJEButton();
      await spinner.waitUntilSpinnerOff();
    }
  });

  it(`JE change dropdowns values (only Non Quantified)`, async () => {
    await page.searchProfileHome(profileName5).then(async () => {
      await page.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });

    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFunctionFilter(4, subFunctionN);
    await listPO.openSPFromListByNo(1);
    await jePO.openJE();
    await jePO.clickEditJEButton();

    await jePL.dropdownACMagn.click();
    await jePL.openedDropdownLvls.get(0).click();
    await spinner.waitUntilSpinnerOff();
    await jePL.dropdownACNatu.click();
    const amACNatuNQ = await jePL.openedDropdownLvls.count();
    await jePL.openedDropdownLvls.get(0).click();
    await spinner.waitUntilSpinnerOff();

    await jePO.saveJEAs(profileName5);
    
    for (let i = 0; i < amACNatuNQ; i++) {
      await jePO.clickEditJEButton();
      await spinner.waitUntilSpinnerOff();

      // AC NATU NQ
      if (i < amACNatuNQ) {
        await jePL.dropdownACNatu.click();
        await spinner.waitUntilSpinnerOff();
        var titleACNatuNQ = await jePL.openedDropdownInfo.get(0).getText();
        var lvlACNatuNQ = await jePL.openedDropdownLvls.get(i % amACNatuNQ).getText();
        var descACNatuNQ1 = await jePL.openedDropdownDescT.get(i % amACNatuNQ / 3 | 0).getText();
        var descACNatuNQ2 = await jePL.openedDropdownDescD.get(i % amACNatuNQ / 3 | 0).getText();
        await jePL.openedDropdownLvls.get(i % amACNatuNQ).click();
        await spinner.waitUntilSpinnerOff();
      }

      await jePO.saveJE();
      
      // AC Natu NQ CHECKS
      if (i < amACNatuNQ) {
        await jePL.tabAC.click();
        await spinner.waitUntilSpinnerOff();
        expect(await jePL.tabHeaderContentAC.get(2).getText() == lvlACNatuNQ)
          .toBeTruthy(`AC Natu NQ(${i + 1} in list): Expected "${lvlACNatuNQ}" instead of "${await jePL.tabHeaderContentAC.get(2).getText()}"`);
        expect(await jePL.tabCompLevelStats.get(2).getText() == lvlACNatuNQ)
          .toBeTruthy(`AC Natu NQ(${i + 1} in list): Expected "${lvlACNatuNQ}" instead of "${await jePL.tabCompLevelStats.get(2).getText()}"`);
        let acNatuNQName = await jePL.tabCompLevelName.get(2).getText();
        expect(acNatuNQName.toLowerCase().trim() == descACNatuNQ1.toLowerCase().trim())
          .toBeTruthy(`AC Natu NQ(${i + 1} in list): Expected "${descACNatuNQ1}" instead of "${await jePL.tabCompLevelName.get(2).getText()}"`);
        expect(await jePL.tabCompLevelTitle.get(2).getText() == titleACNatuNQ)
          .toBeTruthy(`AC Natu NQ(${i + 1} in list): Expected "${titleACNatuNQ}" instead of "${await jePL.tabCompLevelTitle.get(2).getText()}"`);
        expect(await jePL.tabCompLevelDescription.get(2).getText() == descACNatuNQ2)
          .toBeTruthy(`AC Natu NQ(${i + 1} in list): Expected "${descACNatuNQ2}" instead of "${await jePL.tabCompLevelDescription.get(2).getText()}"`);
      }
    }
  });
});


