import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { AppPage as page } from './components/app.po';
import { checkFileExistsWithDelay, checkFileStatWithDelay, deleteFileIfExists } from './components/utils';
import { SuccessProfileListPageObjects as spListPO } from './components/sp-list.po';
import { SuccessProfileDetailsPageObjects as spDetailsPO } from './components/sp-details.po';
import { SuccessProfileLandingPageObjects as spLandingPO } from './components/sp-landing.po';
import { SuccessProfileLandingPageLocators as spLandingPL } from './components/sp-landing.locators';
import { JobEvaluationPageObjects as jePO } from './components/sp-job-evaluation.po';
import { JobEvaluationPageLocators as jePL } from './components/sp-job-evaluation.locators';
import { JobListPageObjects as listPO } from './components/job-list.po';
import { JobListLocators as listPL } from './components/job-list.locators';
import { JobDetailsPageObjects as detailsPO } from './components/job-details.po';
import { JobDetailsLocators as detailsPL } from './components/job-details.locators';
import { JobLandingPageObjects as landingPO } from './components/job-landing.po';
import { JobLandingLocators as landingPL } from './components/job-landing.locators';
import { SpinnerPO as spinner } from './components/spinner';

let xlsx = require(`xlsx`);

const minFileSize = 100000;
const downloadTimeout = 30; //sec

let SP1 = {
  name: `Test ASDFG || arch 1`
  , function: `Corporate Banking`
  , subFunction: `Corporate / Commercial Banking Operations`
  , level: ``
  , subLevel: ``

  , job1: {
    name: `Test ASDFG || Job 1`
    , code: `ADG1`
  }
  , grade: ``
  , shortProfile: ``
  , kfHayPoints: ``

  , resp1: `RR 1`
  , BC1: `BC 1`
  , TC1: {
    name: `TC 2`
    , skill1: `TC Skill 2`
  }
  , task1: `Task 1 1`
  , task2: `Task 1 2`
  , task3: `Task 1 3`
  , tool1: `Tool 1`
  , education: `G Education 1`
  , generalExperience: `G Experience 1`
  , managerialExperience: `M Experience 1`
};

let SP2 = {
  name: `Test ASDFG || arch 2`
  , function: `Pharmaceutical Research and Development`
  , subFunction: `Drug Discovery`

  , job1: {
    name: `Test ASDFG || Job 2`
    , code: `ADG2`
  }
};

let SP3 = {
  name: `Test ASDFG || arch 3`
  , function: `Executive Management`
  , subFunction: `Corporate Executives / C-suite`

  , job1: {
    name: `Test ASDFG || Job 3`
    , code: `ADG3`
  }
  , grade: ``
  , shortProfile: ``
  , kfHayPoints: ``
};

let SP4 = {
  name: `Test ASDFG || arch 4`
  , nameModified: `Test ASDFG || arch 4`
  , function: `Call Center`
  , subFunction: `Inbound an`//d Outbound`
  , functionModified: `Corporate Affairs`
  , subFunctionModified: `Public Relations and Communications`

  , level: ``
  , subLevel: ``

  , job1: {
    name: `Test ASDFG || Job 4`
    , nameModified: `Test ASDFG || Job 4 Modified`
    , code: `ADG4`
    , codeModified: `ADGM4`
  }
  , grade: ``
  , shortProfile: ``
  , kfHayPoints: ``

  , resp1: {
    name: `RR 4`
    , nameModified: `RR 4 Modified`
  }
  , BC1: {
    name: `BC 4`
    , nameModified: `BC 4 Modified`
  }
  , TC1: {
    name: `TC 4`
    , nameModified: `TC 4 Modified`
    , skill1: {
      name: `TC Skill 4`
      , nameModified: `TC Skill 4 Modified`
    }
  }
  , task1: {
    name: `Task 4 1`
    , nameModified: `Task 4 1 Modified`
  }
  , task2: {
    name: `Task 4 2`
    , nameModified: `Task 4 2 Modified`
  }
  , task3: {
    name: `Task 4 3`
    , nameModified: `Task 4 3 Modified`
  }
  , tool1: {
    name: `Tool 4`
    , nameModified: `Tool 4 Modified`
  }
  , education: {
    name: `G Education 4`
    , nameModified: `G Education 4 Modified`
  }
  , generalExperience: {
    name: `G Experience 4`
    , nameModified: `G Experience 4 Modified`
  }
  , managerialExperience: {
    name: `M Experience 4`
    , nameModified: `M Experience 4 Modified`
  }
};

let SP5 = {
  name: `Test ASDFG || arch 5`

  , function: `Call Center`
  , subFunction: `Inbound an` //d Outbound`
  , level: ``
  , subLevel: ``

  , job1: {
    name: `Test ASDFG || Job 5`
    , code: `ADG5`
  }
  , grade: ``
  , shortProfile: ``
  , kfHayPoints: ``


};

let SP6 = {
  name: `Test ASDFG || arch 6`

  , function: `Call Center`
  , subFunction: `Inbound an`//d Outbound`
  , level: ``
  , subLevel: ``

  , job1: {
    name: `Test ASDFG || Job 6`
    , code: `ADG6`
  }
  , grade: ``
  , shortProfile: ``
  , kfHayPoints: ``


};

let SPC1 = {
  name: `Test ASDFG || arch compare 1`

  , function: `Call Center`
  , subFunction: `Inbound an`//d Outbound`
  , functionModified: `Corporate Banking`
  , subFunctionModified: `Corporate / Commercial Banking Operations`
  , level: ``
  , subLevel: ``

  , job1: {
    name: `Test ASDFG || Job compare 1`
    , nameModified: `Test ASDFG || Job compare 1 modified`
    , code: `ADGC1`
    , codeModified: `ADGC1M`
  }
  , grade: ``
  , shortProfile: ``
  , kfHayPoints: ``

  , resp1: {
    name: `RR С1`
    , nameModified: `RR С1 Modified`
  }
  , BC1: {
    name: `BC С1`
    , nameModified: `BC С1 Modified`
  }
  , TC1: {
    name: `TC С1`
    , nameModified: `TC С1 Modified`
    , skill1: {
      name: `TC Skill С1`
      , nameModified: `TC Skill С1 Modified`
    }
  }
  , task1: {
    name: `Task С1 1`
    , nameModified: `Task С1 1 Modified`
  }
  , task2: {
    name: `Task С1 2`
    , nameModified: `Task С1 2 Modified`
  }
  , task3: {
    name: `Task С1 3`
    , nameModified: `Task С1 3 Modified`
  }
  , tool1: {
    name: `Tool С1`
    , nameModified: `Tool С1 Modified`
  }
  , education: {
    name: `G Education С1`
    , nameModified: `G Education С1 Modified`
  }
  , generalExperience: {
    name: `G Experience С1`
    , nameModified: `G Experience С1 Modified`
  }
  , managerialExperience: {
    name: `M Experience С1`
    , nameModified: `M Experience С1 Modified`
  },

  Scores: {
    kh1: ``
    , kh2: ``
    , kh3: ``
    , kh4: ``

    , ps1: ``
    , ps2: ``
    , ps3: ``
    , ps4: ``

    , ac1: ``
    , ac2: ``
    , ac3: ``
    , ac4: ``
  }
};

let SPC2 = {
  name: `Test ASDFG || arch compare 2`

  , function: `Call Center`
  , subFunction: `Inbound an`//d Outbound`
  , functionModified: `Corporate Banking`
  , subFunctionModified: `Corporate / Commercial Banking Operations`
  , level: ``
  , subLevel: ``

  , job1: {
    name: `Test ASDFG || Job compare 2`
    , nameModified: `Test ASDFG || Job compare 2 modified`
    , code: `ADGC2`
    , codeModified: `ADGC2M`
  }
  , grade: ``
  , shortProfile: ``
  , kfHayPoints: ``

  , resp1: {
    name: `RR С2`
    , nameModified: `RR С2 Modified`
  }
  , BC1: {
    name: `BC С2`
    , nameModified: `BC С2 Modified`
  }
  , TC1: {
    name: `TC С2`
    , nameModified: `TC С2 Modified`
    , skill1: {
      name: `TC Skill С2`
      , nameModified: `TC Skill С2 Modified`
    }
  }
  , task1: {
    name: `Task С2 1`
    , nameModified: `Task С2 1 Modified`
  }
  , task2: {
    name: `Task С2 2`
    , nameModified: `Task С2 2 Modified`
  }
  , task3: {
    name: `Task С2 3`
    , nameModified: `Task С2 3 Modified`
  }
  , tool1: {
    name: `Tool С2`
    , nameModified: `Tool С2 Modified`
  }
  , education: {
    name: `G Education С2`
    , nameModified: `G Education С2 Modified`
  }
  , generalExperience: {
    name: `G Experience С2`
    , nameModified: `G Experience С2 Modified`
  }
  , managerialExperience: {
    name: `M Experience С2`
    , nameModified: `M Experience С2 Modified`
  },

  Scores: {
    kh1: ``
    , kh2: ``
    , kh3: ``
    , kh4: ``

    , ps1: ``
    , ps2: ``
    , ps3: ``
    , ps4: ``

    , ac1: ``
    , ac2: ``
    , ac3: ``
    , ac4: ``
  }
};

describe('KFA. Jobs.', () => {
  beforeEach(async () => {
    await page.login(`userOne`);
    await browser.waitForAngularEnabled(false);
  });

  afterEach(async () => {
    await browser.manage().logs().get('browser').then(async (browserLog) => {
      if (browserLog.length > 0) {
        await browserLog.forEach(async log => {
          await expect(browserLog.length > 0).toBeFalsy(`error in console`);//.toBeFalsy(log.message);
        });
      }
    });
  });

  it(`Job: create, search. KFA dashboard`, async () => {
    await page.moveToSPList();

    await spListPO.searchProfile(SP1.name);
    await spListPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await spListPO.deleteSPViaThreeDotsMenu();
      };
    });
    await spListPO.clearSearch();

    await spListPO.setFilter(5, 3);
    await spListPO.copyProfileViaThreeDotsMenu();
    await spDetailsPO.openEditPageViaActions();
    await spLandingPO.selectEditPageItem(1);
    await spLandingPO.editProfileTitle(SP1.name);
    await spLandingPO.setProfileFunctionByName(SP1.function);
    await spLandingPO.setProfileSubFunctionByName(SP1.subFunction);
    await spLandingPO.clickNextButton();

    await spLandingPO.selectEditPageItem(2);
    await spLandingPO.deleteAllResponsibilities();
    await spLandingPO.addResponsibility();
    await spLandingPO.editResponsibilityName(1, SP1.resp1);
    await spLandingPO.clickNextButton();
    await spLandingPO.confirmUpdate(2);
    await spLandingPO.deleteAllTasks();
    await spLandingPO.addTask();
    await spLandingPO.editTask(1, 1, SP1.task1);
    await spLandingPO.addTask();
    await spLandingPO.editTask(2, 1, SP1.task2);
    await spLandingPO.addTask();
    await spLandingPO.editTask(3, 2, SP1.task3);
    await spLandingPO.clickNextButton();

    await spLandingPO.saveProfile();
    await spDetailsPO.openEditPageViaActions();

    await spLandingPO.selectEditPageItem(3);
    await spLandingPO.recommendationsDone();
    await spLandingPO.deleteAllBCompetencies();
    await spLandingPO.addBCompetency();
    await spLandingPO.editBCompetencyName(1, SP1.BC1);
    await spLandingPO.clickNextButton();
    await spLandingPO.recommendationsDone();
    await spLandingPO.deleteAllTCompetencies();
    await spLandingPO.addTCompetency();
    await spLandingPO.editTCompetencyName(1, SP1.TC1.name);
    await spLandingPO.addNewSkillActions(1, SP1.TC1.skill1);
    await spLandingPO.clickNextButton();
    await spLandingPO.deleteAllTools();
    await spLandingPO.clickAddTool();
    await spLandingPO.editToolName(1, SP1.tool1);
    await spLandingPO.addToolExample(1, 'ONE');
    await spLandingPO.clickNextButton();
    await spLandingPO.deleteAllEduNExp();
    await spLandingPO.addAllEducation();
    await spLandingPO.editEducationName(1, SP1.education);
    await spLandingPO.addAllExperience();
    await spLandingPO.editExperienceName(1, SP1.generalExperience);
    await spLandingPO.editExperienceName(2, SP1.managerialExperience);
    await spLandingPO.clickNextButton();

    let leveldata1 = await spLandingPL.landingDetails.get(2).getText();
    let levelInfo1 = leveldata1.split(`|`);
    SP1.level = levelInfo1[0].replace(`Level: `, ``).trim();
    SP1.subLevel = levelInfo1[1].trim();

    await spLandingPO.saveProfile();
    await page.moveToSPList();
    await spListPO.clearAllFiltersF();

    await page.moveToArchitectTab();
    await listPO.moveToJobsList();

    await listPO.searchProfile(SP1.job1.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.openJobFromList(1);
        await detailsPO.deleteJobViaActions();
        await listPO.moveToJobsList();
      };
    });
    await listPO.clearSearch();

    await listPO.clickAddNewJobButton();
    await listPO.searchProfile(SP1.name);
    await listPO.clickUseThisBtn(1);
    await listPO.customizeJob(SP1.job1.name, SP1.job1.code);
    await listPO.clickAddCustomizeJob();
    await listPO.clearSearch();

    await listPO.moveToJobsList();
    await listPO.searchJobByTitle(SP1.job1.name);
    await listPO.openJobFromList();
    await detailsPO.editJobViaActions();
    await landingPO.clickEditButton(1);
    await landingPO.changeJobFunction(SP1.function);
    await landingPO.changeJobSubFunction(SP1.subFunction);
    await landingPO.clickNextButton();
    await landingPO.saveJob();

    await listPO.moveToJobsList();
    await listPO.searchJobByTitle(SP1.job1.name);
    await listPO.setMatrixView();
    await listPO.setFunctionFilteByName(SP1.subFunction);

    await listPO.clickMatrixItem(1);
    await spinner.waitUntilSpinnerOff();
    let bName = await listPL.popupName.getText();
    await expect(bName == SP1.job1.name).toBeTruthy(`Job name: expected '${SP1.job1.name}' instead of '${bName}'`);
    // let bGrade = await listPL.popupGrade.getText();
    // await expect(bGrade == `Grade: ${SP1.grade}`).toBeTruthy(`Job grade: expected 'Grade: ${SP1.grade}' instead of '${bGrade}'`);
    // let bShortProfile = await listPL.popupShortProfile.getText();
    // await expect(bShortProfile == `Short Profile: ${SP1.shortProfile}`).toBeTruthy(`Job short profile: expected 'Short Profile: ${SP1.shortProfile}' instead of '${bShortProfile}'`);
    // let bKFHayPoints = await listPL.popupHayPoints.getText();
    // await expect(bKFHayPoints == `Korn Ferry Hay Points: ${SP1.kfHayPoints}`).toBeTruthy(`Job Hay points: expected 'Korn Ferry Hay Points: ${SP1.kfHayPoints}' instead of '${bKFHayPoints}'`);

    await page.moveToArchitectTab();
    await listPO.setRecentlyModified();

    let lmName = await listPL.dashboardJobNames.get(0).getText();
    await expect(lmName == SP1.job1.name).toBeTruthy(`Dashboard: expected '${SP1.name}' instead of '${lmName}'`);
    let lmLevel = await listPL.dashboardJobLevel.get(0).getText();
    await expect(lmLevel.indexOf(SP1.level) !== -1).toBeTruthy(`Dashboard: expected '${SP1.level}' instead of '${lmLevel}'`);
    let lmFunc = await listPL.dashboardJobFunctions.get(0).getText();
    await expect(lmFunc.indexOf(SP1.function) !== -1).toBeTruthy(`Dashboard: expected '${SP1.function}' instead of '${lmFunc}'`);
    let lmSubFunc = await listPL.dashboardJobSubFunctions.get(0).getText();
    await expect(SP1.subFunction.indexOf(lmSubFunc.replace(`...`, ``)) !== -1).toBeTruthy(`Dashboard: expected '${SP1.subFunction}' instead of '${lmSubFunc}'`);
    // let lmGrade = await listPL.dashboardJobGrades.get(0).getText();
    // await expect(lmGrade == SP1.grade).toBeTruthy(`Dashboard: expected '${SP1.grade}' instead of '${lmGrade}'`);
    // let lmSP = await listPL.dashboardJobShortProfiles.get(0).getText();
    // await expect(lmSP == SP1.shortProfile).toBeTruthy(`Dashboard: expected '${SP1.shortProfile}' instead of '${lmSP}'`);
    // let lmHP = await listPL.dashboardJobHayPoints.get(0).getText();
    // await expect(lmHP == SP1.kfHayPoints).toBeTruthy(`Dashboard: expected '${SP1.kfHayPoints}' instead of '${lmHP}'`);

    await listPO.moveToJobsList();
    await listPO.searchJobByTitle(SP1.job1.name);
    await listPO.openJobFromList(1);
    await page.moveToArchitectTab();
    await listPO.setRecentlyViewed();

    let lmName2 = await listPL.dashboardJobNames.get(0).getText();
    await expect(lmName2 == SP1.job1.name).toBeTruthy(`Dashboard(recently viewed): expected '${SP1.job1.name}' instead of '${lmName2}'`);
    let lmLevel2 = await listPL.dashboardJobLevel.get(0).getText();
    await expect(lmLevel2.indexOf(SP1.level) !== -1).toBeTruthy(`Dashboard(recently viewed): expected '${SP1.level}' instead of '${lmLevel2}'`);
    let lmFunc2 = await listPL.dashboardJobFunctions.get(0).getText();
    await expect(lmFunc2.indexOf(SP1.function) !== -1).toBeTruthy(`Dashboard(recently viewed): expected '${SP1.function}' instead of '${lmFunc2}'`);
    let lmSubFunc2 = await listPL.dashboardJobSubFunctions.get(0).getText();
    await expect(SP1.subFunction.indexOf(lmSubFunc2.replace(`...`, ``)) !== -1).toBeTruthy(`Dashboard(recently viewed): expected '${SP1.subFunction}' instead of '${lmSubFunc2}'`);
    // let lmGrade2 = await listPL.dashboardJobGrades.get(0).getText();
    // await expect(lmGrade2 == SP1.grade).toBeTruthy(`Dashboard(recently viewed): expected '${SP1.grade}' instead of '${lmGrade2}'`);
    // let lmSP2 = await listPL.dashboardJobShortProfiles.get(0).getText();
    // await expect(lmSP2 == SP1.shortProfile).toBeTruthy(`Dashboard(recently viewed): expected '${SP1.shortProfile}' instead of '${lmSP2}'`);
    // let lmHP2 = await listPL.dashboardJobHayPoints.get(0).getText();
    // await expect(lmHP2 == SP1.kfHayPoints).toBeTruthy(`Dashboard(recently viewed): expected '${SP1.kfHayPoints}' instead of '${lmHP2}'`);

    await listPO.moveToJobsList();
    await listPO.searchJobByTitle(SP1.job1.name);

    let lTitle = await listPL.jobListNames.get(0).getText();
    await expect(lTitle == SP1.job1.name).toBeTruthy(`List: expected '${SP1.job1.name}' instead of '${lTitle}'`);
    let lFunc = await listPL.jobListFunction.get(0).getText();
    await expect(lFunc == SP1.function).toBeTruthy(`List: expected '${SP1.function}' instead of '${lFunc}'`);
    // let lHP = await listPL.jobListHP.get(0).getText();
    // await expect(lHP == SP1.kfHayPoints).toBeTruthy(`List: expected '${SP1.kfHayPoints}' instead of '${lHP}'`);
    // let lGrade = await listPL.jobListGrade.get(0).getText();
    // await expect(lGrade == SP1.grade).toBeTruthy(`List: expected '${SP1.grade}' instead of '${lGrade}'`);

    await listPO.openJobFromList(1);

    let aTitle = await detailsPL.title.getText();
    await expect(aTitle == SP1.job1.name).toBeTruthy(`Details: expected '${SP1.job1.name}' instead of '${aTitle}'`);
    let aLevel = await detailsPL.level.getText();
    await expect(aLevel.indexOf(SP1.level) !== -1).toBeTruthy(`Details: expected '${SP1.level}' instead of '${aLevel}'`);
    let aSubLevel = await detailsPL.subLevel.getText();
    await expect(aSubLevel.indexOf(SP1.subLevel) !== -1).toBeTruthy(`Details: expected '${SP1.subLevel}' instead of '${aSubLevel}'`);
    let aFunc = await detailsPL.function.getText();
    await expect(aFunc.indexOf(SP1.function) !== -1).toBeTruthy(`Details: expected '${SP1.function}' instead of '${aFunc}'`);
    let aSubFunc = await detailsPL.subFunction.getText();
    await expect(aSubFunc.indexOf(SP1.subFunction) !== -1).toBeTruthy(`Details: expected '${SP1.subFunction}' instead of '${aSubFunc}'`);
    // let aGrade = await detailsPL.grade.getText();
    // await expect(aGrade == SP1.grade).toBeTruthy(`Details: expected '${SP1.grade}' instead of '${aGrade}'`);
    // let aSP = await detailsPL.shortProfile.getText();
    // await expect(aSP == SP1.shortProfile).toBeTruthy(`Details: expected '${SP1.shortProfile}' instead of '${aSP}'`);
    // let aHP = await detailsPL.hayPoints.getText();
    // await expect(aHP == SP1.kfHayPoints).toBeTruthy(`Details: expected '${SP1.kfHayPoints}' instead of '${aHP}'`);

    let aResp = await detailsPL.resps.get(0).getText();
    await expect(aResp == SP1.resp1).toBeTruthy(`Details: expected '${SP1.resp1}' instead of '${aResp}'`);
    let aBC = await detailsPL.BC.get(0).getText();
    await expect(aBC == SP1.BC1).toBeTruthy(`Details: expected '${SP1.BC1}' instead of '${aBC}'`);
    let aTC = await detailsPL.TC.get(0).getText();
    await expect(aTC == SP1.TC1.name).toBeTruthy(`Details: expected '${SP1.TC1.name}' instead of '${aTC}'`);

    await listPO.moveToJobsList();
    // await listPO.searchJobByCode(SP1.job1.code);

    // let lTitle2 = await listPL.jobListNames.get(0).getText();
    // await expect(lTitle2 == SP1.job1.name).toBeTruthy(`List: expected '${SP1.job1.name}' instead of '${lTitle2}'`);
    // let lCode2 = await listPL.jobListCode.get(0).getText();
    // await expect(lCode2 == SP1.job1.code).toBeTruthy(`List: expected '${SP1.job1.code}' instead of '${lCode2}'`);
    // let lFunc2 = await listPL.jobListFunction.get(0).getText();
    // await expect(lFunc2 == SP1.function).toBeTruthy(`List: expected '${SP1.function}' instead of '${lFunc2}'`);
    // let lHP2 = await listPL.jobListHP.get(0).getText();
    // await expect(lHP2 == SP1.kfHayPoints).toBeTruthy(`List: expected '${SP1.kfHayPoints}' instead of '${lHP2}'`);
    // let lGrade2 = await listPL.jobListGrade.get(0).getText();
    // await expect(lGrade2 == SP1.grade).toBeTruthy(`List: expected '${SP1.grade}' instead of '${lGrade2}'`);

    await listPO.clearSearch();
    await listPO.searchJobByFunction(SP1.function);

    let lTitle3 = await listPL.jobListNames.get(0).getText();
    await expect(lTitle3 == SP1.job1.name).toBeTruthy(`List: expected '${SP1.job1.name}' instead of '${lTitle3}'`);
    let lFunc3 = await listPL.jobListFunction.get(0).getText();
    await expect(lFunc3 == SP1.function).toBeTruthy(`List: expected '${SP1.function}' instead of '${lFunc3}'`);
    // let lHP3 = await listPL.jobListHP.get(0).getText();
    // await expect(lHP3 == SP1.kfHayPoints).toBeTruthy(`List: expected '${SP1.kfHayPoints}' instead of '${lHP3}'`);
    // let lGrade3 = await listPL.jobListGrade.get(0).getText();
    // await expect(lGrade3 == SP1.grade).toBeTruthy(`List: expected '${SP1.grade}' instead of '${lGrade3}'`);

    await listPO.clearSearch();
    await listPO.clearAllFilters();
  });

  it(`Download Job summary report: from job details, from KFA dashboard`, async () => {
    await page.moveToSPList();

    await spListPO.searchProfile(SP2.name);
    await spListPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await spListPO.deleteSPViaThreeDotsMenu();
      };
    });
    await spListPO.clearSearch();

    await spListPO.setFilter(5, 3);
    await spListPO.copyProfileViaThreeDotsMenu();
    await spDetailsPO.openEditPageViaActions();
    await spLandingPO.selectEditPageItem(1);
    await spLandingPO.editProfileTitle(SP2.name);
    await spLandingPO.setProfileFunctionByName(SP2.function);
    await spLandingPO.setProfileSubFunctionByName(SP2.subFunction);
    await spLandingPO.clickNextButton();
    await spLandingPO.saveProfile();

    await page.moveToSPList();
    await spListPO.clearAllFilters();

    await page.moveToArchitectTab();
    await listPO.moveToJobsList();

    await listPO.searchJobByTitle(SP2.job1.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.openJobFromList(1);
        await detailsPO.deleteJobViaActions();
        await listPO.moveToJobsList();
      };
    });
    await listPO.clearSearch();

    await listPO.clickAddNewJobButton();
    await listPO.searchProfile(SP2.name);
    await listPO.clickUseThisBtn(1);

    await listPO.customizeJob(SP2.job1.name, SP2.job1.code);
    await listPO.clickAddCustomizeJob();
    await listPO.clearSearch();

    await listPO.moveToJobsList();
    await listPO.searchJobByTitle(SP2.job1.name);
    await listPO.openJobFromList();

    let temp = `temp/${SP2.job1.name}.pdf`;
    const fileName = temp.replace(`|`, `_`).replace(`|`, `_`);

    deleteFileIfExists(fileName);
    await detailsPO.clickSummaryReportLink();
    await expect(await checkFileExistsWithDelay(fileName, downloadTimeout)).toBeTruthy(`Details: Can't load Summary report for '${SP2.job1.name}' job`);
    await expect(await checkFileStatWithDelay(fileName, minFileSize, downloadTimeout)).toBeTruthy(`Details: File must be greater than ${minFileSize}(Job name: ${SP2.job1.name})`);

    await page.moveToArchitectTab();
    let n = await listPL.dashboardDownloadListNames.get(0).getText();
    await expect(n == SP2.job1.name).toBeTruthy(`Dashboard: expected last summary download ${SP1.job1.name} instead of ${n}`);

    deleteFileIfExists(fileName);
    await listPO.clickSummaryReportLink();
    await expect(await checkFileExistsWithDelay(fileName, downloadTimeout)).toBeTruthy(`Dashboard: Can't load Summary report for '${SP2.job1.name}' job`);
    await expect(await checkFileStatWithDelay(fileName, minFileSize, downloadTimeout)).toBeTruthy(`Dashboard: File must be greater than ${minFileSize}(Job name: ${SP2.job1.name})`);

  });

  xit(`KFA Dashboard: Grade/Amount statistics`, async () => {
    await page.moveToSPList();

    await spListPO.searchProfile(SP3.name);
    await spListPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await spListPO.deleteSPViaThreeDotsMenu();
      };
    });
    await spListPO.clearSearch();

    await spListPO.setFilter(5, 3);
    await spListPO.copyProfileViaThreeDotsMenu();
    await spDetailsPO.openEditPageViaActions();
    await spLandingPO.selectEditPageItem(1);
    await spLandingPO.editProfileTitle(SP3.name);
    await spLandingPO.setProfileFunctionByName(SP3.function);
    await spLandingPO.setProfileSubFunctionByName(SP3.subFunction);
    await spLandingPO.clickNextButton();

    await spLandingPO.selectEditPageItem(2);
    await spLandingPO.allRespLVLUP(10);
    await spLandingPO.clickNextButton();
    await spLandingPO.ConfirmUpdate();
    await spLandingPO.clickNextButton();

    await spLandingPO.saveProfile();
    SP3.grade = `${await detailsPL.detailGrade.get(0).getText()} ${await detailsPL.detailGrade.get(1).getText()}`;
    await page.moveToSPList();
    await spListPO.clearAllFilters();

    await page.moveToArchitectTab();
    await listPO.moveToJobsList();

    await listPO.searchProfile(SP3.job1.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.openJobFromList(1);
        await detailsPO.deleteJobViaActions();
        await listPO.moveToJobsList();
      };
    });
    await listPO.clearSearch();

    await page.moveToArchitectTab();
    let amountBefore = ``;
    let am = await listPL.gradesListNames.count();
    for (let i = 0; i < am; i++) {
      let t = await listPL.gradesListNames.get(i).getText();
      if (t == SP3.grade) {
        amountBefore = await listPL.gradesListAmounts.get(i).getText();
        break;
      }
    }
    console.log(amountBefore);
    await listPO.moveToJobsList();

    await listPO.clickAddNewJobButton();
    await listPO.searchProfile(SP3.name);
    await listPO.clickUseThisBtn(1);
    await listPO.customizeJob(SP3.job1.name, SP3.job1.code);
    await listPO.clickAddCustomizeJob();
    await listPO.clearSearch();

    await page.moveToArchitectTab();
    let amountAfter = ``;
    for (let i = 0; i < am; i++) {
      let t = await listPL.gradesListNames.get(i).getText();
      console.log(SP3.grade, `|`, t);
      if (t == SP3.grade) {
        amountAfter = await listPL.gradesListAmounts.get(i).getText();
        break;
      }
    }
    console.log(amountAfter);
    await expect(parseInt(amountAfter) - parseInt(amountBefore) == 1).toBeTruthy(`Dashboard: expected ${parseInt(amountBefore) + 1} jobs instead of ${amountAfter} for grade ${SP3.grade}`);
  });

  it(`Job: edit`, async () => {
    await page.moveToSPList();

    await spListPO.searchProfile(SP4.name);
    await spListPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await spListPO.deleteSPViaThreeDotsMenu();
      };
    });
    await spListPO.clearSearch();

    await spListPO.setFilter(5, 3);
    await spListPO.copyProfileViaThreeDotsMenu();
    await spDetailsPO.openEditPageViaActions();
    await spLandingPO.selectEditPageItem(1);
    await spLandingPO.editProfileTitle(SP4.name);
    await spLandingPO.setProfileFunctionByName(SP4.function);
    await spLandingPO.setProfileSubFunctionByName(SP4.subFunction);
    await spLandingPO.clickNextButton();
    await spLandingPO.saveProfile();

    await page.moveToSPList();
    await spListPO.clearAllFilters();

    await page.moveToArchitectTab();
    await listPO.moveToJobsList();

    await listPO.searchProfile(SP4.job1.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.openJobFromList(1);
        await detailsPO.deleteJobViaActions();
        await listPO.moveToJobsList();
      };
    });
    await listPO.clearSearch();

    await page.moveToArchitectTab();
    await listPO.moveToJobsList();

    await listPO.clickAddNewJobButton();
    await listPO.searchProfile(SP4.name);
    await listPO.clickUseThisBtn(1);

    await listPO.customizeJob(SP4.job1.name, SP4.job1.code);
    await listPO.clickAddCustomizeJob();
    await listPO.clearSearch();

    await listPO.moveToJobsList();
    await listPO.searchJobByTitle(SP4.job1.name);
    await listPO.openJobFromList();
    await detailsPO.editJobViaActions();

    await landingPO.clickEditButton(1);
    await landingPO.changeJobName(SP4.job1.nameModified);
    await landingPO.changeJobCode(SP4.job1.codeModified);
    await landingPO.changeJobFunction(SP4.functionModified);
    await landingPO.changeJobSubFunction(SP4.subFunctionModified);
    await landingPO.clickNextButton();

    await landingPO.clickEditButton(2);
    await landingPO.deleteAllResps();
    await landingPO.addResp();
    await landingPO.renameResp(1, SP4.resp1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.confirmPMTUpdate();
    await landingPO.deleteAllTasks();
    await landingPO.addTask();
    await landingPO.editTask(1, 1, SP4.task1.nameModified);
    await landingPO.addTask();
    await landingPO.editTask(2, 1, SP4.task2.nameModified);
    await landingPO.addTask();
    await landingPO.editTask(3, 2, SP4.task3.nameModified);
    await landingPO.clickNextButton();

    await landingPO.saveJob();
    await detailsPO.editJobViaActions();

    await landingPO.clickEditButton(3);
    await landingPO.recommendationsDone();
    await landingPO.deleteAllBCompetencies();
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(1, SP4.BC1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SP4.TC1.nameModified);
    await landingPO.addNewSkillActions(1, SP4.TC1.skill1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SP4.tool1.nameModified);
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.deleteAllEduNExp();
    await landingPO.addAllEducation();
    await landingPO.editEducationName(1, SP4.education.nameModified);
    await landingPO.addAllExperience();
    await landingPO.editExperienceName(1, SP4.generalExperience.nameModified);
    await landingPO.editExperienceName(2, SP4.managerialExperience.nameModified);
    await landingPO.clickNextButton();

    SP4.grade = await landingPL.landingDetails.get(5).getText();
    let leveldata1 = await landingPL.landingDetails.get(2).getText();
    let levelInfo1 = leveldata1.split(`|`);
    SP4.level = levelInfo1[0].replace(`Level: `, ``).trim();
    SP4.subLevel = levelInfo1[1].trim();

    await landingPO.saveJob();
    await detailsPO.openJE();

    SP4.grade = await jePL.textCharts.get(0).getText();
    SP4.shortProfile = await jePL.textCharts.get(1).getText();
    SP4.kfHayPoints = await jePL.textCharts.get(2).getText();

    await detailsPO.clickCancelJE();

    let aTitle = await detailsPL.title.getText();
    await expect(aTitle == SP4.job1.nameModified).toBeTruthy(`Details: expected '${SP4.job1.nameModified}' instead of '${aTitle}'`);
    let aCode = await detailsPL.code.getText();
    await expect(aCode == SP4.job1.codeModified).toBeTruthy(`Details: expected '${SP4.job1.codeModified}' instead of '${aCode}'`);
    let aLevel = await detailsPL.level.getText();
    await expect(aLevel.indexOf(SP4.level) !== -1).toBeTruthy(`Details: expected '${SP4.level}' instead of '${aLevel}'`);
    let aSubLevel = await detailsPL.subLevel.getText();
    await expect(aSubLevel.indexOf(SP4.subLevel) !== -1).toBeTruthy(`Details: expected '${SP4.subLevel}' instead of '${aSubLevel}'`);
    let aFunc = await detailsPL.function.getText();
    await expect(aFunc.indexOf(SP4.functionModified) !== -1).toBeTruthy(`Details: expected '${SP4.functionModified}' instead of '${aFunc}'`);
    let aSubFunc = await detailsPL.subFunction.getText();
    await expect(aSubFunc.indexOf(SP4.subFunctionModified) !== -1).toBeTruthy(`Details: expected '${SP4.subFunctionModified}' instead of '${aSubFunc}'`);
    let aGrade = await detailsPL.grade.getText();
    await expect(aGrade == SP4.grade).toBeTruthy(`Details: expected '${SP4.grade}' instead of '${aGrade}'`);
    let aSP = await detailsPL.shortProfile.getText();
    await expect(aSP == SP4.shortProfile).toBeTruthy(`Details: expected '${SP4.shortProfile}' instead of '${aSP}'`);
    let aHP = await detailsPL.hayPoints.getText();
    await expect(aHP == SP4.kfHayPoints).toBeTruthy(`Details: expected '${SP4.kfHayPoints}' instead of '${aHP}'`);

    let aResp = await detailsPL.resps.get(0).getText();
    await expect(aResp == SP4.resp1.nameModified).toBeTruthy(`Details: expected '${SP4.resp1.nameModified}' instead of '${aResp}'`);
    let aBC = await detailsPL.BC.get(0).getText();
    await expect(aBC == SP4.BC1.nameModified).toBeTruthy(`Details: expected '${SP4.BC1.nameModified}' instead of '${aBC}'`);
    let aTC = await detailsPL.TC.get(0).getText();
    await expect(aTC == SP4.TC1.nameModified).toBeTruthy(`Details: expected '${SP4.TC1.nameModified}' instead of '${aTC}'`);

  });

  it(`Job: search, filters`, async () => {
    let tod = new Date();

    await page.moveToSPList();

    await spListPO.searchProfile(SP5.name);
    await spListPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await spListPO.deleteSPViaThreeDotsMenu();
      };
    });
    await spListPO.clearSearch();

    await spListPO.setFilter(5, 3);
    await spListPO.copyProfileViaThreeDotsMenu();
    await spDetailsPO.openEditPageViaActions();
    await spLandingPO.selectEditPageItem(1);
    await spLandingPO.editProfileTitle(SP5.name);
    await spLandingPO.setProfileFunctionByName(SP5.function);
    await spLandingPO.setProfileSubFunctionByName(SP5.subFunction);
    await spLandingPO.clickNextButton();
    await spLandingPO.saveProfile();
    // await jePO.openJE();
    // await jePO.clickEditJEButton();
    // SP3.grade = `${await detailsPL.detailGrade.get(0).getText()} ${await detailsPL.detailGrade.get(1).getText()}`;
    // SP5.shortProfile = await jePL.editTextCharts.get(1).getText();
    // SP5.kfHayPoints = await jePL.editTextCharts.get(2).getText();
    // await jePO.saveJEWOConfirm();
    // await jePO.clickCancelButton();
    await page.moveToSPList();
    await spListPO.clearAllFilters();

    await page.moveToArchitectTab();
    await listPO.moveToJobsList();

    await listPO.searchProfile(SP5.job1.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.openJobFromList(1);
        await detailsPO.deleteJobViaActions();
        await listPO.moveToJobsList();
      };
    });
    await listPO.clearSearch();

    await page.moveToArchitectTab();
    await listPO.moveToJobsList();

    await listPO.clickAddNewJobButton();
    await listPO.searchProfile(SP5.name);
    await listPO.clickUseThisBtn(1);
    await listPO.customizeJob(SP5.job1.name, SP5.job1.code);
    await listPO.clickAddCustomizeJob();
    await listPO.clearSearch();

    await listPO.moveToJobsList();
    await listPO.searchProfile(SP5.job1.name);
    await listPO.openJobFromList();
    await detailsPO.openJE();
    SP5.grade = await jePL.editTextCharts.get(0).getText();
    SP5.shortProfile = await jePL.editTextCharts.get(1).getText();
    SP5.kfHayPoints = await jePL.editTextCharts.get(2).getText();
    console.log(SP5);
    await detailsPO.clickCancelJEwoCofirm();

    await listPO.moveToJobsList();

    await listPO.setGradeFilter(SP5.grade);
    await listPO.searchJobByTitle(SP5.job1.name);
    let lTitle = await listPL.jobListNames.get(0).getText();
    await expect(lTitle == SP5.job1.name).toBeTruthy(`List: expected '${SP5.job1.name}' instead of '${lTitle}'`);
    let lFunc = await listPL.jobListFunction.get(0).getText();
    await expect(lFunc.indexOf(SP5.function) !== -1).toBeTruthy(`List: expected '${SP5.function}' instead of '${lFunc}'`);
    let lHP = await listPL.jobListHP.get(0).getText();
    await expect(lHP == SP5.kfHayPoints).toBeTruthy(`List: expected '${SP5.kfHayPoints}' instead of '${lHP}'`);
    let lGrade = await listPL.jobListGrade.get(0).getText();
    await expect(lGrade == SP5.grade).toBeTruthy(`List: expected '${SP5.grade}' instead of '${lGrade}'`);

    await listPO.setUpdatedByFilter(`Pavel Minrov`); // TEST: clm user one // STAGE: clm user one // PROD // EU: Pavel Minrov
    await listPO.searchJobByTitle(SP5.job1.name);
    lTitle = await listPL.jobListNames.get(0).getText();
    await expect(lTitle == SP5.job1.name).toBeTruthy(`List: expected '${SP5.job1.name}' instead of '${lTitle}'`);
    lFunc = await listPL.jobListFunction.get(0).getText();
    await expect(lFunc.indexOf(SP5.function) !== -1).toBeTruthy(`List: expected '${SP5.function}' instead of '${lFunc}'`);
    lHP = await listPL.jobListHP.get(0).getText();
    await expect(lHP == SP5.kfHayPoints).toBeTruthy(`List: expected '${SP5.kfHayPoints}' instead of '${lHP}'`);
    lGrade = await listPL.jobListGrade.get(0).getText();
    await expect(lGrade == SP5.grade).toBeTruthy(`List: expected '${SP5.grade}' instead of '${lGrade}'`);

    await listPO.setKFHPFilter(SP5.kfHayPoints, SP5.kfHayPoints);
    await listPO.searchJobByTitle(SP5.job1.name);
    lTitle = await listPL.jobListNames.get(0).getText();
    await expect(lTitle == SP5.job1.name).toBeTruthy(`List: expected '${SP5.job1.name}' instead of '${lTitle}'`);
    lFunc = await listPL.jobListFunction.get(0).getText();
    await expect(lFunc.indexOf(SP5.function) !== -1).toBeTruthy(`List: expected '${SP5.function}' instead of '${lFunc}'`);
    lHP = await listPL.jobListHP.get(0).getText();
    await expect(lHP == SP5.kfHayPoints).toBeTruthy(`List: expected '${SP5.kfHayPoints}' instead of '${lHP}'`);
    lGrade = await listPL.jobListGrade.get(0).getText();
    await expect(lGrade == SP5.grade).toBeTruthy(`List: expected '${SP5.grade}' instead of '${lGrade}'`);

    await listPO.setModifiedDateFilter(tod, tod);
    await listPO.searchJobByTitle(SP5.job1.name);
    lTitle = await listPL.jobListNames.get(0).getText();
    await expect(lTitle == SP5.job1.name).toBeTruthy(`List: expected '${SP5.job1.name}' instead of '${lTitle}'`);
    lFunc = await listPL.jobListFunction.get(0).getText();
    await expect(lFunc.indexOf(SP5.function) !== -1).toBeTruthy(`List: expected '${SP5.function}' instead of '${lFunc}'`);
    lHP = await listPL.jobListHP.get(0).getText();
    await expect(lHP == SP5.kfHayPoints).toBeTruthy(`List: expected '${SP5.kfHayPoints}' instead of '${lHP}'`);
    lGrade = await listPL.jobListGrade.get(0).getText();
    await expect(lGrade == SP5.grade).toBeTruthy(`List: expected '${SP5.grade}' instead of '${lGrade}'`);

    await listPO.clearAllFilters();

    await listPO.setMatrixView();
    await listPO.setGradeFilter(SP5.grade);
    await listPO.searchJobByTitle(SP5.job1.name);
    await listPO.clickMatrixItem(1);
    let bName = await listPL.popupName.getText();
    await expect(bName == SP5.job1.name).toBeTruthy(`Job name: expected '${SP5.job1.name}' instead of '${bName}'`);
    let bGrade = await listPL.popupGrade.getText();
    await expect(bGrade == `Grade: ${SP5.grade}`).toBeTruthy(`Job grade: expected 'Grade: ${SP5.grade}' instead of '${bGrade}'`);
    let bShortProfile = await listPL.popupShortProfile.getText();
    await expect(bShortProfile == `Short Profile: ${SP5.shortProfile}`).toBeTruthy(`Job short profile: expected 'Short Profile: ${SP5.shortProfile}' instead of '${bShortProfile}'`);
    let bKFHayPoints = await listPL.popupHayPoints.getText();
    await expect(bKFHayPoints == `Korn Ferry Hay Points: ${SP5.kfHayPoints}`).toBeTruthy(`Job Hay points: expected 'Korn Ferry Hay Points: ${SP5.kfHayPoints}' instead of '${bKFHayPoints}'`);
    await listPO.clearAllFilters();

    await listPO.setUpdatedByFilter(`clm user one`);
    await listPO.searchJobByTitle(SP5.job1.name);
    await listPO.clickMatrixItem(1);
    bName = await listPL.popupName.getText();
    await expect(bName == SP5.job1.name).toBeTruthy(`Job name: expected '${SP5.job1.name}' instead of '${bName}'`);
    bGrade = await listPL.popupGrade.getText();
    await expect(bGrade == `Grade: ${SP5.grade}`).toBeTruthy(`Job grade: expected 'Grade: ${SP5.grade}' instead of '${bGrade}'`);
    bShortProfile = await listPL.popupShortProfile.getText();
    await expect(bShortProfile == `Short Profile: ${SP5.shortProfile}`).toBeTruthy(`Job short profile: expected 'Short Profile: ${SP5.shortProfile}' instead of '${bShortProfile}'`);
    bKFHayPoints = await listPL.popupHayPoints.getText();
    await expect(bKFHayPoints == `Korn Ferry Hay Points: ${SP5.kfHayPoints}`).toBeTruthy(`Job Hay points: expected 'Korn Ferry Hay Points: ${SP5.kfHayPoints}' instead of '${bKFHayPoints}'`);

    await listPO.setKFHPFilter(SP5.kfHayPoints, SP5.kfHayPoints);
    await listPO.searchJobByTitle(SP5.job1.name);
    await listPO.clickMatrixItem(1);
    bName = await listPL.popupName.getText();
    await expect(bName == SP5.job1.name).toBeTruthy(`Job name: expected '${SP5.job1.name}' instead of '${bName}'`);
    bGrade = await listPL.popupGrade.getText();
    await expect(bGrade == `Grade: ${SP5.grade}`).toBeTruthy(`Job grade: expected 'Grade: ${SP5.grade}' instead of '${bGrade}'`);
    bShortProfile = await listPL.popupShortProfile.getText();
    await expect(bShortProfile == `Short Profile: ${SP5.shortProfile}`).toBeTruthy(`Job short profile: expected 'Short Profile: ${SP5.shortProfile}' instead of '${bShortProfile}'`);
    bKFHayPoints = await listPL.popupHayPoints.getText();
    await expect(bKFHayPoints == `Korn Ferry Hay Points: ${SP5.kfHayPoints}`).toBeTruthy(`Job Hay points: expected 'Korn Ferry Hay Points: ${SP5.kfHayPoints}' instead of '${bKFHayPoints}'`);
    await listPO.clearAllFilters();

    await listPO.setModifiedDateFilter(tod, tod);
    await listPO.searchJobByTitle(SP5.job1.name);
    await listPO.clickMatrixItem(1);
    bName = await listPL.popupName.getText();
    await expect(bName == SP5.job1.name).toBeTruthy(`Job name: expected '${SP5.job1.name}' instead of '${bName}'`);
    bGrade = await listPL.popupGrade.getText();
    await expect(bGrade == `Grade: ${SP5.grade}`).toBeTruthy(`Job grade: expected 'Grade: ${SP5.grade}' instead of '${bGrade}'`);
    bShortProfile = await listPL.popupShortProfile.getText();
    await expect(bShortProfile == `Short Profile: ${SP5.shortProfile}`).toBeTruthy(`Job short profile: expected 'Short Profile: ${SP5.shortProfile}' instead of '${bShortProfile}'`);
    bKFHayPoints = await listPL.popupHayPoints.getText();
    await expect(bKFHayPoints == `Korn Ferry Hay Points: ${SP5.kfHayPoints}`).toBeTruthy(`Job Hay points: expected 'Korn Ferry Hay Points: ${SP5.kfHayPoints}' instead of '${bKFHayPoints}'`);

    await listPO.clearAllFilters();
  });

  it(`Matrix view: Export (Job)`, async () => {
    await page.moveToSPList();

    await spListPO.searchProfile(SP6.name);
    await spListPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await spListPO.deleteSPViaThreeDotsMenu();
      };
    });
    await spListPO.clearSearch();

    await spListPO.setFilter(5, 3);
    await spListPO.copyProfileViaThreeDotsMenu();
    await spDetailsPO.openEditPageViaActions();
    await spLandingPO.selectEditPageItem(1);
    await spLandingPO.editProfileTitle(SP6.name);
    await spLandingPO.setProfileFunctionByName(SP6.function);
    await spLandingPO.setProfileSubFunctionByName(SP6.subFunction);
    await spLandingPO.clickNextButton();

    let leveldata1 = await spLandingPL.landingDetails.get(2).getText();
    let levelInfo1 = leveldata1.split(`|`);
    SP6.level = levelInfo1[0].replace(`Level: `, ``).trim();
    SP6.subLevel = levelInfo1[1].trim();

    await spLandingPO.saveProfile();
    SP6.grade = `${await detailsPL.detailGrade.get(0).getText()} ${await detailsPL.detailGrade.get(1).getText()}`;
    
    await jePO.clickEditJEButton();
    await jePO.saveJEWOConfirm();
    await jePO.clickCancelButton();
    await page.moveToSPList();
    await spListPO.clearAllFilters();

    await page.moveToArchitectTab();
    await listPO.moveToJobsList();

    await listPO.searchProfile(SP6.job1.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.openJobFromList(1);
        await detailsPO.deleteJobViaActions();
        await listPO.moveToJobsList();
      };
    });
    await listPO.clearSearch();

    await page.moveToArchitectTab();
    await listPO.moveToJobsList();

    await listPO.clickAddNewJobButton();
    await listPO.searchProfile(SP6.name);
    await listPO.clickUseThisBtn(1);

    await listPO.customizeJob(SP6.job1.name, SP6.job1.code);
    await listPO.clickAddCustomizeJob();
    await listPO.clearSearch();

    await listPO.moveToJobsList();
    await listPO.setMatrixView();
    await listPO.searchJobByTitle(SP6.job1.name);

    let fileName = `temp/export.xlsx`;
    let downloadTimeout = 60; //sec
    await deleteFileIfExists(fileName);
    await browser.wait(EC.elementToBeClickable(listPL.matrixDownloadButton), 60 * 1000, `There is no download link`);
    await listPL.matrixDownloadButton.click();
    console.log(await checkFileExistsWithDelay(fileName, downloadTimeout));
    await expect(await checkFileExistsWithDelay(fileName, downloadTimeout)).toBeTruthy(`Can't export matrix 2`);

    let wb = await xlsx.readFile(`./temp/export.xlsx`);
    let workSheet = wb.Sheets[`Data`];

    let afunc = workSheet[`B2`].v;
    let asubfunc = workSheet[`B3`].v;
    let adata = workSheet[`B4`].v;
    let agrade = workSheet[`A4`].v;

    console.log(adata);

    await expect(adata.indexOf(SP6.job1.name) !== -1).toBeTruthy(`Export: there is no expected name(${SP6.name}) in: ${adata}`);
    await expect(afunc.indexOf(SP6.function) !== -1).toBeTruthy(`Export file function: expected ${SP6.function} instead of ${afunc}`);
    await expect(asubfunc.indexOf(SP6.subFunction) !== -1).toBeTruthy(`Export file sub function: expected ${SP6.subFunction} instead of ${asubfunc}`);
    await expect(agrade.indexOf(SP6.grade) !== -1).toBeTruthy(`Export file function: expected ${SP6.grade} instead of ${agrade}`);
  });

  it(`Job: Compare`, async () => {
    await page.moveToSPList();

    await spListPO.searchProfile(SPC1.name);
    await spListPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await spListPO.deleteSPViaThreeDotsMenu();
      };
    });
    await spListPO.clearSearch();
    await spListPO.searchProfile(SPC2.name);
    await spListPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await spListPO.deleteSPViaThreeDotsMenu();
      };
    });
    await spListPO.clearSearch();

    await spListPO.setFilter(5, 3);
    await spListPO.copyProfileViaThreeDotsMenu();
    await spDetailsPO.openEditPageViaActions();
    await spLandingPO.selectEditPageItem(1);
    await spLandingPO.editProfileTitle(SPC1.name);
    await spLandingPO.setProfileFunctionByName(SPC1.function);
    await spLandingPO.setProfileSubFunctionByName(SPC1.subFunction);
    await spLandingPO.clickNextButton();

    await spLandingPO.saveProfile();
    await page.moveToSPList();

    await spListPO.copyProfileViaThreeDotsMenu();
    await spDetailsPO.openEditPageViaActions();
    await spLandingPO.selectEditPageItem(1);
    await spLandingPO.editProfileTitle(SPC2.name);
    await spLandingPO.setProfileFunctionByName(SPC2.function);
    await spLandingPO.setProfileSubFunctionByName(SPC2.subFunction);
    await spLandingPO.clickNextButton();

    await spLandingPO.saveProfile();
    await page.moveToSPList();

    await spListPO.clearAllFilters();

    await page.moveToArchitectTab();
    await listPO.moveToJobsList();

    await listPO.searchProfile(SPC1.job1.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.openJobFromList(1);
        await detailsPO.deleteJobViaActions();
        await listPO.moveToJobsList();
      };
    });
    await listPO.clearSearch();
    await listPO.searchProfile(SPC2.job1.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.openJobFromList(1);
        await detailsPO.deleteJobViaActions();
        await listPO.moveToJobsList();
      };
    });
    await listPO.clearSearch();

    await page.moveToArchitectTab();
    await listPO.moveToJobsList();

    await listPO.clickAddNewJobButton();
    await listPO.searchProfile(SPC1.name);
    await listPO.clickUseThisBtn(1);
    await listPO.customizeJob(SPC1.job1.name, SPC1.job1.code);
    await listPO.clickAddCustomizeJob();
    await listPO.clearSearch();

    await listPO.moveToJobsList();
    await listPO.searchJobByTitle(SPC1.job1.name);
    await listPO.openJobFromList();
    await detailsPO.editJobViaActions();

    await landingPO.clickEditButton(1);
    await landingPO.changeJobFunction(SPC1.function);
    await landingPO.changeJobSubFunction(SPC1.subFunction);
    await landingPO.clickNextButton();

    await landingPO.clickEditButton(2);
    await landingPO.deleteAllResps();
    await landingPO.addResp();
    await landingPO.renameResp(1, SPC1.resp1.name);
    await landingPO.clickNextButton();
    await landingPO.confirmPMTUpdate();
    await landingPO.deleteAllTasks();
    await landingPO.addTask();
    await landingPO.editTask(1, 1, SPC1.task1.name);
    await landingPO.addTask();
    await landingPO.editTask(2, 1, SPC1.task2.name);
    await landingPO.addTask();
    await landingPO.editTask(3, 2, SPC1.task3.name);
    await landingPO.clickNextButton();

    await landingPO.saveJob();
    await detailsPO.editJobViaActions();

    await landingPO.clickEditButton(3);
    await landingPO.recommendationsDone();
    await landingPO.deleteAllBCompetencies();
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(1, SPC1.BC1.name);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SPC1.TC1.name);
    await landingPO.addNewSkillActions(1, SPC1.TC1.skill1.name);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SPC1.tool1.name);
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.deleteAllEduNExp();
    await landingPO.addAllEducation();
    await landingPO.editEducationName(1, SPC1.education.name);
    await landingPO.addAllExperience();
    await landingPO.editExperienceName(1, SPC1.generalExperience.name);
    await landingPO.editExperienceName(2, SPC1.managerialExperience.name);
    await landingPO.clickNextButton();

    let leveldata1 = await landingPL.landingDetails.get(2).getText();
    let levelInfo1 = leveldata1.split(`|`);
    SPC1.level = levelInfo1[0].replace(`Level: `, ``).trim();
    SPC1.subLevel = levelInfo1[1].trim();

    await landingPO.saveJob();
    await detailsPO.openJE();

    SPC1.grade = await jePL.textCharts.get(0).getText();
    SPC1.shortProfile = await jePL.textCharts.get(1).getText();
    SPC1.kfHayPoints = await jePL.textCharts.get(2).getText();

    SPC1.Scores.kh1 = await jePL.jeScoresKH.get(0).getText();
    SPC1.Scores.kh2 = await jePL.jeScoresKH.get(1).getText();
    SPC1.Scores.kh3 = await jePL.jeScoresKH.get(2).getText();
    SPC1.Scores.kh4 = await jePL.jeScoresKH4.getText();

    SPC1.Scores.ps1 = await jePL.jeScoresPS.get(0).getText();
    SPC1.Scores.ps2 = await jePL.jeScoresPS.get(1).getText();
    SPC1.Scores.ps3 = await jePL.jeScoresPS.get(2).getText();
    SPC1.Scores.ps4 = await jePL.jeScoresPS4.getText();

    SPC1.Scores.ac1 = await jePL.jeScoresAC.get(0).getText();
    SPC1.Scores.ac2 = await jePL.jeScoresAC.get(1).getText();
    SPC1.Scores.ac3 = await jePL.jeScoresAC.get(2).getText();
    SPC1.Scores.ac4 = await jePL.jeScoresAC4.getText();

    await detailsPO.clickCancelJEwoCofirm();

    await listPO.moveToJobsList();

    await listPO.clickAddNewJobButton();
    await listPO.searchProfile(SPC2.name);
    await listPO.clickUseThisBtn(1);

    await listPO.customizeJob(SPC2.job1.name, SPC2.job1.code);
    await listPO.clickAddCustomizeJob();
    await listPO.clearSearch();

    await listPO.moveToJobsList();
    await listPO.searchJobByTitle(SPC2.job1.name);
    await listPO.openJobFromList();
    await detailsPO.editJobViaActions();

    await landingPO.clickEditButton(1);
    await landingPO.changeJobFunction(SPC2.function);
    await landingPO.changeJobSubFunction(SPC2.subFunction);
    await landingPO.clickNextButton();

    await landingPO.clickEditButton(2);
    await landingPO.deleteAllResps();
    await landingPO.addResp();
    await landingPO.renameResp(1, SPC2.resp1.name);
    await landingPO.clickNextButton();
    await landingPO.confirmPMTUpdate();
    await landingPO.deleteAllTasks();
    await landingPO.addTask();
    await landingPO.editTask(1, 1, SPC2.task1.name);
    await landingPO.addTask();
    await landingPO.editTask(2, 1, SPC2.task2.name);
    await landingPO.addTask();
    await landingPO.editTask(3, 2, SPC2.task3.name);
    await landingPO.clickNextButton();

    await landingPO.saveJob();
    await detailsPO.editJobViaActions();

    await landingPO.clickEditButton(3);
    await landingPO.recommendationsDone();
    await landingPO.deleteAllBCompetencies();
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(1, SPC2.BC1.name);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SPC2.TC1.name);
    await landingPO.addNewSkillActions(1, SPC2.TC1.skill1.name);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SPC2.tool1.name);
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.deleteAllEduNExp();
    await landingPO.addAllEducation();
    await landingPO.editEducationName(1, SPC2.education.name);
    await landingPO.addAllExperience();
    await landingPO.editExperienceName(1, SPC2.generalExperience.name);
    await landingPO.editExperienceName(2, SPC2.managerialExperience.name);
    await landingPO.clickNextButton();

    let leveldata2 = await landingPL.landingDetails.get(2).getText();
    let levelInfo2 = leveldata2.split(`|`);
    SPC2.level = levelInfo2[0].replace(`Level: `, ``).trim();
    SPC2.subLevel = levelInfo2[1].trim();

    await landingPO.saveJob();
    await detailsPO.openJE();

    SPC2.grade = await jePL.textCharts.get(0).getText();
    SPC2.shortProfile = await jePL.textCharts.get(1).getText();
    SPC2.kfHayPoints = await jePL.textCharts.get(2).getText();

    SPC2.Scores.kh1 = await jePL.jeScoresKH.get(0).getText();
    SPC2.Scores.kh2 = await jePL.jeScoresKH.get(1).getText();
    SPC2.Scores.kh3 = await jePL.jeScoresKH.get(2).getText();
    SPC2.Scores.kh4 = await jePL.jeScoresKH4.getText();

    SPC2.Scores.ps1 = await jePL.jeScoresPS.get(0).getText();
    SPC2.Scores.ps2 = await jePL.jeScoresPS.get(1).getText();
    SPC2.Scores.ps3 = await jePL.jeScoresPS.get(2).getText();
    SPC2.Scores.ps4 = await jePL.jeScoresPS4.getText();

    SPC2.Scores.ac1 = await jePL.jeScoresAC.get(0).getText();
    SPC2.Scores.ac2 = await jePL.jeScoresAC.get(1).getText();
    SPC2.Scores.ac3 = await jePL.jeScoresAC.get(2).getText();
    SPC2.Scores.ac4 = await jePL.jeScoresAC4.getText();

    await detailsPO.clickCancelJEwoCofirm();

    await listPO.moveToJobsList();

    await listPO.searchJobByTitle(SPC1.job1.name);
    await listPO.addToCompare(1);
    await listPO.clearSearch();

    await listPO.searchJobByTitle(SPC2.job1.name);
    await listPO.addToCompare(1);
    await listPO.clickRemoveFromCompareList(2);
    await listPO.addToCompare(1);
    await listPO.clearSearch();

    await listPO.clickCompareAll();
    await listPO.removeProfileFromCompareView(2);
    await listPO.clickBackButton();
    await listPO.searchJobByTitle(SPC2.job1.name);
    await listPO.addToCompare(1);
    await listPO.clearSearch();
    await listPO.clickCompareAll();

    await listPO.setComparison(1, 1); // JE
    let grade1 = await listPL.jeCellsSP1.get(0).getText();
    await expect(grade1 == SPC1.grade).toBeTruthy(`Compare view: expected ${SPC1.grade} instead of ${grade1}`);
    let kfHP1 = await listPL.jeCellsSP1.get(2).getText();
    await expect(kfHP1 == SPC1.kfHayPoints).toBeTruthy(`Compare view: expected ${SPC1.kfHayPoints} instead of ${kfHP1}`);
    let kfSP1 = await listPL.jeCellsSP1.get(3).getText();
    await expect(kfSP1 == SPC1.shortProfile).toBeTruthy(`Compare view: expected ${SPC1.shortProfile} instead of ${kfSP1}`);

    let score1KH1 = await listPL.jeCellsScoresSP1.get(0).getText();
    let score1KH2 = await listPL.jeCellsScoresSP1.get(1).getText();
    let score1KH3 = await listPL.jeCellsScoresSP1.get(2).getText();
    let score1KH4 = await listPL.jeCellsScoresSP1.get(3).getText();
    await expect(score1KH1 == SPC1.Scores.kh1).toBeTruthy(`Compare view: expected ${SPC1.Scores.kh1} instead of ${score1KH1}`);
    await expect(score1KH2 == SPC1.Scores.kh2).toBeTruthy(`Compare view: expected ${SPC1.Scores.kh2} instead of ${score1KH2}`);
    await expect(score1KH3 == SPC1.Scores.kh3).toBeTruthy(`Compare view: expected ${SPC1.Scores.kh3} instead of ${score1KH3}`);
    await expect(score1KH4 == SPC1.Scores.kh4).toBeTruthy(`Compare view: expected ${SPC1.Scores.kh4} instead of ${score1KH4}`);

    let score1PS1 = await listPL.jeCellsScoresSP1.get(4).getText();
    let score1PS2 = await listPL.jeCellsScoresSP1.get(5).getText();
    let score1PS3 = await listPL.jeCellsScoresSP1.get(6).getText();
    let score1PS4 = await listPL.jeCellsScoresSP1.get(7).getText();
    await expect(score1PS1 == SPC1.Scores.ps1).toBeTruthy(`Compare view: expected ${SPC1.Scores.ps1} instead of ${score1PS1}`);
    await expect(score1PS2 == SPC1.Scores.ps2).toBeTruthy(`Compare view: expected ${SPC1.Scores.ps2} instead of ${score1PS2}`);
    await expect(score1PS3 == SPC1.Scores.ps3).toBeTruthy(`Compare view: expected ${SPC1.Scores.ps3} instead of ${score1PS3}`);
    await expect(score1PS4 == SPC1.Scores.ps4).toBeTruthy(`Compare view: expected ${SPC1.Scores.ps4} instead of ${score1PS4}`);

    let score1AC1 = await listPL.jeCellsScoresSP1.get(8).getText();
    let score1AC2 = await listPL.jeCellsScoresSP1.get(9).getText();
    let score1AC3 = await listPL.jeCellsScoresSP1.get(10).getText();
    let score1AC4 = await listPL.jeCellsScoresSP1.get(11).getText();
    await expect(score1AC1 == SPC1.Scores.ac1).toBeTruthy(`Compare view: expected ${SPC1.Scores.ac1} instead of ${score1AC1}`);
    await expect(score1AC2 == SPC1.Scores.ac2).toBeTruthy(`Compare view: expected ${SPC1.Scores.ac2} instead of ${score1AC2}`);
    await expect(score1AC3 == SPC1.Scores.ac3).toBeTruthy(`Compare view: expected ${SPC1.Scores.ac3} instead of ${score1AC3}`);
    await expect(score1AC4 == SPC1.Scores.ac4).toBeTruthy(`Compare view: expected ${SPC1.Scores.ac4} instead of ${score1AC4}`);

    let grade2 = await listPL.jeCellsSP2.get(0).getText();
    await expect(grade2 == SPC2.grade).toBeTruthy(`Compare view: expected ${SPC2.grade} instead of ${grade2}`);
    let kfHP2 = await listPL.jeCellsSP2.get(2).getText();
    await expect(kfHP2 == SPC2.kfHayPoints).toBeTruthy(`Compare view: expected ${SPC2.kfHayPoints} instead of ${kfHP2}`);
    let kfSP2 = await listPL.jeCellsSP2.get(3).getText();
    await expect(kfSP2 == SPC2.shortProfile).toBeTruthy(`Compare view: expected ${SPC2.shortProfile} instead of ${kfSP2}`);

    let score2KH1 = await listPL.jeCellsScoresSP2.get(0).getText();
    let score2KH2 = await listPL.jeCellsScoresSP2.get(1).getText();
    let score2KH3 = await listPL.jeCellsScoresSP2.get(2).getText();
    let score2KH4 = await listPL.jeCellsScoresSP2.get(3).getText();
    await expect(score2KH1 == SPC2.Scores.kh1).toBeTruthy(`Compare view: expected ${SPC2.Scores.kh1} instead of ${score2KH1}`);
    await expect(score2KH2 == SPC2.Scores.kh2).toBeTruthy(`Compare view: expected ${SPC2.Scores.kh2} instead of ${score2KH2}`);
    await expect(score2KH3 == SPC2.Scores.kh3).toBeTruthy(`Compare view: expected ${SPC2.Scores.kh3} instead of ${score2KH3}`);
    await expect(score2KH4 == SPC2.Scores.kh4).toBeTruthy(`Compare view: expected ${SPC2.Scores.kh4} instead of ${score2KH4}`);

    let score2PS1 = await listPL.jeCellsScoresSP2.get(4).getText();
    let score2PS2 = await listPL.jeCellsScoresSP2.get(5).getText();
    let score2PS3 = await listPL.jeCellsScoresSP2.get(6).getText();
    let score2PS4 = await listPL.jeCellsScoresSP2.get(7).getText();
    await expect(score2PS1 == SPC2.Scores.ps1).toBeTruthy(`Compare view: expected ${SPC2.Scores.ps1} instead of ${score2PS1}`);
    await expect(score2PS2 == SPC2.Scores.ps2).toBeTruthy(`Compare view: expected ${SPC2.Scores.ps2} instead of ${score2PS2}`);
    await expect(score2PS3 == SPC2.Scores.ps3).toBeTruthy(`Compare view: expected ${SPC2.Scores.ps3} instead of ${score2PS3}`);
    await expect(score2PS4 == SPC2.Scores.ps4).toBeTruthy(`Compare view: expected ${SPC2.Scores.ps4} instead of ${score2PS4}`);

    let score2AC1 = await listPL.jeCellsScoresSP2.get(8).getText();
    let score2AC2 = await listPL.jeCellsScoresSP2.get(9).getText();
    let score2AC3 = await listPL.jeCellsScoresSP2.get(10).getText();
    let score2AC4 = await listPL.jeCellsScoresSP2.get(11).getText();
    await expect(score2AC1 == SPC2.Scores.ac1).toBeTruthy(`Compare view: expected ${SPC2.Scores.ac1} instead of ${score2AC1}`);
    await expect(score2AC2 == SPC2.Scores.ac2).toBeTruthy(`Compare view: expected ${SPC2.Scores.ac2} instead of ${score2AC2}`);
    await expect(score2AC3 == SPC2.Scores.ac3).toBeTruthy(`Compare view: expected ${SPC2.Scores.ac3} instead of ${score2AC3}`);
    await expect(score2AC4 == SPC2.Scores.ac4).toBeTruthy(`Compare view: expected ${SPC2.Scores.ac4} instead of ${score2AC4}`);

    await listPO.setComparison(2, 1); // Details
    let jobTitle1 = await listPL.cellsSP1.get(1).getText();
    await expect(jobTitle1 == SPC1.job1.name).toBeTruthy(`Compare view: expected ${SPC1.job1.name} instead of ${jobTitle1}`);
    let jobCode1 = await listPL.cellsSP1.get(2).getText();
    await expect(jobCode1 == SPC1.job1.code).toBeTruthy(`Compare view: expected ${SPC1.job1.code} instead of ${jobCode1}`);
    let jobLevel1 = await listPL.cellsSP1.get(3).getText();
    await expect(jobLevel1.indexOf(SPC1.level) !== -1).toBeTruthy(`Compare view: expected ${SPC1.level} instead of ${jobLevel1}`);
    let jobFunc1 = await listPL.cellsSP1.get(4).getText();
    await expect(jobFunc1.indexOf(SPC1.function) !== -1).toBeTruthy(`Compare view: expected ${SPC1.function} instead of ${jobFunc1}`);

    let jobTitle2 = await listPL.cellsSP2.get(1).getText();
    await expect(jobTitle2 == SPC2.job1.name).toBeTruthy(`Compare view: expected ${SPC2.job1.name} instead of ${jobTitle2}`);
    let jobCode2 = await listPL.cellsSP2.get(2).getText();
    await expect(jobCode2 == SPC2.job1.code).toBeTruthy(`Compare view: expected ${SPC2.job1.code} instead of ${jobCode2}`);
    let jobLevel2 = await listPL.cellsSP2.get(3).getText();
    await expect(jobLevel2.indexOf(SPC2.level) !== -1).toBeTruthy(`Compare view: expected ${SPC2.level} instead of ${jobLevel2}`);
    let jobFunc2 = await listPL.cellsSP2.get(4).getText();
    await expect(jobFunc2.indexOf(SPC2.function) !== -1).toBeTruthy(`Compare view: expected ${SPC2.function} instead of ${jobFunc2}`);

    await listPO.setComparison(3, 2); // Accountability / Resp
    await listPO.setToggleShowDescriptions();
    let resp1 = await listPL.cellDataSP1.get(1).getText();
    await expect(resp1 = SPC1.resp1.name).toBeTruthy(`Compare view: expected ${SPC1.resp1.name} instead of ${resp1}`);
    let resp2 = await listPL.cellDataSP2.get(1).getText();
    await expect(resp2 = SPC2.resp1.name).toBeTruthy(`Compare view: expected ${SPC2.resp1.name} instead of ${resp2}`);

    await listPO.setComparison(4, 1); // Capability / BC
    let tc1 = await listPL.cellDataSP1.get(1).getText();
    await expect(tc1 = SPC1.TC1.name).toBeTruthy(`Compare view: expected ${SPC1.TC1.name} instead of ${tc1}`);
    let tc2 = await listPL.cellDataSP2.get(1).getText();
    await expect(tc2 = SPC2.TC1.name).toBeTruthy(`Compare view: expected ${SPC2.TC1.name} instead of ${tc2}`);

    await listPO.setComparison(4, 2); // Capability / TC
    let bc1 = await listPL.cellDataSP1.get(1).getText();
    await expect(bc1 = SPC1.BC1.name).toBeTruthy(`Compare view: expected ${SPC1.BC1.name} instead of ${bc1}`);
    let bc2 = await listPL.cellDataSP2.get(1).getText();
    await expect(bc2 = SPC2.BC1.name).toBeTruthy(`Compare view: expected ${SPC2.BC1.name} instead of ${bc2}`);

    await listPO.setComparison(4, 3); // Capability / EduNExp
    let ged1 = await listPL.cellDataSP1.get(1).getText();
    await expect(ged1 = SPC1.education.name).toBeTruthy(`Compare view: expected ${SPC1.education.name} instead of ${ged1}`);
    let gex1 = await listPL.cellDataSP1.get(1).getText();
    await expect(gex1 = SPC1.generalExperience.name).toBeTruthy(`Compare view: expected ${SPC1.generalExperience.name} instead of ${gex1}`);
    let mex1 = await listPL.cellDataSP1.get(1).getText();
    await expect(mex1 = SPC1.managerialExperience.name).toBeTruthy(`Compare view: expected ${SPC1.managerialExperience.name} instead of ${mex1}`);

    let ged2 = await listPL.cellDataSP2.get(1).getText();
    await expect(ged2 = SPC2.education.name).toBeTruthy(`Compare view: expected ${SPC2.education.name} instead of ${ged2}`);
    let gex2 = await listPL.cellDataSP2.get(1).getText();
    await expect(gex2 = SPC2.generalExperience.name).toBeTruthy(`Compare view: expected ${SPC2.generalExperience.name} instead of ${gex2}`);
    let mex2 = await listPL.cellDataSP2.get(1).getText();
    await expect(mex2 = SPC2.managerialExperience.name).toBeTruthy(`Compare view: expected ${SPC2.managerialExperience.name} instead of ${mex2}`);

    await listPO.moveToJobsList();
    await listPO.searchJobByTitle(SPC1.job1.name);
    await listPO.openJobFromList();
    await detailsPO.editJobViaActions();

    await landingPO.clickEditButton(1);
    await landingPO.changeJobName(SPC1.job1.nameModified);
    await landingPO.changeJobCode(SPC1.job1.codeModified);
    await landingPO.changeJobFunction(SPC1.functionModified);
    await landingPO.changeJobSubFunction(SPC1.subFunctionModified);
    await landingPO.clickNextButton();

    await landingPO.clickEditButton(2);
    await landingPO.deleteAllResps();
    await landingPO.addResp();
    await landingPO.renameResp(1, SPC1.resp1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.confirmPMTUpdate();
    await landingPO.deleteAllTasks();
    await landingPO.addTask();
    await landingPO.editTask(1, 1, SPC1.task1.nameModified);
    await landingPO.addTask();
    await landingPO.editTask(2, 1, SPC1.task2.nameModified);
    await landingPO.addTask();
    await landingPO.editTask(3, 2, SPC1.task3.nameModified);
    await landingPO.clickNextButton();

    await landingPO.saveJob();
    await detailsPO.editJobViaActions();

    await landingPO.clickEditButton(3);
    await landingPO.recommendationsDone();
    await landingPO.deleteAllBCompetencies();
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(1, SPC1.BC1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SPC1.TC1.nameModified);
    await landingPO.addNewSkillActions(1, SPC1.TC1.skill1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SPC1.tool1.nameModified);
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.deleteAllEduNExp();
    await landingPO.addAllEducation();
    await landingPO.editEducationName(1, SPC1.education.nameModified);
    await landingPO.addAllExperience();
    await landingPO.editExperienceName(1, SPC1.generalExperience.nameModified);
    await landingPO.editExperienceName(2, SPC1.managerialExperience.nameModified);
    await landingPO.clickNextButton();

    leveldata2 = await landingPL.landingDetails.get(2).getText();
    levelInfo2 = leveldata2.split(`|`);
    SPC1.level = levelInfo2[0].replace(`Level: `, ``).trim();
    SPC1.subLevel = levelInfo2[1].trim();

    await landingPO.saveJob();
    await detailsPO.openJE();

    SPC1.grade = await jePL.textCharts.get(0).getText();
    SPC1.shortProfile = await jePL.textCharts.get(1).getText();
    SPC1.kfHayPoints = await jePL.textCharts.get(2).getText();

    SPC1.Scores.kh1 = await jePL.jeScoresKH.get(0).getText();
    SPC1.Scores.kh2 = await jePL.jeScoresKH.get(1).getText();
    SPC1.Scores.kh3 = await jePL.jeScoresKH.get(2).getText();
    SPC1.Scores.kh4 = await jePL.jeScoresKH4.getText();

    SPC1.Scores.ps1 = await jePL.jeScoresPS.get(0).getText();
    SPC1.Scores.ps2 = await jePL.jeScoresPS.get(1).getText();
    SPC1.Scores.ps3 = await jePL.jeScoresPS.get(2).getText();
    SPC1.Scores.ps4 = await jePL.jeScoresPS4.getText();

    SPC1.Scores.ac1 = await jePL.jeScoresAC.get(0).getText();
    SPC1.Scores.ac2 = await jePL.jeScoresAC.get(1).getText();
    SPC1.Scores.ac3 = await jePL.jeScoresAC.get(2).getText();
    SPC1.Scores.ac4 = await jePL.jeScoresAC4.getText();

    await detailsPO.clickCancelJE();
    await listPO.moveToJobsList();

    await listPO.searchJobByTitle(SPC2.job1.name);
    await listPO.openJobFromList();
    await detailsPO.editJobViaActions();

    await landingPO.clickEditButton(1);
    await landingPO.changeJobName(SPC2.job1.nameModified);
    await landingPO.changeJobCode(SPC2.job1.codeModified);
    await landingPO.changeJobFunction(SPC2.functionModified);
    await landingPO.changeJobSubFunction(SPC2.subFunctionModified);
    await landingPO.clickNextButton();

    await landingPO.clickEditButton(2);
    await landingPO.deleteAllResps();
    await landingPO.addResp();
    await landingPO.renameResp(1, SPC2.resp1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.confirmPMTUpdate();
    await landingPO.deleteAllTasks();
    await landingPO.addTask();
    await landingPO.editTask(1, 1, SPC2.task1.nameModified);
    await landingPO.addTask();
    await landingPO.editTask(2, 1, SPC2.task2.nameModified);
    await landingPO.addTask();
    await landingPO.editTask(3, 2, SPC2.task3.nameModified);
    await landingPO.clickNextButton();

    await landingPO.saveJob();
    await detailsPO.editJobViaActions();

    await landingPO.clickEditButton(3);
    await landingPO.recommendationsDone();
    await landingPO.deleteAllBCompetencies();
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(1, SPC2.BC1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SPC2.TC1.nameModified);
    await landingPO.addNewSkillActions(1, SPC2.TC1.skill1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SPC2.tool1.nameModified);
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.deleteAllEduNExp();
    await landingPO.addAllEducation();
    await landingPO.editEducationName(1, SPC2.education.nameModified);
    await landingPO.addAllExperience();
    await landingPO.editExperienceName(1, SPC2.generalExperience.nameModified);
    await landingPO.editExperienceName(2, SPC2.managerialExperience.nameModified);
    await landingPO.clickNextButton();

    leveldata2 = await landingPL.landingDetails.get(2).getText();
    levelInfo2 = leveldata2.split(`|`);
    SPC2.level = levelInfo2[0].replace(`Level: `, ``).trim();
    SPC2.subLevel = levelInfo2[1].trim();

    await landingPO.saveJob();
    await detailsPO.openJE();

    SPC2.grade = await jePL.textCharts.get(0).getText();
    SPC2.shortProfile = await jePL.textCharts.get(1).getText();
    SPC2.kfHayPoints = await jePL.textCharts.get(2).getText();

    SPC2.Scores.kh1 = await jePL.jeScoresKH.get(0).getText();
    SPC2.Scores.kh2 = await jePL.jeScoresKH.get(1).getText();
    SPC2.Scores.kh3 = await jePL.jeScoresKH.get(2).getText();
    SPC2.Scores.kh4 = await jePL.jeScoresKH4.getText();

    SPC2.Scores.ps1 = await jePL.jeScoresPS.get(0).getText();
    SPC2.Scores.ps2 = await jePL.jeScoresPS.get(1).getText();
    SPC2.Scores.ps3 = await jePL.jeScoresPS.get(2).getText();
    SPC2.Scores.ps4 = await jePL.jeScoresPS4.getText();

    SPC2.Scores.ac1 = await jePL.jeScoresAC.get(0).getText();
    SPC2.Scores.ac2 = await jePL.jeScoresAC.get(1).getText();
    SPC2.Scores.ac3 = await jePL.jeScoresAC.get(2).getText();
    SPC2.Scores.ac4 = await jePL.jeScoresAC4.getText();

    await detailsPO.clickCancelJE();

    await listPO.moveToJobsList();
    await listPO.clickCompareAll();

    await listPO.setComparison(1, 1); // JE
    grade1 = await listPL.jeCellsSP1.get(0).getText();
    await expect(grade1 == SPC1.grade).toBeTruthy(`Compare view: expected ${SPC1.grade} instead of ${grade1}`);
    kfHP1 = await listPL.jeCellsSP1.get(2).getText();
    await expect(kfHP1 == SPC1.kfHayPoints).toBeTruthy(`Compare view: expected ${SPC1.kfHayPoints} instead of ${kfHP1}`);
    kfSP1 = await listPL.jeCellsSP1.get(3).getText();
    await expect(kfSP1 == SPC1.shortProfile).toBeTruthy(`Compare view: expected ${SPC1.shortProfile} instead of ${kfSP1}`);

    score1KH1 = await listPL.jeCellsScoresSP1.get(0).getText();
    score1KH2 = await listPL.jeCellsScoresSP1.get(1).getText();
    score1KH3 = await listPL.jeCellsScoresSP1.get(2).getText();
    score1KH4 = await listPL.jeCellsScoresSP1.get(3).getText();
    await expect(score1KH1 == SPC1.Scores.kh1).toBeTruthy(`Compare view: expected ${SPC1.Scores.kh1} instead of ${score1KH1}`);
    await expect(score1KH2 == SPC1.Scores.kh2).toBeTruthy(`Compare view: expected ${SPC1.Scores.kh2} instead of ${score1KH2}`);
    await expect(score1KH3 == SPC1.Scores.kh3).toBeTruthy(`Compare view: expected ${SPC1.Scores.kh3} instead of ${score1KH3}`);
    await expect(score1KH4 == SPC1.Scores.kh4).toBeTruthy(`Compare view: expected ${SPC1.Scores.kh4} instead of ${score1KH4}`);

    score1PS1 = await listPL.jeCellsScoresSP1.get(4).getText();
    score1PS2 = await listPL.jeCellsScoresSP1.get(5).getText();
    score1PS3 = await listPL.jeCellsScoresSP1.get(6).getText();
    score1PS4 = await listPL.jeCellsScoresSP1.get(7).getText();
    await expect(score1PS1 == SPC1.Scores.ps1).toBeTruthy(`Compare view: expected ${SPC1.Scores.ps1} instead of ${score1PS1}`);
    await expect(score1PS2 == SPC1.Scores.ps2).toBeTruthy(`Compare view: expected ${SPC1.Scores.ps2} instead of ${score1PS2}`);
    await expect(score1PS3 == SPC1.Scores.ps3).toBeTruthy(`Compare view: expected ${SPC1.Scores.ps3} instead of ${score1PS3}`);
    await expect(score1PS4 == SPC1.Scores.ps4).toBeTruthy(`Compare view: expected ${SPC1.Scores.ps4} instead of ${score1PS4}`);

    score1AC1 = await listPL.jeCellsScoresSP1.get(8).getText();
    score1AC2 = await listPL.jeCellsScoresSP1.get(9).getText();
    score1AC3 = await listPL.jeCellsScoresSP1.get(10).getText();
    score1AC4 = await listPL.jeCellsScoresSP1.get(11).getText();
    await expect(score1AC1 == SPC1.Scores.ac1).toBeTruthy(`Compare view: expected ${SPC1.Scores.ac1} instead of ${score1AC1}`);
    await expect(score1AC2 == SPC1.Scores.ac2).toBeTruthy(`Compare view: expected ${SPC1.Scores.ac2} instead of ${score1AC2}`);
    await expect(score1AC3 == SPC1.Scores.ac3).toBeTruthy(`Compare view: expected ${SPC1.Scores.ac3} instead of ${score1AC3}`);
    await expect(score1AC4 == SPC1.Scores.ac4).toBeTruthy(`Compare view: expected ${SPC1.Scores.ac4} instead of ${score1AC4}`);

    grade2 = await listPL.jeCellsSP2.get(0).getText();
    await expect(grade2 == SPC2.grade).toBeTruthy(`Compare view: expected ${SPC2.grade} instead of ${grade2}`);
    kfHP2 = await listPL.jeCellsSP2.get(2).getText();
    await expect(kfHP2 == SPC2.kfHayPoints).toBeTruthy(`Compare view: expected ${SPC2.kfHayPoints} instead of ${kfHP2}`);
    kfSP2 = await listPL.jeCellsSP2.get(3).getText();
    await expect(kfSP2 == SPC2.shortProfile).toBeTruthy(`Compare view: expected ${SPC2.shortProfile} instead of ${kfSP2}`);

    score2KH1 = await listPL.jeCellsScoresSP2.get(0).getText();
    score2KH2 = await listPL.jeCellsScoresSP2.get(1).getText();
    score2KH3 = await listPL.jeCellsScoresSP2.get(2).getText();
    score2KH4 = await listPL.jeCellsScoresSP2.get(3).getText();
    await expect(score2KH1 == SPC2.Scores.kh1).toBeTruthy(`Compare view: expected ${SPC2.Scores.kh1} instead of ${score2KH1}`);
    await expect(score2KH2 == SPC2.Scores.kh2).toBeTruthy(`Compare view: expected ${SPC2.Scores.kh2} instead of ${score2KH2}`);
    await expect(score2KH3 == SPC2.Scores.kh3).toBeTruthy(`Compare view: expected ${SPC2.Scores.kh3} instead of ${score2KH3}`);
    await expect(score2KH4 == SPC2.Scores.kh4).toBeTruthy(`Compare view: expected ${SPC2.Scores.kh4} instead of ${score2KH4}`);

    score2PS1 = await listPL.jeCellsScoresSP2.get(4).getText();
    score2PS2 = await listPL.jeCellsScoresSP2.get(5).getText();
    score2PS3 = await listPL.jeCellsScoresSP2.get(6).getText();
    score2PS4 = await listPL.jeCellsScoresSP2.get(7).getText();
    await expect(score2PS1 == SPC2.Scores.ps1).toBeTruthy(`Compare view: expected ${SPC2.Scores.ps1} instead of ${score2PS1}`);
    await expect(score2PS2 == SPC2.Scores.ps2).toBeTruthy(`Compare view: expected ${SPC2.Scores.ps2} instead of ${score2PS2}`);
    await expect(score2PS3 == SPC2.Scores.ps3).toBeTruthy(`Compare view: expected ${SPC2.Scores.ps3} instead of ${score2PS3}`);
    await expect(score2PS4 == SPC2.Scores.ps4).toBeTruthy(`Compare view: expected ${SPC2.Scores.ps4} instead of ${score2PS4}`);

    score2AC1 = await listPL.jeCellsScoresSP2.get(8).getText();
    score2AC2 = await listPL.jeCellsScoresSP2.get(9).getText();
    score2AC3 = await listPL.jeCellsScoresSP2.get(10).getText();
    score2AC4 = await listPL.jeCellsScoresSP2.get(11).getText();
    await expect(score2AC1 == SPC2.Scores.ac1).toBeTruthy(`Compare view: expected ${SPC2.Scores.ac1} instead of ${score2AC1}`);
    await expect(score2AC2 == SPC2.Scores.ac2).toBeTruthy(`Compare view: expected ${SPC2.Scores.ac2} instead of ${score2AC2}`);
    await expect(score2AC3 == SPC2.Scores.ac3).toBeTruthy(`Compare view: expected ${SPC2.Scores.ac3} instead of ${score2AC3}`);
    await expect(score2AC4 == SPC2.Scores.ac4).toBeTruthy(`Compare view: expected ${SPC2.Scores.ac4} instead of ${score2AC4}`);

    await listPO.setComparison(2, 1); // Details
    jobTitle1 = await listPL.cellsSP1.get(1).getText();
    await expect(jobTitle1 == SPC1.job1.nameModified).toBeTruthy(`Compare view: expected ${SPC1.job1.nameModified} instead of ${jobTitle1}`);
    jobCode1 = await listPL.cellsSP1.get(2).getText();
    await expect(jobCode1 == SPC1.job1.codeModified).toBeTruthy(`Compare view: expected ${SPC1.job1.codeModified} instead of ${jobCode1}`);
    jobLevel1 = await listPL.cellsSP1.get(3).getText();
    await expect(jobLevel1.indexOf(SPC1.level) !== -1).toBeTruthy(`Compare view: expected ${SPC1.level} instead of ${jobLevel1}`);
    jobFunc1 = await listPL.cellsSP1.get(4).getText();
    await expect(jobFunc1.indexOf(SPC1.functionModified) !== -1).toBeTruthy(`Compare view: expected ${SPC1.functionModified} instead of ${jobFunc1}`);

    jobTitle2 = await listPL.cellsSP2.get(1).getText();
    await expect(jobTitle2 == SPC2.job1.nameModified).toBeTruthy(`Compare view: expected ${SPC2.job1.nameModified} instead of ${jobTitle2}`);
    jobCode2 = await listPL.cellsSP2.get(2).getText();
    await expect(jobCode2 == SPC2.job1.codeModified).toBeTruthy(`Compare view: expected ${SPC2.job1.codeModified} instead of ${jobCode2}`);
    jobLevel2 = await listPL.cellsSP2.get(3).getText();
    await expect(jobLevel2.indexOf(SPC2.level) !== -1).toBeTruthy(`Compare view: expected ${SPC2.level} instead of ${jobLevel2}`);
    jobFunc2 = await listPL.cellsSP2.get(4).getText();
    await expect(jobFunc2.indexOf(SPC2.functionModified) !== -1).toBeTruthy(`Compare view: expected ${SPC2.functionModified} instead of ${jobFunc2}`);

    await listPO.setComparison(3, 2); // Accountability / Resp
    await listPO.setToggleShowDescriptions();
    resp1 = await listPL.cellDataSP1.get(1).getText();
    await expect(resp1 = SPC1.resp1.nameModified).toBeTruthy(`Compare view: expected ${SPC1.resp1.nameModified} instead of ${resp1}`);
    resp2 = await listPL.cellDataSP2.get(1).getText();
    await expect(resp2 = SPC2.resp1.nameModified).toBeTruthy(`Compare view: expected ${SPC2.resp1.nameModified} instead of ${resp2}`);

    await listPO.setComparison(4, 1); // Capability / BC
    tc1 = await listPL.cellDataSP1.get(1).getText();
    await expect(tc1 = SPC1.TC1.nameModified).toBeTruthy(`Compare view: expected ${SPC1.TC1.nameModified} instead of ${tc1}`);
    tc2 = await listPL.cellDataSP2.get(1).getText();
    await expect(tc2 = SPC2.TC1.nameModified).toBeTruthy(`Compare view: expected ${SPC2.TC1.nameModified} instead of ${tc2}`);

    await listPO.setComparison(4, 2); // Capability / TC
    bc1 = await listPL.cellDataSP1.get(1).getText();
    await expect(bc1 = SPC1.BC1.nameModified).toBeTruthy(`Compare view: expected ${SPC1.BC1.nameModified} instead of ${bc1}`);
    bc2 = await listPL.cellDataSP2.get(1).getText();
    await expect(bc2 = SPC2.BC1.nameModified).toBeTruthy(`Compare view: expected ${SPC2.BC1.nameModified} instead of ${bc2}`);

    await listPO.setComparison(4, 3); // Capability / EduNExp
    ged1 = await listPL.cellDataSP1.get(1).getText();
    await expect(ged1 = SPC1.education.nameModified).toBeTruthy(`Compare view: expected ${SPC1.education.nameModified} instead of ${ged1}`);
    gex1 = await listPL.cellDataSP1.get(1).getText();
    await expect(gex1 = SPC1.generalExperience.nameModified).toBeTruthy(`Compare view: expected ${SPC1.generalExperience.nameModified} instead of ${gex1}`);
    mex1 = await listPL.cellDataSP1.get(1).getText();
    await expect(mex1 = SPC1.managerialExperience.nameModified).toBeTruthy(`Compare view: expected ${SPC1.managerialExperience.nameModified} instead of ${mex1}`);

    ged2 = await listPL.cellDataSP2.get(1).getText();
    await expect(ged2 = SPC2.education.nameModified).toBeTruthy(`Compare view: expected ${SPC2.education.nameModified} instead of ${ged2}`);
    gex2 = await listPL.cellDataSP2.get(1).getText();
    await expect(gex2 = SPC2.generalExperience.nameModified).toBeTruthy(`Compare view: expected ${SPC2.generalExperience.nameModified} instead of ${gex2}`);
    mex2 = await listPL.cellDataSP2.get(1).getText();
    await expect(mex2 = SPC2.managerialExperience.nameModified).toBeTruthy(`Compare view: expected ${SPC2.managerialExperience.nameModified} instead of ${mex2}`);

    await listPO.removeProfileFromCompareView();
    await listPO.removeProfileFromCompareView();
    await listPO.clearSearch();
  });

});