import { browser } from 'protractor';
import { AppPage as page } from './components/app.po';
import { checkFileExistsWithDelay, checkFileStatWithDelay, deleteFileIfExists } from './components/utils';
import { SuccessProfileListPageObjects as listPO } from './components/sp-list.po';
import { SuccessProfileListLocators as listPL } from './components/sp-list.locators';
import { SuccessProfileSummaryPageObjects as summaryPO } from './components/sp-summary.po';
import { SuccessProfileDetailsPageObjects as detailsPO } from './components/sp-details.po';

describe('Summary check + Download report', () => {
  const minFileSize = 250000;
  const minFileSize2 = 100000;
  const downloadTimeout = 30; //sec

  beforeEach(async () => {
    await page.login();
    await browser.waitForAngularEnabled(false);
  });

  afterEach(async () => {
    await browser.manage().logs().get('browser').then(async (browserLog) => {
      if (browserLog.length > 0) {
        await browserLog.forEach(async log => {
          await expect(browserLog.length > 0).toBeFalsy(`error in console`);//.toBeFalsy(log.message);
        });
      }
    });
  });

  for (let j = 1; j < 4; j++) {
    for (let i = 1; i < 4; i++) {
      it('Summary and report download', async () => {
        await page.moveToSPList();
        await listPO.setFilter(1, j);
        await listPO.setFilter(5, 3);
        const rowCo = await page.getCount();
        var rowNo = page.generateRandomInt(1, 60);
        while (rowCo < rowNo) rowNo = await page.generateRandomInt(1, 60);
        console.log(`\n ${j} Profile type; ${i} try; ${rowNo} profile position in list`);
        await listPO.openSPFromListByNo(rowNo);

        // Summary
        const pName = await detailsPO.getSPReportTitle();
        const pId = await detailsPO.getProfileId();
        await summaryPO.openSummary();
        // Check Accountability
        await summaryPO.clickAccountabilityTab();
        await summaryPO.clickResponsibilitiesTab();
        await expect(await summaryPO.getCurrentTabTRCount())
          .toBeGreaterThanOrEqual(1, `${pId} ${pName}: Should be 1 or more Responsibilities`);
        // Check Capability
        await summaryPO.clickCapabilityTab();
        await summaryPO.clickBCTab();
        await expect(summaryPO.getCurrentTabTRCount())
          .toBeGreaterThanOrEqual(1, `${pId} ${pName}: Should be 1 or more Behavioral Competency`);
        await summaryPO.clickTCTab();
        await expect(summaryPO.getCurrentTabTRCount())
          .toBeGreaterThanOrEqual(1, `${pId} ${pName}: Should be 1 or more Technical Competency`);
        await summaryPO.clickEducationTab();
        await expect(await summaryPO.getCurrentTabTRCount() == 1)
          .toBeTruthy(`${pId} ${pName}: Should be only 1 education`);
        await summaryPO.clickExperienceTab();
        await expect(await summaryPO.getCurrentTabTRCount() == 2)
          .toBeTruthy(`${pId} ${pName}: Should be only 2 experiences`);
        // Identity
        await summaryPO.clickIdentityTab();
        await summaryPO.clickTraitsTab();
        await expect(await summaryPO.getCurrentTabTRCount() == 8)
          .toBeTruthy(`${pId} ${pName}: Should be 8 traits`);
        await summaryPO.clickDriversTab();
        await expect(await summaryPO.getCurrentTabTRCount() == 6)
          .toBeTruthy(`${pId} ${pName}: Should be 6 drivers`);
        await summaryPO.closeSummary();

        const profileName = await detailsPO.getSPReportTitle();
        const tempName = profileName.replace('/', '_');
        const fileName = `temp/${tempName}.pdf`;
        const profileId = await detailsPO.getProfileId();
        const fileName2 = `temp/InterviewGuide_${profileId}.pdf`;

        console.log(`SP: ${fileName}`);
        deleteFileIfExists(fileName);
        await detailsPO.clickSPReportLink();
        await detailsPO.clickDownloadButton();
        await expect(await checkFileExistsWithDelay(fileName, downloadTimeout)).toBeTruthy(`Can't load SP report for SP(${profileName})`);
        await expect(await checkFileStatWithDelay(fileName, minFileSize, downloadTimeout)).toBeTruthy(`File must be greater than ${minFileSize}(SP name: ${profileName})`);

        console.log(`IG: ${fileName2}`);
        deleteFileIfExists(fileName2);
        await detailsPO.clickSPIGReportLink();
        await expect(await checkFileExistsWithDelay(fileName2, downloadTimeout)).toBeTruthy(`Can't load Interview Guide for SP with ID ${profileId}`);
        await expect(await checkFileStatWithDelay(fileName2, minFileSize2, downloadTimeout)).toBeTruthy(`File must be greater than ${minFileSize2}(SP ID: ${profileId})`);

        await page.moveToSPList();
        await listPO.clearAllFiltersF();
      });
    }
  }
});
