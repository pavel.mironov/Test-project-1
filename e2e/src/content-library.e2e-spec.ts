import { by, element, browser, ExpectedConditions as EC, protractor } from 'protractor';
import { AppPage as page } from './components/app.po';
import { ContentLibraryLocators as clPL } from './components/content-library.locators';
import { ContentLibraryPageObjects as clPO } from './components/content-library.po';
import { SuccessProfileListPageObjects as listPO } from './components/sp-list.po';
import { SuccessProfileDetailsPageLocators as detailsPL } from './components/sp-details.locators';
import { SuccessProfileDetailsPageObjects as detailsPO } from './components/sp-details.po';
import { SuccessProfileLandingPageLocators as landingPL } from './components/sp-landing.locators';
import { SuccessProfileLandingPageObjects as landingPO } from './components/sp-landing.po';
import { SpinnerPO as spinner } from './components/spinner';

let dd = new Date();
console.log(dd.toUTCString());

let SP1 = {
  name: `Test ASDFG || content`
}

let respDeleted = {
  name: `Test ASDFG || delete me`
  , category: `Adapt`
};

let resp1 = {
  name: `Test ASDFG || resp 1`
  , nameModified: `Test ASDFG || resp 1 modified`
  , category: `Innovation`
  , categoryModified: `Digital Leadership`

  , definitionL1: {
    i: 1
    , name: `ASDFG 1 Level 1 Definition`
    , nameModified: `ASDFG 1 Level 1 M Definition`
  }
  , definitionL2: {
    i: 2
    , name: `ASDFG 1 Level 2 Definition`
    , nameModified: `ASDFG 1 Level 2 M Definition`
  }
  , definitionL3: {
    i: 3
    , name: `ASDFG 1 Level 3 Definition`
    , nameModified: `ASDFG 1 Level 3 M Definition`
  }
};

let bcDeleted = {
  name: `Test ASDFG || delete me`
  , category: `Research & Development`
};

let bc1 = {
  name: `Test ASDFG || beh comp 1`
  , nameModified: `Test ASDFG || beh comp modified 1`
  , category: `Customer Service`
  , categoryModified: `Healthcare`

  , definitionL1: {
    i: 1
    , name: `ASDFG 1 Level 1 Definition`
    , nameModified: `ASDFG 1 Level 1 M Definition`
  }
  , definitionL2: {
    i: 2
    , name: `ASDFG 1 Level 2 Definition`
    , nameModified: `ASDFG 1 Level 2 M Definition`
  }
  , definitionL3: {
    i: 3
    , name: `ASDFG 1 Level 3 Definition`
    , nameModified: `ASDFG 1 Level 3 M Definition`
  }
};

describe('Library', () => {
  beforeEach(async () => {
    await page.login();
    await browser.waitForAngularEnabled(false);
  });

  afterEach(async () => {
    await browser.manage().logs().get('browser').then(async (browserLog) => {
      if (browserLog.length > 0) {
        await browserLog.forEach(async log => {
          await expect(browserLog.length > 0).toBeFalsy(`error in console`);//.toBeFalsy(log.message);
        });
        console.log(browserLog);
      }
    });
  });


  it(`Responsibilies: create, edit, add to SP`, async () => {
    await clPO.moveToCL();
    await clPO.moveToResps();

    // rename instead delete
    await clPO.searchResp(resp1.name);
    await clPL.respNamesA.count().then(async am => {
      for (let i = 0; i < am; i++) {
        await clPO.searchResp(resp1.name);
        await clPO.openRespByName(resp1.name);
        await clPO.startEdit();
        await clPO.setRespName(respDeleted.name);
        await clPO.setCategoryByName(respDeleted.category);
        await clPO.saveEditResp();
        await clPO.clickClose();
        await clPO.searchResp(respDeleted.name);
        await clPO.setToggleOff();
      };
    });
    await clPO.searchResp(resp1.nameModified);
    await clPL.respNamesA.count().then(async am => {
      for (let i = 0; i < am; i++) {
        await clPO.searchResp(resp1.nameModified);
        await clPO.openRespByName(resp1.nameModified);
        await clPO.startEdit();
        await clPO.setRespName(respDeleted.name);
        await clPO.setCategoryByName(respDeleted.category);
        await clPO.saveEditResp();
        await clPO.clickClose();
        await clPO.searchResp(respDeleted.name);
        await clPO.setToggleOff();
      };
    });


    // add new one
    await clPO.addNewResp();
    await clPO.setRespName(resp1.name); //${dd.toUTCString()}
    await clPO.setCategoryByName(resp1.category);
    await clPO.setLevelDefinition(resp1.definitionL1.i, resp1.definitionL1.name);
    await clPO.setLevelDefinition(resp1.definitionL2.i, resp1.definitionL2.name);
    await clPO.setLevelDefinition(resp1.definitionL3.i, resp1.definitionL3.name);
    await clPO.saveNewResp();

    // Edit created resp
    await clPO.moveToCL();
    await clPO.moveToResps();
    await clPO.searchResp(resp1.name);
    await clPO.openRespByName(resp1.name);
    await clPO.startEdit();
    await clPO.setRespName(resp1.nameModified); //${dd.toUTCString()}
    await clPO.setCategoryByName(resp1.categoryModified);
    await clPO.setLevelDefinition(resp1.definitionL1.i, resp1.definitionL1.nameModified);
    await clPO.setLevelDefinition(resp1.definitionL2.i, resp1.definitionL2.nameModified);
    await clPO.setLevelDefinition(resp1.definitionL3.i, resp1.definitionL3.nameModified);
    await clPO.saveEditResp();
    await clPO.clickClose();

    // Add new resp to profile
    await page.navigateTo('app/tarc/#');
    await spinner.waitUntilSpinnerOff();
    await page.moveToSPList();

    await listPO.setFilter(5, 1);
    await listPO.searchProfile(SP1.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.deleteSPViaThreeDotsMenu();
      };
    });
    await listPO.clearAllFilters();

    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 2);
    await listPO.copyProfileViaThreeDotsMenu(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(SP1.name);

    await landingPO.selectEditPageItem(2);
    await landingPO.deleteAllResponsibilities();
    await landingPO.addResponsibilityByName(resp1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.confirmUpdate(2);
    await landingPO.clickNextButton();

    await browser.wait(EC.presenceOf(landingPL.landingDetails.get(0)), browser.params.cooldown.small, `There is no Accountability section on landing page`)
    let respText = await landingPL.landingAccountability.get(0).getText();
    await expect(respText.indexOf(resp1.nameModified) !== -1).toBeTruthy(`There is no "${resp1.nameModified}" in Landing page/Accountability`);

    await landingPO.saveProfile();

    await browser.wait(EC.presenceOf(detailsPL.detailResps.get(0)), browser.params.cooldown.medium, `Details page: there is no resps section`);
    await expect(await detailsPL.detailResps.get(0).getText() == resp1.nameModified)
      .toBeTruthy(`Details / responsibilities: expected ${resp1.nameModified} instead of ${await detailsPL.detailResps.get(0).getText()}`);

    await detailsPO.openEditPageViaActions();

    await browser.wait(EC.presenceOf(landingPL.landingDetails.get(0)), browser.params.cooldown.small, `There is no Accountability section on landing page`)
    respText = await landingPL.landingAccountability.get(0).getText();
    await expect(respText.indexOf(resp1.nameModified) !== -1).toBeTruthy(`There is no "${resp1.nameModified}" in Landing page/Accountability`);

    await landingPO.clickBackButton();
    await page.moveToSPList();
    await listPO.clearSearch();
    await listPO.clearAllFilters();
  });


  xit(`BC: create, edit, add to SP`, async () => {
    await clPO.moveToCL();
    await clPO.moveToBC();

    // rename instead delete
    await clPO.searchResp(bc1.name);
    await clPL.respNamesA.count().then(async am => {
      for (let i = 0; i < am; i++) {
        await clPO.searchResp(bc1.name);
        await clPO.openRespByName(bc1.name);
        await clPO.startEdit();
        await clPO.setRespName(bcDeleted.name);
        await clPO.setCategoryByName(bcDeleted.category);
        await clPO.saveEditResp();
        await clPO.clickClose();
        await clPO.searchResp(bcDeleted.name);
        await clPO.setToggleOff();
      };
    });
    await clPO.searchResp(resp1.nameModified);
    await clPL.respNamesA.count().then(async am => {
      for (let i = 0; i < am; i++) {
        await clPO.searchResp(resp1.nameModified);
        await clPO.openRespByName(resp1.nameModified);
        await clPO.startEdit();
        await clPO.setRespName(bcDeleted.name);
        await clPO.setCategoryByName(bcDeleted.category);
        await clPO.saveEditResp();
        await clPO.clickClose();
        await clPO.searchResp(bcDeleted.name);
        await clPO.setToggleOff();
      };
    });


    // add new one
    await clPO.addNewResp();
    await clPO.setRespName(bc1.name); //${dd.toUTCString()}
    await clPO.setCategoryByName(bc1.category);
    await clPO.setLevelDefinition(bc1.definitionL1.i, bc1.definitionL1.name);
    await clPO.setLevelDefinition(bc1.definitionL2.i, bc1.definitionL2.name);
    await clPO.setLevelDefinition(bc1.definitionL3.i, bc1.definitionL3.name);
    await clPO.saveNewResp();

    // Edit created resp
    await clPO.moveToCL();
    await clPO.moveToResps();
    await clPO.searchResp(bc1.name);
    await clPO.openRespByName(bc1.name);
    await clPO.startEdit();
    await clPO.setRespName(bc1.nameModified); //${dd.toUTCString()}
    await clPO.setCategoryByName(bc1.categoryModified);
    await clPO.setLevelDefinition(bc1.definitionL1.i, bc1.definitionL1.nameModified);
    await clPO.setLevelDefinition(bc1.definitionL2.i, bc1.definitionL2.nameModified);
    await clPO.setLevelDefinition(bc1.definitionL3.i, bc1.definitionL3.nameModified);
    await clPO.saveEditResp();
    await clPO.clickClose();

    // // Add new resp to profile
    // await spinner.waitUntilSpinnerOff();
    // await spinner.delay(3000);
    // await page.navigateTo('app/tarc/#');
    // await spinner.waitUntilSpinnerOff();
    // await page.moveToSPList();

    // await listPO.setFilter(5, 1);
    // await listPO.searchProfile(SP1.name);
    // await listPO.getCount().then(async (am) => {
    //   for (let i = am; i > 0; i--) {
    //     await listPO.deleteSPViaThreeDotsMenu();
    //   };
    // });
    // await listPO.clearAllFilters();

    // await listPO.clearSearch();
    // await listPO.setFilter(5, 3);
    // await listPO.setFilter(1, 2);
    // await listPO.copyProfileViaThreeDotsMenu(1);
    // await detailsPO.openEditPageViaActions();
    // await landingPO.changeProfileName(SP1.name);

    // await landingPO.selectEditPageItem(2);
    // await landingPO.deleteAllResponsibilities();
    // await landingPO.addResponsibilityByName(resp2.name);
    // await landingPO.clickNextButton();
    // await landingPO.confirmUpdate(2);
    // await landingPO.clickNextButton();

    // await browser.wait(EC.presenceOf(landingPL.landingDetails.get(0)), browser.params.cooldown.small, `There is no Accountability section on landing page`)
    // let respText = await landingPL.landingAccountability.get(0).getText();
    // await expect(respText.indexOf(resp2.name) !== -1).toBeTruthy(`There is no "${resp2.name}" in Landing page/Accountability`);

    // await landingPO.saveProfile();

    // await browser.wait(EC.presenceOf(detailsPL.detailResps.get(0)), browser.params.cooldown.medium, `Details page: there is no resps section`);
    // await expect(await detailsPL.detailResps.get(0).getText() == resp2.name)
    //   .toBeTruthy(`Details / responsibilities: expected ${resp2.name} instead of ${await detailsPL.detailResps.get(0).getText()}`);

    // await detailsPO.openEditPageViaActions();

    // await browser.wait(EC.presenceOf(landingPL.landingDetails.get(0)), browser.params.cooldown.small, `There is no Accountability section on landing page`)
    // respText = await landingPL.landingAccountability.get(0).getText();
    // await expect(respText.indexOf(resp2.name) !== -1).toBeTruthy(`There is no "${resp2.name}" in Landing page/Accountability`);

    // await landingPO.clickBackButton();
    // await page.moveToSPList();
    // await listPO.clearSearch();
    // await listPO.clearAllFilters();
  });


});