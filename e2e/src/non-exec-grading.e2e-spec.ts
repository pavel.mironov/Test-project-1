import { by, element, browser, ExpectedConditions as EC, protractor } from 'protractor';
import { AppPage as page } from './components/app.po';
import { SuccessProfileListPageObjects as listPO } from './components/sp-list.po';
import { SuccessProfileDetailsPageObjects as detailsPO } from './components/sp-details.po';
import { SuccessProfileLandingPageObjects as landingPO } from './components/sp-landing.po';
import { SuccessProfileLandingPageLocators as landingPL } from './components/sp-landing.locators';
import { SpinnerPO as spinner } from './components/spinner';

const profileName4 = `Test ASDFG || DDDD`;

describe('Non-exec grade', () => {
  beforeEach(async () => {
    await page.login(`userTwo`);
    await browser.waitForAngularEnabled(false);
  });

  afterEach(async () => {
    await browser.manage().logs().get('browser').then(async (browserLog) => {
      if (browserLog.length > 0) {
        await browserLog.forEach(async log => {
          await expect(browserLog.length > 0).toBeFalsy(`error in console`);//.toBeFalsy(log.message);
        });
      }
    });
  });

  it(`Delete old SP`, async () => {
    await page.searchProfileHome(profileName4).then(async () => {
      await page.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          console.log(i, am);
          if (i % 10 == 0) {
            await listPO.clearSearch();
            await listPO.searchProfile(profileName4);
          }
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });
  });
  // The changes to responsibilities indicate a change in job size for this profile. This may have an impact on the final grade. Please review and confirm the job size.
  const expectMsgRES50004 = `The Profile Match Tool settings indicate a role smaller than that described by the responsibility levels. To ensure accuracy, please review the Profile Match Tool settings.`;
  const expectGrade1 = `15`;
  it(`lowerSizeOfRole`, async () => {
    await page.searchProfileHome('Manager of Personal Assistants III');
    await listPO.setFilter(5, 3);
    await listPO.copySPFromListByName('Manager of Personal Assistants III');

    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(`${profileName4} lowerSizeOfRole`);

    await landingPO.selectEditPageItem(2);
    await landingPO.responsibilityLVLDOWN(2, 1);
    // await landingPO.allRespLVLDOWN(2);
    await landingPO.clickNextButton();
    await expect(EC.presenceOf(landingPL.messagePMT)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMT.getText() == expectMsgRES50004).toBeTruthy(`\n'${await landingPL.messagePMT.getText()}'\n must be equal \n'${expectMsgRES50004}'\n`);
    await landingPO.confirmUpdate(2);
    await landingPO.clickNextButton();

    await expect(EC.presenceOf(landingPL.messagePMT)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMT.getText() == expectMsgRES50004).toBeTruthy(`\n'${await landingPL.messagePMT.getText()}'\n must be equal \n'${expectMsgRES50004}'\n`);
    await landingPO.selectEditPageItem(2);
    await expect(EC.presenceOf(landingPL.messagePMTwButton)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMTwButton.getText() == expectMsgRES50004).toBeTruthy(`\n'${await landingPL.messagePMTwButton.getText()}'\n must be equal \n'${expectMsgRES50004}'\n`);
    await landingPO.clickCancelButton();

    let grade = await landingPL.landingDetails.get(5).getText();
    await expect(grade.indexOf(expectGrade1) !== -1).toBeTruthy(`${grade} different from ${expectGrade1}`);

    await landingPO.saveProfile();
  });

  const expectMsgRES50005 = `The Profile Match Tool settings indicate a role larger than that described by the responsibility levels. To ensure accuracy, please review the Profile Match Tool settings.`
  const expectGrade2 = `13`;
  it(`increaseSizeOfRole`, async () => {
    await page.searchProfileHome('Manager of Personal Assistants III');
    await listPO.setFilter(5, 3);
    await listPO.copySPFromListByName('Manager of Personal Assistants III');
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(`${profileName4} increaseSizeOfRole`);

    await landingPO.selectEditPageItem(2);
    await landingPO.responsibilityLVLDOWN(1, 1);
    await landingPO.allRespLVLDOWN(1);
    await landingPO.clickNextButton();
    await expect(EC.presenceOf(landingPL.messagePMT)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMT.getText() == expectMsgRES50005).toBeTruthy(`\n'${await landingPL.messagePMT.getText()}'\n must be equal \n'${expectMsgRES50005}'\n`);
    await landingPO.confirmUpdate(2);
    await landingPO.clickNextButton();

    await expect(EC.presenceOf(landingPL.messagePMT)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMT.getText() == expectMsgRES50005).toBeTruthy(`\n'${await landingPL.messagePMT.getText()}'\n must be equal \n'${expectMsgRES50005}'\n`);
    await landingPO.selectEditPageItem(2);
    await expect(EC.presenceOf(landingPL.messagePMTwButton)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMTwButton.getText() == expectMsgRES50005).toBeTruthy(`\n'${await landingPL.messagePMTwButton.getText()}'\n must be equal \n'${expectMsgRES50005}'\n`);
    await landingPO.clickCancelButton();

    let grade = await landingPL.landingDetails.get(5).getText();
    await expect(grade.indexOf(expectGrade2) !== -1).toBeTruthy(`${grade} different from ${expectGrade2}`);

    await landingPO.saveProfile();
  });

  const expectMsgRES50008 = `The responsibility levels for this Success Profile indicate a job size (grade) that is significantly higher than usual. Consider starting with a Success Profile that has levels closer to your desired outcome, as this will result in the most accurate grade.`;
  const expectGrade3 = `16`;
  it(`seniorProfile`, async () => {
    await page.searchProfileHome('Manager of Personal Assistants III');
    await listPO.setFilter(5, 3);
    await listPO.copySPFromListByName('Manager of Personal Assistants III');
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(`${profileName4} seniorProfile`);

    await landingPO.selectEditPageItem(2);
    await landingPO.allRespLVLUP(15);
    await landingPO.clickNextButton();
    await expect(EC.presenceOf(landingPL.messagePMT)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMT.getText() == expectMsgRES50008).toBeTruthy(`\n'${await landingPL.messagePMT.getText()}'\n must be equal \n'${expectMsgRES50008}'\n`);
    await landingPO.confirmUpdate(2);
    await landingPO.clickNextButton();

    await expect(EC.presenceOf(landingPL.messagePMT)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMT.getText() == expectMsgRES50008).toBeTruthy(`\n'${await landingPL.messagePMT.getText()}'\n must be equal \n'${expectMsgRES50008}'\n`);
    await landingPO.selectEditPageItem(2);
    await expect(EC.presenceOf(landingPL.messagePMTwButton)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMTwButton.getText() == expectMsgRES50008).toBeTruthy(`\n'${await landingPL.messagePMTwButton.getText()}'\n must be equal \n'${expectMsgRES50008}'\n`);
    await landingPO.clickCancelButton();

    let grade = await landingPL.landingDetails.get(5).getText();
    await expect(grade.indexOf(expectGrade3) !== -1).toBeTruthy(`${grade} different from ${expectGrade3}`);

    await landingPO.saveProfile();
  });

  const expectMsgRES50009 = `The responsibility levels for this Success Profile indicate a job size (grade) that is significantly lower than usual. Consider starting with a Success Profile that has levels closer to your desired outcome, as this will result in the most accurate grade.`;
  const expectGrade4 = `11`;
  it(`juniorProfile`, async () => {
    await page.searchProfileHome('Manager of Personal Assistants III');
    await listPO.setFilter(5, 3);
    await listPO.copySPFromListByName('Manager of Personal Assistants III');
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(`${profileName4} juniorProfile`);

    await landingPO.selectEditPageItem(2);
    await landingPO.allRespLVLDOWN(15);
    await landingPO.clickNextButton();
    await expect(EC.presenceOf(landingPL.messagePMT)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMT.getText() == expectMsgRES50009).toBeTruthy(`\n'${await landingPL.messagePMT.getText()}'\n must be equal \n'${expectMsgRES50009}'\n`);
    await landingPO.confirmUpdate(2);
    await landingPO.clickNextButton();

    await expect(EC.presenceOf(landingPL.messagePMT)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMT.getText() == expectMsgRES50009).toBeTruthy(`\n'${await landingPL.messagePMT.getText()}'\n must be equal \n'${expectMsgRES50009}'\n`);
    await landingPO.selectEditPageItem(2);
    await expect(EC.presenceOf(landingPL.messagePMTwButton)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMTwButton.getText() == expectMsgRES50009).toBeTruthy(`\n'${await landingPL.messagePMTwButton.getText()}'\n must be equal \n'${expectMsgRES50009}'\n`);
    await landingPO.clickCancelButton();

    let grade = await landingPL.landingDetails.get(5).getText();
    await expect(grade.indexOf(expectGrade4) !== -1).toBeTruthy(`${grade} different from ${expectGrade4}`);

    await landingPO.saveProfile();
  });

  const expectMsgRES50007 = `The changes to responsibilities indicate a job size (grade) that is slightly lower than usual for this profile. Please ensure the responsibility levels are not set too low.`;
  const expectGrade5 = `11`;
  it(`lowResponsibilities`, async () => {
    await page.searchProfileHome('General Clerk I');
    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 2);
    await listPO.copySPFromListByName('General Clerk I');
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(`${profileName4} lowResponsibilities`);

    await landingPO.selectEditPageItem(2);
    await landingPO.responsibilityLVLUP(1, 1);
    await landingPO.responsibilityLVLUP(2, 1);
    await landingPO.clickNextButton();
    await landingPO.setPMTSlidersMIN();
    await landingPO.confirmUpdate(2);
    await landingPO.clickNextButton();

    await expect(EC.presenceOf(landingPL.messagePMT)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMT.getText() == expectMsgRES50007).toBeTruthy(`\n'${await landingPL.messagePMT.getText()}'\n must be equal \n'${expectMsgRES50007}'\n`);
    await landingPO.selectEditPageItem(2);
    await expect(EC.presenceOf(landingPL.messagePMTwButton)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMTwButton.getText() == expectMsgRES50007).toBeTruthy(`\n'${await landingPL.messagePMTwButton.getText()}'\n must be equal \n'${expectMsgRES50007}'\n`);
    await landingPO.clickCancelButton();

    let grade = await landingPL.landingDetails.get(5).getText();
    await expect(grade.indexOf(expectGrade5) !== -1).toBeTruthy(`${grade} different from ${expectGrade5}`);

    await landingPO.saveProfile();
  });

  const expectMsgRES50006 = `The changes to responsibilities indicate a job size (grade) that is slightly higher than usual for this profile. Please ensure the responsibility levels are not set too high.`;
  const expectGrade6 = `15`;
  it(`highResponsibilities`, async () => {
    await page.searchProfileHome('General Clerk III');
    await listPO.setFilter(5, 3);
    await listPO.copySPFromListByName('General Clerk III');
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(`${profileName4} highResponsibilities`);

    await landingPO.selectEditPageItem(2);
    await landingPO.responsibilityLVLUP(4, 2); //4
    await landingPO.responsibilityLVLUP(5, 3); //5
    await landingPO.clickNextButton();
    await landingPO.setPMTSlidersMAX();
    await landingPO.confirmUpdate(2);
    await landingPO.clickNextButton();

    await expect(EC.presenceOf(landingPL.messagePMT)).toBeTruthy(`There is no PMT message`);
    await expect(await landingPL.messagePMT.getText() == expectMsgRES50006).toBeTruthy(`\n'${await landingPL.messagePMT.getText()}'\n must be equal \n'${expectMsgRES50006}'\n`);
    await landingPO.selectEditPageItem(2);
    await expect(EC.presenceOf(landingPL.messagePMTwButton)).toBeTruthy(`There is no PMT message with button`);
    await expect(await landingPL.messagePMTwButton.getText() == expectMsgRES50006).toBeTruthy(`\n'${await landingPL.messagePMTwButton.getText()}'\n must be equal \n'${expectMsgRES50006}'\n`);
    await landingPO.clickCancelButton();

    let grade = await landingPL.landingDetails.get(5).getText();
    await expect(grade.indexOf(expectGrade6) !== -1).toBeTruthy(`${grade} different from ${expectGrade6}`);

    await landingPO.saveProfile();
  });

});