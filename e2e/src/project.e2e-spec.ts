import { by, element, browser, ExpectedConditions as EC, protractor } from 'protractor';
import { AppPage as page, AppPage } from './components/app.po';
import { SuccessProfileListPageObjects as listPO } from './components/sp-list.po';
import { ProjectListPageObject as projectListPO } from './components/project-list.po';
import { ProjectListLocators as projectListPL } from './components/project-list.locators';
import { SuccessProfileDetailsPageLocators as detailsPL } from './components/sp-details.locators';
import { SpinnerPO as spinner } from './components/spinner';

const now = new Date();
let projectNameP = `Test ASDFG || create`;
let projectName = `Test ASDFG || create ${now.toUTCString()}`;

describe('Project', () => {
  beforeEach(async () => {
    await page.login();
    await browser.waitForAngularEnabled(false);
  });

  afterEach(async () => {
    await browser.manage().logs().get('browser').then(async (browserLog) => {
      if (browserLog.length > 0) {
        await browserLog.forEach(async log => {
          await expect(browserLog.length > 0).toBeFalsy(log.message); // .toBeFalsy(`error in console`);
        });
      }
    });
  });

  it(`Create project: Assess`, async () => {
    await page.moveToSPList();
    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 1);

    await listPO.openSPFromListByNo(1);
    let profileName = await detailsPL.profileName.getText();
    await projectListPO.createAssessTypeProject();

    await projectListPO.setProjectName(projectName);
    let b = new Date();
    let a = new Date();
    b.setDate(new Date().getDate() + 1);
    await projectListPO.setProjectEndDate(b.getDate().toString());

    await projectListPO.clickContinueCreation();
    await projectListPO.clickContinueDialog();
    await projectListPO.clickFinishCreation();
    await projectListPO.clickConfirmDialogue();    

    await page.moveToAssessmentProjectTab();
    await projectListPO.searchProject(projectName);
    await projectListPO.getCount();

    let actualProjectName = await projectListPL.projectListNames.get(0).getText();
    await expect(actualProjectName == projectName).toBeTruthy(`Project Name different: expected ${projectName} instead ${actualProjectName}`);
    // let actualProfileName = await projectListPL.projectListSPName.getText();
    // await expect(actualProfileName == profileName).toBeTruthy(`Profile Name different: expected ${profileName} instead ${actualProfileName}`);
    let startDate = await projectListPL.projectListCreatedDate.getText();
    let actualStartDate = new Date(startDate);
    await expect(actualStartDate.toDateString() == a.toDateString()).toBeTruthy(`Start date iis different: expected ${a} instead of ${actualStartDate}`);
    let endDate = await projectListPL.projectListEndDate.getText();
    let actualEndDate = new Date(endDate);
    await expect(actualEndDate.toDateString() == b.toDateString()).toBeTruthy(`Start date iis different: expected ${b} instead of ${actualEndDate}`);

    // await page.moveToHomeTab();
    // await page.moveToSPList();
    // await listPO.clearAllFiltersF();
  });

});