import { browser, by, element, ExpectedConditions as EC, protractor } from 'protractor';
import { AppPage as page } from './components/app.po';
import { checkFileExistsWithDelay, checkFileStatWithDelay, deleteFileIfExists } from './components/utils';
import { SuccessProfileListPageObjects as listPO } from './components/sp-list.po';
import { SuccessProfileListLocators as listPL } from './components/sp-list.locators';
import { SuccessProfileDetailsPageObjects as detailsPO } from './components/sp-details.po';
import { SuccessProfileDetailsPageLocators as detailsPL } from './components/sp-details.locators';
import { SuccessProfileLandingPageObjects as landingPO } from './components/sp-landing.po';
import { SuccessProfileLandingPageLocators as landingPL } from './components/sp-landing.locators';
import { JobEvaluationPageObjects as jePO } from './components/sp-job-evaluation.po';
import { JobEvaluationPageLocators as jePL } from './components/sp-job-evaluation.locators';
import { SpinnerPO as spinner } from './components/spinner';

let xlsx = require(`xlsx`);

let subFuncValue5 = `Category Management`;
let funcForScroll6 = `Health and Safety`;

let SP1 = {
  name: `Test ASDFG || compare 1`
  , nameModified: `Test ASDFG || compare 1 modified`
  , function: `Visual Merchandising`
  , functionModified: `Pharmaceutical Research and Development`
  , subFunction: `Stores Visual Merchandising`
  , subFunctionModified: `Pre-clinical Research`

  , level: ``
  , subLevel: ``
  , grade: ``
  , gradeLetter: ``

  , resp1: {
    name: `RR 1`
    , nameModified: `RR 1 Modified`
  }
  , BC1: {
    name: `BC 1`
    , nameModified: `BC 1 Modified`
  }
  , TC1: {
    name: `TC 1`
    , nameModified: `TC 1 Modified`
    , skill1: {
      name: `TC Skill 1`
      , nameModified: `TC Skill 1 Modified`
    }
  }
  , task1: {
    name: `Task 1`
    , nameModified: `Task 1 Modified`
  }
  , task2: {
    name: `Task 2`
    , nameModified: `Task 2 Modified`
  }
  , task3: {
    name: `Task 3`
    , nameModified: `Task 3 Modified`
  }
  , tool1: {
    name: `Tool 1`
    , nameModified: `Tool 1 Modified`
  }
  ,
  education: {
    name: `G Education 1`
    , nameModified: `G Education 1 Modified`
  },
  generalExperience: {
    name: `G Experience 1`
    , nameModified: `G Experience 1 Modified`
  },
  managerialExperience: {
    name: `M Experience 1`
    , nameModified: `M Experience 1 Modified`
  }

};

let SP2 = {
  name: `Test ASDFG || compare 2`
  , function: `Content, Design and Engineering`
  , subFunction: `Content Creation and Management`
  , level: ``
  , subLevel: ``
  , grade: ``
  , gradeLetter: ``
  , resp1: `RR 2`
  , BC1: `BC 2`
  , TC1: {
    name: `TC 2`
    , skill1: `TC Skill 2`
  }
  , task1: `Task 4`
  , task2: `Task 5`
  , task3: `Task 6`
  , tool1: `Tool 2`
  , education: `G Education 2`
  , generalExperience: `G Experience 2`
  , managerialExperience: `M Experience 2`
};

let SP3 = {
  name: `Test ASDFG || compare 3`
  , function: `Branch Financial Services / Banking`
  , subFunction: `Financial Services S`//ales`
  , resp1: `RR 3`
  , BC1: `BC 3`
  , TC1: {
    name: `TC 3`
    , skill1: `TC Skill 3`
  }
  , task1: `Task 7`
  , task2: `Task 8`
  , task3: `Task 9`
  , tool1: `Tool 3`
  , education: `G Education 3`
  , generalExperience: `G Experience 3`
  , managerialExperience: `M Experience 3`
};

let SP4 = {
  name: `Test ASDFG || compare 4`
  , nameModified: `Test ASDFG || compare 4 modified`
  , function: `Information Technology`
  , subFunction: `Software Development and Implementation`
  , functionModified: `Hardware Engineering`
  , subFunctionModified: `Hardware Engineering`

  , resp1: {
    name: `RR 4`
    , nameModified: `RR 4 Modified`
  }
  , BC1: {
    name: `BC 4`
    , nameModified: `BC 4 Modified`
  }
  , TC1: {
    name: `TC 4`
    , nameModified: `TC 4 Modified`
    , skill1: {
      name: `TC Skill 4`
      , nameModified: `TC Skill 4 Modified`
    }
  }
  , task1: {
    name: `Task 10`
    , nameModified: `Task 10 Modified`
  }
  , task2: {
    name: `Task 11`
    , nameModified: `Task 11 Modified`
  }
  , task3: {
    name: `Task 12`
    , nameModified: `Task 12 Modified`
  }
  , tool1: {
    name: `Tool 4`
    , nameModified: `Tool 4 Modified`
  }
  ,
  education: {
    name: `G Education 4`
    , nameModified: `G Education 4 Modified`
  },
  generalExperience: {
    name: `G Experience 4`
    , nameModified: `G Experience 4 Modified`
  },
  managerialExperience: {
    name: `M Experience 4`
    , nameModified: `M Experience 4 Modified`
  }
};

let SP5 = {
  name: `Test ASDFG || compare 5 modified`
  , function: `Petroleum Engineering/Production`
  , subFunction: `Drilling / Production`
  , level: ``
  , subLevel: ``
  , grade: ``
  , resp1: `RR 5 Modified`
  , BC1: `BC 5 Modified`
  , TC1: {
    name: `TC 5 Modified`
    , skill1: `TC Skill 5 Modified`
  }
  , task1: `Task 13`
  , task2: `Task 14`
  , task3: `Task 15`
  , tool1: `Tool 5 Modified`
  , education: `G Education 5 Modified`
  , generalExperience: `G Experience 5 Modified`
  , managerialExperience: `M Experience 5 Modified`
};

let SP6 = {
  name: `Test ASDFG || compare 6`,
  function: `Loss Prevention`,
  subFunction: `Corporate Loss Prevention`, // should be same as SP7

  resp1: {
    name: ``
    , primary: true
    , levelInc: 1
    , levelText: ``
  },
  resp2: {
    name: ``
    , primary: true
    , levelInc: 2
    , levelText: ``
  },
  resp3: {
    name: ``
    , primary: false
    , levelInc: 3
    , levelText: ``
  },

  BC1: {
    name: ``
    , primary: false
    , levelInc: 1
    , levelText: ``
  },
  BC2: {
    name: ``
    , primary: false
    , levelInc: 2
    , levelText: ``
  },
  BC3: {
    name: ``
    , primary: false
    , levelInc: 3
    , levelText: ``
  },

  TC1: {
    name: ``
    , primary: false
    , levelInc: 1
    , levelText: ``
  },
  TC2: {
    name: ``
    , primary: false
    , levelInc: 2
    , levelText: ``
  },
  TC3: {
    name: ``
    , primary: false
    , levelInc: 3
    , levelText: ``
  },

  education: {
    name: ``
    , primary: false
    , levelInc: 1
    , levelText: ``
  },
  generalExperience: {
    name: ``
    , primary: false
    , levelInc: 2
    , levelText: ``
  },
  managerialExperience: {
    name: ``
    , primary: false
    , levelInc: 3
    , levelText: ``
  }

};

let SP7 = {
  name: `Test ASDFG || compare 7`,
  function: `Loss Prevention`,
  subFunction: `Corporate Loss Prevention`, // should be same as SP6

  resp1: {
    name: ``
    , primary: true
    , levelInc: 2
    , levelText: ``
  },
  resp2: {
    name: ``
    , primary: true
    , levelInc: 3
    , levelText: ``
  },
  resp3: {
    name: ``
    , primary: false
    , levelInc: 1
    , levelText: ``
  },

  BC1: {
    name: ``
    , primary: false
    , levelInc: 2
    , levelText: ``
  },
  BC2: {
    name: ``
    , primary: false
    , levelInc: 3
    , levelText: ``
  },
  BC3: {
    name: ``
    , primary: false
    , levelInc: 1
    , levelText: ``
  },

  TC1: {
    name: ``
    , primary: false
    , levelInc: 2
    , levelText: ``
  },
  TC2: {
    name: ``
    , primary: false
    , levelInc: 3
    , levelText: ``
  },
  TC3: {
    name: ``
    , primary: false
    , levelInc: 1
    , levelText: ``
  },

  education: {
    name: ``
    , primary: false
    , levelInc: 2
    , levelText: ``
  },
  generalExperience: {
    name: ``
    , primary: false
    , levelInc: 3
    , levelText: ``
  },
  managerialExperience: {
    name: ``
    , primary: false
    , levelInc: 1
    , levelText: ``
  }

};

let SP8 = {
  name: `Test ASDFG || compare 8`,
  function: `Loss Prevention`,
  subFunction: `Corporate Loss Prevention`,

  resp1: {
    name: ``
    , primary: true
    , levelInc: 3
    , levelText: ``
  },
  resp2: {
    name: ``
    , primary: true
    , levelInc: 1
    , levelText: ``
  },
  resp3: {
    name: ``
    , primary: false
    , levelInc: 2
    , levelText: ``
  },

  BC1: {
    name: ``
    , primary: false
    , levelInc: 3
    , levelText: ``
  },
  BC2: {
    name: ``
    , primary: false
    , levelInc: 1
    , levelText: ``
  },
  BC3: {
    name: ``
    , primary: false
    , levelInc: 2
    , levelText: ``
  },

  TC1: {
    name: ``
    , primary: false
    , levelInc: 3
    , levelText: ``
  },
  TC2: {
    name: ``
    , primary: false
    , levelInc: 1
    , levelText: ``
  },
  TC3: {
    name: ``
    , primary: false
    , levelInc: 2
    , levelText: ``
  },

  education: {
    name: ``
    , primary: false
    , levelInc: 3
    , levelText: ``
  },
  generalExperience: {
    name: ``
    , primary: false
    , levelInc: 1
    , levelText: ``
  },
  managerialExperience: {
    name: ``
    , primary: false
    , levelInc: 2
    , levelText: ``
  }
};

let SP9 = {
  name: `Test ASDFG || compare 9`,
  function: `Loss Prevention`,
  subFunction: `Corporate Loss Prevention`,

  resp1: {
    name: ``
    , primary: true
    , levelInc: 4
    , levelText: ``
  },
  resp2: {
    name: ``
    , primary: true
    , levelInc: 0
    , levelText: ``
  },
  resp3: {
    name: ``
    , primary: false
    , levelInc: 4
    , levelText: ``
  },

  BC1: {
    name: ``
    , primary: false
    , levelInc: 4
    , levelText: ``
  },
  BC2: {
    name: ``
    , primary: false
    , levelInc: 4
    , levelText: ``
  },
  BC3: {
    name: ``
    , primary: false
    , levelInc: 0
    , levelText: ``
  },

  TC1: {
    name: ``
    , primary: false
    , levelInc: 0
    , levelText: ``
  },
  TC2: {
    name: ``
    , primary: false
    , levelInc: 4
    , levelText: ``
  },
  TC3: {
    name: ``
    , primary: false
    , levelInc: 4
    , levelText: ``
  },

  education: {
    name: ``
    , primary: false
    , levelInc: 0
    , levelText: ``
  },
  generalExperience: {
    name: ``
    , primary: false
    , levelInc: 0
    , levelText: ``
  },
  managerialExperience: {
    name: ``
    , primary: false
    , levelInc: 0
    , levelText: ``
  }
};

let SPF1 = {
  name: `Test ASDFG || compare F1`
  , function: `Logistics / Supply Chain`
  , subFunction: `Distribution / Transportation`
  , level: ``
  , subLevel: ``
  , grade: ``
};

describe('SP. Matrix view + Compare.', () => {
  beforeEach(async () => {
    await page.login(`userTwo`);
    await browser.waitForAngularEnabled(false);
  });

  afterEach(async () => {
    await browser.manage().logs().get('browser').then(async (browserLog) => {
      if (browserLog.length > 0) {
        await browserLog.forEach(async log => {
          await expect(browserLog.length > 0).toBeFalsy(`error in console`);//.toBeFalsy(log.message);
        });
      }
    });
  });

  it(`SP Matrix view`, async () => {
    // 1. create 2 SP
    await page.moveToSPList();

    await listPO.setFilter(5, 1);
    await listPO.setFunctionFilter(4, SP1.subFunction);
    await listPO.setFunctionFilter(4, SP2.subFunction);
    await listPO.getCount().then(async (am) => {
      console.log(am);
      for (let i = am; i > 0; i--) {
        await listPO.deleteSPViaThreeDotsMenu();
      };
    });
    await listPO.clearSearch();
    await listPO.clearAllFilters();

    await listPO.setFilter(5, 3);
    await listPO.setFunctionFilter(4, SP1.subFunction);
    await listPO.copyProfileViaThreeDotsMenu(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.selectEditPageItem(1);
    await landingPO.editProfileTitle(SP1.name);
    await landingPO.setProfileFunctionByName(SP1.function);
    await landingPO.setProfileSubFunctionByName(SP1.subFunction);
    await landingPO.clickNextButton();

    await landingPO.selectEditPageItem(2);
    await landingPO.deleteAllResponsibilities();
    await landingPO.addResponsibility();
    await landingPO.editResponsibilityName(1, SP1.resp1.name);
    await landingPO.clickNextButton();
    await landingPO.confirmUpdate(2);
    await landingPO.deleteAllTasks();
    await landingPO.addTask();
    await landingPO.editTask(1, 1, SP1.task1.name);
    await landingPO.addTask();
    await landingPO.editTask(2, 1, SP1.task2.name);
    await landingPO.addTask();
    await landingPO.editTask(3, 2, SP1.task3.name);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();
    await detailsPO.openEditPageViaActions();

    await landingPO.selectEditPageItem(3);
    await landingPO.recommendationsDone();
    await landingPO.deleteAllBCompetencies();
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(1, SP1.BC1.name);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SP1.TC1.name);
    await landingPO.addNewSkillActions(1, SP1.TC1.skill1.name);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SP1.tool1.name);
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.deleteAllEduNExp();
    await landingPO.addAllEducation();
    await landingPO.editEducationName(1, SP1.education.name);
    await landingPO.addAllExperience();
    await landingPO.editExperienceName(1, SP1.generalExperience.name);
    await landingPO.editExperienceName(2, SP1.managerialExperience.name);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();
    SP1.level = await detailsPL.levelInfo.get(0).getText();
    SP1.subLevel = await detailsPL.levelInfo.get(1).getText();
    SP1.gradeLetter = await detailsPL.gradeInfo.getText();
    await page.moveToSPList();

    await listPO.copyProfileViaThreeDotsMenu(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.selectEditPageItem(1);
    await landingPO.editProfileTitle(SP2.name);
    await landingPO.setProfileFunctionByName(SP2.function);
    await landingPO.setProfileSubFunctionByName(SP2.subFunction);
    await landingPO.clickNextButton();

    await landingPO.selectEditPageItem(2);
    await landingPO.deleteAllResponsibilities();
    await landingPO.addResponsibility();
    await landingPO.editResponsibilityName(1, SP2.resp1);
    await landingPO.clickNextButton();
    await landingPO.confirmUpdate(2);
    await landingPO.deleteAllTasks();
    await landingPO.addTask();
    await landingPO.editTask(1, 1, SP2.task1);
    await landingPO.addTask();
    await landingPO.editTask(2, 1, SP2.task2);
    await landingPO.addTask();
    await landingPO.editTask(3, 2, SP2.task3);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();
    await detailsPO.openEditPageViaActions();

    await landingPO.selectEditPageItem(3);
    await landingPO.recommendationsDone();
    await landingPO.deleteAllBCompetencies();
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(1, SP2.BC1);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SP2.TC1.name);
    await landingPO.addNewSkillActions(1, SP2.TC1.skill1);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SP2.tool1)
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.deleteAllEduNExp();
    await landingPO.addAllEducation();
    await landingPO.editEducationName(1, SP2.education);
    await landingPO.addAllExperience();
    await landingPO.editExperienceName(1, SP2.generalExperience);
    await landingPO.editExperienceName(2, SP2.managerialExperience);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();
    SP2.level = await detailsPL.levelInfo.get(0).getText();
    SP2.subLevel = await detailsPL.levelInfo.get(1).getText();
    SP2.gradeLetter = await detailsPL.gradeInfo.getText();
    await page.moveToSPList();
    await listPO.clearAllFilters();

    // 2. matrix, check fitler: by me, by KF, function filter
    await listPO.setMatrixView();
    await listPO.clearAllFiltersF();

    await listPO.setFilter(2, 3); // created by KF
    await listPO.setFunctionFilter(1, SP1.subFunction);
    await expect(await listPL.matrixItems.count() == 4).toBeTruthy(`Should be 4 profiles in matrix`);
    await listPO.clearAllFilters();

    await listPO.setFilter(2, 1); // created by me
    await listPO.setFunctionFilter(1, SP1.subFunction);
    await listPO.setFunctionFilter(1, SP2.subFunction);
    await expect(await listPL.matrixItems.count() == 2).toBeTruthy(`Should be 2 profiles in matrix`);

    // 3. matrix, location according to grade
    //   ???

    //     4. matrix, popup content according to SP details
    // 5. add to compare, delete from compare, add once more
    await listPO.openItemPopup(2);
    await browser.wait(EC.visibilityOf(listPL.popupData.get(1)), 30 * 1000, `Popupp still invisible`);
    let actualtitle1 = await listPL.popupData.get(1).getText();
    await expect(actualtitle1.indexOf(SP1.name) !== -1).toBeTruthy(`Expected title in popup: ${SP1.name} instead of ${actualtitle1}`);
    let actuallevel1 = await listPL.popupData.get(2).getText();
    let actualsublevel1 = await listPL.popupData.get(3).getText();
    await expect(SP1.level.indexOf(actuallevel1) !== -1)
      .toBeTruthy(`Expected level in popup: '${SP1.level}' instead of '${actuallevel1}'`);
    await expect(SP1.subLevel.indexOf(actualsublevel1) !== -1)
      .toBeTruthy(`Expected sub level in popup '${SP1.subLevel}' instead of '${actualsublevel1}'`);
    let actualgrade1 = await listPL.popupData.get(4).getText();
    await expect(actualgrade1.indexOf(SP1.gradeLetter) !== -1).toBeTruthy(`Expected grade in popup: ${SP1.gradeLetter} instead of ${actualgrade1}`);
    await listPO.setCompareCheckbox();
    await listPO.closeItemPopup();

    await listPO.openItemPopup(1);
    let actualtitle2 = await listPL.popupData.get(1).getText()
    await expect(actualtitle2.indexOf(SP2.name) !== -1)
      .toBeTruthy(`Expected title in popup: ${SP2.name} instead of ${actualtitle2}`);
    let actuallevel2 = await listPL.popupData.get(2).getText();
    let actualsublevel2 = await listPL.popupData.get(3).getText();
    await expect(SP2.level.indexOf(actuallevel2) !== -1).toBeTruthy(`Expected level in popup: '${SP2.level}' instead of '${actuallevel2}'`);
    await expect(SP2.subLevel.indexOf(actualsublevel2) !== -1).toBeTruthy(`Expected sub level in popup '${SP2.subLevel}' instead of '${actualsublevel2}'`);
    let actualgrade2 = await listPL.popupData.get(4).getText();
    await expect(actualgrade2.indexOf(SP2.gradeLetter) !== -1).toBeTruthy(`Expected grade in popup: ${SP2.gradeLetter} instead of ${actualgrade2}`);
    await listPO.setCompareCheckbox();
    await listPO.closeItemPopup();

    await expect(await listPL.listCompareContainer.count() !== 0).toBeTruthy(`List of compared profiles shouldn't be empty`);

    await listPO.clickRemoveFromCompareList(2);
    await listPO.addToCompareCustom(2);

    await expect(await listPL.listCompareContainer.count() == 0).toBeTruthy(`Should be no list with compared profiles. its empty`);

    // check fields content accroding to saved values
    await listPO.addToCompareCustom(2);
    await listPO.clickCompareAllButton();

    await listPO.setComparison(1, 2) //resps
    await listPL.toggles.get(1).click();
    let aResp1 = await listPL.compareProfile1.get(1).getText();
    await expect(aResp1.indexOf(SP1.resp1.name) !== -1).toBeTruthy(`Responsibilities: expected ${SP1.resp1.name} instead of ${aResp1}`);
    await listPO.setComparison(2, 1) //BC
    let aBC1 = await listPL.compareProfile1.get(1).getText();
    await expect(aBC1.indexOf(SP1.BC1.name) !== -1).toBeTruthy(`BC: expected ${SP1.BC1.name} instead of ${aBC1}`);
    await listPO.setComparison(2, 2) //TC
    let aTC1 = await listPL.compareProfile1.get(1).getText();
    await expect(aTC1.indexOf(SP1.TC1.name) !== -1).toBeTruthy(`TC: expected ${SP1.TC1.name} instead of ${aTC1}`);
    await listPO.setComparison(2, 3) //Edu n exp
    let aEdu1 = await listPL.compareProfile1.get(1).getText();
    let aExp1 = await listPL.compareProfile1.get(4).getText();
    let aExp2 = await listPL.compareProfile1.get(7).getText();
    await expect(aEdu1.indexOf(SP1.education.name) !== -1).toBeTruthy(`General Education: expected ${SP1.education.name} instead of ${aEdu1}`);
    await expect(aExp1.indexOf(SP1.generalExperience.name) !== -1).toBeTruthy(`General Experience: expected ${SP1.generalExperience.name} instead of ${aExp1}`);
    await expect(aExp2.indexOf(SP1.managerialExperience.name) !== -1).toBeTruthy(`Managerial Experience: expected ${SP1.managerialExperience.name} instead of ${aExp2}`);

    await listPL.toggles.get(1).click();
    await listPO.removeAllProfileFromCompareView();
    await listPO.clearAllFilters();
  });

  it(`SP Compare view`, async () => {
    await page.moveToSPList();
    await listPO.setFilter(5, 1);
    await listPO.setFunctionFilter(4, SP3.subFunction);
    await listPO.setFunctionFilter(4, SP4.subFunction);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.deleteSPViaThreeDotsMenu();
      };
    });
    await listPO.clearAllFilters();

    await listPO.setMatrixView();
    await listPO.clearAllFiltersF();

    // i.	Pick 5 random Roles from various functions and 
    await listPO.setFilter(2, 3);
    await listPO.setFunctionFilter(1, SP1.subFunction);
    await listPO.setFunctionFilter(1, SP2.subFunction);
    await listPO.setFunctionFilter(1, SP3.subFunction);
    await listPO.setFunctionFilter(1, SP4.subFunction);
    await listPO.setFunctionFilter(1, subFuncValue5);

    await expect(EC.not(EC.elementToBeClickable(listPL.rNavButtons.get(0)))).toBeTruthy(`Scroll should be disabled if filtered less than 6 sub functions`);
    await listPO.setFunctionFilter(1, funcForScroll6);
    await expect(EC.elementToBeClickable(listPL.rNavButtons.get(0))).toBeTruthy(`Scroll should be enabled if filtered 6 or more sub functions`);
    await listPO.setFunctionFilter(1, funcForScroll6);

    // ii.	go through the profile match tool and 
    await listPO.createCopyviaPMT(1);

    // iii.	Land on a profile, edit the profile (in this case it creates is) and save it.
    await detailsPO.openEditPageViaActions();
    await landingPO.selectEditPageItem(1);
    await landingPO.editProfileTitle(SP3.name);
    await landingPO.setProfileFunctionByName(SP3.function);
    await landingPO.setProfileSubFunctionByName(SP3.subFunction);
    await landingPO.clickNextButton();

    await landingPO.selectEditPageItem(2);
    await landingPO.deleteAllResponsibilities();
    await landingPO.addResponsibility();
    await landingPO.editResponsibilityName(1, SP3.resp1);
    await landingPO.clickNextButton();
    await landingPO.confirmUpdate(2);
    await landingPO.deleteAllTasks();
    await landingPO.addTask();
    await landingPO.editTask(1, 1, SP3.task1);
    await landingPO.addTask();
    await landingPO.editTask(2, 1, SP3.task2);
    await landingPO.addTask();
    await landingPO.editTask(3, 2, SP3.task3);
    await landingPO.clickNextButton();

    await landingPO.saveProfileAs();
    await detailsPO.openEditPageViaActions();

    await landingPO.selectEditPageItem(3);
    await landingPO.recommendationsDone();
    await landingPO.deleteAllBCompetencies();
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(1, SP3.BC1);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SP3.TC1.name);
    await landingPO.addNewSkillActions(1, SP3.TC1.skill1);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SP3.tool1)
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.deleteAllEduNExp();
    await landingPO.addAllEducation();
    await landingPO.editEducationName(1, SP3.education);
    await landingPO.addAllExperience();
    await landingPO.editExperienceName(1, SP3.generalExperience);
    await landingPO.editExperienceName(2, SP3.managerialExperience);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();

    await page.moveToSPList();
    await listPO.setMatrixView();

    await listPO.createCopyviaPMT(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.selectEditPageItem(1);
    await landingPO.editProfileTitle(SP4.name);
    await landingPO.setProfileFunctionByName(SP4.function);
    await landingPO.setProfileSubFunctionByName(SP4.subFunction);
    await landingPO.clickNextButton();

    await landingPO.selectEditPageItem(2);
    await landingPO.deleteAllResponsibilities();
    await landingPO.addResponsibility();
    await landingPO.editResponsibilityName(1, SP4.resp1.name);
    await landingPO.allRespLVLUP(1);
    await landingPO.clickNextButton();
    await landingPO.confirmUpdate(2);
    await landingPO.deleteAllTasks();
    await landingPO.addTask();
    await landingPO.editTask(1, 1, SP4.task1.name);
    await landingPO.addTask();
    await landingPO.editTask(2, 1, SP4.task2.name);
    await landingPO.addTask();
    await landingPO.editTask(3, 2, SP4.task3.name);
    await landingPO.clickNextButton();

    await landingPO.saveProfileAs();
    await detailsPO.openEditPageViaActions();

    await landingPO.selectEditPageItem(3);
    await landingPO.recommendationsDone();
    await landingPO.deleteAllBCompetencies();
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(1, SP4.BC1.name);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SP4.TC1.name);
    await landingPO.addNewSkillActions(1, SP4.TC1.skill1.name);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SP4.tool1.name)
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.deleteAllEduNExp();
    await landingPO.addAllEducation();
    await landingPO.editEducationName(1, SP4.education.name);
    await landingPO.addAllExperience();
    await landingPO.editExperienceName(1, SP4.generalExperience.name);
    await landingPO.editExperienceName(2, SP4.managerialExperience.name);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();

    await page.moveToSPList();
    await listPO.setMatrixView();
    await listPO.clearAllFilters();

    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP3.subFunction);
    await listPO.setFunctionFilter(1, SP4.subFunction);
    await browser.wait(EC.presenceOf(listPL.matrixItems.get(0)), browser.params.cooldown.medium, `There is no profiles in matrix view`);
    await expect(await listPL.matrixItems.count() == 2).toBeTruthy(`Should be 2 profiles in matrix`);

    await listPO.addToCompareCustom(1);
    await listPO.addToCompareCustom(2);
    await listPO.clickCompareAllButton();

    // 3. delete from view, add back to view
    await listPO.removeProfileFromCompareView(2);
    await listPO.clickAddNewProfile();
    await listPO.addToCompareCustom(2);
    await listPO.clickCompareAllButton();

    // 4. add created by KF profile + add 2 related profiles 
    await listPO.clickAddNewProfile();
    await listPO.clearAllFilters();
    await listPO.setFilter(2, 3);
    await listPO.addToCompare(1);
    await listPO.clickCompareAllButton();
    await listPO.addRelatedToCompare(1);
    await listPO.addRelatedToCompare(1);

    // 5. toggles
    await listPO.setComparison(1, 2);
    await listPL.toggles.get(0).click();
    await expect(await listPL.highlighted.count() > 0).toBeTruthy(`There is no highlighted elements`);

    await listPL.toggles.get(1).click();
    await expect(await listPL.levelDescriptions.count() > 0).toBeTruthy(`There is no descriptions`);

    await listPL.toggles.get(0).click();
    await expect(await listPL.highlighted.count() == 0).toBeTruthy(`Should be no highlighted elements`);

    await listPL.toggles.get(1).click();
    await expect(await listPL.levelDescriptions.count() == 0).toBeTruthy(`Should be no descriptions`);

    // 6. comparison drop down usage
    await listPO.setComparison(1, 1); // Acc / Work characteristics
    await listPO.setComparison(1, 2); // Acc / Responsibilities
    await listPO.setComparison(2, 1); // Capability / BC
    await listPO.setComparison(2, 2); // Capability / TC
    await listPO.setComparison(2, 3); // Capability / EduNExp
    await listPO.setComparison(3, 1); // Identity / Traits
    await listPO.setComparison(3, 1); // Identity / Drivers

    await listPO.removeProfileFromCompareView(5);
    await listPO.removeProfileFromCompareView(4);
    await listPO.removeProfileFromCompareView(3);
    await listPO.removeProfileFromCompareView(2);

    await listPO.setComparison(1, 2) //resps
    await listPL.toggles.get(1).click();
    let aResp3 = await listPL.compareProfile1.get(1).getText();
    await expect(aResp3.indexOf(SP3.resp1) !== -1).toBeTruthy(`Responsibilities: expected ${SP3.resp1} instead of ${aResp3}`);
    await listPO.setComparison(2, 1) //BC
    let aBC3 = await listPL.compareProfile1.get(1).getText();
    await expect(aBC3.indexOf(SP3.BC1) !== -1).toBeTruthy(`BC: expected ${SP3.BC1} instead of ${aBC3}`);
    await listPO.setComparison(2, 2) //TC
    let aTC3 = await listPL.compareProfile1.get(1).getText();
    await expect(aTC3.indexOf(SP3.TC1.name) !== -1).toBeTruthy(`TC: expected ${SP3.TC1.name} instead of ${aTC3}`);
    await listPO.setComparison(2, 3) //Edu n exp
    let aEdu3 = await listPL.compareProfile1.get(1).getText();
    let aExp5 = await listPL.compareProfile1.get(4).getText();
    let aExp6 = await listPL.compareProfile1.get(7).getText();
    await expect(aEdu3.indexOf(SP3.education) !== -1).toBeTruthy(`General Education: expected ${SP3.education} instead of ${aEdu3}`);
    await expect(aExp5.indexOf(SP3.generalExperience) !== -1).toBeTruthy(`General Experience: expected ${SP3.generalExperience} instead of ${aExp5}`);
    await expect(aExp6.indexOf(SP3.managerialExperience) !== -1).toBeTruthy(`Managerial Experience: expected ${SP3.managerialExperience} instead of ${aExp6}`);

    await listPL.toggles.get(1).click();
    await listPO.removeProfileFromCompareView(1);
    await listPO.clearAllFilters();
  });

  it(`SPx4 Comparing`, async () => {
    await page.moveToSPList();
    await listPO.setMatrixView();
    await listPO.clearAllFiltersF();

    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP1.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP2.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP3.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP4.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clickCompareAllButton();

    await listPO.setComparison(1, 2); // resp
    await listPL.toggles.get(1).click();
    let aResp1 = await listPL.compareProfile1.get(1).getText();
    await expect(aResp1.indexOf(`(${SP1.resp1.name})`) !== -1).toBeTruthy(`Responsibilities: expected ${SP1.resp1.name} instead of ${aResp1}`);
    let aResp2 = await listPL.compareProfile2.get(1).getText();
    await expect(aResp2.indexOf(`(${SP2.resp1})`) !== -1).toBeTruthy(`Responsibilities: expected ${SP2.resp1} instead of ${aResp2}`);
    let aResp3 = await listPL.compareProfile3.get(1).getText();
    await expect(aResp3.indexOf(`(${SP3.resp1})`) !== -1).toBeTruthy(`Responsibilities: expected ${SP3.resp1} instead of ${aResp3}`);
    let aResp4 = await listPL.compareProfile4.get(1).getText();
    await expect(aResp4.indexOf(`(${SP4.resp1.name})`) !== -1).toBeTruthy(`Responsibilities: expected ${SP4.resp1.name} instead of ${aResp4}`);

    await listPO.setComparison(2, 1); // bc
    let aBC1 = await listPL.compareProfile1.get(1).getText();
    await expect(aBC1.indexOf(`(${SP1.BC1.name})`) !== -1).toBeTruthy(`BC: expected ${SP1.BC1.name} instead of ${aBC1}`);
    let aBC2 = await listPL.compareProfile2.get(1).getText();
    await expect(aBC2.indexOf(`(${SP2.BC1})`) !== -1).toBeTruthy(`BC: expected ${SP2.BC1} instead of ${aBC2}`);
    let aBC3 = await listPL.compareProfile3.get(1).getText();
    await expect(aBC3.indexOf(`(${SP3.BC1})`) !== -1).toBeTruthy(`BC: expected ${SP3.BC1} instead of ${aBC3}`);
    let aBC4 = await listPL.compareProfile4.get(1).getText();
    await expect(aBC4.indexOf(`(${SP4.BC1.name})`) !== -1).toBeTruthy(`BC: expected ${SP4.BC1.name} instead of ${aBC4}`);

    await listPO.setComparison(2, 2); // tc
    let aTC1 = await listPL.compareProfile1.get(1).getText();
    await expect(aTC1.indexOf(`(${SP1.TC1.name})`) !== -1).toBeTruthy(`TC: expected ${SP1.TC1.name} instead of ${aTC1}`);
    let aTC2 = await listPL.compareProfile2.get(1).getText();
    await expect(aTC2.indexOf(`(${SP2.TC1.name})`) !== -1).toBeTruthy(`TC: expected ${SP2.TC1.name} instead of ${aTC2}`);
    let aTC3 = await listPL.compareProfile3.get(1).getText();
    await expect(aTC3.indexOf(`(${SP3.TC1.name})`) !== -1).toBeTruthy(`TC: expected ${SP3.TC1.name} instead of ${aTC3}`);
    let aTC4 = await listPL.compareProfile4.get(1).getText();
    await expect(aTC4.indexOf(`(${SP4.TC1.name})`) !== -1).toBeTruthy(`TC: expected ${SP4.TC1.name} instead of ${aTC4}`);

    await listPO.setComparison(2, 3); // Edu n Exp
    let aEdu1 = await listPL.compareProfile1.get(1).getText();
    let aExp1 = await listPL.compareProfile1.get(4).getText();
    let aExp2 = await listPL.compareProfile1.get(7).getText();
    await expect(aEdu1.indexOf(`(${SP1.education.name})`) !== -1)
      .toBeTruthy(`Education: expected ${SP1.education.name} instead of ${aEdu1}`);
    await expect(aExp1.indexOf(`(${SP1.generalExperience.name})`) !== -1)
      .toBeTruthy(`Experience: expected ${SP1.generalExperience.name} instead of ${aExp1}`);
    await expect(aExp2.indexOf(`(${SP1.managerialExperience.name})`) !== -1)
      .toBeTruthy(`Experience: expected ${SP1.managerialExperience.name} instead of ${aExp2}`);
    let aEdu2 = await listPL.compareProfile2.get(1).getText();
    let aExp3 = await listPL.compareProfile2.get(4).getText();
    let aExp4 = await listPL.compareProfile2.get(7).getText();
    await expect(aEdu2.indexOf(`(${SP2.education})`) !== -1).toBeTruthy(`Education: expected ${SP2.education} instead of ${aEdu2}`);
    await expect(aExp3.indexOf(`(${SP2.generalExperience})`) !== -1).toBeTruthy(`Experience: expected ${SP2.generalExperience} instead of ${aExp3}`);
    await expect(aExp4.indexOf(`(${SP2.managerialExperience})`) !== -1).toBeTruthy(`Experience: expected ${SP2.managerialExperience} instead of ${aExp4}`);
    let aEdu3 = await listPL.compareProfile3.get(1).getText();
    let aExp5 = await listPL.compareProfile3.get(4).getText();
    let aExp6 = await listPL.compareProfile3.get(7).getText();
    await expect(aEdu3.indexOf(`(${SP3.education})`) !== -1).toBeTruthy(`Education: expected ${SP3.education} instead of ${aEdu3}`);
    await expect(aExp5.indexOf(`(${SP3.generalExperience})`) !== -1).toBeTruthy(`Experience: expected ${SP3.generalExperience} instead of ${aExp5}`);
    await expect(aExp6.indexOf(`(${SP3.managerialExperience})`) !== -1).toBeTruthy(`Experience: expected ${SP3.managerialExperience} instead of ${aExp6}`);
    let aEdu4 = await listPL.compareProfile4.get(1).getText();
    let aExp7 = await listPL.compareProfile4.get(4).getText();
    let aExp8 = await listPL.compareProfile4.get(7).getText();
    await expect(aEdu4.indexOf(`(${SP4.education.name})`) !== -1).toBeTruthy(`Education: expected ${SP4.education.name} instead of ${aEdu4}`);
    await expect(aExp7.indexOf(`(${SP4.generalExperience.name})`) !== -1).toBeTruthy(`Experience: expected ${SP4.generalExperience.name} instead of ${aExp7}`);
    await expect(aExp8.indexOf(`(${SP4.managerialExperience.name})`) !== -1).toBeTruthy(`Experience: expected ${SP4.managerialExperience.name} instead of ${aExp8}`);

    await listPL.toggles.get(1).click();
    await listPO.removeAllProfileFromCompareView();
    await listPO.clearAllFiltersF();
  });

  it(`Matrix view: SP edit, p1`, async () => {
    await page.moveToSPList();

    await listPO.setFilter(5, 1);
    await listPO.setFunctionFilter(4, SP1.subFunctionModified);
    await listPO.searchProfile(SP1.nameModified);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.deleteSPViaThreeDotsMenu();
      };
    });

    await listPO.setMatrixView();
    await listPO.clearAllFiltersF();

    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP1.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP2.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP3.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP4.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 3);
    await listPO.setFunctionFilter(1, subFuncValue5);
    await listPO.addToCompare(1);

    await listPL.compareListNames.get(0).click();
    await spinner.waitUntilSpinnerOff();


    await detailsPO.openEditPageViaActions();
    await landingPO.selectEditPageItem(1);
    await landingPO.editProfileTitle(SP1.nameModified);
    await landingPO.setProfileFunctionByName(SP1.functionModified);
    await landingPO.setProfileSubFunctionByName(SP1.subFunctionModified);
    await landingPO.clickNextButton();

    await landingPO.selectEditPageItem(2);
    await landingPO.editResponsibilityName(1, SP1.resp1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.confirmUpdate(2);
    await landingPO.editTask(1, 1, SP1.task1.nameModified);
    await landingPO.editTask(2, 1, SP1.task2.nameModified);
    await landingPO.editTask(3, 2, SP1.task3.nameModified);
    await landingPO.clickNextButton();

    await landingPO.selectEditPageItem(3);
    await landingPO.recommendationsDone();
    await landingPO.editBCompetencyName(1, SP1.BC1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SP1.TC1.nameModified);
    await landingPO.addNewSkillActions(1, SP1.TC1.skill1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SP1.tool1.nameModified)
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.editEducationName(1, SP1.education.nameModified);
    await landingPO.editExperienceName(1, SP1.generalExperience.nameModified);
    await landingPO.editExperienceName(2, SP1.managerialExperience.nameModified);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();
    await page.moveToSPList();
    await listPO.searchProfile(SP1.nameModified);

    await listPO.openSPFromListByNo(1);
    //details check
    let aTitle1 = await detailsPL.profileName.getText();
    await expect(aTitle1.indexOf(SP1.nameModified) !== -1)
      .toBeTruthy(`Title: expected ${SP1.nameModified} instead of ${aTitle1}`);
    const aFunc1M = await detailsPL.functionInfo.get(0).getText();
    await expect(aFunc1M.indexOf(SP1.functionModified) !== -1)
      .toBeTruthy(`Function: expected ${SP1.functionModified} instead of ${aFunc1M}`);
    const aSubFunc1M = await detailsPL.functionInfo.get(1).getText();
    await expect(aSubFunc1M.indexOf(SP1.subFunctionModified) !== -1)
      .toBeTruthy(`Sub function: expected ${SP1.subFunctionModified} instead of ${aSubFunc1M}`);
    const aResp1M = await detailsPL.detailResps.get(0).getText();
    await expect(aResp1M.indexOf(SP1.resp1.nameModified) !== -1)
      .toBeTruthy(`Responsibilities: expected ${SP1.resp1.nameModified} instead of ${aResp1M}`);
    let aTask1M = await detailsPL.detailTasks.get(0).getText();
    let aTask2M = await detailsPL.detailTasks.get(1).getText();
    let aTask3M = await detailsPL.detailTasks.get(2).getText();
    await expect(aTask1M.indexOf(SP1.task1.nameModified) !== -1).toBeTruthy(`Tasks: expected ${SP1.task1.nameModified} instead of ${aTask1M}`);
    await expect(aTask2M.indexOf(SP1.task2.nameModified) !== -1).toBeTruthy(`Tasks: expected ${SP1.task2.nameModified} instead of ${aTask2M}`);
    await expect(aTask3M.indexOf(SP1.task3.nameModified) !== -1).toBeTruthy(`Tasks: expected ${SP1.task3.nameModified} instead of ${aTask3M}`);
    let aBC1M = await detailsPL.detailsBCs.get(0).getText();
    await expect(aBC1M.indexOf(SP1.BC1.nameModified) !== -1)
      .toBeTruthy(`BC: expected ${SP1.BC1.nameModified} instead of ${aBC1M}`);
    let aTC1M = await detailsPL.detailsTCs.get(0).getText();
    await expect(aTC1M.indexOf(SP1.TC1.nameModified) !== -1)
      .toBeTruthy(`TC: expected ${SP1.TC1.nameModified} instead of ${aTC1M}`);
    let aSkill1M = await detailsPL.detailTSkills.get(0).getText();
    await expect(aSkill1M.indexOf(SP1.TC1.skill1.nameModified) !== -1)
      .toBeTruthy(`Skills: expected ${SP1.TC1.skill1.nameModified} instead of ${aSkill1M}`);
    let aTool1M = await detailsPL.detailTools.get(0).getText();
    await expect(aTool1M.indexOf(SP1.tool1.nameModified) !== -1)
      .toBeTruthy(`Tools: expected ${SP1.tool1.nameModified} instead of ${aTool1M}`);
    let aEdu1M = await detailsPL.detailEducation.getText();
    let aExp1M = await detailsPL.detailExperinces.get(0).getText();
    let aExp2M = await detailsPL.detailExperinces.get(1).getText();
    await expect(aEdu1M.indexOf(SP1.education.nameModified) !== -1)
      .toBeTruthy(`Education: expected ${SP1.education.nameModified} instead of ${aEdu1M}`);
    await expect(aExp1M.indexOf(SP1.generalExperience.nameModified) !== -1)
      .toBeTruthy(`Experience: expected ${SP1.generalExperience.nameModified} instead of ${aExp1M}`);
    await expect(aExp2M.indexOf(SP1.managerialExperience.nameModified) !== -1)
      .toBeTruthy(`Experience: expected ${SP1.managerialExperience.nameModified} instead of ${aExp2M}`);

    await page.moveToSPList();
    await listPO.setMatrixView();
    await listPO.removeAllFromCompareList();
    await listPO.clearAllFiltersF();
  });

  it(`Matrix view: SP edit, p2`, async () => {
    await page.moveToSPList();

    await listPO.setFilter(5, 1);
    await listPO.setFunctionFilter(4, SP4.subFunctionModified);
    await listPO.searchProfile(SP4.nameModified);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.deleteSPViaThreeDotsMenu();
      };
    });
    await listPO.clearAllFilters();
    await listPO.setMatrixView();
    await listPO.clearAllFiltersF();

    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP1.subFunctionModified);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP2.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP3.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP4.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 3);
    await listPO.setFunctionFilter(1, subFuncValue5);
    await listPO.addToCompare(1);

    await listPL.compareListNames.get(3).click();
    await spinner.waitUntilSpinnerOff();


    await detailsPO.openEditPageViaActions();
    await landingPO.selectEditPageItem(1);
    await landingPO.editProfileTitle(SP4.nameModified);
    await landingPO.setProfileFunctionByName(SP4.functionModified);
    await landingPO.setProfileSubFunctionByName(SP4.subFunctionModified);
    await landingPO.clickNextButton();

    await landingPO.selectEditPageItem(2);
    await landingPO.editResponsibilityName(1, SP4.resp1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.confirmUpdate(2);
    await landingPO.editTask(1, 1, SP4.task1.nameModified);
    await landingPO.editTask(2, 1, SP4.task2.nameModified);
    await landingPO.editTask(3, 2, SP4.task3.nameModified);
    await landingPO.clickNextButton();

    await landingPO.selectEditPageItem(3);
    await landingPO.recommendationsDone();
    await landingPO.editBCompetencyName(1, SP4.BC1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SP4.TC1.nameModified);
    await landingPO.addNewSkillActions(1, SP4.TC1.skill1.nameModified);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SP4.tool1.nameModified)
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.editEducationName(1, SP4.education.nameModified);
    await landingPO.editExperienceName(1, SP4.generalExperience.nameModified);
    await landingPO.editExperienceName(2, SP4.managerialExperience.nameModified);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();
    await page.moveToSPList();
    await listPO.setFilter(5, 1);
    await listPO.searchProfile(SP4.nameModified);
    await listPO.openSPFromListByName(SP4.nameModified);
    //details check
    let aTitle4 = await detailsPL.profileName.getText();
    await expect(aTitle4.indexOf(SP4.nameModified) !== -1)
      .toBeTruthy(`Title: expected ${SP4.nameModified} instead of ${aTitle4}`);
    const aFunc4M = await detailsPL.functionInfo.get(0).getText();
    await expect(aFunc4M.indexOf(SP4.functionModified) !== -1)
      .toBeTruthy(`Function: expected ${SP4.functionModified} instead of ${aFunc4M}`);
    const aSubFunc4M = await detailsPL.functionInfo.get(1).getText();
    await expect(aSubFunc4M.indexOf(SP4.subFunctionModified) !== -1)
      .toBeTruthy(`Sub function: expected ${SP4.subFunctionModified} instead of ${aSubFunc4M}`);
    const aResp4M = await detailsPL.detailResps.get(0).getText();
    await expect(aResp4M.indexOf(SP4.resp1.nameModified) !== -1)
      .toBeTruthy(`Responsibilities: expected ${SP4.resp1.nameModified} instead of ${aResp4M}`);
    let aTask10M = await detailsPL.detailTasks.get(0).getText();
    let aTask11M = await detailsPL.detailTasks.get(1).getText();
    let aTask12M = await detailsPL.detailTasks.get(2).getText();
    await expect(aTask10M.indexOf(SP4.task1.nameModified) !== -1)
      .toBeTruthy(`Tasks: expected ${SP4.task1.nameModified} instead of ${aTask10M}`);
    await expect(aTask11M.indexOf(SP4.task2.nameModified) !== -1)
      .toBeTruthy(`Tasks: expected ${SP4.task2.nameModified} instead of ${aTask11M}`);
    await expect(aTask12M.indexOf(SP4.task3.nameModified) !== -1)
      .toBeTruthy(`Tasks: expected ${SP4.task3.nameModified} instead of ${aTask12M}`);
    let aBC4M = await detailsPL.detailsBCs.get(0).getText();
    await expect(aBC4M.indexOf(SP4.BC1.nameModified) !== -1)
      .toBeTruthy(`BC: expected ${SP4.BC1.nameModified} instead of ${aBC4M}`);
    let aTC4M = await detailsPL.detailsTCs.get(0).getText();
    await expect(aTC4M.indexOf(SP4.TC1.nameModified) !== -1)
      .toBeTruthy(`TC: expected ${SP4.TC1.nameModified} instead of ${aTC4M}`);
    let aSkill4M = await detailsPL.detailTSkills.get(0).getText();
    await expect(aSkill4M.indexOf(SP4.TC1.skill1.nameModified) !== -1)
      .toBeTruthy(`Skills: expected ${SP4.TC1.skill1.nameModified} instead of ${aSkill4M}`);
    let aTool4M = await detailsPL.detailTools.get(0).getText();
    await expect(aTool4M.indexOf(SP4.tool1.nameModified) !== -1)
      .toBeTruthy(`Tools: expected ${SP4.tool1.nameModified} instead of ${aTool4M}`);
    let aEdu4M = await detailsPL.detailEducation.getText();
    let aExp7M = await detailsPL.detailExperinces.get(0).getText();
    let aExp8M = await detailsPL.detailExperinces.get(1).getText();
    await expect(aEdu4M.indexOf(SP4.education.nameModified) !== -1)
      .toBeTruthy(`Education: expected ${SP4.education.nameModified} instead of ${aEdu4M}`);
    await expect(aExp7M.indexOf(SP4.generalExperience.nameModified) !== -1)
      .toBeTruthy(`Experience: expected ${SP4.generalExperience.nameModified} instead of ${aExp7M}`);
    await expect(aExp8M.indexOf(SP4.managerialExperience.nameModified) !== -1)
      .toBeTruthy(`Experience: expected ${SP4.managerialExperience.nameModified} instead of ${aExp8M}`);

    await page.moveToSPList();
    await listPO.setMatrixView();
    await listPO.removeAllFromCompareList();
    await listPO.clearAllFiltersF();
  });

  it(`Matrix view: copy SP via PMT`, async () => {
    await page.moveToSPList();

    await listPO.setFilter(5, 1);
    await listPO.setFunctionFilter(4, SP5.subFunction);
    await listPO.searchProfile(SP5.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.deleteSPViaThreeDotsMenu();
      };
    });
    await listPO.clearAllFilters();
    await listPO.setMatrixView();
    await listPO.clearAllFiltersF();

    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP1.subFunctionModified);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP2.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP3.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP4.subFunctionModified);
    await listPO.addToCompareCustom(1);

    await listPO.clearAllFilters();
    await listPO.setFilter(2, 3);
    await listPO.setFunctionFilter(1, subFuncValue5);
    await listPO.addToCompare(1);

    await browser.wait(EC.elementToBeClickable(listPL.compareListNames.get(4)), browser.params.cooldown.medium, `There is no SP Name(4)`);
    await listPL.compareListNames.get(4).click();
    await spinner.waitUntilSpinnerOff();

    await detailsPO.openEditPageViaActions();

    await expect(EC.not(EC.elementToBeClickable(landingPL.saveButton))).toBeTruthy(`There is Save button. Shouldn't`);

    await landingPO.selectEditPageItem(1);
    await landingPO.editProfileTitle(SP5.name);
    await landingPO.setProfileFunctionByName(SP5.function);
    await landingPO.setProfileSubFunctionByName(SP5.subFunction);
    await landingPO.clickNextButton();

    await landingPO.selectEditPageItem(2);
    await landingPO.deleteAllResponsibilities();
    await landingPO.addResponsibility();
    await landingPO.editResponsibilityName(1, SP5.resp1);
    await landingPO.clickNextButton();
    await landingPO.confirmUpdate(2);
    await landingPO.deleteAllTasks();
    await landingPO.addTask();
    await landingPO.editTask(1, 1, SP5.task1);
    await landingPO.addTask();
    await landingPO.editTask(2, 1, SP5.task2);
    await landingPO.addTask();
    await landingPO.editTask(3, 2, SP5.task3);
    await landingPO.clickNextButton();

    await landingPO.saveProfileAs();
    await detailsPO.openEditPageViaActions();

    await landingPO.selectEditPageItem(3);
    await landingPO.recommendationsDone();
    await landingPO.deleteAllBCompetencies();
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(1, SP5.BC1);
    await landingPO.clickNextButton();
    await landingPO.recommendationsDone();
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, SP5.TC1.name);
    await landingPO.addNewSkillActions(1, SP5.TC1.skill1);
    await landingPO.clickNextButton();
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, SP5.tool1);
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickNextButton();
    await landingPO.deleteAllEduNExp();
    await landingPO.addAllEducation();
    await landingPO.editEducationName(1, SP5.education);
    await landingPO.addAllExperience();
    await landingPO.editExperienceName(1, SP5.generalExperience);
    await landingPO.editExperienceName(2, SP5.managerialExperience);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();
    await page.moveToSPList();
    await listPO.setFunctionFilter(4, SP5.subFunction);
    await listPO.searchProfile(SP5.name);

    await listPO.openSPFromListByNo(1);
    //details check
    let aTitle5M = await detailsPL.profileName.getText();
    await expect(aTitle5M.indexOf(SP5.name) !== -1).toBeTruthy(`Title: expected ${SP5.name} instead of ${aTitle5M}`);
    const aFunc5M = await detailsPL.functionInfo.get(0).getText();
    await expect(aFunc5M.indexOf(SP5.function) !== -1).toBeTruthy(`Function: expected ${SP5.function} instead of ${aFunc5M}`);
    const aSubFunc5M = await detailsPL.functionInfo.get(1).getText();
    await expect(aSubFunc5M.indexOf(SP5.subFunction) !== -1).toBeTruthy(`Sub function: expected ${SP5.subFunction} instead of ${aSubFunc5M}`);
    const aResp5M = await detailsPL.detailResps.get(0).getText();
    await expect(aResp5M.indexOf(SP5.resp1) !== -1).toBeTruthy(`Responsibilities: expected ${SP5.resp1} instead of ${aResp5M}`);
    let aTask13M = await detailsPL.detailTasks.get(0).getText();
    let aTask14M = await detailsPL.detailTasks.get(1).getText();
    let aTask15M = await detailsPL.detailTasks.get(2).getText();
    await expect(aTask13M.indexOf(SP5.task1) !== -1).toBeTruthy(`Tasks: expected ${SP5.task1} instead of ${aTask13M}`);
    await expect(aTask14M.indexOf(SP5.task2) !== -1).toBeTruthy(`Tasks: expected ${SP5.task2} instead of ${aTask14M}`);
    await expect(aTask15M.indexOf(SP5.task3) !== -1).toBeTruthy(`Tasks: expected ${SP5.task3} instead of ${aTask15M}`);
    let aBC5M = await detailsPL.detailsBCs.get(0).getText();
    await expect(aBC5M.indexOf(SP5.BC1) !== -1).toBeTruthy(`BC: expected ${SP5.BC1} instead of ${aBC5M}`);
    let aTC5M = await detailsPL.detailsTCs.get(0).getText();
    await expect(aTC5M.indexOf(SP5.TC1.name) !== -1).toBeTruthy(`TC: expected ${SP5.TC1.name} instead of ${aTC5M}`);
    let aSkill5M = await detailsPL.detailTSkills.get(0).getText();
    await expect(aSkill5M.indexOf(SP5.TC1.skill1) !== -1).toBeTruthy(`Skills: expected ${SP5.TC1.skill1} instead of ${aSkill5M}`);
    let aTool5M = await detailsPL.detailTools.get(0).getText();
    await expect(aTool5M.indexOf(SP5.tool1) !== -1).toBeTruthy(`Tools: expected ${SP5.tool1} instead of ${aTool5M}`);
    let aEdu5M = await detailsPL.detailEducation.getText();
    let aExp9M = await detailsPL.detailExperinces.get(0).getText();
    let aExp10M = await detailsPL.detailExperinces.get(1).getText();
    await expect(aEdu5M.indexOf(SP5.education) !== -1)
      .toBeTruthy(`Education: expected ${SP5.managerialExperience} instead of ${aEdu5M}`);
    await expect(aExp9M.indexOf(SP5.generalExperience) !== -1)
      .toBeTruthy(`Experience: expected ${SP5.managerialExperience} instead of ${aExp9M}`);
    await expect(aExp10M.indexOf(SP5.managerialExperience) !== -1)
      .toBeTruthy(`Experience: expected ${SP5.managerialExperience} instead of ${aExp10M}`);

    await page.moveToSPList();
    await listPO.setMatrixView();
    await listPO.removeAllFromCompareList();
    await listPO.clearAllFiltersF();
  });

  it(`Compare view: SP edit, custom`, async () => {
    await page.moveToSPList();

    await listPO.setFunctionFilter(4, SP6.subFunction);
    await listPO.searchProfile(SP6.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.deleteSPViaThreeDotsMenu();
      };
    });
    await listPO.clearSearch();
    await listPO.clearAllFilters();

    await listPO.setFilter(5, 3);
    await listPO.setFunctionFilter(4, SP6.subFunction);
    await listPO.copyProfileViaThreeDotsMenu();
    await detailsPO.openEditPageViaActions();
    await landingPO.selectEditPageItem(1);
    await landingPO.editProfileTitle(SP6.name);
    await landingPO.setProfileFunctionByName(SP6.function);
    await landingPO.setProfileSubFunctionByName(SP6.subFunction);
    await landingPO.clickNextButton();
    await landingPO.saveProfile();

    await page.moveToSPList();
    await listPO.setMatrixView();
    await listPO.clearAllFiltersF();

    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP6.subFunction);
    await listPO.addToCompareCustom(1);

    await listPO.clickCompareAllButton();

    await listPL.compareEditButton.click();
    await spinner.waitUntilSpinnerOff();

    // Resps
    await listPO.setComparisonEditPage(1, 2);
    await listPO.compareEditRemoveAllRespOComp();

    await listPO.compareEditAddRespOComp(4);
    await listPO.compareEditRespOComp(1, 1, SP6.resp1.primary, SP6.resp1.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP6.resp2.primary, SP6.resp3.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP6.resp3.primary, SP6.resp3.levelInc);
    await listPO.compareEditRespOComp(1, 4, false, 6);
    SP6.resp1.name = await listPL.compareEditRespOCompNames.get(0).getText();
    SP6.resp2.name = await listPL.compareEditRespOCompNames.get(1).getText();
    SP6.resp3.name = await listPL.compareEditRespOCompNames.get(2).getText();
    SP6.resp1.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP6.resp2.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP6.resp3.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.compareEditClickRemoveRespOComp(1, 4);

    // BCs
    await listPO.setComparisonEditPage(2, 1);
    await listPO.compareEditRemoveAllRespOComp();

    await listPO.compareEditAddRespOComp(4);
    await listPO.compareEditRespOComp(1, 1, SP6.BC1.primary, SP6.BC1.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP6.BC2.primary, SP6.BC2.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP6.BC3.primary, SP6.BC3.levelInc);
    await listPO.compareEditRespOComp(1, 4, false, 8);
    SP6.BC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP6.BC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP6.BC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP6.BC1.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP6.BC2.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP6.BC3.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.compareEditClickRemoveRespOComp(1, 4);

    // TCs
    await listPO.setComparisonEditPage(2, 2);
    await listPO.compareEditRemoveAllRespOComp();

    await listPO.compareEditAddRespOComp(4);
    await listPO.compareEditRespOComp(1, 1, SP6.TC1.primary, SP6.TC1.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP6.TC2.primary, SP6.TC2.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP6.TC3.primary, SP6.TC3.levelInc);
    await listPO.compareEditRespOComp(1, 4, false, 3);
    SP6.TC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP6.TC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP6.TC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP6.TC1.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP6.TC2.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP6.TC3.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.compareEditClickRemoveRespOComp(1, 4);

    // Education n Experience
    await listPO.setComparisonEditPage(2, 3);
    await listPO.compareEditRespOComp(1, 1, SP6.education.primary, SP6.education.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP6.generalExperience.primary, SP6.generalExperience.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP6.managerialExperience.primary, SP6.managerialExperience.levelInc);
    SP6.education.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP6.generalExperience.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP6.managerialExperience.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP6.education.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP6.generalExperience.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP6.managerialExperience.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.clickSaveCompareView();
    await listPO.clickContinueNSave();

    // check data on compare view
    await listPO.setComparison(1, 2) //resps
    await expect(await listPL.compareLeftColumn.get(0).getText() == SP6.resp1.name)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP6.resp2.name)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP6.resp3.name)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile1.get(0).getText() == SP6.resp1.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp1.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP6.resp2.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp2.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP6.resp3.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp3.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await listPO.setComparison(2, 1) //BC
    await expect(await listPL.compareLeftColumn.get(0).getText() == SP6.BC1.name)
      .toBeTruthy(`BC: expected '${SP6.BC1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP6.BC2.name)
      .toBeTruthy(`BC: expected '${SP6.BC2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP6.BC3.name)
      .toBeTruthy(`BC: expected '${SP6.BC3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile1.get(0).getText() == SP6.BC1.levelText)
      .toBeTruthy(`BC: expected 'Level ${SP6.BC1.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP6.BC2.levelText)
      .toBeTruthy(`BC: expected 'Level ${SP6.BC2.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP6.BC3.levelText)
      .toBeTruthy(`BC: expected 'Level ${SP6.BC3.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await listPO.setComparison(2, 2) //TC
    await expect(await listPL.compareLeftColumn.get(0).getText() == SP6.TC1.name)
      .toBeTruthy(`TC: expected '${SP6.TC1.name}' instead of ${await listPL.compareLeftColumn.get(0).getText()}`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP6.TC2.name)
      .toBeTruthy(`TC: expected '${SP6.TC2.name}' instead of ${await listPL.compareLeftColumn.get(1).getText()}`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP6.TC3.name)
      .toBeTruthy(`TC: expected '${SP6.TC3.name}' instead of ${await listPL.compareLeftColumn.get(2).getText()}`);
    await expect(await listPL.compareProfile1.get(0).getText() == SP6.TC1.levelText)
      .toBeTruthy(`TC: expected 'Level ${SP6.TC1.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP6.TC2.levelText)
      .toBeTruthy(`TC: expected 'Level ${SP6.TC2.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP6.TC3.levelText)
      .toBeTruthy(`TC: expected 'Level ${SP6.TC3.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await listPO.setComparison(2, 3) //Edu n exp
    await expect(await listPL.compareProfile1.get(0).getText() == SP6.education.levelText)
      .toBeTruthy(`General Education: expected '${SP6.education.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP6.generalExperience.levelText)
      .toBeTruthy(`General Experience: expected '${SP6.generalExperience.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP6.managerialExperience.levelText)
      .toBeTruthy(`Managerial Experience: expected '${SP6.managerialExperience.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await listPO.removeAllProfileFromCompareView();
    await page.moveToSPList();
    await listPO.clearAllFilters();
    await listPO.setFilter(5, 1);
    await listPO.setFunctionFilter(4, SP6.subFunction);
    await listPO.searchProfile(SP6.name);
    await listPO.openSPFromListByName(SP6.name);

    await browser.wait(EC.presenceOf(detailsPL.detailResps.get(0)), browser.params.cooldown.medium, `There is no Responsibilities on details page`);
    let aResp1 = await detailsPL.detailResps.get(0).getText();
    await expect(SP6.resp1.name.toLowerCase() == aResp1.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP6.resp1.name}' instead of '${await detailsPL.detailResps.get(0).getText()}'`);
    let aResp2 = await detailsPL.detailResps.get(1).getText();
    await expect(SP6.resp2.name.toLowerCase() == aResp2.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP6.resp2.name}' instead of '${await detailsPL.detailResps.get(1).getText()}'`);
    let aResp3 = await detailsPL.detailResps.get(2).getText();
    await expect(SP6.resp3.name.toLowerCase() == aResp3.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP6.resp3.name}' instead of '${await detailsPL.detailResps.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsBCs.get(0)), browser.params.cooldown.medium, `There is no BC on details page`);
    let aBC1 = await detailsPL.detailsBCs.get(0).getText();
    console.log(aBC1.replace(`...`, ``).toLowerCase(), ` OOO `, SP6.BC1.name.toLowerCase());
    await expect(aBC1.replace(`...`, ``).toLowerCase().indexOf(SP6.BC1.name.toLowerCase()) !== -1)
      .toBeTruthy(`BC: expected '${SP6.BC1.name}' instead of '${await detailsPL.detailsBCs.get(0).getText()}'`);
    let aBC2 = await detailsPL.detailsBCs.get(1).getText();
    await expect(aBC2.replace(`...`, ``).toLowerCase().indexOf(SP6.BC2.name.toLowerCase()) !== -1)
      .toBeTruthy(`BC: expected '${SP6.BC2.name}' instead of '${await detailsPL.detailsBCs.get(1).getText()}'`);
    let aBC3 = await detailsPL.detailsBCs.get(2).getText();
    await expect(aBC3.replace(`...`, ``).toLowerCase().indexOf(SP6.BC3.name.toLowerCase()) !== -1)
      .toBeTruthy(`BC: expected '${SP6.BC3.name}' instead of '${await detailsPL.detailsBCs.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsTCs.get(0)), browser.params.cooldown.medium, `There is no TC on details page`);
    let aTC1 = await detailsPL.detailsTCs.get(0).getText();
    await expect(SP6.TC1.name.toLowerCase() == aTC1.toLowerCase())
      .toBeTruthy(`TC: expected '${SP6.TC1.name}' instead of '${await detailsPL.detailsTCs.get(0).getText()}'`);
    let aTC2 = await detailsPL.detailsTCs.get(1).getText();
    await expect(SP6.TC2.name.toLowerCase() == aTC2.toLowerCase())
      .toBeTruthy(`TC: expected '${SP6.TC2.name}' instead of '${await detailsPL.detailsTCs.get(1).getText()}'`);
    let aTC3 = await detailsPL.detailsTCs.get(2).getText();
    await expect(SP6.TC3.name.toLowerCase() == aTC3.toLowerCase())
      .toBeTruthy(`TC: expected '${SP6.TC3.name}' instead of '${await detailsPL.detailsTCs.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailEducation), browser.params.cooldown.medium, `There is no Education info on details page`);
    let aEdu1 = await detailsPL.detailEducation.getText();
    await expect(SP6.education.name.toLowerCase() == aEdu1.toLowerCase())
      .toBeTruthy(`Education: expected '${SP6.education.name}' instead of '${await detailsPL.detailEducation.getText()}'`);
    let aExp1 = await detailsPL.detailExperinces.get(0).getText();
    await expect(SP6.generalExperience.name.toLowerCase() == aExp1.toLowerCase())
      .toBeTruthy(`G Experience: expected '${SP6.generalExperience.name}' instead of '${await detailsPL.detailExperinces.get(0).getText()}'`);
    let aExp2 = await detailsPL.detailExperinces.get(1).getText();
    await expect(SP6.managerialExperience.name.toLowerCase() == aExp2.toLowerCase())
      .toBeTruthy(`M Experience: expected '${SP6.managerialExperience.name}' instead of '${await detailsPL.detailExperinces.get(1).getText()}'`);

    await page.moveToSPList();
    await listPO.clearAllFiltersF();
    await listPO.clearSearch();
  });

  it(`Compare view: SP edit, copy to edit`, async () => {
    await page.moveToSPList();

    await listPO.setFilter(5, 1);
    await listPO.setFunctionFilter(4, SP7.subFunction);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.deleteSPViaThreeDotsMenu();
      };
    });
    await listPO.clearSearch();
    await listPO.clearAllFilters();

    await listPO.setMatrixView();
    await listPO.clearAllFiltersF();

    await listPO.setFilter(2, 3);
    await listPO.setFunctionFilter(1, SP7.subFunction);
    await listPO.addToCompare(1);

    await listPO.clickCompareAllButton();
    await listPO.clickCompareEditButton();

    await listPO.clickCopyToEditLink(1);
    await listPO.setCopyName(SP7.name);
    await listPO.clickCreateCopy();

    // Resps
    await listPO.setComparisonEditPage(1, 2);
    await listPO.dismissAllRecommendations();
    await listPO.compareEditRemoveAllRespOComp();

    await listPO.compareEditAddRespOComp(4);
    await listPO.compareEditRespOComp(1, 1, SP7.resp1.primary, SP7.resp1.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP7.resp2.primary, SP7.resp3.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP7.resp3.primary, SP7.resp3.levelInc);
    await listPO.compareEditRespOComp(1, 4, false, 1);
    SP7.resp1.name = await listPL.compareEditRespOCompNames.get(0).getText();
    SP7.resp2.name = await listPL.compareEditRespOCompNames.get(1).getText();
    SP7.resp3.name = await listPL.compareEditRespOCompNames.get(2).getText();
    SP7.resp1.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP7.resp2.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP7.resp3.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.compareEditClickRemoveRespOComp(1, 4);

    // BCs
    await listPO.setComparisonEditPage(2, 1);
    await listPO.dismissAllRecommendations();
    await listPO.compareEditRemoveAllRespOComp();

    await listPO.compareEditAddRespOComp(4);
    await listPO.compareEditRespOComp(1, 1, SP7.BC1.primary, SP7.BC1.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP7.BC2.primary, SP7.BC2.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP7.BC3.primary, SP7.BC3.levelInc);
    await listPO.compareEditRespOComp(1, 4, false, 1);
    SP7.BC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP7.BC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP7.BC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP7.BC1.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP7.BC2.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP7.BC3.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.compareEditClickRemoveRespOComp(1, 4);

    // TCs
    await listPO.setComparisonEditPage(2, 2);
    await listPO.dismissAllRecommendations();
    await listPO.compareEditRemoveAllRespOComp();

    await listPO.compareEditAddRespOComp(4);
    await listPO.compareEditRespOComp(1, 1, SP7.TC1.primary, SP7.TC1.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP7.TC2.primary, SP7.TC2.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP7.TC3.primary, SP7.TC3.levelInc);
    await listPO.compareEditRespOComp(1, 4, false, 3);
    SP7.TC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP7.TC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP7.TC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP7.TC1.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP7.TC2.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP7.TC3.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.compareEditClickRemoveRespOComp(1, 4);

    // Education n Experience
    await listPO.setComparisonEditPage(2, 3);
    await listPO.dismissAllRecommendations();
    await listPO.compareEditRespOComp(1, 1, SP7.education.primary, SP7.education.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP7.generalExperience.primary, SP7.generalExperience.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP7.managerialExperience.primary, SP7.managerialExperience.levelInc);
    SP7.education.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP7.generalExperience.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP7.managerialExperience.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP7.education.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP7.generalExperience.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP7.managerialExperience.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.clickSaveCompareView();
    await listPO.clickContinueNSave();

    // check data on compare view
    await listPO.setComparison(1, 2) //resps
    await expect(await listPL.compareLeftColumn.get(0).getText() == SP7.resp1.name)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP7.resp2.name)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP7.resp3.name)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile1.get(0).getText() == SP7.resp1.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp1.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP7.resp2.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp2.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP7.resp3.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp3.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await listPO.setComparison(2, 1) //BC
    await expect(await listPL.compareLeftColumn.get(0).getText() == SP7.BC1.name)
      .toBeTruthy(`BC: expected '${SP7.BC1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP7.BC2.name)
      .toBeTruthy(`BC: expected '${SP7.BC2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP7.BC3.name)
      .toBeTruthy(`BC: expected '${SP7.BC3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile1.get(0).getText() == SP7.BC1.levelText)
      .toBeTruthy(`BC: expected 'Level ${SP7.BC1.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP7.BC2.levelText)
      .toBeTruthy(`BC: expected 'Level ${SP7.BC2.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP7.BC3.levelText)
      .toBeTruthy(`BC: expected 'Level ${SP7.BC3.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await listPO.setComparison(2, 2) //TC
    await expect(await listPL.compareLeftColumn.get(0).getText() == SP7.TC1.name)
      .toBeTruthy(`TC: expected '${SP7.TC1.name}' instead of ${await listPL.compareLeftColumn.get(0).getText()}`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP7.TC2.name)
      .toBeTruthy(`TC: expected '${SP7.TC2.name}' instead of ${await listPL.compareLeftColumn.get(1).getText()}`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP7.TC3.name)
      .toBeTruthy(`TC: expected '${SP7.TC3.name}' instead of ${await listPL.compareLeftColumn.get(2).getText()}`);
    await expect(await listPL.compareProfile1.get(0).getText() == SP7.TC1.levelText)
      .toBeTruthy(`TC: expected 'Level ${SP7.TC1.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP7.TC2.levelText)
      .toBeTruthy(`TC: expected 'Level ${SP7.TC2.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP7.TC3.levelText)
      .toBeTruthy(`TC: expected 'Level ${SP7.TC3.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await listPO.setComparison(2, 3) //Edu n exp
    await expect(await listPL.compareProfile1.get(0).getText() == SP7.education.levelText)
      .toBeTruthy(`General Education: expected '${SP7.education.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP7.generalExperience.levelText)
      .toBeTruthy(`General Experience: expected '${SP7.generalExperience.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP7.managerialExperience.levelText)
      .toBeTruthy(`Managerial Experience: expected '${SP7.managerialExperience.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await listPO.removeAllProfileFromCompareView();
    await page.moveToSPList();
    await listPO.setFunctionFilter(4, SP7.subFunction);
    await listPO.searchProfile(SP7.name);
    await listPO.openSPFromListByNo(1);

    await browser.wait(EC.presenceOf(detailsPL.detailResps.get(0)), browser.params.cooldown.medium, `There is no Responsibilities on details page`);
    let aResp1 = await detailsPL.detailResps.get(0).getText();
    await expect(SP7.resp1.name.toLowerCase() == aResp1.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP7.resp1.name}' instead of '${await detailsPL.detailResps.get(0).getText()}'`);
    let aResp2 = await detailsPL.detailResps.get(1).getText();
    await expect(SP7.resp2.name.toLowerCase() == aResp2.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP7.resp2.name}' instead of '${await detailsPL.detailResps.get(1).getText()}'`);
    let aResp3 = await detailsPL.detailResps.get(2).getText();
    await expect(SP7.resp3.name.toLowerCase() == aResp3.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP7.resp3.name}' instead of '${await detailsPL.detailResps.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsBCs.get(0)), browser.params.cooldown.medium, `There is no BC on details page`);
    let aBC1 = await detailsPL.detailsBCs.get(0).getText();
    await expect(aBC1.toLowerCase().toLowerCase().replace(`...`, ``).indexOf(SP7.BC1.name.trim().toLowerCase()) !== -1)
      .toBeTruthy(`BC: expected '${SP7.BC1.name}' instead of '${aBC1.replace(`...`, ``)}'`);
    let aBC2 = await detailsPL.detailsBCs.get(1).getText();
    await expect(aBC2.toLowerCase().toLowerCase().replace(`...`, ``).indexOf(SP7.BC2.name.trim().toLowerCase()) !== -1)
      .toBeTruthy(`BC: expected '${SP7.BC2.name}' instead of '${aBC2.replace(`...`, ``)}'`);
    let aBC3 = await detailsPL.detailsBCs.get(2).getText();
    await expect(aBC3.toLowerCase().toLowerCase().replace(`...`, ``).indexOf(SP7.BC3.name.trim().toLowerCase()) !== -1)
      .toBeTruthy(`BC: expected '${SP7.BC3.name}' instead of '${aBC3.replace(`...`, ``)}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsTCs.get(0)), browser.params.cooldown.medium, `There is no TC on details page`);
    let aTC1 = await detailsPL.detailsTCs.get(0).getText();
    await expect(SP7.TC1.name.replace(`...`, ``).toLowerCase().indexOf(aTC1.replace(`...`, ``).toLowerCase()) !== -1 || aTC1.replace(`...`, ``).toLowerCase().indexOf(SP7.TC1.name.replace(`...`, ``).toLowerCase()) !== -1)
      .toBeTruthy(`TC: expected '${SP7.TC1.name}' instead of '${await detailsPL.detailsTCs.get(0).getText()}'`);
    let aTC2 = await detailsPL.detailsTCs.get(1).getText();
    await expect(SP7.TC2.name.replace(`...`, ``).toLowerCase().indexOf(aTC2.replace(`...`, ``).toLowerCase()) !== -1 || aTC2.replace(`...`, ``).toLowerCase().indexOf(SP7.TC2.name.replace(`...`, ``).toLowerCase()) !== -1)
      .toBeTruthy(`TC: expected '${SP7.TC2.name}' instead of '${await detailsPL.detailsTCs.get(1).getText()}'`);
    let aTC3 = await detailsPL.detailsTCs.get(2).getText();
    await expect(SP7.TC3.name.replace(`...`, ``).toLowerCase().indexOf(aTC3.replace(`...`, ``).toLowerCase()) !== -1 || aTC3.replace(`...`, ``).toLowerCase().indexOf(SP7.TC3.name.replace(`...`, ``).toLowerCase()) !== -1)
      .toBeTruthy(`TC: expected '${SP7.TC3.name}' instead of '${await detailsPL.detailsTCs.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailEducation), browser.params.cooldown.medium, `There is no Education info on details page`);
    let aEdu1 = await detailsPL.detailEducation.getText();
    await expect(SP7.education.name.toLowerCase() == aEdu1.toLowerCase())
      .toBeTruthy(`Education: expected '${SP7.education.name}' instead of '${await detailsPL.detailEducation.getText()}'`);
    let aExp1 = await detailsPL.detailExperinces.get(0).getText();
    await expect(SP7.generalExperience.name.toLowerCase() == aExp1.toLowerCase())
      .toBeTruthy(`G Experience: expected '${SP7.generalExperience.name}' instead of '${await detailsPL.detailExperinces.get(0).getText()}'`);
    let aExp2 = await detailsPL.detailExperinces.get(1).getText();
    await expect(SP7.managerialExperience.name.toLowerCase() == aExp2.toLowerCase())
      .toBeTruthy(`M Experience: expected '${SP7.managerialExperience.name}' instead of '${await detailsPL.detailExperinces.get(1).getText()}'`);

    await page.moveToSPList();
    await listPO.clearAllFilters();
    await listPO.clearSearch();
  });

  it(`Compare view: bulk SP edit`, async () => {
    await page.moveToSPList();

    await listPO.setFilter(5, 1);
    await listPO.setFunctionFilter(4, SP6.subFunction);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.deleteSPViaThreeDotsMenu();
      };
    });
    await listPO.clearAllFilters();

    await listPO.setFilter(5, 3);
    await listPO.setFunctionFilter(4, SP6.subFunction);
    await listPO.copyProfileViaThreeDotsMenu(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(SP6.name);
    await landingPO.saveProfile();
    await page.moveToSPList();
    await listPO.clearAllFilters();

    await listPO.setFilter(5, 3);
    await listPO.setFunctionFilter(4, SP7.subFunction);
    await listPO.copyProfileViaThreeDotsMenu(2);
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(SP7.name);
    await landingPO.saveProfile();
    await page.moveToSPList();
    await listPO.clearAllFilters();

    await listPO.setMatrixView();
    await listPO.clearAllFiltersF();

    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SP6.subFunction);
    await listPO.addToCompareCustom(1);
    await listPO.addToCompareCustom(2);
    await listPO.clearAllFilters();

    await listPO.setFilter(2, 3);
    await listPO.setFunctionFilter(1, SP6.subFunction);
    await listPO.addToCompare(1);
    await listPO.addToCompare(2);

    await listPO.clickCompareAllButton();
    await listPO.clickCompareEditButton();

    await listPO.clickCopyToEditLink(2);
    await listPO.setCopyName(SP9.name);
    await listPO.clickCreateCopy();
    await listPO.clickCopyToEditLink(1);
    await listPO.setCopyName(SP8.name);
    await listPO.clickCreateCopy();

    // Resps
    await listPO.setComparisonEditPage(1, 2);
    await listPO.compareEditRemoveAllRespOComp();

    await listPO.compareEditAddRespOComp(4);

    await listPO.compareEditRespOComp(1, 1, SP6.resp1.primary, SP6.resp1.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP6.resp2.primary, SP6.resp3.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP6.resp3.primary, SP6.resp3.levelInc);
    await listPO.compareEditRespOComp(1, 4, false, 1);
    SP6.resp1.name = await listPL.compareEditRespOCompNames.get(0).getText();
    SP6.resp2.name = await listPL.compareEditRespOCompNames.get(1).getText();
    SP6.resp3.name = await listPL.compareEditRespOCompNames.get(2).getText();
    SP6.resp1.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP6.resp2.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP6.resp3.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.compareEditRespOComp(2, 1, SP7.resp1.primary, SP7.resp1.levelInc);
    await listPO.compareEditRespOComp(2, 2, SP7.resp2.primary, SP7.resp3.levelInc);
    await listPO.compareEditRespOComp(2, 3, SP7.resp3.primary, SP7.resp3.levelInc);
    await listPO.compareEditRespOComp(2, 4, false, 2);
    SP7.resp1.name = await listPL.compareEditRespOCompNames.get(0).getText();
    SP7.resp2.name = await listPL.compareEditRespOCompNames.get(1).getText();
    SP7.resp3.name = await listPL.compareEditRespOCompNames.get(2).getText();
    SP7.resp1.levelText = await listPL.compareEditLevelTextP2.get(0).getText();
    SP7.resp2.levelText = await listPL.compareEditLevelTextP2.get(1).getText();
    SP7.resp3.levelText = await listPL.compareEditLevelTextP2.get(2).getText();

    await listPO.compareEditRespOComp(3, 1, SP8.resp1.primary, SP8.resp1.levelInc);
    await listPO.compareEditRespOComp(3, 2, SP8.resp2.primary, SP8.resp3.levelInc);
    await listPO.compareEditRespOComp(3, 3, SP8.resp3.primary, SP8.resp3.levelInc);
    await listPO.compareEditRespOComp(3, 4, false, 3);
    SP8.resp1.name = await listPL.compareEditRespOCompNames.get(0).getText();
    SP8.resp2.name = await listPL.compareEditRespOCompNames.get(1).getText();
    SP8.resp3.name = await listPL.compareEditRespOCompNames.get(2).getText();
    SP8.resp1.levelText = await listPL.compareEditLevelTextP3.get(0).getText();
    SP8.resp2.levelText = await listPL.compareEditLevelTextP3.get(1).getText();
    SP8.resp3.levelText = await listPL.compareEditLevelTextP3.get(2).getText();

    await listPO.compareEditRespOComp(4, 1, SP9.resp1.primary, SP9.resp1.levelInc);
    await listPO.compareEditRespOComp(4, 2, SP9.resp2.primary, SP9.resp3.levelInc);
    await listPO.compareEditRespOComp(4, 3, SP9.resp3.primary, SP9.resp3.levelInc);
    await listPO.compareEditRespOComp(4, 4, false, 4);
    SP9.resp1.name = await listPL.compareEditRespOCompNames.get(0).getText();
    SP9.resp2.name = await listPL.compareEditRespOCompNames.get(1).getText();
    SP9.resp3.name = await listPL.compareEditRespOCompNames.get(2).getText();
    SP9.resp1.levelText = await listPL.compareEditLevelTextP4.get(0).getText();
    SP9.resp2.levelText = await listPL.compareEditLevelTextP4.get(1).getText();
    SP9.resp3.levelText = await listPL.compareEditLevelTextP4.get(2).getText();

    await listPO.compareEditClickRemoveRespOComp(1, 4);
    await listPO.compareEditClickRemoveRespOComp(2, 4);
    await listPO.compareEditClickRemoveRespOComp(3, 4);
    await listPO.compareEditClickRemoveRespOComp(4, 4);

    // BCs
    await listPO.setComparisonEditPage(2, 1);
    await listPO.compareEditRemoveAllRespOComp();

    await listPO.compareEditAddRespOComp(4);
    await listPO.compareEditRespOComp(1, 1, SP6.BC1.primary, SP6.BC1.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP6.BC2.primary, SP6.BC2.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP6.BC3.primary, SP6.BC3.levelInc);
    await listPO.compareEditRespOComp(1, 4, false, 0);
    SP6.BC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP6.BC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP6.BC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP6.BC1.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP6.BC2.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP6.BC3.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.compareEditRespOComp(2, 1, SP7.BC1.primary, SP7.BC1.levelInc);
    await listPO.compareEditRespOComp(2, 2, SP7.BC2.primary, SP7.BC2.levelInc);
    await listPO.compareEditRespOComp(2, 3, SP7.BC3.primary, SP7.BC3.levelInc);
    await listPO.compareEditRespOComp(2, 4, false, 3);
    SP7.BC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP7.BC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP7.BC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP7.BC1.levelText = await listPL.compareEditLevelTextP2.get(0).getText();
    SP7.BC2.levelText = await listPL.compareEditLevelTextP2.get(1).getText();
    SP7.BC3.levelText = await listPL.compareEditLevelTextP2.get(2).getText();

    await listPO.compareEditRespOComp(3, 1, SP8.BC1.primary, SP8.BC1.levelInc);
    await listPO.compareEditRespOComp(3, 2, SP8.BC2.primary, SP8.BC2.levelInc);
    await listPO.compareEditRespOComp(3, 3, SP8.BC3.primary, SP8.BC3.levelInc);
    await listPO.compareEditRespOComp(3, 4, false, 2);
    SP8.BC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP8.BC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP8.BC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP8.BC1.levelText = await listPL.compareEditLevelTextP3.get(0).getText();
    SP8.BC2.levelText = await listPL.compareEditLevelTextP3.get(1).getText();
    SP8.BC3.levelText = await listPL.compareEditLevelTextP3.get(2).getText();

    await listPO.compareEditRespOComp(4, 1, SP9.BC1.primary, SP9.BC1.levelInc);
    await listPO.compareEditRespOComp(4, 2, SP9.BC2.primary, SP9.BC2.levelInc);
    await listPO.compareEditRespOComp(4, 3, SP9.BC3.primary, SP9.BC3.levelInc);
    await listPO.compareEditRespOComp(4, 4, false, 2);
    SP9.BC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP9.BC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP9.BC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP9.BC1.levelText = await listPL.compareEditLevelTextP4.get(0).getText();
    SP9.BC2.levelText = await listPL.compareEditLevelTextP4.get(1).getText();
    SP9.BC3.levelText = await listPL.compareEditLevelTextP4.get(2).getText();

    await listPO.compareEditClickRemoveRespOComp(1, 4);
    await listPO.compareEditClickRemoveRespOComp(2, 4);
    await listPO.compareEditClickRemoveRespOComp(3, 4);
    await listPO.compareEditClickRemoveRespOComp(4, 4);

    // TCs
    await listPO.setComparisonEditPage(2, 2);
    await listPO.compareEditRemoveAllRespOComp();

    await listPO.compareEditAddRespOComp(4);

    await listPO.compareEditRespOComp(1, 1, SP6.TC1.primary, SP6.TC1.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP6.TC2.primary, SP6.TC2.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP6.TC3.primary, SP6.TC3.levelInc);
    await listPO.compareEditRespOComp(1, 4, false, 3);
    SP6.TC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP6.TC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP6.TC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP6.TC1.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP6.TC2.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP6.TC3.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.compareEditRespOComp(2, 1, SP7.TC1.primary, SP7.TC1.levelInc);
    await listPO.compareEditRespOComp(2, 2, SP7.TC2.primary, SP7.TC2.levelInc);
    await listPO.compareEditRespOComp(2, 3, SP7.TC3.primary, SP7.TC3.levelInc);
    await listPO.compareEditRespOComp(2, 4, false, 3);
    SP7.TC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP7.TC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP7.TC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP7.TC1.levelText = await listPL.compareEditLevelTextP2.get(0).getText();
    SP7.TC2.levelText = await listPL.compareEditLevelTextP2.get(1).getText();
    SP7.TC3.levelText = await listPL.compareEditLevelTextP2.get(2).getText();

    await listPO.compareEditRespOComp(3, 1, SP8.TC1.primary, SP8.TC1.levelInc);
    await listPO.compareEditRespOComp(3, 2, SP8.TC2.primary, SP8.TC2.levelInc);
    await listPO.compareEditRespOComp(3, 3, SP8.TC3.primary, SP8.TC3.levelInc);
    await listPO.compareEditRespOComp(3, 4, false, 3);
    SP8.TC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP8.TC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP8.TC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP8.TC1.levelText = await listPL.compareEditLevelTextP3.get(0).getText();
    SP8.TC2.levelText = await listPL.compareEditLevelTextP3.get(1).getText();
    SP8.TC3.levelText = await listPL.compareEditLevelTextP3.get(2).getText();

    await listPO.compareEditRespOComp(4, 1, SP9.TC1.primary, SP9.TC1.levelInc);
    await listPO.compareEditRespOComp(4, 2, SP9.TC2.primary, SP9.TC2.levelInc);
    await listPO.compareEditRespOComp(4, 3, SP9.TC3.primary, SP9.TC3.levelInc);
    await listPO.compareEditRespOComp(4, 4, false, 3);
    SP9.TC1.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP9.TC2.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP9.TC3.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP9.TC1.levelText = await listPL.compareEditLevelTextP4.get(0).getText();
    SP9.TC2.levelText = await listPL.compareEditLevelTextP4.get(1).getText();
    SP9.TC3.levelText = await listPL.compareEditLevelTextP4.get(2).getText();

    await listPO.compareEditClickRemoveRespOComp(1, 4);
    await listPO.compareEditClickRemoveRespOComp(2, 4);
    await listPO.compareEditClickRemoveRespOComp(3, 4);
    await listPO.compareEditClickRemoveRespOComp(4, 4);

    // Education n Experience
    await listPO.setComparisonEditPage(2, 3);
    await listPO.compareEditRespOComp(1, 1, SP6.education.primary, SP6.education.levelInc);
    await listPO.compareEditRespOComp(1, 2, SP6.generalExperience.primary, SP6.generalExperience.levelInc);
    await listPO.compareEditRespOComp(1, 3, SP6.managerialExperience.primary, SP6.managerialExperience.levelInc);
    SP6.education.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP6.generalExperience.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP6.managerialExperience.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP6.education.levelText = await listPL.compareEditLevelTextP1.get(0).getText();
    SP6.generalExperience.levelText = await listPL.compareEditLevelTextP1.get(1).getText();
    SP6.managerialExperience.levelText = await listPL.compareEditLevelTextP1.get(2).getText();

    await listPO.compareEditRespOComp(2, 1, SP7.education.primary, SP7.education.levelInc);
    await listPO.compareEditRespOComp(2, 2, SP7.generalExperience.primary, SP7.generalExperience.levelInc);
    await listPO.compareEditRespOComp(2, 3, SP7.managerialExperience.primary, SP7.managerialExperience.levelInc);
    SP7.education.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP7.generalExperience.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP7.managerialExperience.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP7.education.levelText = await listPL.compareEditLevelTextP2.get(0).getText();
    SP7.generalExperience.levelText = await listPL.compareEditLevelTextP2.get(1).getText();
    SP7.managerialExperience.levelText = await listPL.compareEditLevelTextP2.get(2).getText();

    await listPO.compareEditRespOComp(3, 1, SP8.education.primary, SP8.education.levelInc);
    await listPO.compareEditRespOComp(3, 2, SP8.generalExperience.primary, SP8.generalExperience.levelInc);
    await listPO.compareEditRespOComp(3, 3, SP8.managerialExperience.primary, SP8.managerialExperience.levelInc);
    SP8.education.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP8.generalExperience.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP8.managerialExperience.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP8.education.levelText = await listPL.compareEditLevelTextP3.get(0).getText();
    SP8.generalExperience.levelText = await listPL.compareEditLevelTextP3.get(1).getText();
    SP8.managerialExperience.levelText = await listPL.compareEditLevelTextP3.get(2).getText();

    await listPO.compareEditRespOComp(4, 1, SP9.education.primary, SP9.education.levelInc);
    await listPO.compareEditRespOComp(4, 2, SP9.generalExperience.primary, SP9.generalExperience.levelInc);
    await listPO.compareEditRespOComp(4, 3, SP9.managerialExperience.primary, SP9.managerialExperience.levelInc);
    SP9.education.name = await listPL.compareEditRespOCompNames.get(3).getText();
    SP9.generalExperience.name = await listPL.compareEditRespOCompNames.get(4).getText();
    SP9.managerialExperience.name = await listPL.compareEditRespOCompNames.get(5).getText();
    SP9.education.levelText = await listPL.compareEditLevelTextP4.get(0).getText();
    SP9.generalExperience.levelText = await listPL.compareEditLevelTextP4.get(1).getText();
    SP9.managerialExperience.levelText = await listPL.compareEditLevelTextP4.get(2).getText();

    await listPO.clickSaveCompareView();
    await listPO.clickContinueNSave();

    // check data on compare view
    await listPO.setComparison(1, 2) //resps
    await expect(await listPL.compareLeftColumn.get(0).getText() == SP6.resp1.name)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP6.resp2.name)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP6.resp3.name)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile1.get(0).getText() == SP6.resp1.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp1.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP6.resp2.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp2.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP6.resp3.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP6.resp3.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await expect(await listPL.compareLeftColumn.get(0).getText() == SP7.resp1.name)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP7.resp2.name)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP7.resp3.name)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile2.get(0).getText() == SP7.resp1.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp1.levelText}' instead of '${await listPL.compareProfile2.get(0).getText()}'`);
    await expect(await listPL.compareProfile2.get(1).getText() == SP7.resp2.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp2.levelText}' instead of '${await listPL.compareProfile2.get(1).getText()}'`);
    await expect(await listPL.compareProfile2.get(2).getText() == SP7.resp3.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP7.resp3.levelText}' instead of '${await listPL.compareProfile2.get(2).getText()}'`);

    await expect(await listPL.compareLeftColumn.get(0).getText() == SP8.resp1.name)
      .toBeTruthy(`Responsibilities: expected '${SP8.resp1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP8.resp2.name)
      .toBeTruthy(`Responsibilities: expected '${SP8.resp2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP8.resp3.name)
      .toBeTruthy(`Responsibilities: expected '${SP8.resp3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile3.get(0).getText() == SP8.resp1.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP8.resp1.levelText}' instead of '${await listPL.compareProfile3.get(0).getText()}'`);
    await expect(await listPL.compareProfile3.get(1).getText() == SP8.resp2.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP8.resp2.levelText}' instead of '${await listPL.compareProfile3.get(1).getText()}'`);
    await expect(await listPL.compareProfile3.get(2).getText() == SP8.resp3.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP8.resp3.levelText}' instead of '${await listPL.compareProfile3.get(2).getText()}'`);

    await expect(await listPL.compareLeftColumn.get(0).getText() == SP9.resp1.name)
      .toBeTruthy(`Responsibilities: expected '${SP9.resp1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP9.resp2.name)
      .toBeTruthy(`Responsibilities: expected '${SP9.resp2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP9.resp3.name)
      .toBeTruthy(`Responsibilities: expected '${SP9.resp3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile4.get(0).getText() == SP9.resp1.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP9.resp1.levelText}' instead of '${await listPL.compareProfile4.get(0).getText()}'`);
    await expect(await listPL.compareProfile4.get(1).getText() == SP9.resp2.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP9.resp2.levelText}' instead of '${await listPL.compareProfile4.get(1).getText()}'`);
    await expect(await listPL.compareProfile4.get(2).getText() == SP9.resp3.levelText)
      .toBeTruthy(`Responsibilities: expected '${SP9.resp3.levelText}' instead of '${await listPL.compareProfile4.get(2).getText()}'`);

    await listPO.setComparison(2, 1) //BC
    await expect(await listPL.compareLeftColumn.get(0).getText() == SP6.BC1.name)
      .toBeTruthy(`BC: expected '${SP6.BC1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP6.BC2.name)
      .toBeTruthy(`BC: expected '${SP6.BC2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP6.BC3.name)
      .toBeTruthy(`BC: expected '${SP6.BC3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile1.get(0).getText() == SP6.BC1.levelText)
      .toBeTruthy(`BC: expected '${SP6.BC1.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP6.BC2.levelText)
      .toBeTruthy(`BC: expected '${SP6.BC2.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP6.BC3.levelText)
      .toBeTruthy(`BC: expected '${SP6.BC3.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await expect(await listPL.compareLeftColumn.get(0).getText() == SP7.BC1.name)
      .toBeTruthy(`BC: expected '${SP7.BC1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP7.BC2.name)
      .toBeTruthy(`BC: expected '${SP7.BC2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP7.BC3.name)
      .toBeTruthy(`BC: expected '${SP7.BC3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile2.get(0).getText() == SP7.BC1.levelText)
      .toBeTruthy(`BC: expected '${SP7.BC1.levelText}' instead of '${await listPL.compareProfile2.get(0).getText()}'`);
    await expect(await listPL.compareProfile2.get(1).getText() == SP7.BC2.levelText)
      .toBeTruthy(`BC: expected '${SP7.BC2.levelText}' instead of '${await listPL.compareProfile2.get(1).getText()}'`);
    await expect(await listPL.compareProfile2.get(2).getText() == SP7.BC3.levelText)
      .toBeTruthy(`BC: expected '${SP7.BC3.levelText}' instead of '${await listPL.compareProfile2.get(2).getText()}'`);

    await expect(await listPL.compareLeftColumn.get(0).getText() == SP8.BC1.name)
      .toBeTruthy(`BC: expected '${SP8.BC1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP8.BC2.name)
      .toBeTruthy(`BC: expected '${SP8.BC2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP8.BC3.name)
      .toBeTruthy(`BC: expected '${SP8.BC3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile3.get(0).getText() == SP8.BC1.levelText)
      .toBeTruthy(`BC: expected '${SP8.BC1.levelText}' instead of '${await listPL.compareProfile3.get(0).getText()}'`);
    await expect(await listPL.compareProfile3.get(1).getText() == SP8.BC2.levelText)
      .toBeTruthy(`BC: expected '${SP8.BC2.levelText}' instead of '${await listPL.compareProfile3.get(1).getText()}'`);
    await expect(await listPL.compareProfile3.get(2).getText() == SP8.BC3.levelText)
      .toBeTruthy(`BC: expected '${SP8.BC3.levelText}' instead of '${await listPL.compareProfile3.get(2).getText()}'`);

    await expect(await listPL.compareLeftColumn.get(0).getText() == SP9.BC1.name)
      .toBeTruthy(`BC: expected '${SP9.BC1.name}' instead of '${await listPL.compareLeftColumn.get(0).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP9.BC2.name)
      .toBeTruthy(`BC: expected '${SP9.BC2.name}' instead of '${await listPL.compareLeftColumn.get(1).getText()}'`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP9.BC3.name)
      .toBeTruthy(`BC: expected '${SP9.BC3.name}' instead of '${await listPL.compareLeftColumn.get(2).getText()}'`);
    await expect(await listPL.compareProfile4.get(0).getText() == SP9.BC1.levelText)
      .toBeTruthy(`BC: expected '${SP9.BC1.levelText}' instead of '${await listPL.compareProfile4.get(0).getText()}'`);
    await expect(await listPL.compareProfile4.get(1).getText() == SP9.BC2.levelText)
      .toBeTruthy(`BC: expected '${SP9.BC2.levelText}' instead of '${await listPL.compareProfile4.get(1).getText()}'`);
    await expect(await listPL.compareProfile4.get(2).getText() == SP9.BC3.levelText)
      .toBeTruthy(`BC: expected '${SP9.BC3.levelText}' instead of '${await listPL.compareProfile4.get(2).getText()}'`);

    await listPO.setComparison(2, 2) //TC
    await expect(await listPL.compareLeftColumn.get(0).getText() == SP6.TC1.name)
      .toBeTruthy(`TC: expected '${SP6.TC1.name}' instead of ${await listPL.compareLeftColumn.get(0).getText()}`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP6.TC2.name)
      .toBeTruthy(`TC: expected '${SP6.TC2.name}' instead of ${await listPL.compareLeftColumn.get(1).getText()}`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP6.TC3.name)
      .toBeTruthy(`TC: expected '${SP6.TC3.name}' instead of ${await listPL.compareLeftColumn.get(2).getText()}`);
    await expect(await listPL.compareProfile1.get(0).getText() == SP6.TC1.levelText)
      .toBeTruthy(`TC: expected '${SP6.TC1.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP6.TC2.levelText)
      .toBeTruthy(`TC: expected '${SP6.TC2.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP6.TC3.levelText)
      .toBeTruthy(`TC: expected '${SP6.TC3.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await expect(await listPL.compareLeftColumn.get(0).getText() == SP7.TC1.name)
      .toBeTruthy(`TC: expected '${SP7.TC1.name}' instead of ${await listPL.compareLeftColumn.get(0).getText()}`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP7.TC2.name)
      .toBeTruthy(`TC: expected '${SP7.TC2.name}' instead of ${await listPL.compareLeftColumn.get(1).getText()}`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP7.TC3.name)
      .toBeTruthy(`TC: expected '${SP7.TC3.name}' instead of ${await listPL.compareLeftColumn.get(2).getText()}`);
    await expect(await listPL.compareProfile2.get(0).getText() == SP7.TC1.levelText)
      .toBeTruthy(`TC: expected '${SP7.TC1.levelText}' instead of '${await listPL.compareProfile2.get(0).getText()}'`);
    await expect(await listPL.compareProfile2.get(1).getText() == SP7.TC2.levelText)
      .toBeTruthy(`TC: expected '${SP7.TC2.levelText}' instead of '${await listPL.compareProfile2.get(1).getText()}'`);
    await expect(await listPL.compareProfile2.get(2).getText() == SP7.TC3.levelText)
      .toBeTruthy(`TC: expected '${SP7.TC3.levelText}' instead of '${await listPL.compareProfile2.get(2).getText()}'`);

    await expect(await listPL.compareLeftColumn.get(0).getText() == SP8.TC1.name)
      .toBeTruthy(`TC: expected '${SP8.TC1.name}' instead of ${await listPL.compareLeftColumn.get(0).getText()}`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP8.TC2.name)
      .toBeTruthy(`TC: expected '${SP8.TC2.name}' instead of ${await listPL.compareLeftColumn.get(1).getText()}`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP8.TC3.name)
      .toBeTruthy(`TC: expected '${SP8.TC3.name}' instead of ${await listPL.compareLeftColumn.get(2).getText()}`);
    await expect(await listPL.compareProfile3.get(0).getText() == SP8.TC1.levelText)
      .toBeTruthy(`TC: expected '${SP8.TC1.levelText}' instead of '${await listPL.compareProfile3.get(0).getText()}'`);
    await expect(await listPL.compareProfile3.get(1).getText() == SP8.TC2.levelText)
      .toBeTruthy(`TC: expected '${SP8.TC2.levelText}' instead of '${await listPL.compareProfile3.get(1).getText()}'`);
    await expect(await listPL.compareProfile3.get(2).getText() == SP8.TC3.levelText)
      .toBeTruthy(`TC: expected '${SP8.TC3.levelText}' instead of '${await listPL.compareProfile3.get(2).getText()}'`);

    await expect(await listPL.compareLeftColumn.get(0).getText() == SP9.TC1.name)
      .toBeTruthy(`TC: expected '${SP9.TC1.name}' instead of ${await listPL.compareLeftColumn.get(0).getText()}`);
    await expect(await listPL.compareLeftColumn.get(1).getText() == SP9.TC2.name)
      .toBeTruthy(`TC: expected '${SP9.TC2.name}' instead of ${await listPL.compareLeftColumn.get(1).getText()}`);
    await expect(await listPL.compareLeftColumn.get(2).getText() == SP9.TC3.name)
      .toBeTruthy(`TC: expected '${SP9.TC3.name}' instead of ${await listPL.compareLeftColumn.get(2).getText()}`);
    await expect(await listPL.compareProfile4.get(0).getText() == SP9.TC1.levelText)
      .toBeTruthy(`TC: expected '${SP9.TC1.levelText}' instead of '${await listPL.compareProfile4.get(0).getText()}'`);
    await expect(await listPL.compareProfile4.get(1).getText() == SP9.TC2.levelText)
      .toBeTruthy(`TC: expected '${SP9.TC2.levelText}' instead of '${await listPL.compareProfile4.get(1).getText()}'`);
    await expect(await listPL.compareProfile4.get(2).getText() == SP9.TC3.levelText)
      .toBeTruthy(`TC: expected '${SP9.TC3.levelText}' instead of '${await listPL.compareProfile4.get(2).getText()}'`);

    await listPO.setComparison(2, 3) //Edu n exp
    await expect(await listPL.compareProfile1.get(0).getText() == SP6.education.levelText)
      .toBeTruthy(`General Education: expected '${SP6.education.levelText}' instead of '${await listPL.compareProfile1.get(0).getText()}'`);
    await expect(await listPL.compareProfile1.get(1).getText() == SP6.generalExperience.levelText)
      .toBeTruthy(`General Experience: expected '${SP6.generalExperience.levelText}' instead of '${await listPL.compareProfile1.get(1).getText()}'`);
    await expect(await listPL.compareProfile1.get(2).getText() == SP6.managerialExperience.levelText)
      .toBeTruthy(`Managerial Experience: expected '${SP6.managerialExperience.levelText}' instead of '${await listPL.compareProfile1.get(2).getText()}'`);

    await expect(await listPL.compareProfile2.get(0).getText() == SP7.education.levelText)
      .toBeTruthy(`General Education: expected '${SP7.education.levelText}' instead of '${await listPL.compareProfile2.get(0).getText()}'`);
    await expect(await listPL.compareProfile2.get(1).getText() == SP7.generalExperience.levelText)
      .toBeTruthy(`General Experience: expected '${SP7.generalExperience.levelText}' instead of '${await listPL.compareProfile2.get(1).getText()}'`);
    await expect(await listPL.compareProfile2.get(2).getText() == SP7.managerialExperience.levelText)
      .toBeTruthy(`Managerial Experience: expected '${SP7.managerialExperience.levelText}' instead of '${await listPL.compareProfile2.get(2).getText()}'`);

    await expect(await listPL.compareProfile3.get(0).getText() == SP8.education.levelText)
      .toBeTruthy(`General Education: expected '${SP8.education.levelText}' instead of '${await listPL.compareProfile3.get(0).getText()}'`);
    await expect(await listPL.compareProfile3.get(1).getText() == SP8.generalExperience.levelText)
      .toBeTruthy(`General Experience: expected '${SP8.generalExperience.levelText}' instead of '${await listPL.compareProfile3.get(1).getText()}'`);
    await expect(await listPL.compareProfile3.get(2).getText() == SP8.managerialExperience.levelText)
      .toBeTruthy(`Managerial Experience: expected '${SP8.managerialExperience.levelText}' instead of '${await listPL.compareProfile3.get(2).getText()}'`);

    await expect(await listPL.compareProfile4.get(0).getText() == SP9.education.levelText)
      .toBeTruthy(`General Education: expected '${SP9.education.levelText}' instead of '${await listPL.compareProfile4.get(0).getText()}'`);
    await expect(await listPL.compareProfile4.get(1).getText() == SP9.generalExperience.levelText)
      .toBeTruthy(`General Experience: expected '${SP9.generalExperience.levelText}' instead of '${await listPL.compareProfile4.get(1).getText()}'`);
    await expect(await listPL.compareProfile4.get(2).getText() == SP9.managerialExperience.levelText)
      .toBeTruthy(`Managerial Experience: expected '${SP9.managerialExperience.levelText}' instead of '${await listPL.compareProfile4.get(2).getText()}'`);

    await listPO.removeAllProfileFromCompareView();
    await page.moveToSPList();

    // 6
    await listPO.setFunctionFilter(4, SP6.subFunction);
    await listPO.searchProfile(SP6.name);
    await listPO.openSPFromListByNo(1);

    await browser.wait(EC.presenceOf(detailsPL.detailResps.get(0)), browser.params.cooldown.medium, `There is no Responsibilities on details page`);
    let aResp1 = await detailsPL.detailResps.get(0).getText();
    await expect(SP6.resp1.name.toLowerCase() == aResp1.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP6.resp1.name}' instead of '${await detailsPL.detailResps.get(0).getText()}'`);
    let aResp2 = await detailsPL.detailResps.get(1).getText();
    await expect(SP6.resp2.name.toLowerCase() == aResp2.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP6.resp2.name}' instead of '${await detailsPL.detailResps.get(1).getText()}'`);
    let aResp3 = await detailsPL.detailResps.get(2).getText();
    await expect(SP6.resp3.name.toLowerCase() == aResp3.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP6.resp3.name}' instead of '${await detailsPL.detailResps.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsBCs.get(0)), browser.params.cooldown.medium, `There is no BC on details page`);
    let aBC1 = await detailsPL.detailsBCs.get(0).getText();
    await expect(aBC1.replace(`...`, ``).indexOf(SP6.BC1.name) !== -1)
      .toBeTruthy(`BC: expected '${SP6.BC1.name}' instead of '${aBC1.replace(`...`, ``)}'`);
    let aBC2 = await detailsPL.detailsBCs.get(1).getText();
    await expect(aBC2.replace(`...`, ``).indexOf(SP6.BC2.name) !== -1)
      .toBeTruthy(`BC: expected '${SP6.BC2.name}' instead of '${aBC2.replace(`...`, ``)}'`);
    let aBC3 = await detailsPL.detailsBCs.get(2).getText();
    await expect(aBC3.replace(`...`, ``).indexOf(SP6.BC3.name) !== -1)
      .toBeTruthy(`BC: expected '${SP6.BC3.name}' instead of '${aBC3.replace(`...`, ``)}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsTCs.get(0)), browser.params.cooldown.medium, `There is no TC on details page`);
    let aTC1 = await detailsPL.detailsTCs.get(0).getText();
    await expect(SP6.TC1.name.toLowerCase() == aTC1.toLowerCase())
      .toBeTruthy(`TC: expected '${SP6.TC1.name}' instead of '${await detailsPL.detailsTCs.get(0).getText()}'`);
    let aTC2 = await detailsPL.detailsTCs.get(1).getText();
    await expect(SP6.TC2.name.toLowerCase() == aTC2.toLowerCase())
      .toBeTruthy(`TC: expected '${SP6.TC2.name}' instead of '${await detailsPL.detailsTCs.get(1).getText()}'`);
    let aTC3 = await detailsPL.detailsTCs.get(2).getText();
    await expect(SP6.TC3.name.toLowerCase() == aTC3.toLowerCase())
      .toBeTruthy(`TC: expected '${SP6.TC3.name}' instead of '${await detailsPL.detailsTCs.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailEducation), browser.params.cooldown.medium, `There is no Education info on details page`);
    let aEdu1 = await detailsPL.detailEducation.getText();
    await expect(SP6.education.name.toLowerCase() == aEdu1.toLowerCase())
      .toBeTruthy(`Education: expected '${SP6.education.name}' instead of '${await detailsPL.detailEducation.getText()}'`);
    let aExp1 = await detailsPL.detailExperinces.get(0).getText();
    await expect(SP6.generalExperience.name.toLowerCase() == aExp1.toLowerCase())
      .toBeTruthy(`G Experience: expected '${SP6.generalExperience.name}' instead of '${await detailsPL.detailExperinces.get(0).getText()}'`);
    let aExp2 = await detailsPL.detailExperinces.get(1).getText();
    await expect(SP6.managerialExperience.name.toLowerCase() == aExp2.toLowerCase())
      .toBeTruthy(`M Experience: expected '${SP6.managerialExperience.name}' instead of '${await detailsPL.detailExperinces.get(1).getText()}'`);

    await page.moveToSPList();
    await listPO.clearAllFilters();
    await listPO.clearSearch();

    // 7
    await listPO.setFunctionFilter(4, SP7.subFunction);
    await listPO.searchProfile(SP7.name);
    await listPO.openSPFromListByNo(1);

    await browser.wait(EC.presenceOf(detailsPL.detailResps.get(0)), browser.params.cooldown.medium, `There is no Responsibilities on details page`);
    aResp1 = await detailsPL.detailResps.get(0).getText();
    await expect(SP7.resp1.name.toLowerCase() == aResp1.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP7.resp1.name}' instead of '${await detailsPL.detailResps.get(0).getText()}'`);
    aResp2 = await detailsPL.detailResps.get(1).getText();
    await expect(SP7.resp2.name.toLowerCase() == aResp2.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP7.resp2.name}' instead of '${await detailsPL.detailResps.get(1).getText()}'`);
    aResp3 = await detailsPL.detailResps.get(2).getText();
    await expect(SP7.resp3.name.toLowerCase() == aResp3.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP7.resp3.name}' instead of '${await detailsPL.detailResps.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsBCs.get(0)), browser.params.cooldown.medium, `There is no BC on details page`);
    aBC1 = await detailsPL.detailsBCs.get(0).getText();
    await expect(aBC1.replace(`...`, ``).indexOf(SP7.BC1.name) !== -1)
      .toBeTruthy(`BC: expected '${SP7.BC1.name}' instead of '${await detailsPL.detailsBCs.get(0).getText()}'`);
    aBC2 = await detailsPL.detailsBCs.get(1).getText();
    await expect(aBC2.replace(`...`, ``).indexOf(SP7.BC2.name) !== -1)
      .toBeTruthy(`BC: expected '${SP7.BC2.name}' instead of '${await detailsPL.detailsBCs.get(1).getText()}'`);
    aBC3 = await detailsPL.detailsBCs.get(2).getText();
    await expect(aBC3.replace(`...`, ``).indexOf(SP7.BC3.name) !== -1)
      .toBeTruthy(`BC: expected '${SP7.BC3.name}' instead of '${await detailsPL.detailsBCs.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsTCs.get(0)), browser.params.cooldown.medium, `There is no TC on details page`);
    aTC1 = await detailsPL.detailsTCs.get(0).getText();
    await expect(SP7.TC1.name.toLowerCase() == aTC1.toLowerCase())
      .toBeTruthy(`TC: expected '${SP7.TC1.name}' instead of '${await detailsPL.detailsTCs.get(0).getText()}'`);
    aTC2 = await detailsPL.detailsTCs.get(1).getText();
    await expect(SP7.TC2.name.toLowerCase() == aTC2.toLowerCase())
      .toBeTruthy(`TC: expected '${SP7.TC2.name}' instead of '${await detailsPL.detailsTCs.get(1).getText()}'`);
    aTC3 = await detailsPL.detailsTCs.get(2).getText();
    await expect(SP7.TC3.name.toLowerCase() == aTC3.toLowerCase())
      .toBeTruthy(`TC: expected '${SP7.TC3.name}' instead of '${await detailsPL.detailsTCs.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailEducation), browser.params.cooldown.medium, `There is no Education info on details page`);
    aEdu1 = await detailsPL.detailEducation.getText();
    await expect(SP7.education.name.toLowerCase() == aEdu1.toLowerCase())
      .toBeTruthy(`Education: expected '${SP7.education.name}' instead of '${await detailsPL.detailEducation.getText()}'`);
    aExp1 = await detailsPL.detailExperinces.get(0).getText();
    await expect(SP7.generalExperience.name.toLowerCase() == aExp1.toLowerCase())
      .toBeTruthy(`G Experience: expected '${SP7.generalExperience.name}' instead of '${await detailsPL.detailExperinces.get(0).getText()}'`);
    aExp2 = await detailsPL.detailExperinces.get(1).getText();
    await expect(SP7.managerialExperience.name.toLowerCase() == aExp2.toLowerCase())
      .toBeTruthy(`M Experience: expected '${SP7.managerialExperience.name}' instead of '${await detailsPL.detailExperinces.get(1).getText()}'`);

    await page.moveToSPList();
    await listPO.clearAllFilters();
    await listPO.clearSearch();

    // 8
    await listPO.setFunctionFilter(4, SP8.subFunction);
    await listPO.searchProfile(SP8.name);
    await listPO.openSPFromListByNo(1);

    await browser.wait(EC.presenceOf(detailsPL.detailResps.get(0)), browser.params.cooldown.medium, `There is no Responsibilities on details page`);
    aResp1 = await detailsPL.detailResps.get(0).getText();
    await expect(SP8.resp1.name.toLowerCase() == aResp1.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP8.resp1.name}' instead of '${await detailsPL.detailResps.get(0).getText()}'`);
    aResp2 = await detailsPL.detailResps.get(1).getText();
    await expect(SP8.resp2.name.toLowerCase() == aResp2.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP8.resp2.name}' instead of '${await detailsPL.detailResps.get(1).getText()}'`);
    aResp3 = await detailsPL.detailResps.get(2).getText();
    await expect(SP8.resp3.name.toLowerCase() == aResp3.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP8.resp3.name}' instead of '${await detailsPL.detailResps.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsBCs.get(0)), browser.params.cooldown.medium, `There is no BC on details page`);
    aBC1 = await detailsPL.detailsBCs.get(0).getText();
    await expect(aBC1.replace(`...`, ``).indexOf(SP8.BC1.name) !== -1)
      .toBeTruthy(`BC: expected '${SP8.BC1.name}' instead of '${await detailsPL.detailsBCs.get(0).getText()}'`);
    aBC2 = await detailsPL.detailsBCs.get(1).getText();
    await expect(aBC2.replace(`...`, ``).indexOf(SP8.BC2.name) !== -1)
      .toBeTruthy(`BC: expected '${SP8.BC2.name}' instead of '${await detailsPL.detailsBCs.get(1).getText()}'`);
    aBC3 = await detailsPL.detailsBCs.get(2).getText();
    await expect(aBC3.replace(`...`, ``).indexOf(SP8.BC3.name) !== -1)
      .toBeTruthy(`BC: expected '${SP8.BC3.name}' instead of '${await detailsPL.detailsBCs.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsTCs.get(0)), browser.params.cooldown.medium, `There is no TC on details page`);
    aTC1 = await detailsPL.detailsTCs.get(0).getText();
    await expect(SP8.TC1.name.toLowerCase() == aTC1.toLowerCase())
      .toBeTruthy(`TC: expected '${SP8.TC1.name}' instead of '${await detailsPL.detailsTCs.get(0).getText()}'`);
    aTC2 = await detailsPL.detailsTCs.get(1).getText();
    await expect(SP8.TC2.name.toLowerCase() == aTC2.toLowerCase())
      .toBeTruthy(`TC: expected '${SP8.TC2.name}' instead of '${await detailsPL.detailsTCs.get(1).getText()}'`);
    aTC3 = await detailsPL.detailsTCs.get(2).getText();
    await expect(SP8.TC3.name.toLowerCase() == aTC3.toLowerCase())
      .toBeTruthy(`TC: expected '${SP8.TC3.name}' instead of '${await detailsPL.detailsTCs.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailEducation), browser.params.cooldown.medium, `There is no Education info on details page`);
    aEdu1 = await detailsPL.detailEducation.getText();
    await expect(SP8.education.name.toLowerCase() == aEdu1.toLowerCase())
      .toBeTruthy(`Education: expected '${SP8.education.name}' instead of '${await detailsPL.detailEducation.getText()}'`);
    aExp1 = await detailsPL.detailExperinces.get(0).getText();
    await expect(SP8.generalExperience.name.toLowerCase() == aExp1.toLowerCase())
      .toBeTruthy(`G Experience: expected '${SP8.generalExperience.name}' instead of '${await detailsPL.detailExperinces.get(0).getText()}'`);
    aExp2 = await detailsPL.detailExperinces.get(1).getText();
    await expect(SP8.managerialExperience.name.toLowerCase() == aExp2.toLowerCase())
      .toBeTruthy(`M Experience: expected '${SP8.managerialExperience.name}' instead of '${await detailsPL.detailExperinces.get(1).getText()}'`);

    await page.moveToSPList();
    await listPO.clearAllFilters();
    await listPO.clearSearch();

    // 9
    await listPO.setFunctionFilter(4, SP9.subFunction);
    await listPO.searchProfile(SP9.name);
    await listPO.openSPFromListByNo(1);

    await browser.wait(EC.presenceOf(detailsPL.detailResps.get(0)), browser.params.cooldown.medium, `There is no Responsibilities on details page`);
    aResp1 = await detailsPL.detailResps.get(0).getText();
    await expect(SP9.resp1.name.toLowerCase() == aResp1.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP9.resp1.name}' instead of '${await detailsPL.detailResps.get(0).getText()}'`);
    aResp2 = await detailsPL.detailResps.get(1).getText();
    await expect(SP9.resp2.name.toLowerCase() == aResp2.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP9.resp2.name}' instead of '${await detailsPL.detailResps.get(1).getText()}'`);
    aResp3 = await detailsPL.detailResps.get(2).getText();
    await expect(SP9.resp3.name.toLowerCase() == aResp3.toLowerCase())
      .toBeTruthy(`Responsibilities: expected '${SP9.resp3.name}' instead of '${await detailsPL.detailResps.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsBCs.get(0)), browser.params.cooldown.medium, `There is no BC on details page`);
    aBC1 = await detailsPL.detailsBCs.get(0).getText();
    await expect(aBC1.replace(`...`, ``).indexOf(SP9.BC1.name) !== -1)
      .toBeTruthy(`BC: expected '${SP9.BC1.name}' instead of '${await detailsPL.detailsBCs.get(0).getText()}'`);
    aBC2 = await detailsPL.detailsBCs.get(1).getText();
    await expect(aBC1.replace(`...`, ``).indexOf(SP9.BC2.name) !== -1)
      .toBeTruthy(`BC: expected '${SP9.BC2.name}' instead of '${await detailsPL.detailsBCs.get(1).getText()}'`);
    aBC3 = await detailsPL.detailsBCs.get(2).getText();
    await expect(aBC1.replace(`...`, ``).indexOf(SP9.BC3.name) !== -1)
      .toBeTruthy(`BC: expected '${SP9.BC3.name}' instead of '${await detailsPL.detailsBCs.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailsTCs.get(0)), browser.params.cooldown.medium, `There is no TC on details page`);
    aTC1 = await detailsPL.detailsTCs.get(0).getText();
    await expect(SP9.TC1.name.toLowerCase() == aTC1.toLowerCase())
      .toBeTruthy(`TC: expected '${SP9.TC1.name}' instead of '${await detailsPL.detailsTCs.get(0).getText()}'`);
    aTC2 = await detailsPL.detailsTCs.get(1).getText();
    await expect(SP9.TC2.name.toLowerCase() == aTC2.toLowerCase())
      .toBeTruthy(`TC: expected '${SP9.TC2.name}' instead of '${await detailsPL.detailsTCs.get(1).getText()}'`);
    aTC3 = await detailsPL.detailsTCs.get(2).getText();
    await expect(SP9.TC3.name.toLowerCase() == aTC3.toLowerCase())
      .toBeTruthy(`TC: expected '${SP9.TC3.name}' instead of '${await detailsPL.detailsTCs.get(2).getText()}'`);

    await browser.wait(EC.presenceOf(detailsPL.detailEducation), browser.params.cooldown.medium, `There is no Education info on details page`);
    aEdu1 = await detailsPL.detailEducation.getText();
    await expect(SP9.education.name.toLowerCase() == aEdu1.toLowerCase())
      .toBeTruthy(`Education: expected '${SP9.education.name}' instead of '${await detailsPL.detailEducation.getText()}'`);
    aExp1 = await detailsPL.detailExperinces.get(0).getText();
    await expect(SP9.generalExperience.name.toLowerCase() == aExp1.toLowerCase())
      .toBeTruthy(`G Experience: expected '${SP9.generalExperience.name}' instead of '${await detailsPL.detailExperinces.get(0).getText()}'`);
    aExp2 = await detailsPL.detailExperinces.get(1).getText();
    await expect(SP9.managerialExperience.name.toLowerCase() == aExp2.toLowerCase())
      .toBeTruthy(`M Experience: expected '${SP9.managerialExperience.name}' instead of '${await detailsPL.detailExperinces.get(1).getText()}'`);

    await page.moveToSPList();
    await listPO.clearAllFilters();
    await listPO.clearSearch();
  });

  it(`Matrix view: Export (SP)`, async () => {
    await page.moveToSPList();

    await listPO.setFilter(5, 1);
    await listPO.searchProfile(SPF1.name);
    await listPO.getCount().then(async (am) => {
      for (let i = am; i > 0; i--) {
        await listPO.deleteSPViaThreeDotsMenu();
      };
    });
    await listPO.clearSearch();
    await listPO.clearAllFilters();

    await listPO.setFilter(5, 3);
    await listPO.copyProfileViaThreeDotsMenu(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.selectEditPageItem(1);
    await landingPO.editProfileTitle(SPF1.name);
    await landingPO.setProfileFunctionByName(SPF1.function);
    await landingPO.setProfileSubFunctionByName(SPF1.subFunction);
    await landingPO.clickNextButton();

    let leveldata5M = await landingPL.landingDetails.get(2).getText();
    let levelInfo5M = leveldata5M.split(`|`);
    SPF1.level = levelInfo5M[0].replace(`Level: `, ``).trim();
    SPF1.subLevel = levelInfo5M[1].trim();

    await landingPO.saveProfile();
    await jePO.openJE();

    SPF1.grade = await jePL.textCharts.get(0).getText();
    // SP5.shortProfile = await jePL.textCharts.get(1).getText();
    // SP5.kfHayPoints = await jePL.textCharts.get(2).getText();

    await jePO.clickEditJEButton();
    await jePO.saveJE();
    await jePO.clickCancelButton();

    await page.moveToSPList();
    await listPO.setMatrixView();
    await listPO.clearAllFiltersF();

    await listPO.setFilter(2, 1);
    await listPO.setFunctionFilter(1, SPF1.subFunction);

    let fileName = `temp/export.xlsx`;
    let downloadTimeout = 60; //sec
    let minFileSize = 16;
    await deleteFileIfExists(fileName);
    await browser.wait(EC.elementToBeClickable(listPL.matrixDownloadButton), 60 * 1000, `There is no download link`);
    await listPL.matrixDownloadButton.click();
    console.log(checkFileExistsWithDelay(fileName, downloadTimeout));
    await expect(await checkFileExistsWithDelay(fileName, downloadTimeout)).toBeTruthy(`Can't export matrix`);
    await expect(await checkFileStatWithDelay(fileName, minFileSize, downloadTimeout)).toBeTruthy(`File must be greater than ${minFileSize}kb`);

    let wb = await xlsx.readFile(`./temp/export.xlsx`);
    let workSheet = wb.Sheets[`Data`];

    let afunc = workSheet[`B2`].v;
    let asubfunc = workSheet[`B3`].v;
    let adata = workSheet[`B4`].v;
    let agrade = workSheet[`A4`].v;

    console.log(adata);

    await expect(adata.indexOf(SPF1.name) !== -1).toBeTruthy(`There is no expected name(${SPF1.name}) in: ${adata}`);
    await expect(adata.indexOf(SPF1.level) !== -1).toBeTruthy(`There is no expected level(${SPF1.level}) in: ${adata}`);
    await expect(adata.indexOf(SPF1.subLevel) !== -1).toBeTruthy(`There is no expected sub level(${SPF1.subLevel}) in: ${adata}`);
    await expect(afunc.indexOf(SPF1.function) !== -1).toBeTruthy(`Export file function: expected ${SPF1.function} instead of ${afunc}`);
    await expect(asubfunc.indexOf(SPF1.subFunction) !== -1).toBeTruthy(`Export file sub function: expected ${SPF1.subFunction} instead of ${asubfunc}`);
    await expect(agrade.indexOf(SPF1.grade) !== -1).toBeTruthy(`Export file function: expected ${SPF1.grade} instead of ${agrade}`);

    await listPO.clearAllFilters();
  });
});