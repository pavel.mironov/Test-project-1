import { by, element, browser, ExpectedConditions as EC, protractor } from 'protractor';
import { AppPage as page } from './components/app.po';
import { SuccessProfileListPageObjects as listPO } from './components/sp-list.po';
import { SuccessProfileDetailsPageLocators as detailsPL } from './components/sp-details.locators';
import { SuccessProfileDetailsPageObjects as detailsPO } from './components/sp-details.po';
import { SuccessProfileLandingPageLocators as landingPL } from './components/sp-landing.locators';
import { SuccessProfileLandingPageObjects as landingPO } from './components/sp-landing.po';
import { SuccessProfileSummaryPageLocators as summaryPL } from './components/sp-summary.locators';
import { SuccessProfileSummaryPageObjects as summaryPO } from './components/sp-summary.po';
import { SpinnerPO } from './components/spinner';

let i = 0;
let profileName1 = 'Test ASDFG || AAAA';
let profileName2 = 'Test ASDFG || EERR';
let profileName3 = 'Test ASDFG || EETT';
let profileName4 = 'Test ASDFG || BBBB';
let profileName5 = 'Test ASDFG || BBAA';
let profileName6 = 'Test ASDFG || BBDD';
let profileNameS1 = 'Test ASDFG || SS';

let descriptio = `Test description, no sense at all`;

let respName1 = 'Responsibility 1';
let respName2 = 'Responsibility 2';
let respName3 = 'Responsibility 3';

let taskName1 = 'Task 1';
let taskName2 = 'Task 2';
let taskName3 = 'Task 3';

let bCompetency1 = 'BC 1';
let bCompetency2 = 'BC 2';
let bCompetency3 = 'BC 3';

let tCompetency1 = 'TC 1';
let tCompetency2 = 'TC 2';
let tCompetency3 = 'TC 3';

let skillName1 = 'TC Skill 1';
let skillName2 = 'TC Skill 2';
let skillName3 = 'TC Skill 3';

let tool1 = 'Tool 1';
let tool2 = 'Tool 2';
let tool3 = 'Tool 3';

let education = 'Education AA';
let exp1 = 'Experience 1';
let exp2 = 'Experience 2';

describe('Success Profile editing', () => {
  beforeEach(async () => {
    await page.login();
    await browser.waitForAngularEnabled(false);
  });

  afterEach(async () => {
    await browser.manage().logs().get('browser').then(async (browserLog) => {
      if (browserLog.length > 0) {
        await browserLog.forEach(async log => {
          await expect(browserLog.length > 0).toBeFalsy(`error in console`);//.toBeFalsy(log.message);
        });
      }
    });
  });

  it(`Traits and drivers(region norm)`, async () => {
    // Get rid of old profile
    await page.searchProfileHome(profileName1).then(async () => {
      await listPO.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });

    // Create new on via copy
    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 2);
    await listPO.copyProfileViaThreeDotsMenu(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(profileName1);

    await landingPO.selectEditPageItem(1);
    const rBefore = await landingPO.editRegion(1);
    await landingPO.setGlobalRadio(2);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();

    await detailsPO.clickMagnifier();
    const traitsBefore = await summaryPO.getSummaryAllTraits();
    const driversBefore = await summaryPO.getSummaryAllDrivers();
    await summaryPO.clickIdentityTab();
    await summaryPO.clickTraitsTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 8)
      .toBeTruthy(`${profileName1}: Should be 8 traits`);
    await summaryPO.clickDriversTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 6)
      .toBeTruthy(`${profileName1}: Should be 6 drivers`);
    await summaryPO.closeSummary();


    await detailsPO.openActionsMenu();
    await detailsPO.clickItemInActions(2);

    await landingPO.selectEditPageItem(1);
    const rAfter = await landingPO.editRegion(2);
    await landingPO.setGlobalRadio(2);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();

    await detailsPO.clickMagnifier();
    const traitsAfter = await summaryPO.getSummaryAllTraits();
    const driversAfter = await summaryPO.getSummaryAllDrivers();
    await summaryPO.clickIdentityTab();
    await summaryPO.clickTraitsTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 8)
      .toBeTruthy(`${profileName1}: Should be 8 traits`);
    await summaryPO.clickDriversTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 6)
      .toBeTruthy(`${profileName1}: Should be 6 drivers`);
    await summaryPO.closeSummary();

    await expect(traitsBefore !== traitsAfter).toBeTruthy(`Top Traits stays the samae: ${traitsBefore}`);
    await expect(driversBefore !== driversAfter).toBeTruthy(`Top Drivers stays the same: ${driversBefore}`);
  });

  it(`Traits and drivers(UCP sliders)`, async () => {
    // Get rid of old profile
    await page.searchProfileHome(profileName2).then(async () => {
      await listPO.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });

    // Create new on via copy
    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 2);
    await listPO.copyProfileViaThreeDotsMenu(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(profileName2);

    await landingPO.selectEditPageItem(4);
    await landingPO.setAllUCPSlidersMin();
    await landingPO.clickNextButton();
    await landingPO.setCongnitiveAbilities(0, 1, 0);
    await landingPO.clickDoneSetUCPSliders();

    await landingPO.saveProfile();

    await detailsPO.clickMagnifier();
    const traitsBefore = await summaryPO.getSummaryAllTraits();
    const driversBefore = await summaryPO.getSummaryAllDrivers();
    await summaryPO.clickIdentityTab();
    await summaryPO.clickTraitsTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 8)
      .toBeTruthy(`${profileName2}: Should be 8 traits`);
    await summaryPO.clickDriversTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 6)
      .toBeTruthy(`${profileName2}: Should be 6 drivers`);
    await summaryPO.closeSummary();

    await expect(EC.presenceOf(detailsPL.numericalAbility)).toBeTruthy(`Verbal ability should be unchecked`);
    await expect(EC.presenceOf(detailsPL.verbalAbilityChecked)).toBeTruthy(`Verbal ability should be checked`);
    await expect(EC.presenceOf(detailsPL.checkingAbility)).toBeTruthy(`Verbal ability should be unchecked`);

    await detailsPO.openActionsMenu();
    await detailsPO.clickItemInActions(2);

    await landingPO.selectEditPageItem(4);
    await landingPO.setAllUCPSlidersMax();
    await landingPO.clickNextButton();
    await landingPO.setCongnitiveAbilities(1, 0, 1);
    await landingPO.clickDoneSetUCPSliders();

    await landingPO.saveProfile();

    await detailsPO.clickMagnifier();
    const traitsAfter = await summaryPO.getSummaryAllTraits();
    const driversAfter = await summaryPO.getSummaryAllDrivers();
    await summaryPO.clickIdentityTab();
    await summaryPO.clickTraitsTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 8)
      .toBeTruthy(`${profileName2}: Should be 8 traits`);
    await summaryPO.clickDriversTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 6)
      .toBeTruthy(`${profileName2}: Should be 6 drivers`);
    await summaryPO.closeSummary();

    await expect(EC.presenceOf(detailsPL.numericalAbilityChecked)).toBeTruthy(`Numerical ability should be checked`);
    await expect(EC.presenceOf(detailsPL.verbalAbility)).toBeTruthy(`Verbal ability should be unchecked`);
    await expect(EC.presenceOf(detailsPL.checkingAbilityChecked)).toBeTruthy(`Checking ability should be checked`);

    await expect(traitsBefore !== traitsAfter).toBeTruthy(`Top Traits stays the samae: ${traitsBefore}`);
    await expect(driversBefore !== driversAfter).toBeTruthy(`Top Drivers stays the same: ${driversBefore}`);
  });

  it(`Traits and drivers(profile level)`, async () => {
    // Get rid of old profile
    await page.searchProfileHome(profileName3).then(async () => {
      await listPO.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });

    // Create new on via copy
    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 2);
    await listPO.copyProfileViaThreeDotsMenu(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(profileName3);

    await landingPO.selectEditPageItem(1);
    await landingPO.setProfileLevel(1);
    await landingPO.setProfileSubLevel(1);
    await landingPO.clickNextButton();

    await landingPO.saveProfile();

    await detailsPO.clickMagnifier();
    const traitsBefore = await summaryPO.getSummaryAllTraits();
    const driversBefore = await summaryPO.getSummaryAllDrivers();
    await summaryPO.clickIdentityTab();
    await summaryPO.clickTraitsTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 8)
      .toBeTruthy(`${profileName3}: Should be 8 traits`);
    await summaryPO.clickDriversTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 6)
      .toBeTruthy(`${profileName3}: Should be 6 drivers`);
    await summaryPO.closeSummary();

    await detailsPO.openActionsMenu();
    await detailsPO.clickItemInActions(2);

    await landingPO.selectEditPageItem(1);
    await landingPO.setProfileLevel(3);
    await landingPO.setProfileSubLevel(1);

    await landingPO.clickNextButton();

    await landingPO.saveProfile();

    await detailsPO.clickMagnifier();
    const traitsAfter = await summaryPO.getSummaryAllTraits();
    const driversAfter = await summaryPO.getSummaryAllDrivers();
    await summaryPO.clickIdentityTab();
    await summaryPO.clickTraitsTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 8)
      .toBeTruthy(`${profileName3}: Should be 8 traits`);
    await summaryPO.clickDriversTab();
    await expect(await summaryPO.getCurrentTabTRCount() == 6)
      .toBeTruthy(`${profileName3}: Should be 6 drivers`);
    await summaryPO.closeSummary();

    await expect(traitsBefore !== traitsAfter).toBeTruthy(`Top Traits stays the samae: ${traitsBefore}`);
    await expect(driversBefore !== driversAfter).toBeTruthy(`Top Drivers stays the same: ${driversBefore}`);
  });

  it(`Landing page edit (Details section)`, async () => {
    // Get rid of old profile
    await page.searchProfileHome(profileName4).then(async () => {
      await listPO.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });

    // Create new on via copy
    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 2);
    await listPO.copyProfileViaThreeDotsMenu(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(profileName4);

    await landingPO.selectEditPageItem(1);
    await landingPO.editProfileTitle(profileName4);
    const spLevel = await landingPO.setProfileLevel(2 + i);
    const spSubLevel = await landingPO.setProfileSubLevel(1 + i);
    const spFunction = await landingPO.setProfileFunction(5 + i);
    const spSubFunction = await landingPO.setProfileSubFunction(1 + i);
    await landingPO.editProfileDescription(descriptio);
    const regionName = await landingPO.editRegion(i + 1);
    await landingPO.setGlobalRadio(2);
    await landingPO.clickNextButton();

    // Check name, level/sublevel, function/sub function, description, region
    const nameInfo = await landingPL.landingDetails.get(0).getText();
    await expect(nameInfo.indexOf(profileName4) !== -1).toBeTruthy(`Expected name(${profileName4}) on landing page`);
    const functionInfo = await landingPL.landingDetails.get(1).getText();
    await expect(functionInfo.indexOf(spFunction) !== -1).toBeTruthy(`Expected function(${spFunction}) on landing page`);
    await expect(functionInfo.indexOf(spSubFunction) !== -1).toBeTruthy(`Expected sub function(${spSubFunction}) on landing page`);
    const levelInfo = await landingPL.landingDetails.get(2).getText();
    await expect(levelInfo.indexOf(spLevel) !== -1).toBeTruthy(`Expected level(${spLevel}) on landing page`);
    await expect(levelInfo.indexOf(spSubLevel) !== -1).toBeTruthy(`Expected sub level(${spSubLevel}) on landing page`);
    const descriptionInfo = await landingPL.landingDetails.get(3).getText();
    await expect(descriptionInfo.indexOf(descriptio) !== -1).toBeTruthy(`Expected description(${descriptio}) on landing page`);
    const regionInfo = await landingPL.landingDetails.get(4).getText();
    await expect(regionInfo.indexOf(regionName) !== -1).toBeTruthy(`Expected region(${regionName}) on landing page`);

    await landingPO.saveProfile();

    // Check title on details page
    await browser.wait(EC.presenceOf(await detailsPL.profileName));
    const title = await detailsPL.profileName.getText();
    await expect(title.indexOf(profileName4) !== -1).toBeTruthy(`No title(${title}) on details page`)
    // Check level/sub level info on details page
    const level = await detailsPL.levelInfo.get(0).getText();
    await expect(level.indexOf(spLevel) !== -1).toBeTruthy(`No Level(${spLevel}) in profile info`);
    const sublevel = await detailsPL.levelInfo.get(1).getText();
    await expect(sublevel.indexOf(spSubLevel) !== -1).toBeTruthy(`No sub-level(${spSubLevel}) in profile info`);
    // Check function/sub function info on details page
    const functio = await detailsPL.functionInfo.get(0).getText();
    await expect(functio.indexOf(spFunction) !== -1).toBeTruthy(`No function(${spFunction}) in profile info`);
    const subfunctio = await await detailsPL.functionInfo.get(1).getText();
    await expect(subfunctio.indexOf(spSubFunction) !== -1).toBeTruthy(`No sub-function(${spSubFunction}) in profile info`);
    // Check region info
    await browser.wait(EC.visibilityOf(await detailsPL.regionName));
    await SpinnerPO.waitUntilSpinnerOff();
    const region = await detailsPL.regionName.getText();
    await expect(region.indexOf(regionName) !== -1).toBeTruthy(`No region name(${regionName})(${region}) in profile info`);
    const desc = await detailsPL.profileDescription.getText();
    await expect(desc.indexOf(descriptio) !== -1).toBeTruthy(`no description(${descriptio}) on details page`)

    await detailsPO.openActionsMenu();
    await detailsPO.clickItemInActions(2);

    // Check name, level/sublevel, function/sub function, description, region
    await expect(nameInfo.indexOf(profileName4) !== -1).toBeTruthy(`Expected name(${profileName4}) on landing page`);
    await expect(functionInfo.indexOf(spFunction) !== -1).toBeTruthy(`Expected function(${spFunction}) on landing page`);
    await expect(functionInfo.indexOf(spSubFunction) !== -1).toBeTruthy(`Expected sub function(${spSubFunction}) on landing page`);
    await expect(levelInfo.indexOf(spLevel) !== -1).toBeTruthy(`Expected level(${spLevel}) on landing page`);
    await expect(levelInfo.indexOf(spSubLevel) !== -1).toBeTruthy(`Expected sub level(${spSubLevel}) on landing page`);
    await expect(descriptionInfo.indexOf(descriptio) !== -1).toBeTruthy(`Expected description(${descriptio}) on landing page`);
    await expect(regionInfo.indexOf(regionName) !== -1).toBeTruthy(`Expected region(${regionName}) on landing page`);

    await landingPO.clickBackButton();
  });

  it(`Landing page edit (Accountability section)`, async () => {
    // Get rid of old profile
    await page.searchProfileHome(profileName5).then(async () => {
      await listPO.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });

    // Create new on via copy
    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 2);
    await listPO.copyProfileViaThreeDotsMenu(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(profileName5);

    await landingPO.selectEditPageItem(2);
    await landingPO.deleteAllResponsibilities();
    await landingPO.addResponsibility();
    await landingPO.editResponsibilityName(1, respName1);
    await landingPO.addResponsibility();
    await landingPO.editResponsibilityName(2, respName2);
    await landingPO.addResponsibility();
    await landingPO.editResponsibilityName(3, respName3);
    await landingPO.clickNextButton();
    await landingPO.confirmUpdate(2);

    var coreTaskNum = 0;
    var supTaskNum = 0;
    await landingPO.deleteAllTasks();
    await landingPO.addTask();
    await landingPO.editTask(1, 1, taskName1); coreTaskNum++;
    await landingPO.addTask();
    await landingPO.editTask(2, 1, taskName2); coreTaskNum++;
    await landingPO.addTask();
    await landingPO.editTask(3, 2, taskName3); supTaskNum++;
    await landingPO.clickNextButton();

    await browser.wait(EC.presenceOf(landingPL.landingDetails.get(0)), browser.params.cooldown.small, `There is no Accountability section on landing page`)
    let respText = await landingPL.landingAccountability.get(0).getText();
    await expect(respText.indexOf(respName1) !== -1).toBeTruthy(`There is no "${respName1}" in Landing page/Accountability`);
    await expect(respText.indexOf(respName2) !== -1).toBeTruthy(`There is no "${respName2}" in Landing page/Accountability`);
    await expect(respText.indexOf(respName3) !== -1).toBeTruthy(`There is no "${respName3}" in Landing page/Accountability`);

    let tasksText = await landingPL.landingAccountability.get(1).getText();
    await expect(tasksText.indexOf(`${coreTaskNum} Core Tasks, ${supTaskNum} Supplemental Tasks`) !== -1).toBeTruthy(`There is no "${coreTaskNum} Core Tasks, ${supTaskNum} Supplemental Tasks" in Landing page/Accountability`);

    await landingPO.saveProfile();

    await browser.wait(EC.presenceOf(detailsPL.detailResps.get(0)), browser.params.cooldown.medium, `Details page: there is no top task section`);
    await expect(await detailsPL.detailResps.get(0).getText() == respName1)
      .toBeTruthy(`Details / responsibilities: expected ${respName1} instead of ${await detailsPL.detailResps.get(0).getText()}`);
    await expect(await detailsPL.detailResps.get(1).getText() == respName2)
      .toBeTruthy(`Details / responsibilities: expected ${respName2} instead of ${await detailsPL.detailResps.get(1).getText()}`);
    await expect(await detailsPL.detailResps.get(2).getText() == respName3)
      .toBeTruthy(`Details / responsibilities: expected ${respName3} instead of ${await detailsPL.detailResps.get(2).getText()}`);

    await browser.wait(EC.presenceOf(detailsPL.detailTasks.get(0)), browser.params.cooldown.medium, `Details page: there is no top task section`);
    await expect(await detailsPL.detailTasks.get(0).getText() == taskName1)
      .toBeTruthy(`Details / tasks: expected ${taskName1} instead of ${await detailsPL.detailTasks.get(0).getText()}`);
    await expect(await detailsPL.detailTasks.get(1).getText() == taskName2)
      .toBeTruthy(`Details / tasks: expected ${taskName2} instead of ${await detailsPL.detailTasks.get(1).getText()}`);
    await expect(await detailsPL.detailTasks.get(2).getText() == taskName3)
      .toBeTruthy(`Details / tasks: expected ${taskName3} instead of ${await detailsPL.detailTasks.get(2).getText()}`);

    await detailsPO.openActionsMenu();
    await detailsPO.clickItemInActions(2);

    await browser.wait(EC.presenceOf(landingPL.landingDetails.get(0)), browser.params.cooldown.small, `There is no Accountability section on landing page`)
    respText = await landingPL.landingAccountability.get(0).getText();
    await expect(respText.indexOf(respName1) !== -1).toBeTruthy(`There is no "${respName1}" in Landing page/Accountability`);
    await expect(respText.indexOf(respName2) !== -1).toBeTruthy(`There is no "${respName2}" in Landing page/Accountability`);
    await expect(respText.indexOf(respName3) !== -1).toBeTruthy(`There is no "${respName3}" in Landing page/Accountability`);

    tasksText = await landingPL.landingAccountability.get(1).getText();
    await expect(tasksText.indexOf(`${coreTaskNum} Core Tasks, ${supTaskNum} Supplemental Tasks`) !== -1).toBeTruthy(`There is no "${coreTaskNum} Core Tasks, ${supTaskNum} Supplemental Tasks" in Landing page/Accountability`);

    await landingPO.clickBackButton();
  });

  it(`Landing page edit (Capability section)`, async () => {
    // Get rid of old profile
    await page.searchProfileHome(profileName6).then(async () => {
      await listPO.getCount().then(async (am) => {
        for (let i = am; i > 0; i--) {
          await listPO.deleteSPViaThreeDotsMenu();
        };
      });
    });

    // Create new on via copy
    await listPO.clearSearch();
    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 2);
    await listPO.copyProfileViaThreeDotsMenu(1);
    await detailsPO.openEditPageViaActions();
    await landingPO.changeProfileName(profileName6);

    await landingPO.selectEditPageItem(3);
    // BC
    await landingPO.deleteAllBCompetencies();
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(1, bCompetency1);
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(2, bCompetency2);
    await landingPO.addBCompetency();
    await landingPO.editBCompetencyName(3, bCompetency3);
    await landingPO.clickNextButton();
    // TC
    await landingPO.deleteAllTCompetencies();
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(1, tCompetency1);
    await landingPO.addNewSkillActions(1, skillName1);
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(2, tCompetency2);
    await landingPO.addNewSkillActions(2, skillName2);
    await landingPO.addTCompetency();
    await landingPO.editTCompetencyName(3, tCompetency3);
    await landingPO.addNewSkillActions(3, skillName3);
    await landingPO.clickNextButton();
    // Tools
    await landingPO.deleteAllTools();
    await landingPO.clickAddTool();
    await landingPO.editToolName(1, tool1)
    await landingPO.addToolExample(1, 'ONE');
    await landingPO.clickAddTool();
    await landingPO.editToolName(2, tool2)
    await landingPO.addToolExample(2, 'TWO');
    await landingPO.clickAddTool();
    await landingPO.editToolName(3, tool3)
    await landingPO.addToolExample(3, 'THREE');
    await landingPO.clickNextButton();
    // Education & Experience
    await landingPO.deleteAllEduNExp();
    await landingPO.addAllEducation();
    await landingPO.editEducationName(1, education);
    await landingPO.addAllExperience();
    await landingPO.editExperienceName(1, exp1);
    await landingPO.editExperienceName(2, exp2);
    await landingPO.clickNextButton();

    await browser.wait(EC.presenceOf(landingPL.landingCapability.get(0)), browser.params.cooldown.small, `There is no Capability section on landing page`)
    let bcText = await landingPL.landingCapability.get(0).getText();
    await expect(bcText.indexOf(bCompetency1) !== -1).toBeTruthy(`There is no "${bCompetency1}" in Landing page/Capability`);
    await expect(bcText.indexOf(bCompetency2) !== -1).toBeTruthy(`There is no "${bCompetency2}" in Landing page/Capability`);
    await expect(bcText.indexOf(bCompetency3) !== -1).toBeTruthy(`There is no "${bCompetency3}" in Landing page/Capability`);

    let tcText = await landingPL.landingCapability.get(1).getText();
    await expect(tcText.indexOf(tCompetency1) !== -1).toBeTruthy(`There is no "${tCompetency1}" in Landing page/Capability`);
    await expect(tcText.indexOf(tCompetency2) !== -1).toBeTruthy(`There is no "${tCompetency2}" in Landing page/Capability`);
    await expect(tcText.indexOf(tCompetency3) !== -1).toBeTruthy(`There is no "${tCompetency3}" in Landing page/Capability`);

    let skillsText = await landingPL.landingCapability.get(2).getText();
    await expect(skillsText.indexOf(skillName1) !== -1).toBeTruthy(`There is no "${skillName1}" in Landing page/Capability`);
    await expect(skillsText.indexOf(skillName2) !== -1).toBeTruthy(`There is no "${skillName2}" in Landing page/Capability`);
    await expect(skillsText.indexOf(skillName3) !== -1).toBeTruthy(`There is no "${skillName3}" in Landing page/Capability`);

    let toolsText = await landingPL.landingCapability.get(3).getText();
    await expect(toolsText.indexOf(tool1) !== -1).toBeTruthy(`There is no "${tool1}" in Landing page/Capability`);
    await expect(toolsText.indexOf(tool2) !== -1).toBeTruthy(`There is no "${tool2}" in Landing page/Capability`);
    await expect(toolsText.indexOf(tool3) !== -1).toBeTruthy(`There is no "${tool3}" in Landing page/Capability`);

    let eeText = await landingPL.landingCapability.get(4).getText();
    await expect(eeText.indexOf(`${education}, ${exp1}, ${exp2}`) !== -1).toBeTruthy(`There is no "${education}, ${exp1}, ${exp2}" in Landing page/Capability`);

    await landingPO.saveProfile();

    await detailsPO.clickMagnifier();
    await summaryPO.clickCapabilityTab();
    await summaryPO.clickTCTab();

    skillsText = await summaryPL.summarySkills.get(0).getText();
    await expect(skillsText.indexOf(skillName1) !== -1).toBeTruthy(`There is no "${skillName1}" in Summary page/Capability/TC`)
    skillsText = await summaryPL.summarySkills.get(1).getText();
    await expect(skillsText.indexOf(skillName2) !== -1).toBeTruthy(`There is no "${skillName1}" in Summary page/Capability/TC`)
    skillsText = await summaryPL.summarySkills.get(2).getText();
    await expect(skillsText.indexOf(skillName3) !== -1).toBeTruthy(`There is no "${skillName1}" in Summary page/Capability/TC`)

    await summaryPO.closeSummary();

    await browser.wait(EC.presenceOf(detailsPL.allCompetencies.get(0)), browser.params.cooldown.medium, `Details page: there is no top BC section`);
    let bc1 = await detailsPL.allCompetencies.get(0).getText();
    await expect(bc1.indexOf(bCompetency1) !== -1).toBeTruthy(`There is no "${bCompetency1}" on Details page`);
    let bc2 = await detailsPL.allCompetencies.get(1).getText();
    await expect(bc2.indexOf(bCompetency2) !== -1).toBeTruthy(`There is no "${bCompetency1}" on Details page`);
    let bc3 = await detailsPL.allCompetencies.get(2).getText();
    await expect(bc3.indexOf(bCompetency3) !== -1).toBeTruthy(`There is no "${bCompetency1}" on Details page`);

    await detailsPO.checkDetailsTopTC(
      [
        tCompetency1,
        tCompetency2,
        tCompetency3
      ]
    );

    await browser.wait(EC.presenceOf(detailsPL.detailTools.get(0)), browser.params.cooldown.medium, `Details page: there is no tools`);
    await expect(await detailsPL.detailTools.get(0).getText() == tool1)
      .toBeTruthy(`Details/tools: expected ${tool1} instead of ${await detailsPL.detailTools.get(0).getText()}`);
    await expect(await detailsPL.detailTools.get(1).getText() == tool2)
      .toBeTruthy(`Details/tools: expected ${tool1} instead of ${await detailsPL.detailTools.get(0).getText()}`);
    await expect(await detailsPL.detailTools.get(2).getText() == tool3)
      .toBeTruthy(`Details/tools: expected ${tool1} instead of ${await detailsPL.detailTools.get(0).getText()}`);

    await detailsPO.openActionsMenu();
    await detailsPO.clickItemInActions(2);

    await browser.wait(EC.presenceOf(landingPL.landingCapability.get(0)), browser.params.cooldown.small, `There is no Capability section on landing page`)
    bcText = await landingPL.landingCapability.get(0).getText();
    await expect(bcText.indexOf(bCompetency1) !== -1).toBeTruthy(`There is no "${bCompetency1}" in Landing page/Capability`);
    await expect(bcText.indexOf(bCompetency2) !== -1).toBeTruthy(`There is no "${bCompetency2}" in Landing page/Capability`);
    await expect(bcText.indexOf(bCompetency3) !== -1).toBeTruthy(`There is no "${bCompetency3}" in Landing page/Capability`);

    tcText = await landingPL.landingCapability.get(1).getText();
    await expect(tcText.indexOf(tCompetency1) !== -1).toBeTruthy(`There is no "${tCompetency1}" in Landing page/Capability`);
    await expect(tcText.indexOf(tCompetency2) !== -1).toBeTruthy(`There is no "${tCompetency2}" in Landing page/Capability`);
    await expect(tcText.indexOf(tCompetency3) !== -1).toBeTruthy(`There is no "${tCompetency3}" in Landing page/Capability`);

    skillsText = await landingPL.landingCapability.get(2).getText();
    await expect(skillsText.indexOf(skillName1) !== -1).toBeTruthy(`There is no "${skillName1}" in Landing page/Capability`);
    await expect(skillsText.indexOf(skillName2) !== -1).toBeTruthy(`There is no "${skillName2}" in Landing page/Capability`);
    await expect(skillsText.indexOf(skillName3) !== -1).toBeTruthy(`There is no "${skillName3}" in Landing page/Capability`);

    toolsText = await landingPL.landingCapability.get(3).getText();
    await expect(toolsText.indexOf(tool1) !== -1).toBeTruthy(`There is no "${tool1}" in Landing page/Capability`);
    await expect(toolsText.indexOf(tool2) !== -1).toBeTruthy(`There is no "${tool2}" in Landing page/Capability`);
    await expect(toolsText.indexOf(tool3) !== -1).toBeTruthy(`There is no "${tool3}" in Landing page/Capability`);

    eeText = await landingPL.landingCapability.get(4).getText();
    await expect(eeText.indexOf(`${education}, ${exp1}, ${exp2}`) !== -1).toBeTruthy(`There is no "${education}, ${exp1}, ${exp2}" in Landing page/Capability`);

    await landingPO.clickBackButton();
    await page.moveToSPList();
    await listPO.clearAllFilters();
  });

  it(`Check profile type`, async () => {
    await page.moveToSPList();
    await listPO.clearAllFiltersF();

    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 1);
    await listPO.openSPFromListByNo(1);
    let pType = await detailsPL.profileType.get(0).getText();
    await expect(pType.indexOf(`Level`) !== -1).toBeTruthy(`Expected Level type`);
    await page.moveToSPList();
    await listPO.clearAllFilters();

    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 2);
    await listPO.openSPFromListByNo(1);
    let pType2 = await detailsPL.profileType.get(0).getText();
    await expect(pType2.indexOf(`Function`) !== -1).toBeTruthy(`Expected Function type`);
    await page.moveToSPList();
    await listPO.clearAllFilters();

    await listPO.setFilter(5, 3);
    await listPO.setFilter(1, 3);
    await listPO.openSPFromListByNo(1);
    let pType3 = await detailsPL.profileType.get(0).getText();
    await expect(pType3.indexOf(`Job`) !== -1).toBeTruthy(`Expected Job type`);
    await page.moveToSPList();
    await listPO.clearAllFilters();

    await listPO.setFilter(1, 4);
    await listPO.openSPFromListByNo(1);
    let pType4 = await detailsPL.profileType.get(0).getText();
    await expect(pType4.indexOf(`Custom`) !== -1).toBeTruthy(`Expected Custom type`);
    await page.moveToSPList();
    await listPO.clearAllFilters();
  });

  it(`Select your view`, async () => {
    await page.moveToSPList();
    await listPO.setFilter(5, 3);
    await listPO.openSPFromListByNo(1);

    await detailsPO.openViewMenu();
    let sourcingCandidatesView = await detailsPL.selectViewMenuItems.get(1).getText();
    await expect(sourcingCandidatesView.indexOf(`Sourcing Candidates`) !== -1).toBeTruthy(`No Sourcing Candidates view`);
    await detailsPL.selectViewMenuItems.get(1).click();
    await SpinnerPO.waitUntilSpinnerOff();

    await detailsPO.openViewMenu();
    let selectingCandidatesView = await detailsPL.selectViewMenuItems.get(2).getText();
    await expect(selectingCandidatesView.indexOf(`Selecting Candidates`) !== -1).toBeTruthy(`No Selecting Candidates view`);
    await detailsPL.selectViewMenuItems.get(2).click();
    await SpinnerPO.waitUntilSpinnerOff();

    await detailsPO.openViewMenu();
    let compensationView = await detailsPL.selectViewMenuItems.get(3).getText();
    await expect(compensationView.indexOf(`Compensation`) !== -1).toBeTruthy(`No Compensation view`);
    await detailsPL.selectViewMenuItems.get(3).click();
    await SpinnerPO.waitUntilSpinnerOff();

    await detailsPO.openViewMenu();
    await detailsPL.selectViewMenuItems.get(4).click(); //reset view
    await SpinnerPO.waitUntilSpinnerOff();

    await page.moveToSPList();
    await listPO.clearAllFilters();
  });


});