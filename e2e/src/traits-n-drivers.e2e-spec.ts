import { browser } from 'protractor';
import { AppPage as page } from './components/app.po';
import { getCSVFileData, getTraits, getDrivers, getRegionSettings } from './components/utils';
import { SuccessProfileListPageObjects as listPO } from './components/sp-list.po';
import { SuccessProfileSummaryPageObjects as summaryPO } from './components/sp-summary.po';
import { SuccessProfileSummaryPageLocators as summaryPL } from './components/sp-summary.locators';
import { SuccessProfileDetailsPageObjects as detailsPO } from './components/sp-details.po';
import { SuccessProfileDetailsPageLocators as detailsPL } from './components/sp-details.locators';
import { SuccessProfileLandingPageObjects as landingPO } from './components/sp-landing.po';
import { SpinnerPO as spinner } from './components/spinner';

let profileName3 = 'Test ASDFG || CCCC';
let lines = getCSVFileData(browser.params.files.expectedTND);

describe('Traits and drivers check after UCP sliders changing', () => {
  beforeEach(async () => {
    await page.login();
    await browser.waitForAngularEnabled(false);
  });

  afterEach(async () => {
    await browser.manage().logs().get('browser').then(async (browserLog) => {
      if (browserLog.length > 0) {
        await browserLog.forEach(async log => {
          await expect(browserLog.length > 0).toBeFalsy(`error in console`);//.toBeFalsy(log.message);
        });
      }
    });
  });


  for (let i = 1; i < lines.length; i++) {
    it(`TraitsDriversID: ${lines[i][0]}`, async () => {
      console.log(`\nTraitsDriversID: ${lines[i][0]}. Test start.`)
      // 1-2 : Details Settings
      let levelSettings = [];
      for (let j = 1; j < 3; j = j + 1)  levelSettings.push(lines[i][j]);
      // 4 Region
      let regionSettings = getRegionSettings(4, lines[i]);
      // 5-29 : UCP Slider Settings
      let slidersSettings = [];
      for (let j = 5; j < 5 + 24; j = j + 1)  slidersSettings.push(lines[i][j]);
      // 29-32 : Culture Settings
      let cultureSettings = [];
      for (let j = 29; j < 33; j = j + 1)  cultureSettings.push([lines[0][j], lines[i][j]]);
      // 33-49, step 2 : Traits top + values
      let topTraits = getTraits(33, 49, [lines[0], lines[i]]);
      // 49-60, step 2 : Drivers top + values
      let topDrivers = getDrivers(49, 61, [lines[0], lines[i]]);

      // Get rid of old profile
      await page.searchProfileHome(`${profileName3} TND ${lines[i][0]}`).then(async () => {
        await listPO.getCount().then(async (am) => {
          for (let i = am; i > 0; i--) {
            await listPO.deleteSPViaThreeDotsMenu();
          };
        });
      });

      // Create new on via copy
      await listPO.clearSearch();
      await listPO.setFilter(5, 3);
      await listPO.setFilter(1, 2);
      await listPO.copyProfileViaThreeDotsMenu(1);
      await detailsPO.openEditPageViaActions();

      await landingPO.selectEditPageItem(1);
      await landingPO.editProfileTitle(`${profileName3} TND ${lines[i][0]}`);
      // console.log(`Level settings: ${levelSettings[0]} | ${levelSettings[1]}`);
      await landingPO.setProfileLevelByName(levelSettings[0]);
      await landingPO.setProfileSubLevelByName(levelSettings[1]);
      // console.log(`Region settings: ${regionSettings[0]}`);
      await landingPO.editRegionByName(regionSettings[0]);
      await landingPO.clickNextButton();

      await landingPO.selectEditPageItem(4);
      await landingPO.setCulture(cultureSettings);
      // console.log(`Sliders settings: `);
      // for (let k = 0; k < slidersSettings.length; k = k + 2) {
      //   console.log(`   ${slidersSettings[k]} | ${slidersSettings[k + 1]}`);
      // };
      for (let i = 0; i < slidersSettings.length; i++) { await landingPO.setUCPSlider(i, slidersSettings[i]); }
      await landingPO.clickNextButton();
      await landingPO.clickDoneSetUCPSliders();

      await landingPO.saveProfile();

      // check top traits
      await spinner.waitUntilSpinnerOff();
      await expect(await detailsPL.traits.get(0).getText() == topTraits[0]).toBeTruthy(`Details, Top Traits: 1st must be ${topTraits[0]} instead ${await detailsPL.traits.get(0).getText()}`);
      await expect(await detailsPL.traits.get(1).getText() == topTraits[1]).toBeTruthy(`Details, Top Traits: 2nd must be ${topTraits[1]} instead ${await detailsPL.traits.get(1).getText()}`);
      await expect(await detailsPL.traits.get(2).getText() == topTraits[2]).toBeTruthy(`Details, Top Traits: 3rd must be ${topTraits[2]} instead ${await detailsPL.traits.get(2).getText()}`);
      // check top drivers
      await detailsPL.drivers.count().then(async (a) => {
        for (let i = 0; i < a; i++) {
          await expect(
            async () => {
              for (let j = 0; j < topDrivers.length; j++) {
                if (await detailsPL.drivers.get(i).getText() == topDrivers[j]) return true;
              }
              return false;
            }).toBeTruthy(`Expected next set of drivers: ${topDrivers}. There is no ${await detailsPL.drivers.get(i).getText()}`);
        }
      });

      // Check Summary / Identity
      await summaryPO.openSummary();
      await summaryPO.clickIdentityTab();
      // Check summary traits names + order
      await summaryPO.clickTraitsTab();
      for (let i = 0; i < topTraits.length; i++) {
        await expect(await summaryPL.summaryRecordNames.get(i).getText() == topTraits[i])
          .toBeTruthy(`Traits: expected "${topTraits[i]}" instead of "${await summaryPL.summaryRecordNames.get(i).getText()}"`);
      }
      // Check summary drivers names + order
      await summaryPO.clickDriversTab();
      for (let i = 0; i < topDrivers.length; i++) {
        await expect(await summaryPL.summaryRecordNames.get(i).getText() == topDrivers[i])
          .toBeTruthy(`Drivers: expected "${topDrivers[i]}" instead of "${await summaryPL.summaryRecordNames.get(i).getText()}"`);
      }
      await summaryPO.closeSummary();

      await spinner.waitUntilSpinnerOff();
    });
  }
});