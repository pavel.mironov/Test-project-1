// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');
const fs = require('fs');
const baseUrl = 'http://products.kornferry.com/';
const xlsx = require(`xlsx`);
const reportsLocation = `./reports/`;
const statsLocation = `./reports/amount/`;

var report = '';
var results = {};
var suites = [];
var specs = [];
var suiteTimes = [];
var startTime = '';

exports.config = {
  params: {
    userOne: {
      login: 'autotest@kornferry.com',
      password: 'Activate123!'
    },
    userTwo: {
      login: 'autotest@kornferry.com',
      password: 'Activate123!'
    },
    userThree: {
      login: 'user.one@haygroup.com',
      password: 'Activate123'
    },
    cooldown: {
      small: 30000,
      medium: 60000,
      large: 120000,
    },
    files: {
      expectedTND: 'C:/Users/pavel.mironov/Documents/__projects/KF/ng/kf-e2e/e2e/src/files/BIC_Scoring_Test_Cases_Template_Updated.csv',
      jeKHTech: 'C:/Users/pavel.mironov/Documents/__projects/KF/ng/kf-e2e/e2e/src/files/JE_KnowHow_Tech.csv',
      jeKHPlan: 'C:/Users/pavel.mironov/Documents/__projects/KF/ng/kf-e2e/e2e/src/files/JE_KnowHow_Plan.csv',
      jeKHComm: 'C:/Users/pavel.mironov/Documents/__projects/KF/ng/kf-e2e/e2e/src/files/JE_KnowHow_Comm.csv',
      jePSEnv: 'C:/Users/pavel.mironov/Documents/__projects/KF/ng/kf-e2e/e2e/src/files/JE_ProblemSolving_Env.csv',
      jePSChal: 'C:/Users/pavel.mironov/Documents/__projects/KF/ng/kf-e2e/e2e/src/files/JE_ProblemSolving_Chal.csv',
      jeACFree: 'C:/Users/pavel.mironov/Documents/__projects/KF/ng/kf-e2e/e2e/src/files/JE_Accountability_Free.csv',
      jeACMagn: 'C:/Users/pavel.mironov/Documents/__projects/KF/ng/kf-e2e/e2e/src/files/JE_Accountability_Magn.csv',
      jeACNatu: 'C:/Users/pavel.mironov/Documents/__projects/KF/ng/kf-e2e/e2e/src/files/JE_Accountability_Natu.csv',
      jeACNatuNQ: 'C:/Users/pavel.mironov/Documents/__projects/KF/ng/kf-e2e/e2e/src/files/JE_Accountability_NatuNQ.csv'
    }
  },
  allScriptsTimeout: 61000,
  specs: [
    './src/**/edit-profile.e2e-spec.ts',
    // './src/**/job-evaluation.e2e-spec.ts',
    // './src/**/non-exec-grading.e2e-spec.ts',
    // './src/**/traits-n-drivers.e2e-spec.ts',
    // './src/**/summary-n-download.e2e-spec.ts',
    // './src/**/job-description.e2e-spec.ts'
    // './src/**/project.e2e-spec.ts',
    // './src/**/content-library.e2e-spec.ts',
    // './src/**/compare.e2e-spec.ts',
    // './src/**/job.e2e-spec.ts',
    // './src/**/sp-upload.e2e-spec.ts',
  ],
  capabilities: {
    'browserName': 'chrome',
    'restartBrowserBetweenTests': true,
    'platform': 'ANY',
    'version': 'ANY',
    shardTestFiles: true,
    maxInstances: 2,
    'chromeOptions': {
      args: [
        // '--no-sandbox',
        // '--headless',
        // '--disable-dev-shm-usage',
        // '--disable-gpu',
        // '--disable-browser-side-navigation',
        '--window-size=1920,1040',
        // '--disable-extensions',
        // '--disable-web-security'
      ],
      prefs: {
        'download': {
          'prompt_for_download': false,
          'directory_upgrade': true,
          'default_directory': 'C:/Users/pavel.mironov/Documents/__projects/KF/ng/kf-e2e/temp',
        }
      }
    },
    'loggingPrefs': {
      'driver': 'SEVERE',
      'server': 'SEVERE',
      'browser': 'SEVERE'
    }
  },
  directConnect: true,
  baseUrl,
  framework: 'jasmine',
  jasmineNodeOpts: {
    args: ['--stop-on-failure=false'],
    showColors: true,
    defaultTimeoutInterval: 7200000,
    print: function () { }
  },
  async beforeLaunch() {
    startTime = new Date();
    //delete previous reports if needed
    fs.readdirSync(reportsLocation).forEach(function (file) {
      if (!(fs.lstatSync(reportsLocation + file).isDirectory()) && file !== `.gitkeep`)
        fs.unlinkSync(reportsLocation + file);
    });
    //delete previous stats if needed
    fs.readdirSync(statsLocation).forEach(function (file) {
      if (!(fs.lstatSync(statsLocation + file).isDirectory()) && file !== `.gitkeep`)
        fs.unlinkSync(statsLocation + file);
    });
  },
  async onPrepare() {
    const capabilities = await browser.getCapabilities();
    console.log(capabilities.get('version'));

    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter(
      {
        displayNumber: true,
        spec: {
          displayStacktrace: true
        }
      }
    ));

    var myRep = {
      jasmineStarted: (suiteInfo) => {
        results.startTime = new Date();
        results.noOfSpecs = suiteInfo.totalSpecsDefined;
      },
      suiteStarted: (suite) => {
        var time = new Date();
        suites.push(suite);
        suiteTimes.push({ 'id': suite.id, 'startTime': time });
      },
      suiteDone: (result) => {
        var time = new Date();
        suiteTimes.forEach((suiteTime, index) => {
          if (suiteTime.id === result.id) { suiteTimes[index].endTime = time; }
        });
      },
      specStarted: (spec) => {
        var specDetails = spec;
        specDetails.startTime = new Date();
        if (specs.length === 0) {
          specs.push(specDetails);
        }
        else {
          if (specs.indexOf(spec) === -1) {
            specs.push(specDetails);
          }
        }
      },
      specDone: (result) => {
        specs.forEach(function (spec, index) {
          if (spec.id === result.id) {
            specs[index].endTime = new Date();
          }
        });
      },
      jasmineDone: async () => {
        results.endTime = new Date();
        suites.forEach(function (suite, index) {
          specs.forEach(function (spec, specIndex) {
            if (suite.fullName.trim() === spec.fullName.replace(spec.description, '').trim()) {
              if (suites[index].specs) {
                suites[index].specs.push(spec);
              }
              else {
                suites[index].specs = [];
                suites[index].specs.push(spec);
              }
            }
          });
          suiteTimes.forEach(function (suiteTime, timeIndex) {
            if (suiteTime.id === suite.id) {
              suites[index].startTime = suiteTime.startTime;
              suites[index].endTime = suiteTime.endTime;
            }
          });
        });
        suites.forEach(function (suite, index) {
          var flag = true;
          suite.specs.forEach(function (spec, specIndex) {
            if (spec.failedExpectations.length > 0) {
              suites[index].testStatus = "Fail";
              flag = false;
            }
          });
          if (flag) {
            suites[index].testStatus = "Pass";
          }
        });

        let wb = xlsx.readFile(`./e2e/src/Tests_description.xlsx`);
        let lines2 = xlsx.utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]]);

        report = '';
        suites.forEach(function (suite, index) {
          report = report + '<br><table border="1" width = 90%>';
          var hh = '<tr><th width = 3%>SuiteID</th><th width = 15%>SuiteName</th><th width = 42%>SuiteDescription</th><th width = 30%>SuiteStartTime</th></tr>';
          var header = hh.replace('SuiteID', suite.id)
            .replace('SuiteName', suite.description)
            .replace('SuiteDescription', 'Description')
            .replace('SuiteStartTime', '');
          var hh2 = '<tr><th>ID</th><th>Name</th><th>Description</th><th>Result</th></tr>';
          report = report + header;
          report = report + hh2;

          specs.forEach((spec, specIndex) => {
            if (suite.fullName.trim() === spec.fullName.replace(spec.description, '').trim()) {
              var rr = '<tr><td style="vertical-align: top; color: black; text-align: center; ">SpecID</td><td style="vertical-align: top; color: black; padding-left: 10px">SpecName</td><td style="vertical-align: top; color: black; text-align: left; padding-left: 10px">SpecDescription</td><td style="vertical-align: top; color:orange; padding-left: 10px;">SpecError</td></tr>';
              var row = rr.replace('SpecID', spec.id)
                .replace('SpecName', spec.description);

              var desc = '';
              row = row.replace('SpecDescription', '<pre><ul style="list-style: none; padding-left: 0px">SpecDescription</ul></pre>');
              for (let j = 0; j < lines2.length; j++) {
                if (lines2[j].Name.trim() === spec.description.trim()) {
                  desc = lines2[j].Description;
                  row = row.replace('SpecDescription', desc);
                  break;
                }
              }

              row = row.replace('SpecError', '<pre><ul style="padding-left: 6px">SpecError</ul></pre>');
              if (spec.status == "failed") {
                row = row.replace(/color:orange;/g, 'color: red;');
              }
              else {
                if (spec.status == "passed") {
                  row = row.replace(/color:orange;/g, 'color: green;');
                }
                row = row.replace('SpecError', '<li>' + spec.status + '</li>');
              }

              var ff = '';
              for (let i = 0; i < spec.failedExpectations.length; i++) { ff = ff + '<li>' + spec.failedExpectations[i].message + '</li>'; }
              ff = ff.replace(/Expected false to be truthy/g, '(unexpected error)').replace(/Expected true to be falsy/g, '(console error)').replace(/'/g, '');
              row = row.replace('SpecError', ff);
              report = report + row;
            }
          });
          report = report + '</table><p>';
        });
      }
    };
    jasmine.getEnv().addReporter(myRep);
  },
  async onComplete() {
    let failAmount = 0;
    let passedAmount = 0;
    let specAmount = 0;
    suites.forEach((suite, index) => {
      suite.specs.forEach((spec, specIndex) => {
        if (spec.status == "failed") {
          failAmount = failAmount + 1;
        }
        if (spec.status == "passed") {
          passedAmount = passedAmount + 1;
        }
        specAmount = specAmount + 1;
      }
      );
    });
    // create partial report
    let tempName = await browser.getProcessedConfig().then(a => {
      let b = a.specs[0].split(`\\`);
      return b[b.length - 1].replace(`.e2e-spec.ts`, ``);
    });
    if (fs.existsSync(`${reportsLocation + tempName}.html`)) fs.unlinkSync(`${reportsLocation + tempName}.html`);
    fs.writeFileSync(`${reportsLocation + tempName}.html`, report, 'utf-8');
    // save stats
    fs.writeFileSync(`${statsLocation + tempName}_total_count.txt`, specAmount, 'utf-8');
    fs.writeFileSync(`${statsLocation + tempName}_failed_count.txt`, failAmount, 'utf-8');
    fs.writeFileSync(`${statsLocation + tempName}_passed_count.txt`, passedAmount, 'utf-8');
  },
  async afterLaunch() {
    let failAmount = 0;
    let passedAmount = 0;
    let specAmount = 0;
    // gather stats
    fs.readdirSync(`${statsLocation}`).forEach((file) => {
      if (file.indexOf(`_total_count.txt`) !== -1) specAmount = specAmount + parseInt(fs.readFileSync(statsLocation + file, 'utf8'));
      if (file.indexOf(`_passed_count.txt`) !== -1) passedAmount = passedAmount + parseInt(fs.readFileSync(statsLocation + file, 'utf8'));
      if (file.indexOf(`_failed_count.txt`) !== -1) failAmount = failAmount + parseInt(fs.readFileSync(statsLocation + file, 'utf8'));
    });

    var endTime = new Date();
    let fullReport = `<head></head><body><p>STARTTIME<p>ENDTIME<p>TOTALCOUNT<p>PASSED<p>FAILED<p>TABLES</body>`;
    fullReport = fullReport.replace('<p>STARTTIME', `<br> Time start: ${startTime};`);
    fullReport = fullReport.replace('<p>ENDTIME', `<br> Time end: ${endTime};`);
    fullReport = fullReport.replace('<p>TOTALCOUNT', `<br> Total count: ${specAmount} spec(s);`);
    fullReport = fullReport.replace('<p>PASSED', `<br> Passed: ${passedAmount} spec(s);`);
    fullReport = fullReport.replace('<p>FAILED', `<br> Failed: ${failAmount} spec(s);`);

    let output = ``;
    fs.readdirSync(reportsLocation).forEach(function (file) {
      if (!(fs.lstatSync(reportsLocation + file).isDirectory()))
        output = output + fs.readFileSync(reportsLocation + file);
    });
    fullReport = fullReport.replace(`TABLES`, output);
    if (fs.existsSync(`${reportsLocation}report.html`)) fs.unlinkSync(`${reportsLocation}report.html`);
    fs.writeFileSync(`${reportsLocation}report.html`, fullReport);

    // const Axios = require('axios');
    // const https = require('https');

    // const axios = Axios.create({
    //   httpsAgent: new https.Agent({
    //     rejectUnauthorized: false
    //   })
    // });

    // const resp = await axios({
    //   url: 'https://testproductsapi.kornferry.com/v1/actions/login',
    //   method: 'POST',
    //   data: {
    //     password: "hay",
    //     username: "user.one@haygroup.com"
    //   }
    // });
    // const { authToken } = resp.data.data;

    // var messageBody = fs.readFileSync('./reports/report.html', 'utf-8')

    // await axios({
    //   url: 'https://testproductsapi.kornferry.com/v1/actions/sendemail',
    //   method: 'POST',
    //   headers: {
    //     authToken
    //   },
    //   data: {
    // "to": "Veera.Chitturi@Kornferry.com, Srujana.Kolishetty@KornFerry.com, Surendra.Gollapudi@KornFerry.com, Yugandhar.Hosamane@KornFerry.com, George.Fu@KornFerry.com, Tia.Carioli@KornFerry.com, Thanmaiyee.Alla@KornFerry.com, Xiyi.Li@KornFerry.com, ekaterina.tokareva@kornferry.com",
    // "from": "noreply.autotest@kornferry.com",
    //     "emailSubjectTemplate": `KF auto tests (beta) (${baseUrl})`,
    //     "emailMessageTemplate": messageBody
    //   }
    // });
  },
};
